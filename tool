#!/usr/bin/env stack
-- stack --resolver lts-16.8 script

{-# LANGUAGE OverloadedStrings #-}

import Turtle
import Prelude hiding (FilePath, concat)
import Control.Exception (catch, SomeException)
import Control.Monad.Trans.Maybe (runMaybeT)
import Data.Coerce (coerce)
import Data.Maybe (maybeToList)
import Data.List.Extra (splitOn)
import Data.List.NonEmpty (NonEmpty(..), toList)
import Data.Semigroup (Option(..), Last(..))
import Data.Text (pack)
import Filesystem.Path (concat)

import Language.Haskell.HLint (hlint, Idea(..))
import qualified Language.Haskell.Brittany as B
import qualified Language.Haskell.Brittany.Internal.Config.Types as BB

data Command = Lint [FilePath] | Refactor [FilePath] | Pretty [FilePath] deriving (Show)

tops :: [FilePath]
tops = ["benchmark", "executable", "library", "test-suite"]

lintArgs = ["--ignore=Eta reduce", "--no-summary"]
refactorArgs = ["--ignore=Eta reduce", "--refactor", "--refactor-options=--inplace"]

hsPat = suffix ".hs"
hsJsonPat = suffix ".hs" <|> suffix ".json"

orTops :: [FilePath] -> Shell FilePath
orTops fx = case fx of
  [] -> select tops
  _ -> select fx

expandPaths :: Pattern a -> [FilePath] -> Shell FilePath
expandPaths pat fx = join $ exp <$> orTops fx
  where
    exp :: FilePath -> Shell FilePath
    exp f = do
      s <- liftIO (stat f)
      exp' f s

    exp' :: FilePath -> FileStatus -> Shell FilePath
    exp' f s | isRegularFile s = return f
             | isDirectory s = find pat f

parser :: Parser Command
parser = lint <|> refactor <|> pretty
  where
    manyPaths = many $ argPath "PATH" $ pure "A path to a source file or directory."
    lint = Lint <$> subcommand "lint" "Run hlint against source files." manyPaths
    refactor = Refactor <$> subcommand "refactor" "Run hlint --refactor --inplace to apply recommended fixes." manyPaths
    pretty = Pretty <$> subcommand "pretty" "Run brittany and pretty-json against source files." manyPaths

dump :: Show s => s -> Shell ()
dump = liftIO . dump'

dump' :: Show s => s -> IO ()
dump' = stderr . select . textToLines . repr

dumpBrittanyErr :: [B.BrittanyError] -> Shell ()
dumpBrittanyErr ex = liftIO $ stderr $ select ll
  where
    ll :: [Line]
    ll = concatMap (toList . textToLines . pack . dumpErr) ex

    dumpErr :: B.BrittanyError -> String
    dumpErr e = case e of
      B.ErrorInput s -> "ErrorInput: " ++ s
      B.ErrorUnusedComment s -> "ErrorUnusedComment: " ++ s
      B.ErrorMacroConfig s t -> "ErrorMacroConfig - parse: " ++ s ++ " input: " ++ t
      B.LayoutWarning s -> "LayoutWarning: " ++ s
      B.ErrorUnknownNode s _ -> "ErrorUnknownNode: " ++ s
      B.ErrorOutputCheck -> "ErrorOutputCheck"

runLint :: FilePath -> Shell ()
runLint path = do
  liftIO $ hlint $ lintArgs ++ [encodeString path]
  return ()

modToPath :: FilePath -> String -> String
modToPath base mod = encodeString $ concat (base : (map decodeString $ splitOn "." mod)) <.> "hs"

runRefactor :: FilePath -> Shell ()
runRefactor path = do
  ideas <- liftIO $ hlint $ "--quiet": lintArgs ++ [encodeString path]
  idea' <- select ideas
  dirtyMod <- select $ ideaModule idea'
  let dirtyPath = modToPath path dirtyMod
  dump dirtyPath
  liftIO $ catch (hlint (refactorArgs ++ [dirtyPath]) >> return ()) (\e -> dump' (e :: SomeException))
  dump "continue"
  return ()

runPretty :: B.Config -> FilePath -> Shell ()
runPretty cfg path = do
  txt <- liftIO $ readTextFile path
  brit <- liftIO $ B.parsePrintModule cfg txt
  case brit of
    Left err -> dumpBrittanyErr err
    Right txt' -> liftIO $ writeTextFile path txt'

brittanyConfig :: FilePath -> IO B.Config
brittanyConfig dir = do
  lcPaths <- B.findLocalConfigPath (encodeString dir)
  let lcPaths' = maybeToList lcPaths
  cfg <- runMaybeT $ B.readConfigsWithUserConfig brittanyBaseConfig lcPaths'
  case cfg of
    Just c -> return c
    Nothing -> fail "Can't read config"

main :: IO ()
main = sh $ do
  c <- options "Build support tool" parser
  case c of
    Lint paths -> orTops paths >>= runLint
    Refactor paths -> orTops paths >>= runRefactor
    Pretty paths -> do
      dir <- pwd
      cfg <- liftIO $ brittanyConfig dir
      paths' <- expandPaths hsPat paths
      runPretty cfg paths'

brittanyBaseConfig :: B.CConfig Option
brittanyBaseConfig = B.Config
  { B._conf_version = purer 1
  , B._conf_debug   = B.DebugConfig
    { B._dconf_dump_config                = purer False
    , B._dconf_dump_annotations           = purer False
    , B._dconf_dump_ast_unknown           = purer False
    , B._dconf_dump_ast_full              = purer False
    , B._dconf_dump_bridoc_raw            = purer False
    , B._dconf_dump_bridoc_simpl_alt      = purer False
    , B._dconf_dump_bridoc_simpl_floating = purer False
    , B._dconf_dump_bridoc_simpl_par      = purer False
    , B._dconf_dump_bridoc_simpl_columns  = purer False
    , B._dconf_dump_bridoc_simpl_indent   = purer False
    , B._dconf_dump_bridoc_final          = purer False
    , B._dconf_roundtrip_exactprint_only  = purer False
    }
  , B._conf_layout = B.LayoutConfig
    { B._lconfig_cols                      = purer (118 :: Int)  -- set
    , B._lconfig_indentPolicy              = purer BB.IndentPolicyFree
    , B._lconfig_indentAmount              = purer (4 :: Int)  -- set
    , B._lconfig_indentWhereSpecial        = purer True
    , B._lconfig_indentListSpecial         = purer True
    , B._lconfig_importColumn              = purer (50 :: Int)
    , B._lconfig_importAsColumn            = purer (50 :: Int)
    , B._lconfig_altChooser                = purer (BB.AltChooserBoundedSearch 3)
    , B._lconfig_columnAlignMode           = purer (BB.ColumnAlignModeMajority 0.7)
    , B._lconfig_alignmentLimit            = purer (30 :: Int)
    , B._lconfig_alignmentBreakOnMultiline = purer True
    , B._lconfig_hangingTypeSignature      = purer False
    , B._lconfig_reformatModulePreamble    = purer True
    , B._lconfig_allowSingleLineExportList = purer False
    , B._lconfig_allowHangingQuasiQuotes   = purer True
    , B._lconfig_experimentalSemicolonNewlines = purer False
    }
  , B._conf_errorHandling = B.ErrorHandlingConfig
    { B._econf_produceOutputOnErrors   = purer False
    , B._econf_Werror                  = purer False
    , B._econf_ExactPrintFallback      = purer BB.ExactPrintFallbackModeInline
    , B._econf_omit_output_valid_check = purer False
    }
  , B._conf_preprocessor = BB.PreProcessorConfig
    { B._ppconf_CPPMode            = purer BB.CPPModeAbort
    , B._ppconf_hackAroundIncludes = purer False
    }
  , B._conf_forward = B.ForwardOptions
    { B._options_ghc = pure []
    }
  , B._conf_roundtrip_exactprint_only  = purer False
  , B._conf_obfuscate                  = purer False
  }
  where purer = pure . pure
