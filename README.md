# [Tenet][]

Tenet is an intuitive language to share data between systems with consistent and reliable semantics. [More details on the site][site].

## Contributions, issues and comments

Rather than bother with a formal issue tracker, [please post questions][groups] to the Google group.

## Build notes

First, [get stack][]. The `stack` command will get almost all other dependencies, including GHC itself.

### Building

Building everything:

    stack build --copy-bins --test --bench --haddock

Running the `tenet` command:

    stack exec tenet -- -h

### Resolver

For build speed on CI, we should set the resolver in `stack.yml` to be the same as the docker tag for `fpco/stack-build`.

The [stackage page][] with the current resolver.

### Linting

The `tool` script will download its prerequisites via `stack`.

    ./tool lint     # Run hlint against source files
    ./tool refactor # Run automatic refactoring
    ./tool pretty   # Run brittany against source files

### Documentation

If you've run:

    stack build --haddock

The generated documentation is located at `.stack-work/dist/*/*/doc`.

[site]: https://tenet-lang.org
[groups]: https://groups.google.com/forum/#!forum/tenet-lang
[Tenet]: https://gitlab.com/contravariance/tenet-haskell/
[stackage page]: https://www.stackage.org/lts-15.11
[get stack]: https://docs.haskellstack.org/en/stable/README/
