module Test.GTest
    ( TestInfo(..)
    , GTest
    , applyEither
    , gt
    , loadSource
    , setSource
    , runTest
    , runTestWithInfo
    , loadExpected
    , saveExpected
    )
where

import           Control.Exception              ( displayException
                                                , tryJust
                                                )
import           Control.Monad.IO.Class         ( liftIO )
import           Control.Monad.Trans.State.Lazy ( StateT
                                                , execStateT
                                                , get
                                                , put
                                                )
import           Data.Text                      ( Text
                                                , unpack
                                                )
import           Data.Text.IO                   ( readFile
                                                , writeFile
                                                )
import           Data.Convertible.Utf8          ( convert )
import           GHC.IO.Exception               ( IOErrorType(..)
                                                , IOException(..)
                                                )
import           Prelude                 hiding ( readFile
                                                , writeFile
                                                )
import           System.Directory               ( createDirectoryIfMissing )
import           System.FilePath                ( takeDirectory )
import           Test.Hspec                     ( Spec
                                                , it
                                                , pendingWith
                                                )
import           Tenet.State                    ( Out )

data TestInfo s t = TestInfo
  { mSrc     :: s
  , mSrcPath :: FilePath
  , mAct     :: t
  , mExpPath :: FilePath
  , mExp     :: Out t
  } deriving (Show)

initial :: TestInfo s t
initial = TestInfo { mSrc     = error "Source not loaded"
                   , mSrcPath = error "Source path not set"
                   , mAct     = error "Actual not set"
                   , mExp     = error "Expected not set"
                   , mExpPath = error "Expected path not set"
                   }

type GTest s t = StateT (TestInfo s t) IO

-- | Load the source for this test.
loadSource :: FilePath -> GTest Text t ()
loadSource path = do
    txt <- liftIO (readFile path)
    setSource path txt

setSource :: String -> s -> GTest s t ()
setSource path txt = do
    info <- get
    put info { mSrcPath = path, mSrc = txt }

-- | Parse and run any steps to produce the test value.
runTest :: (s -> t) -> GTest s t ()
runTest test = runTestWithInfo (test . mSrc)

runTestWithInfo :: (TestInfo s t -> t) -> GTest s t ()
runTestWithInfo test = do
    info <- get
    put info { mAct = test info }

applyEither :: (a -> b) -> (c -> d) -> Either a c -> Either b d
applyEither func _    (Left  left ) = Left $ func left
applyEither _    func (Right right) = Right $ func right

isMissing :: IOException -> Maybe IOException
isMissing exc@IOError { ioe_type = NoSuchThing } = Just exc
isMissing _ = Nothing

loadExpected :: FilePath -> (Text -> t) -> GTest s t ()
loadExpected path parse = do
    result <- liftIO . tryJust isMissing $ readFile path
    info   <- get
    put info { mExpPath = path, mExp = applyEither (convert . displayException) parse result }

-- | If the expected value is missing, save the actual value as expected.
saveExpected :: (t -> Text) -> GTest s t ()
saveExpected render = do
    info <- get
    case mExp info of
        Left _ -> do
            let filePath = mExpPath info
            liftIO . createDirectoryIfMissing True $ takeDirectory filePath
            liftIO . writeFile filePath . render $ mAct info
        Right _ -> return ()

gt :: String -> GTest s t x -> (t -> t -> IO ()) -> Spec
gt testName setup gtest = it testName $ do
    info <- execStateT setup initial
    case mExp info of
        Left  err      -> pendingWith (unpack err)
        Right expected -> gtest (mAct info) expected
