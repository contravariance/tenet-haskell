{-| This module provides 'xmlFormatter' that can be used with 'Test.Hspec.Runner.hspecWith'.

  Example usage:

  > import Test.Hspec.Formatters.Jenkins (xmlFormatter)
  > import Test.Hspec.Runner
  >
  > main :: IO ()
  > main = do
  >   summary <- withFile "results.xml" WriteMode $ \h -> do
  >     let c = defaultConfig
  >           { configFormatter = xmlFormatter
  >           , configHandle = h
  >           }
  >     hspecWith c spec
  >   unless (summaryFailures summary == 0) $
  >     exitFailure

  An example project is located in @example@ directory.

  Output is based on this documentation of the JUnit XML spec: http://llg.cubic.org/docs/junit/
-}
{-# LANGUAGE OverloadedStrings #-}

module Test.Hspec.Formatters.Jenkins
    ( xmlFormatter
    , xmlRef
    , XmlRef
    )
where

import           Control.Monad.IO.Class         ( liftIO )
import           Data.IORef                     ( IORef
                                                , atomicModifyIORef'
                                                , newIORef
                                                , readIORef
                                                )
import           Data.List                      ( foldl'
                                                , intercalate
                                                )
import qualified Data.Map                      as M
import           Test.Hspec.Formatters          ( FailureReason(..)
                                                , FormatM
                                                , Formatter(..)
                                                , Seconds(..)
                                                , getRealTime
                                                , silent
                                                , write
                                                , writeLine
                                                )
import           Test.Hspec.Runner              ( Path )
import           Text.Blaze.Internal            ( Attribute
                                                , Markup
                                                , Tag
                                                , customAttribute
                                                , customParent
                                                , stringValue
                                                , (!)
                                                )
import           Text.Blaze.Renderer.Pretty     ( renderMarkup )

tag = customParent

attr name = customAttribute name . stringValue

attrSh :: Show a => Tag -> a -> Attribute
attrSh name = customAttribute name . stringValue . show

data Case
  = Success (Maybe String)
            String
  | Fail (Maybe String)
         String
         FailureReason
  | Pending (Maybe String)
            String
            (Maybe String)

type CaseMap = M.Map [String] [Case]

type XmlRef = IORef CaseMap

xmlRef :: IO XmlRef
xmlRef = newIORef M.empty

insert :: XmlRef -> [String] -> Case -> FormatM ()
insert ref suite test = liftIO $ atomicModifyIORef' ref addCase
  where
    addCase :: CaseMap -> (CaseMap, ())
    addCase caseMap = (M.insertWith (++) suite [test] caseMap, ())

writeReport :: XmlRef -> FormatM ()
writeReport ref = do
    cases    <- liftIO $ readIORef ref
    realTime <- getRealTime
    write . renderMarkup $ testsuites cases realTime

-- | Stats are: tests, errors, failures, disabled
data Stats =
  Stats Int
        Int
        Int
        Int

stat :: Case -> Stats
stat (Success _ _         ) = Stats 1 0 0 0
stat (Fail _ _ (Error _ _)) = Stats 1 1 0 0
stat Fail{}                 = Stats 1 0 1 0
stat Pending{}              = Stats 1 0 0 1

plusStats :: Stats -> Stats -> Stats
plusStats (Stats a b c d) (Stats e f g h) = Stats (a + e) (b + f) (c + g) (d + h)

stats :: Bool -> [Case] -> Markup -> Markup
stats addSkips cases mark =
    let cases'                       = map stat cases
        Stats tests errs fails pends = foldl' plusStats (Stats 0 0 0 0) cases'
        mark'                        = mark ! attrSh "tests" tests ! attrSh "errors" errs ! attrSh "failures" fails
    in  if addSkips then mark' ! attrSh "skipped" pends else mark'

time :: Seconds -> Attribute
time (Seconds secs) = attrSh "time" secs

testcase :: Case -> Markup
testcase (Success cls tst                ) = tcTag cls tst ""
testcase (Fail    cls tst err@(Error _ _)) = tcTag cls tst $ tag "error" "" ! attr "message" (reasonAsString err)
testcase (Fail    cls tst err            ) = tcTag cls tst $ tag "failure" "" ! attr "message" (reasonAsString err)
testcase (Pending cls tst (Just reason)  ) = tcTag cls tst $ tag "skipped" "" ! attr "message" reason
testcase (Pending cls tst Nothing        ) = tcTag cls tst $ tag "skipped" ""

tcTag :: Maybe String -> String -> Markup -> Markup
tcTag (Just cls) tst content = tag "testcase" content ! attr "name" tst ! attr "className" cls
tcTag Nothing    tst content = tag "testcase" content ! attr "name" tst

allCases :: CaseMap -> [Case]
allCases = concat . M.elems

testsuites :: CaseMap -> Seconds -> Markup
testsuites caseMap secs =
    let suiteTags = mapM_ testsuite . M.assocs $ caseMap
        root      = stats False (allCases caseMap) . tag "testsuites" $ suiteTags
    in  root ! time secs

testsuite :: ([String], [Case]) -> Markup
testsuite (names, cases) =
    let cases' = mapM_ testcase . reverse $ cases
        suite  = stats True cases . tag "testsuite" $ cases'
    in  suite ! attr "name" (intercalate "." names)

reasonAsString :: FailureReason -> String
reasonAsString NoReason = "No reason given."
reasonAsString (Reason reason) = reason
reasonAsString (ExpectedButGot Nothing expected got) = "Expected " ++ expected ++ " but got " ++ got
reasonAsString (ExpectedButGot (Just src) expected got) = src ++ " expected " ++ expected ++ " but got " ++ got
reasonAsString (Error Nothing err) = show err
reasonAsString (Error (Just reason) err) = reason ++ show err

-- | Format Hspec result to Jenkins-friendly XML.
xmlFormatter :: XmlRef -> Formatter
xmlFormatter ref = silent
    { headerFormatter  = writeLine "<?xml version='1.0' encoding='UTF-8'?>"
    , exampleSucceeded = \path _ -> insert ref (pSuite path) $ Success (pCls path) (pTest path)
    , exampleFailed    = \path _ err -> insert ref (pSuite path) $ Fail (pCls path) (pTest path) err
    , examplePending   = \path _ reason -> insert ref (pSuite path) $ Pending (pCls path) (pTest path) reason
    , footerFormatter  = writeReport ref
    }

pTest :: Path -> String
pTest = snd

pSuite :: Path -> [String]
pSuite ([] , _) = []
pSuite ([a], _) = [a]
pSuite (lst, _) = init lst

pCls :: Path -> Maybe String
pCls ([] , _) = Nothing
pCls ([_], _) = Nothing
pCls (lst, _) = Just $ last lst
