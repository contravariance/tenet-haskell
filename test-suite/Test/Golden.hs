{-# LANGUAGE ScopedTypeVariables #-}

module Test.Golden
    ( exampleFiles
    , gdescribe
    , git
    , gitfail
    , gitMp
    , gitfailMp
    , goldTest
    , gtext
    , parseTest
    , resultFiles
    , stdParse'
    , PassFail(..)
    )
where

import           Test.GTest                     ( TestInfo(..)
                                                , GTest
                                                , gt
                                                , loadExpected
                                                , loadSource
                                                , runTest
                                                , runTestWithInfo
                                                , saveExpected
                                                , setSource
                                                )
import           Test.Hspec                     ( Spec
                                                , SpecWith
                                                , around_
                                                , describe
                                                , runIO
                                                , shouldBe
                                                )

import           Data.Char                      ( isPrint )
import           Data.Data                      ( Data )
import           Data.Text                      ( Text
                                                , pack
                                                , unpack
                                                )

import           System.Directory               ( createDirectoryIfMissing
                                                , withCurrentDirectory
                                                )
import           System.FilePath                ( makeRelative
                                                , takeBaseName
                                                , (-<.>)
                                                , (<.>)
                                                , (</>)
                                                )
import           System.FilePath.Find           ( depth
                                                , extension
                                                , find
                                                , (<?)
                                                , (==?)
                                                )
import           Text.JSON.Generic              ( decodeJSON
                                                , encodeJSON
                                                )
import           Text.Megaparsec                ( eof )
import           Tenet.Ast                      ( DocN
                                                , nullNode
                                                )
import           Tenet.State.Out                ( Out
                                                , errorOut
                                                , mapLeft
                                                )
import qualified Tenet.Parsing                 as P
import qualified Tenet.Parsing.Common          as C

data PassFail
  = Pass
  | Fail
  deriving (Eq, Show, Ord)

type Parser pt = FilePath -> Text -> Out pt
type Result pt = (Text, Out pt)

resultsBasePath :: FilePath
resultsBasePath = "test-suite" </> "golden"

-- | List of files under examples/ as (relative, src) pairs.
exampleFiles :: IO [(FilePath, FilePath)]
exampleFiles =
    let examples = "examples"
        pair path = (makeRelative examples path, path)
    in  map pair <$> find (depth <? 4) (extension ==? ".tenet") examples

-- | List of files under test-suite/golden as (relative, src) pairs with an extension
resultFiles :: String -> IO [(FilePath, FilePath)]
resultFiles ext =
    let pair path = (makeRelative resultsBasePath path, path)
    in  map pair <$> find (depth <? 4) (extension ==? ext) resultsBasePath

clean :: String -> FilePath
clean part = map replaceUnsafe part
  where
    replaceUnsafe char | char `elem` "\\/:*<>|" = '-'
                       | char `elem` "\"?"      = '_'
                       | isPrint char           = char
                       | otherwise              = '_'

resultsDir :: String -> FilePath
resultsDir ctx = resultsBasePath </> clean ctx

resultsFileAst :: String -> FilePath
resultsFileAst item = clean item <.> "ast"

resultsFileJson :: String -> FilePath
resultsFileJson item = clean item <.> "json"

gdescribe :: String -> SpecWith a -> SpecWith a
gdescribe label spec = do
    runIO $ createDirectoryIfMissing True dir
    around_ (withCurrentDirectory dir) $ describe label spec
    where dir = resultsDir label

didPass :: Either x y -> PassFail
didPass (Left  _) = Fail
didPass (Right _) = Pass


git :: (Read pt, Show pt, Eq pt) => Parser pt -> FilePath -> Text -> Spec
git parser = parseTest parser Pass

gitfail :: (Read pt, Show pt, Eq pt) => Parser pt -> FilePath -> Text -> Spec
gitfail parser = parseTest parser Fail

parseTest :: forall pt . (Read pt, Show pt, Eq pt) => Parser pt -> PassFail -> String -> Text -> Spec
parseTest parser should item source = gt item setup gtest
  where
    path  = resultsFileAst item
    setup = genericSetup path source parse
    gtest :: Result pt -> Result pt -> IO ()
    gtest (_, act) (_, expc) = do
        act `shouldBe` expc
        didPass act `shouldBe` should
    parse :: TestInfo Text t -> Result pt
    parse info = let contents = mSrc info in (contents, parser (mSrcPath info) contents)

genericSetup
    :: (Read pt, Show pt) => FilePath -> Text -> (TestInfo Text (Result pt) -> Result pt) -> GTest Text (Result pt) ()
genericSetup path source parse = do
    setSource path source
    loadExpected path (read . unpack)
    runTestWithInfo parse
    saveExpected $ pack . show

gitMp :: (Read pt, Show pt, Eq pt) => C.Parser pt -> FilePath -> Text -> Spec
gitMp parser = parseTestMp parser Pass

gitfailMp :: (Read pt, Show pt, Eq pt) => C.Parser pt -> FilePath -> Text -> Spec
gitfailMp parser = parseTestMp parser Fail

parseTestMp :: forall pt . (Read pt, Show pt, Eq pt) => C.Parser pt -> PassFail -> String -> Text -> Spec
parseTestMp parser should item source = gt item setup gtest
  where
    path  = resultsFileAst item
    setup = genericSetup path source parse
    gtest :: Result pt -> Result pt -> IO ()
    gtest (_, act) (_, expc) = do
        act `shouldBe` expc
        didPass act `shouldBe` should
    parse :: TestInfo Text t -> Result pt
    parse info =
        let contents = mSrc info
            result   = C.parse (parser <* eof) (mSrcPath info) contents
        in  (contents, mapLeft (C.parseErrorPretty contents) result)

goldTest
    :: forall out inT
     . (Data out, Show out, Eq out, Data inT, Show inT, Eq inT)
    => String
    -> (inT -> out)
    -> inT
    -> Spec
goldTest item operate input = gt item setup shouldBe
  where
    path = resultsFileJson item
    operate' inp = (inp, operate inp)
    setup = do
        setSource path input
        loadExpected path (decodeJSON . unpack)
        runTest operate'
        saveExpected $ pack . encodeJSON

gtext :: FilePath -> FilePath -> String -> String -> (DocN -> Text) -> Spec
gtext srcFile tgtDir tgtExt suffix action = gt srcName setup shouldBe
  where
    srcName = takeBaseName (srcFile -<.> "") <.> suffix
    tgtFile = resultsBasePath </> tgtDir </> srcName <.> tgtExt
    setup   = do
        loadSource srcFile
        loadExpected tgtFile id
        runTestWithInfo $ action . stdParse
        saveExpected id

stdParse :: TestInfo Text u -> DocN
stdParse info = stdParse' (mSrcPath info) (mSrc info)

stdParse' :: FilePath -> Text -> DocN
stdParse' path contents = nullNode <$ errorOut (P.parseDoc path contents)
