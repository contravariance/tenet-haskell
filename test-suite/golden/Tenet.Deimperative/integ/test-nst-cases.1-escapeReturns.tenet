integer_1 := 42;

integer_2 := - 21;

integer_3 := 5;

name_1 := global_reference;

name_3 := trailing_underscore_;

string_1 := "it's a string";

string_2 := "not sure about single quotes";

tagged_1 := #an_enum;

tagged_2 := #still_an_enum;

tagged_3 := tag ~ 42;

tagged_4 := tags_cant_have_spaces ~ "but strings can";

tagged_5 := #foo;

record_1 := (slot: "value", another_slot: 21);

record_2 := ();

record_get_1 := (one_slot: "value", another_slot: 21).one_slot;

record_get_2 := some_function().one_slot;

list_1 := [1, 2, 3, 4, 5];

list_2 := ["foo", "bar", "qux"];

list_3 := [integer_one, 2, 3];

list_4 := [];

map_1 := {:};

map_2 := { "key":"val", "key_2":"val_2" };

set_1 := { 1, 2, 2, 3, 3 };

set_2 := { };

func_2 := func (arg_a, arg_b: Int, arg_c: Str) {
    return (slot_a: arg_a, slot_b: arg_b + 33, slot_c: arg_c);
};

apply_1 := global_nullary(...)();

func apply_2() {
    return global_nullary(...)();
}

partial_1 := global_nullary(...);

partial_2 := global_binary(left: 22, ...);

func partial_3(an_arg: Global_Type) -> Func(left: Int, right: Int)  -> Int {
    return global_binary(right: an_arg + 42, ...);
}

type Union1 := (alpha~Unit | beta~Int | gamma~Symbol | delta~(slot1: Int, slot2: Global_Type));

type Union2[A, B] := (#alpha | beta~A | gamma~B);

type Record1 := (slot1: Int, slot2: Symbol);

type Record2[A] := (slot1: A, slot2: Symbol);

type Record3[A] := (slot1: A, slot2: Symbol);

type Simple1 := Int;

type Simple2 := Symbol;

type Simple3 := Alpha;

type Simple4 := Omega;

type Func1 := Func(arg1: Int, arg2: Symbol)  -> Int;

type Func2[A, B] := Func(arg1: Int, arg2: A)  -> B;

type Func3 := Func(arg1: Int, arg2: Symbol)  -> (slot1: Int, slot2: Str);

type Global_Type := (#alpha | beta~Int | gamma~Symbol);

func function1() {
    return 42;
}

func function2(thing: Type_Param[My_Param: Int]) {
    return 42;
}

func function_2_a(thing: My_List[My_Param: Int]) {
    return 42;
}

type My_List[My_Param] := [My_Param];

func function_3(foo, bar) {
    return global_func(foo: bar, bar: foo);
}

func let_1() {
    a := 1;
    b := 2;
    return a + b;
}

func let_4() {
    (foo : bar, bar : foo) := global_returns_record();
    foo := foo * bar;
    bar := 1 + bar;
    return foo + bar;
}

func lambda_1(an_arg: Global_Type) {
    return func (thing: Int, stuff) {
        return thing * stuff;
    };
}

func test_guard() {
    return 1 | 2 | 3;
}

func test_nested_apply() {
    return global_unary(value: global_unary(value: global_nullary()));
}

func test_list_index() {
    return [[1, 2], [3, 4]][0][1];
}

func test_map_index() {
    return { "key-1":11, "key-2":22, "key-3":33 }["key-1"];
}

func test_record_get() {
    return global_nullary().slot_a.slot_b;
}

func test_union_get() {
    return global_nullary() ? assume_a_tag.slot_b;
}

global_reference := just_a_symbol;

global_one := 1;

global_two := 2;

global_three := 3;

global_four := 4;

func global_nullary() {
    return 100;
}

func global_unary(value: Int) {
    return 100 + value;
}

func global_binary(left: Int, right: Int) {
    return 50 + left + right;
}

func global_returns_record() {
    return (foo: 6, bar: 7);
}