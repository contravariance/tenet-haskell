value := ordinary_application(
    slot: "has-a-value", another_slot: "has-another-value", nope: "no-default-values-here!"
);

func switch_stmt() {
    switch "Subject goes here" {
    case "Could be this!":
        return "And we'd get this";
    case "Could also be this!":
        return "And then we'd have this.";
    case "And if it's not this":
        return "Which it may not be";
    default:
        return "We'll return the default";
    }
}

func if_clause(first_test, second_test) {
    if first_test {
        return "Then do this.";
    } else if second_test {
        return "Then do this instead.";
    } else {
        return "Elso do this.";
    }
}

func loop() {
    thing := 0;
    func body_(iter__, accum__: (thing: !Unspec)) -> (cont~(thing: !Unspec) | stop~(thing: !Unspec)) {
        temp_ := accum__;
        thing := temp_.thing;
        foo := iter__;
        thing := thing + foo.num;
        return cont ~ (thing:);
    }
    temp_a := !!for_loop(body_, some_list, (thing:));
    thing := temp_a.thing;
    return thing;
}

func loop_2() {
    thing := 0;
    func body_a(iter__, accum__: (thing: !Unspec)) -> (cont~(thing: !Unspec) | stop~(thing: !Unspec)) {
        temp_n := accum__;
        thing := temp_n.thing;
        foo := iter__;
        if foo.skip {
            return cont ~ (thing:);
        } else {
            thing := thing + foo.num;
            if thing % 3 == 1 {
                return stop ~ (thing:);
            } else {
                return cont ~ (thing:);
            }
        }
    }
    temp_t := !!for_loop(body_a, some_list, (thing:));
    thing := temp_t.thing;
    return thing;
}

func switch_pattern(tagged_value, expr: Int) {
    switch tagged_value {
    case bag ~ *:
        var_ := tagged_value ? bag;
        switch var_ {
        case *:
            a := var_;
            return a;
        default:
            return expr;
        }
    case rag ~ *:
        var_a := tagged_value ? rag;
        switch var_a {
        case *:
            b := var_a;
            return b;
        default:
            return expr;
        }
    case tag ~ *:
        var_n := tagged_value ? tag;
        return expr;
    }
}

global_name := 55 + 33;

and_another := "string" + " " + "concatenation";

func simple_assignment() {
    local_name := global_name;
    another_name := and_another;
    return local_name;
}

func reassignment() {
    local_name := global_name;
    another_name := and_another;
    local_name := local_name + global_name;
    return local_name;
}

func augment_assignment() {
    local_name := global_name;
    another_name := and_another;
    local_name := local_name + 3;
    local_name := local_name * 7;
    local_name := local_name - 10;
    local_name := local_name // 5;
    return local_name;
}

foo := !!set_slot(bar, foo, qux);

func destructuring_assignment() {
    temp_j := returns_record();
    name1 := temp_j.slot1;
    name2 := temp_j.slot2;
    name3 := temp_j.slot3;
    temp_g := returns_record();
    name1 := !!set_slot(attr, name1, temp_g.slot1);
    name2 := !!set_index(3, name2, temp_g.slot2);
    temp_w := temp_g.slot3;
    name3 := temp_w.slot1;
    return (name1:, name2:, name3:);
}

some_lambda := func (an_argument: And_Type, but_no_type_here) {
    return "The body goes here.";
};

partial_app := some_function_name(an_argument: "to_value", other_arg: "other value", ...);

func some_function_name(an_argument: Str, other_arg: Str, num: Int) {
    return "body";
}

some_name := "some value.";

type Enum_Type := (#value1 | #value2 | #value3 | #value4);

type Record_Type := (slot1: Int, slot2: Int, slot3: Int);

type Function_Type := Func(param1: Str)  -> (slot1: Int, slot2: Int);

type Type_Name := [{ { Int: Symbol } }];