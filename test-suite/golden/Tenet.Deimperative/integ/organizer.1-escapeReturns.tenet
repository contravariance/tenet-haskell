type Task_Phase := (waiting~Unit | incomplete~Unit | overdue~Unit | complete~Unit | event~Unit);

type Currency_Phase := (past~Unit | recent~Unit | current~Unit | upcoming~Unit | future~Unit);

task_priority_matrix := {
    (phase: #waiting, currency: #past):0,
    (phase: #waiting, currency: #recent):0,
    (phase: #waiting, currency: #current):0,
    (phase: #waiting, currency: #upcoming):0,
    (phase: #waiting, currency: #future):0,
    (phase: #incomplete, currency: #past):1,
    (phase: #incomplete, currency: #recent):1,
    (phase: #incomplete, currency: #current):2,
    (phase: #incomplete, currency: #upcoming):1,
    (phase: #incomplete, currency: #future):0,
    (phase: #overdue, currency: #past):2,
    (phase: #overdue, currency: #recent):2,
    (phase: #overdue, currency: #current):3,
    (phase: #overdue, currency: #upcoming):3,
    (phase: #overdue, currency: #future):0,
    (phase: #complete, currency: #past):4,
    (phase: #complete, currency: #recent):4,
    (phase: #complete, currency: #current):4,
    (phase: #complete, currency: #upcoming):3,
    (phase: #complete, currency: #future):2,
    (phase: #event, currency: #past):0,
    (phase: #event, currency: #recent):1,
    (phase: #event, currency: #current):2,
    (phase: #event, currency: #upcoming):1,
    (phase: #event, currency: #future):0
};

type Timeline := (tl~{ Int: Currency_Phases });

type Currency_Config := (days_prior: Int, days_after: Int, open_seconds: Int, close_seconds: Int);

us_eastern_currency_config := (day_prior: 2, days_after: 3, open_seconds: 3600 * 9, close_seconds: 3600 * 17);

func determine_currency(now: Date_Time, date_time: Date_Time, config: CurrencyConfig) {
    bdp := business_days_prior(
        point: now, open_seconds: config.open_seconds, close_seconds: config.close_seconds, ...
    );
    bda := business_days_after(
        point: now, open_seconds: config.open_seconds, close_seconds: config.close_seconds, ...
    );
    recent := bdp(days_prior: config.days_prior);
    yesterday := bdp(days_prior: 1);
    tomorrow := bda(days_after: 1);
    soon := bda(days_after: config.days_after);
    if date_time < recent {
        return #past;
    } else if date_time < yesterday {
        return #recent;
    } else if date_time < tomorrow {
        return #current;
    } else if date_time < soon {
        return #upcoming;
    } else {
        return #future;
    }
}

type Structure := (
    name: Symbol, today: Date_Time, upcoming: Section, current: Section, closed: Section, discard: Section
);

type Section := (name: Symbol, contents: [Entry]);

type Entry_Type := (#task | #event);

type Entry := (
    type_: Entry_Type, label: Symbol, times: { Task_Phase: Date_Time }, paths: { [Symbol] }, notes: Symbol
);