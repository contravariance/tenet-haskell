type Iso8601Date := (year: Int, month: Int, day: Int);

type Iso8601Time := (hour: Int, minute: Int, second: Int);

type Duration := (seconds~Int);

type Iso8601Simple := (year: Int, month: Int, day: Int, hour: Int, minute: Int, second: Int);

type DateTime := (epoch_ce~Int | epoch_posix~Int | iso8601~Iso8601Simple);

func dt_plus_dur(left: DateTime, right: Duration) {
    dt_seconds := as_epoch_ce(value: left);
    dur_seconds := as_dur_seconds(value: right);
    return epoch_ce ~ (dt_seconds + dur_seconds);
}

func dt_minus_dur(left: DateTime, right: Duration) {
    dt_seconds := as_epoch_ce(value: left);
    dur_seconds := as_dur_seconds(value: right);
    return epoch_ce ~ (dt_seconds - dur_seconds);
}

func dt_minus_dt(left: DateTime, right: DateTime) {
    left_seconds := as_epoch_ce(value: left);
    right_seconds := as_epoch_ce(value: right);
    return dur_seconds ~ (left_seconds - right_seconds);
}

func dur_plus_dur(left: Duration, right: Duration) {
    return dur_seconds ~ (as_dur_seconds(value: left) + as_dur_seconds(value: right));
}

func as_iso8601(value: DateTime) {
    switch value {
    case epoch_posix ~ *:
        epoch_seconds := value ? epoch_posix;
        return epoch_posix_to_iso8601(value: epoch_seconds);
    case epoch_ce ~ *:
        epoch_seconds := value ? epoch_ce;
        return epoch_ce_to_iso8601(value: seoncds);
    case iso8601 ~ *:
        datetime := value ? iso8601;
        return datetime;
    }
}

func as_epoch_ce(value: DateTime) {
    switch value {
    case epoch_posix ~ *:
        epoch_seconds := value ? epoch_posix;
        return epoch_posix_to_ce(value: epoch_seconds);
    case epoch_ce ~ *:
        epoch_seconds := value ? epoch_ce;
        return epoch_seconds;
    case iso8601 ~ *:
        datetime := value ? iso8601;
        return iso8601_to_epoch_ce(value: datetime);
    }
}

func as_epoch_posix(value: DateTime) {
    switch value {
    case epoch_posix ~ *:
        epoch_seconds := value ? epoch_posix;
        return epoch_seconds;
    case epoch_ce ~ *:
        epoch_seconds := value ? epoch_ce;
        return epoch_ce_to_posix(value: epoch_seconds);
    case iso8601 ~ *:
        datetime := value ? iso8601;
        return iso8601_to_epoch_posix(value: datetime);
    }
}

func as_dur_seconds(value: Duration) {
    return value ? seconds;
}

func epoch_ce_to_iso8601(epoch_seconds: Int) {
    temp_n := split_epoch_seconds(epoch_seconds:);
    epoch_days := temp_n.epoch_days;
    day_seconds := temp_n.day_seconds;
    temp_a := epoch_days_to_year_month_day(epoch_days:);
    year := temp_a.year;
    month := temp_a.month;
    day := temp_a.day;
    temp_ := seconds_to_hour_minute_second(day_seconds:);
    hour := temp_.hour;
    minute := temp_.minute;
    second := temp_.second;
    return (year:, month:, day:, hour:, minute:, second:);
}

func epoch_ce_to_posix(epoch_seconds: Int) {
    return epoch_seconds + ce_to_posix_offset;
}

func epoch_posix_to_ce(epoch_seconds: Int) {
    return epoch_seconds - ce_to_posix_offset;
}

func epoch_posix_to_iso8601(epoch_seconds: Int) {
    return epoch_ce_to_iso8601(value: epoch_posix_to_ce(value: epoch_seconds));
}

func iso8601_to_epoch_ce(datetime: Iso8601Simple) {
    temp_t := datetime;
    year := temp_t.year;
    month := temp_t.month;
    day := temp_t.day;
    hour := temp_t.hour;
    minute := temp_t.minute;
    second := temp_t.second;
    epoch_days := year_month_day_to_epoch_days(year:, month:, day:);
    day_seconds := time_of_day_to_seconds(hour:, minute:, second:);
    return combine_epoch_date(epoch_days:, day_seconds:);
}

func iso8601_to_epoch_posix(datetime: Iso8601Simple) {
    return epoch_ce_to_posix(value: iso8601_to_epoch_ce(value: datetime));
}

ce_to_posix_offset := 719162 * seconds_in_day;

seconds_in_day := 24 * 60 * 60;

func split_epoch_seconds(epoch_seconds: Int) {
    temp_g := div_mod(left: epoch_seconds, right: seconds_in_day);
    div := temp_g.div;
    mod := temp_g.mod;
    return (epoch_days: div, day_seconds: mod);
}

func split_epoch_ce(value: DateTime) {
    return split_epoch_seconds(value: as_epoch_ce(value:));
}

func combine_epoch_date(epoch_days: Int, day_seconds: Int) {
    return seconds_in_day * epoch_days + day_seconds;
}

func seconds_to_hour_minute_second(seconds: Int) {
    temp_j := div_mod(left: seconds, right: 60);
    minutes := temp_j.div;
    second := temp_j.mod;
    temp_w := div_mod(left: minutes, right: 60);
    hour := temp_w.div;
    minute := temp_w.mod;
    return (hour:, minute:, second:);
}

func hour_minute_second_to_seconds(hour: Int, minute: Int, second: Int) {
    return 3600 * hour + 60 * minute + second;
}

func month_start(month: Int, is_leap_year: Boolean) {
    if is_leap_year {
        starts := [0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335];
        return starts[month];
    } else {
        starts := [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
        return starts[month];
    }
}

days_in_quad_century := 146097;

days_in_century := 36524;

days_in_quad_year := 1461;

days_in_std_year := 365;

func epoch_day_to_year_month_day(epoch_days: Int) -> Iso8601Date {
    temp_e := div_mod(left: epoch_days, right: days_in_quad_century);
    quad_century := temp_e.div;
    quad_centry_day := temp_e.mod;
    temp_r := div_mod(left: quad_century_day, right: days_in_century);
    century_p := temp_r.div;
    century_day := temp_r.mod;
    if century_p == 4 {
        century := 3;
        temp_d := div_mod(left: century_day, right: days_in_quad_year);
        quad_year := temp_d.div;
        quad_year_day := temp_d.mod;
        temp_q := div_mod(left: quad_year_day, right: days_in_year);
        year_p := temp_q.div;
        year_day := temp_q.mod;
        if year_p == 4 {
            year := 3;
            if century_p == 4 or year_p == 4 {
                year_day_0 := 365;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day_0 + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day_0 {
                    month := month_p - 1;
                    month_start_day_0 := month_start(month:, is_leap_year:);
                    month_day := year_day_0 - month_start_day_0;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day_0 - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            } else {
                year_day_1 := year_day_p;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day_1 + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day_1 {
                    month := month_p - 1;
                    month_start_day_1 := month_start(month:, is_leap_year:);
                    month_day := year_day_1 - month_start_day_1;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day_1 - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            }
        } else {
            year := year_p;
            if century_p == 4 or year_p == 4 {
                year_day_2 := 365;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day_2 + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day_2 {
                    month := month_p - 1;
                    month_start_day_2 := month_start(month:, is_leap_year:);
                    month_day := year_day_2 - month_start_day_2;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day_2 - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            } else {
                year_day_3 := year_day_p;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day_3 + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day_3 {
                    month := month_p - 1;
                    month_start_day_3 := month_start(month:, is_leap_year:);
                    month_day := year_day_3 - month_start_day_3;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day_3 - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            }
        }
    } else {
        century := century_p;
        temp_l := div_mod(left: century_day, right: days_in_quad_year);
        quad_year := temp_l.div;
        quad_year_day := temp_l.mod;
        temp_y := div_mod(left: quad_year_day, right: days_in_year);
        year_p := temp_y.div;
        year_day := temp_y.mod;
        if year_p == 4 {
            year := 3;
            if century_p == 4 or year_p == 4 {
                year_day_4 := 365;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day_4 + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day_4 {
                    month := month_p - 1;
                    month_start_day_4 := month_start(month:, is_leap_year:);
                    month_day := year_day_4 - month_start_day_4;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day_4 - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            } else {
                year_day_5 := year_day_p;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day_5 + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day_5 {
                    month := month_p - 1;
                    month_start_day_5 := month_start(month:, is_leap_year:);
                    month_day := year_day_5 - month_start_day_5;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day_5 - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            }
        } else {
            year := year_p;
            if century_p == 4 or year_p == 4 {
                year_day_6 := 365;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day_6 + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day_6 {
                    month := month_p - 1;
                    month_start_day_6 := month_start(month:, is_leap_year:);
                    month_day := year_day_6 - month_start_day_6;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day_6 - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            } else {
                year_day_7 := year_day_p;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day_7 + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day_7 {
                    month := month_p - 1;
                    month_start_day_7 := month_start(month:, is_leap_year:);
                    month_day := year_day_7 - month_start_day_7;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day_7 - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            }
        }
    }
}

func year_month_day_to_epoch_days(year: Int, month: Int, day: Int) -> Index {
    temp_o := div_mod(left: year - 1, right: 400);
    quad_century := temp_o.div;
    quad_century_year := temp_o.mod;
    temp_h := div_mod(left: quad_century_year, right: 100);
    century := temp_h.div;
    century_year := temp_h.mod;
    temp_u := div_mod(left: century_year, right: 4);
    quad_years := temp_u.div;
    quad_year := temp_u.mod;
    is_leap_year := quad_year == 3 and (century_year != 99 or century == 3);
    month_start_day := month_start(month:, is_leap_year:);
    return day - 1 + month_start_day + days_in_year * quad_year + days_in_quad_year * quad_years + days_in_century * century + days_in_quad_century * quad_century;
}

type Month := (#jan | #feb | #mar | #apr | #may | #jun | #jul | #aug | #sep | #oct | #nov | #dec);

func month_from_index(index: Int) -> Month {
    return [#jan, #feb, #mar, #apr, #may, #jun, #jul, #aug, #sep, #oct, #nov, #dec][index - 1];
}

func index_of_month(month: Month) -> Int {
    return { #jan:1, #feb:2, #mar:3, #apr:4, #may:5, #jun:6, #jul:7, #aug:8, #sep:9, #oct:10, #nov:11, #dec:12 }[
        month
    ];
}

type DayOfWeek := (#sun | #mon | #tue | #wed | #thu | #fri | #sat);

func day_of_week_from_index(index: Int) -> DayOfWeek {
    return [#sun, #mon, #tue, #wed, #thu, #fri, #sat][index];
}

func index_of_day_of_week(day: DayOfWeek) -> Int {
    return { #sun:0, #mon:1, #tue:2, #wed:3, #thu:4, #fri:5, #sat:6 }[day];
}

func day_of_week_of_epoch_days(epoch_days: Int) {
    return (6 + epoch_days) % 7;
}

func day_of_week_date_time(value: DateTime) {
    return day_of_week_of_epoch_days(value: split_epoch_ce(value:).epoch_days);
}

func closest_business_day_forwards(value: Int) {
    switch day_of_week_of_epoch_days(epoch_days: value) {
    case 0:
        return value + 1;
    case 6:
        return value + 2;
    default:
        return value;
    }
}

func closest_business_day_backwards(value: Int) {
    switch day_of_week_of_epoch_days(epoch_days: value) {
    case 0:
        return value - 2;
    case 6:
        return value - 1;
    default:
        return value;
    }
}

func business_days_after(point: DateTime, days_change: Int, open_seconds: Int, close_seconds: Int) {
    temp_f := split_epoch_ce(value: point);
    epoch_days := temp_f.epoch_days;
    day_seconds := temp_f.day_seconds;
    after_hours := day_seconds > close_seconds;
    before_hours := day_seconds < open_seconds;
    if after_hours {
        day := 1 + epoch_days;
        if after_hours or before_hours {
            seconds_p := open_seconds;
            day_0 := closest_business_day_forwards(value: day);
            temp_b := div_mod(left: days_change, right: 5);
            weeks_change := temp_b.div;
            days_change_0 := temp_b.mod;
            day_1 := day_0 + 7 * weeks_change + days_change_0;
            day_2 := closest_business_day_forwards_p(value: day_1);
            return epoch_ce ~ combine_epoch_date(epoch_days: day_2, day_seconds: seconds_p);
        } else {
            seconds_p := day_seconds;
            day_3 := closest_business_day_forwards(value: day);
            temp_z := div_mod(left: days_change, right: 5);
            weeks_change := temp_z.div;
            days_change_1 := temp_z.mod;
            day_4 := day_3 + 7 * weeks_change + days_change_1;
            day_5 := closest_business_day_forwards_p(value: day_4);
            return epoch_ce ~ combine_epoch_date(epoch_days: day_5, day_seconds: seconds_p);
        }
    } else {
        day := epoch_days;
        if after_hours or before_hours {
            seconds_p := open_seconds;
            day_6 := closest_business_day_forwards(value: day);
            temp_m := div_mod(left: days_change, right: 5);
            weeks_change := temp_m.div;
            days_change_2 := temp_m.mod;
            day_7 := day_6 + 7 * weeks_change + days_change_2;
            day_8 := closest_business_day_forwards_p(value: day_7);
            return epoch_ce ~ combine_epoch_date(epoch_days: day_8, day_seconds: seconds_p);
        } else {
            seconds_p := day_seconds;
            day_9 := closest_business_day_forwards(value: day);
            temp_s := div_mod(left: days_change, right: 5);
            weeks_change := temp_s.div;
            days_change_3 := temp_s.mod;
            day_10 := day_9 + 7 * weeks_change + days_change_3;
            day_11 := closest_business_day_forwards_p(value: day_10);
            return epoch_ce ~ combine_epoch_date(epoch_days: day_11, day_seconds: seconds_p);
        }
    }
}

func business_days_prior(point: DateTime, days_change: Int, open_seconds: Int, close_seconds: Int) {
    temp_x := split_epoch_ce(value: point);
    epoch_days := temp_x.epoch_days;
    day_seconds := temp_x.day_seconds;
    after_hours := day_seconds > close_seconds;
    before_hours := day_seconds < open_seconds;
    if before_hours {
        day := epoch_days - 1;
        if after_hours or before_hours {
            seconds_p := close_seconds;
            day_12 := closest_business_day_backwards(value: day);
            temp_v := div_mod(left: days_change, right: 5);
            weeks_change := temp_v.div;
            days_change_4 := temp_v.mod;
            day_13 := day_12 + 7 * weeks_change + days_change_4;
            day_14 := closest_business_day_backwards(value: day_13);
            return epoch_ce ~ combine_epoch_date(epoch_days: day_14, day_seconds: seconds_p);
        } else {
            seconds_p := day_seconds;
            day_15 := closest_business_day_backwards(value: day);
            temp_i := div_mod(left: days_change, right: 5);
            weeks_change := temp_i.div;
            days_change_5 := temp_i.mod;
            day_16 := day_15 + 7 * weeks_change + days_change_5;
            day_17 := closest_business_day_backwards(value: day_16);
            return epoch_ce ~ combine_epoch_date(epoch_days: day_17, day_seconds: seconds_p);
        }
    } else {
        day := epoch_days;
        if after_hours or before_hours {
            seconds_p := close_seconds;
            day_18 := closest_business_day_backwards(value: day);
            temp_p := div_mod(left: days_change, right: 5);
            weeks_change := temp_p.div;
            days_change_6 := temp_p.mod;
            day_19 := day_18 + 7 * weeks_change + days_change_6;
            day_20 := closest_business_day_backwards(value: day_19);
            return epoch_ce ~ combine_epoch_date(epoch_days: day_20, day_seconds: seconds_p);
        } else {
            seconds_p := day_seconds;
            day_21 := closest_business_day_backwards(value: day);
            temp_c := div_mod(left: days_change, right: 5);
            weeks_change := temp_c.div;
            days_change_7 := temp_c.mod;
            day_22 := day_21 + 7 * weeks_change + days_change_7;
            day_23 := closest_business_day_backwards(value: day_22);
            return epoch_ce ~ combine_epoch_date(epoch_days: day_23, day_seconds: seconds_p);
        }
    }
}