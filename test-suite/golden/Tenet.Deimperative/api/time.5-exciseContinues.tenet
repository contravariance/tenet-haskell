type Iso8601Date := (year: Int, month: Int, day: Int);

type Iso8601Time := (hour: Int, minute: Int, second: Int);

type Duration := (seconds~Int);

type Iso8601Simple := (year: Int, month: Int, day: Int, hour: Int, minute: Int, second: Int);

type DateTime := (epoch_ce~Int | epoch_posix~Int | iso8601~Iso8601Simple);

func dt_plus_dur(left: DateTime, right: Duration) {
    dt_seconds := as_epoch_ce(value: left);
    dur_seconds := as_dur_seconds(value: right);
    return epoch_ce ~ (dt_seconds + dur_seconds);
}

func dt_minus_dur(left: DateTime, right: Duration) {
    dt_seconds := as_epoch_ce(value: left);
    dur_seconds := as_dur_seconds(value: right);
    return epoch_ce ~ (dt_seconds - dur_seconds);
}

func dt_minus_dt(left: DateTime, right: DateTime) {
    left_seconds := as_epoch_ce(value: left);
    right_seconds := as_epoch_ce(value: right);
    return dur_seconds ~ (left_seconds - right_seconds);
}

func dur_plus_dur(left: Duration, right: Duration) {
    return dur_seconds ~ (as_dur_seconds(value: left) + as_dur_seconds(value: right));
}

func as_iso8601(value: DateTime) {
    switch value {
    case epoch_posix ~ epoch_seconds:
        return epoch_posix_to_iso8601(value: epoch_seconds);
    case epoch_ce ~ epoch_seconds:
        return epoch_ce_to_iso8601(value: seoncds);
    case iso8601 ~ datetime:
        return datetime;
    }
}

func as_epoch_ce(value: DateTime) {
    switch value {
    case epoch_posix ~ epoch_seconds:
        return epoch_posix_to_ce(value: epoch_seconds);
    case epoch_ce ~ epoch_seconds:
        return epoch_seconds;
    case iso8601 ~ datetime:
        return iso8601_to_epoch_ce(value: datetime);
    }
}

func as_epoch_posix(value: DateTime) {
    switch value {
    case epoch_posix ~ epoch_seconds:
        return epoch_seconds;
    case epoch_ce ~ epoch_seconds:
        return epoch_ce_to_posix(value: epoch_seconds);
    case iso8601 ~ datetime:
        return iso8601_to_epoch_posix(value: datetime);
    }
}

func as_dur_seconds(value: Duration) {
    return value ? seconds;
}

func epoch_ce_to_iso8601(epoch_seconds: Int) {
    (epoch_days, day_seconds) := split_epoch_seconds(epoch_seconds:);
    (year, month, day) := epoch_days_to_year_month_day(epoch_days:);
    (hour, minute, second) := seconds_to_hour_minute_second(day_seconds:);
    return (year:, month:, day:, hour:, minute:, second:);
}

func epoch_ce_to_posix(epoch_seconds: Int) {
    return epoch_seconds + ce_to_posix_offset;
}

func epoch_posix_to_ce(epoch_seconds: Int) {
    return epoch_seconds - ce_to_posix_offset;
}

func epoch_posix_to_iso8601(epoch_seconds: Int) {
    return epoch_ce_to_iso8601(value: epoch_posix_to_ce(value: epoch_seconds));
}

func iso8601_to_epoch_ce(datetime: Iso8601Simple) {
    (year, month, day, hour, minute, second) := datetime;
    epoch_days := year_month_day_to_epoch_days(year:, month:, day:);
    day_seconds := time_of_day_to_seconds(hour:, minute:, second:);
    return combine_epoch_date(epoch_days:, day_seconds:);
}

func iso8601_to_epoch_posix(datetime: Iso8601Simple) {
    return epoch_ce_to_posix(value: iso8601_to_epoch_ce(value: datetime));
}

ce_to_posix_offset := 719162 * seconds_in_day;

seconds_in_day := 24 * 60 * 60;

func split_epoch_seconds(epoch_seconds: Int) {
    (div, mod) := div_mod(left: epoch_seconds, right: seconds_in_day);
    return (epoch_days: div, day_seconds: mod);
}

func split_epoch_ce(value: DateTime) {
    return split_epoch_seconds(value: as_epoch_ce(value:));
}

func combine_epoch_date(epoch_days: Int, day_seconds: Int) {
    return seconds_in_day * epoch_days + day_seconds;
}

func seconds_to_hour_minute_second(seconds: Int) {
    (div : minutes, mod : second) := div_mod(left: seconds, right: 60);
    (div : hour, mod : minute) := div_mod(left: minutes, right: 60);
    return (hour:, minute:, second:);
}

func hour_minute_second_to_seconds(hour: Int, minute: Int, second: Int) {
    return 3600 * hour + 60 * minute + second;
}

func month_start(month: Int, is_leap_year: Boolean) {
    if is_leap_year {
        starts := [0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335];
        return starts[month];
    } else {
        starts := [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334];
        return starts[month];
    }
}

days_in_quad_century := 146097;

days_in_century := 36524;

days_in_quad_year := 1461;

days_in_std_year := 365;

func epoch_day_to_year_month_day(epoch_days: Int) -> Iso8601Date {
    (div : quad_century, mod : quad_centry_day) := div_mod(left: epoch_days, right: days_in_quad_century);
    (div : century_p, mod : century_day) := div_mod(left: quad_century_day, right: days_in_century);
    if century_p == 4 {
        century := 3;
        (div : quad_year, mod : quad_year_day) := div_mod(left: century_day, right: days_in_quad_year);
        (div : year_p, mod : year_day) := div_mod(left: quad_year_day, right: days_in_year);
        if year_p == 4 {
            year := 3;
            if century_p == 4 or year_p == 4 {
                year_day := 365;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day {
                    month := month_p - 1;
                    month_start_day := month_start(month:, is_leap_year:);
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            } else {
                year_day := year_day_p;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day {
                    month := month_p - 1;
                    month_start_day := month_start(month:, is_leap_year:);
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            }
        } else {
            year := year_p;
            if century_p == 4 or year_p == 4 {
                year_day := 365;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day {
                    month := month_p - 1;
                    month_start_day := month_start(month:, is_leap_year:);
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            } else {
                year_day := year_day_p;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day {
                    month := month_p - 1;
                    month_start_day := month_start(month:, is_leap_year:);
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            }
        }
    } else {
        century := century_p;
        (div : quad_year, mod : quad_year_day) := div_mod(left: century_day, right: days_in_quad_year);
        (div : year_p, mod : year_day) := div_mod(left: quad_year_day, right: days_in_year);
        if year_p == 4 {
            year := 3;
            if century_p == 4 or year_p == 4 {
                year_day := 365;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day {
                    month := month_p - 1;
                    month_start_day := month_start(month:, is_leap_year:);
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            } else {
                year_day := year_day_p;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day {
                    month := month_p - 1;
                    month_start_day := month_start(month:, is_leap_year:);
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            }
        } else {
            year := year_p;
            if century_p == 4 or year_p == 4 {
                year_day := 365;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day {
                    month := month_p - 1;
                    month_start_day := month_start(month:, is_leap_year:);
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            } else {
                year_day := year_day_p;
                is_leap_year := century_p == 4 or year_p > 2 or quad_year == 24;
                month_p := (year_day + 50) // 32;
                month_start_day := month_start(month: month_p, is_leap_year:);
                if month_start_day > year_day {
                    month := month_p - 1;
                    month_start_day := month_start(month:, is_leap_year:);
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                } else {
                    month := month_p;
                    month_day := year_day - month_start_day;
                    return (year: 400 * quad_century + 100 * century + year + 1, month: month + 1, day: month_day + 1);
                }
            }
        }
    }
}

func year_month_day_to_epoch_days(year: Int, month: Int, day: Int) -> Index {
    (div : quad_century, mod : quad_century_year) := div_mod(left: year - 1, right: 400);
    (div : century, mod : century_year) := div_mod(left: quad_century_year, right: 100);
    (div : quad_years, mod : quad_year) := div_mod(left: century_year, right: 4);
    is_leap_year := quad_year == 3 and (century_year != 99 or century == 3);
    month_start_day := month_start(month:, is_leap_year:);
    return day - 1 + month_start_day + days_in_year * quad_year + days_in_quad_year * quad_years + days_in_century * century + days_in_quad_century * quad_century;
}

type Month := (#jan | #feb | #mar | #apr | #may | #jun | #jul | #aug | #sep | #oct | #nov | #dec);

func month_from_index(index: Int) -> Month {
    return [#jan, #feb, #mar, #apr, #may, #jun, #jul, #aug, #sep, #oct, #nov, #dec][index - 1];
}

func index_of_month(month: Month) -> Int {
    return { #jan:1, #feb:2, #mar:3, #apr:4, #may:5, #jun:6, #jul:7, #aug:8, #sep:9, #oct:10, #nov:11, #dec:12 }[
        month
    ];
}

type DayOfWeek := (#sun | #mon | #tue | #wed | #thu | #fri | #sat);

func day_of_week_from_index(index: Int) -> DayOfWeek {
    return [#sun, #mon, #tue, #wed, #thu, #fri, #sat][index];
}

func index_of_day_of_week(day: DayOfWeek) -> Int {
    return { #sun:0, #mon:1, #tue:2, #wed:3, #thu:4, #fri:5, #sat:6 }[day];
}

func day_of_week_of_epoch_days(epoch_days: Int) {
    return (6 + epoch_days) % 7;
}

func day_of_week_date_time(value: DateTime) {
    return day_of_week_of_epoch_days(value: split_epoch_ce(value:).epoch_days);
}

func closest_business_day_forwards(value: Int) {
    switch day_of_week_of_epoch_days(epoch_days: value) {
    case 0:
        return value + 1;
    case 6:
        return value + 2;
    default:
        return value;
    }
}

func closest_business_day_backwards(value: Int) {
    switch day_of_week_of_epoch_days(epoch_days: value) {
    case 0:
        return value - 2;
    case 6:
        return value - 1;
    default:
        return value;
    }
}

func business_days_after(point: DateTime, days_change: Int, open_seconds: Int, close_seconds: Int) {
    (epoch_days, day_seconds) := split_epoch_ce(value: point);
    after_hours := day_seconds > close_seconds;
    before_hours := day_seconds < open_seconds;
    if after_hours {
        day := 1 + epoch_days;
        if after_hours or before_hours {
            seconds_p := open_seconds;
            day := closest_business_day_forwards(value: day);
            (div : weeks_change, mod : days_change) := div_mod(left: days_change, right: 5);
            day += 7 * weeks_change + days_change;
            day := closest_business_day_forwards_p(value: day);
            return epoch_ce ~ combine_epoch_date(epoch_days: day, day_seconds: seconds_p);
        } else {
            seconds_p := day_seconds;
            day := closest_business_day_forwards(value: day);
            (div : weeks_change, mod : days_change) := div_mod(left: days_change, right: 5);
            day += 7 * weeks_change + days_change;
            day := closest_business_day_forwards_p(value: day);
            return epoch_ce ~ combine_epoch_date(epoch_days: day, day_seconds: seconds_p);
        }
    } else {
        day := epoch_days;
        if after_hours or before_hours {
            seconds_p := open_seconds;
            day := closest_business_day_forwards(value: day);
            (div : weeks_change, mod : days_change) := div_mod(left: days_change, right: 5);
            day += 7 * weeks_change + days_change;
            day := closest_business_day_forwards_p(value: day);
            return epoch_ce ~ combine_epoch_date(epoch_days: day, day_seconds: seconds_p);
        } else {
            seconds_p := day_seconds;
            day := closest_business_day_forwards(value: day);
            (div : weeks_change, mod : days_change) := div_mod(left: days_change, right: 5);
            day += 7 * weeks_change + days_change;
            day := closest_business_day_forwards_p(value: day);
            return epoch_ce ~ combine_epoch_date(epoch_days: day, day_seconds: seconds_p);
        }
    }
}

func business_days_prior(point: DateTime, days_change: Int, open_seconds: Int, close_seconds: Int) {
    (epoch_days, day_seconds) := split_epoch_ce(value: point);
    after_hours := day_seconds > close_seconds;
    before_hours := day_seconds < open_seconds;
    if before_hours {
        day := epoch_days - 1;
        if after_hours or before_hours {
            seconds_p := close_seconds;
            day := closest_business_day_backwards(value: day);
            (div : weeks_change, mod : days_change) := div_mod(left: days_change, right: 5);
            day += 7 * weeks_change + days_change;
            day := closest_business_day_backwards(value: day);
            return epoch_ce ~ combine_epoch_date(epoch_days: day, day_seconds: seconds_p);
        } else {
            seconds_p := day_seconds;
            day := closest_business_day_backwards(value: day);
            (div : weeks_change, mod : days_change) := div_mod(left: days_change, right: 5);
            day += 7 * weeks_change + days_change;
            day := closest_business_day_backwards(value: day);
            return epoch_ce ~ combine_epoch_date(epoch_days: day, day_seconds: seconds_p);
        }
    } else {
        day := epoch_days;
        if after_hours or before_hours {
            seconds_p := close_seconds;
            day := closest_business_day_backwards(value: day);
            (div : weeks_change, mod : days_change) := div_mod(left: days_change, right: 5);
            day += 7 * weeks_change + days_change;
            day := closest_business_day_backwards(value: day);
            return epoch_ce ~ combine_epoch_date(epoch_days: day, day_seconds: seconds_p);
        } else {
            seconds_p := day_seconds;
            day := closest_business_day_backwards(value: day);
            (div : weeks_change, mod : days_change) := div_mod(left: days_change, right: 5);
            day += 7 * weeks_change + days_change;
            day := closest_business_day_backwards(value: day);
            return epoch_ce ~ combine_epoch_date(epoch_days: day, day_seconds: seconds_p);
        }
    }
}