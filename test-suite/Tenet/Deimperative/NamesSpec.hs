module Tenet.Deimperative.NamesSpec
    ( spec
    )
where

import           Tenet.State.Names              ( findSafePrefix
                                                , prefixStart
                                                , prefixStop
                                                )
import           Test.Hspec
import           Test.QuickCheck

import qualified Data.Set                      as S
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Data.Typeable                  ( Typeable )

arbitraryPrefixChar :: Gen Char
arbitraryPrefixChar = choose (prefixStart, pred prefixStop)

newtype Prefix = Prefix
  { getPrefix :: String
  } deriving (Eq, Ord, Show, Typeable)

instance Arbitrary Prefix where
    arbitrary = Prefix `fmap` listOf arbitraryPrefixChar
    shrink (Prefix xs) = Prefix `fmap` shrink xs

prefixNotInSet :: Text -> S.Set Text -> Bool
prefixNotInSet prefix = S.null . S.filter (T.isPrefixOf prefix)

safePrefix :: S.Set Prefix -> Bool
safePrefix set' = findSafePrefix set `prefixNotInSet` set where set = S.map (T.pack . getPrefix) set'

spec :: Spec
spec = describe "Tenet.Deimperative.Names" $ it "findSafePrefix finds an unused prefix" $ property safePrefix
