{-# LANGUAGE OverloadedStrings #-}

module Tenet.Deimperative.PatternDeconstructSpec
    ( spec
    )
where

import qualified Data.Map                      as M
import           Tenet.Ast
import           Tenet.Deimperative.PatternDeconstruction
import           Test.Hspec

simpleCase pairs = CaseMap { cmMap = M.fromList pairs, cmBody = [], cmNode = nullNode }

cases = map
    simpleCase
    [ [("foo", integerPat 3), ("bar", stringPat "alpha")]
    , [("foo", integerPat 5), ("bar", stringPat "beta")]
    , [("foo", integerPat 3), ("bar", stringPat "alpha")]
    ]

notNarrowEq a b = not $ isNarrowEq a b

-- isWiderEq = flip isNarrowEq

spec :: Spec
spec = describe "Tenet.Deimperative.PatternDeconstruct" $ do
    it "patternsPerSlot" $ patternsPerSlot "foo" cases == [integerPat 3, integerPat 5]
    it "5 isNarrowEq *" $ integerPat 5 `isNarrowEq` anyPat
    it "'foo' isNarrowEq *" $ stringPat "foo" `isNarrowEq` anyPat
    it "* notNarrowEq 5" $ anyPat `notNarrowEq` integerPat 5
    it "* notNarrowEq 'foo'" $ anyPat `notNarrowEq` stringPat "foo"
    it "5 notNarrowEq 'foo'" $ integerPat 5 `notNarrowEq` stringPat "foo"
    it "{a:5, b:3} isNarrowEq {a:*, b:3}" $ tupAB (integerPat 5) `isNarrowEq` tupAB anyPat
    it "{a:'foo', b:3} isNarrowEq {a:*, b:3}" $ tupAB (stringPat "foo") `isNarrowEq` tupAB anyPat
    it "{a:*, b:3} notNarrowEq {a:5, b:3}" $ tupAB anyPat `notNarrowEq` tupAB (integerPat 5)
    it "{a:*, b:3} notNarrowEq {a:'foo', b:3}" $ tupAB anyPat `notNarrowEq` tupAB (stringPat "foo")
    it "{a:5, b:3} notNarrowEq {a:'foo', b:3}" $ tupAB (integerPat 5) `notNarrowEq` tupAB (stringPat "foo")

tupAB pat = recordPat [("a", pat), ("b", integerPat 3)]
anyPat = AnyPat nullNode
integerPat = IntegerPat nullNode
stringPat = StringPat nullNode
recordPat = RecordPat nullNode
