{-# LANGUAGE OverloadedStrings #-}

module Tenet.ParsingSpec
    ( spec
    )
where

import           Test.Golden                    ( gdescribe
                                                , git
                                                , gitfail
                                                )
import           Test.Hspec                     ( Spec )

import           Tenet.Parsing                  ( parseStatement
                                                , parseTopStmt
                                                )

spec :: Spec
spec = do
    gdescribe "Tenet.Parsing.letStatements" $ do
        git parseStatement "let1"    "foo.bar=22+a"
        git parseStatement "let1s"   "foo . bar = 22 + a "
        git parseStatement "let2"    "let foo.bar=22+a"
        git parseStatement "let2s"   "let foo . bar = 22 + a "
        git parseStatement "def1"    "def foo.bar=22+a"
        git parseStatement "def1s"   "def foo . bar = 22 + a "
        git parseStatement "return1" "return 22+a"
        gitfail parseStatement "return1 invalid" "return22+a"
        git parseStatement "return1s" "return 22 + a "
    gdescribe "Tenet.Parsing.ifStatements" $ do
        git parseStatement "if else"             "if a<20{\n  foo.bar=22+a\n}else{\n  return 22+a\n}"
        git parseStatement "if else spaces"      "if a < 20 {\n  foo . bar = 22 + a\n} else { \n  return 22 + a \n}"
        git parseStatement "if simple"           "if a<20{\n  foo.bar=22+a\n}"
        git parseStatement "if simple spaces"    "if a < 20 {  \n  foo . bar = 22 + a \n}"
        git parseStatement "if nested"           "if a<20{\n  if a<20{\n    return 22+a}}"
        git parseStatement "if nested spaces"    "if a < 20 { \n  if a < 20 { \n    return 22 + a } }"
        git parseStatement "if pass else"        "if a<20{\n  pass\n}else{\n  foo.bar=22+a}"
        git parseStatement "if pass else spaces" "if a < 20 { \n  pass \n} else { \n  foo . bar = 22 + a }"
        git parseStatement "if elif else"        "if a<20{\n  pass\n}else if a < 20{\n  pass\n}else{\n  pass}"
    gdescribe "Tenet.Parsing.forStatements" $ do
        git parseStatement "for simple"        "for a in [1,2,3]{\n  foo.bar=22+a\n}"
        git parseStatement "for simple spaces" "for a in [ 1 , 2 , 3 ] { \n  foo . bar = 22 + a\n}"
    gdescribe "Tenet.Parsing.switchStatements" $ do
        git parseStatement "switch simple 1"  "switch var{\ncase 42:\n  pass\n}"
        git parseStatement "switch simple 2"  "switch var{\ncase \"string\":\n  pass\n}"
        git parseStatement "switch simple 3"  "switch var{\ncase name:\n  pass\n}"
        git parseStatement "switch simple 4"  "switch var{\ncase (slot:val):\n  pass\n}"
        git parseStatement "switch simple 5"  "switch var{\ncase #enum:\n  pass\n}"
        git parseStatement "switch simple 6"  "switch var{\ncase tag~x:\n  pass\n}"
        git parseStatement "switch default 1" "switch var{\ncase name:\n  pass\ndefault:\n  pass\n}"
    gdescribe "Tenet.Parsing.funcStatements" $ do
        git parseStatement "func simple"                  "func foo(a:Int)->Int{\n  return a*2\n}"
        git parseStatement "func simple, spaces"          "func foo ( a : Int ) -> Int { \n  return a * 2 \n}"
        git parseStatement "func simple, spaces, newline" "func foo ( a :\n Int ) -> Int { \n  return a * 2 \n}"
        git parseStatement "func no return type"          "func foo(a:Int){\n  return a*2\n}"
        git parseStatement "func no return type, spaces"  "func foo ( a : Int ) { \n  return a * 2 \n}"
        git parseStatement "pass statement1"              "pass"
        git parseStatement "pass statement, semicolon"    "pass;"
    gdescribe "Tenet.Parsing.tryStatements" $ do
        git parseStatement "try simple" "try{\n  a = 1 // x\n  b = 3 + a\n}catch Division_By_Zero{\n  pass \n}"
        git parseStatement
            "try complex catch"
            "try {\n  a = 1 // x\n  b = 3 + a\n} catch Division_By_Zero {\n  a = 5\n  b = 4\n} "
        git parseStatement
            "try multiple catchions"
            "try {\n  a = 1 // x\n  b = [1, 2, 3][a]\n} catch Division_By_Zero, Failed_Lookup {\n  pass\n} "
    gdescribe "Tenet.Parsing.topStatement" $ do
        git parseTopStmt "global variable"             "some_global=2// +4-5;"
        git parseTopStmt "global variable, spaces"     "some_global = 2 // + 4 - 5 ;"
        git parseTopStmt "global compound"             "some_global.foo[3]+=other_global;"
        git parseTopStmt "global compound, spaces"     "some_global . foo [\n 3 ] += other_global ;"
        git parseTopStmt "global let variable"         "let some_global=2// +4-5;"
        git parseTopStmt "global let variable, spaces" "let some_global = 2 // + 4 - 5 ;"
        git parseTopStmt "global let compound"         "let some_global.foo[3]+=other_global;"
        git parseTopStmt "global let compound, spaces" "let some_global . foo [\n 3 ] += other_global ;"
        git parseTopStmt "global def variable"         "def some_global=2// +4-5;"
        git parseTopStmt "global def variable, spaces" "def some_global = 2 // + 4 - 5 ;"
        gitfail parseTopStmt "global def compound"         "def some_global.foo[3]+=other_global;"
        gitfail parseTopStmt "global def compound, spaces" "def some_global . foo [\n 3 ] += other_global ;"
        git parseTopStmt "func top level" "func foo(a:Int,b:[Int])->Int{\n  return a*b[3];\n}"
        git parseTopStmt
            "func top level, spaces"
            "func foo ( a : Int , b : [ Int ] ) -> Int { \n  return a * b [ 3 ] \n}"
        git parseTopStmt
            "func top level, spaces, newlines"
            "func foo ( a : Int ,\n b : [ \n Int ] ) -> Int { \n  return a * b [ \n 3 ] \n}"
        git parseTopStmt "type stmt"             "type Foo=[Integer]"
        git parseTopStmt "type stmt with params" "type Foo[A,B]={A:B}"
    gdescribe "Tenet.Parsing.assignments" $ do
        git parseStatement "assign an empty list"                 "some_name = []\n"
        git parseStatement "assign to an attribute"               "some_name.some_attr = []\n"
        git parseStatement "assign to an index"                   "some_name[0] = 5\n"
        git parseStatement "assign an enum"                       "some_name = #tag_name\n"
        git parseStatement "assign a map with enum key"           "some_name = {#tag_name:0}\n"
        git parseStatement "assign a partial application applied" "some_name = foobar(...)()\n"
        git parseStatement "func no params"                       "func test_name(){\n    pass\n}"
        git parseStatement "func one param"                       "func test_name(var_name){\n    pass\n}"
        git parseStatement "func two params"                      "func test_name(left, right){\n    pass\n}"
