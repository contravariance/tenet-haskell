{-# LANGUAGE OverloadedStrings #-}

module Tenet.DeimperativeSpec
    ( spec
    )
where

import           Control.Monad                  ( forM_
                                                , (>=>)
                                                )
import           Data.Text                      ( Text
                                                , unpack
                                                )
import           TextShow                       ( showt )
import           System.FilePath                ( takeDirectory
                                                , (</>)
                                                )
import           Tenet.Ast                      ( DocN )
import           Tenet.Deimperative             ( runTransforms
                                                , stagePairs
                                                )
import           Tenet.Show                     ( showAst
                                                , stdLayout
                                                )
import           Tenet.State                    ( NameState )
import           Test.Golden                    ( exampleFiles
                                                , gtext
                                                )
import           Test.Hspec                     ( Spec
                                                , describe
                                                , runIO
                                                )

type TransStep = (Text, DocN -> NameState DocN)

extraPair :: [TransStep]
extraPair = ("identity", return) : stagePairs

stack :: [TransStep]
stack = [ (showt idx <> "-" <> name, func) | (idx, (name, func)) <- zip ([0 ..] :: [Integer]) extraPair ]

stack' :: [TransStep]
stack' = scanl1 step stack
  where
    step :: TransStep -> TransStep -> TransStep
    step (_, priorAct) (suffix, act) = (suffix, priorAct >=> act)

gast :: FilePath -> FilePath -> Spec
gast srcFile tgtDir = forM_ stack'
    $ \(suffix, action) -> gtext srcFile tgtDir "tenet" (unpack suffix) $ showAst stdLayout . runTransforms action

spec :: Spec
spec = describe "Tenet.Deimperative.steps" $ do
    examples <- runIO exampleFiles
    forM_ examples $ \(rpath, spath) -> gast spath ("Tenet.Deimperative" </> takeDirectory rpath)
