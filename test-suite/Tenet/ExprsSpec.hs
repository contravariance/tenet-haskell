module Tenet.ExprsSpec
    ( spec
    )
where

import           Test.Golden                    ( exampleFiles
                                                , gtext
                                                )
import           Test.Hspec                     ( Spec
                                                , describe
                                                , runIO
                                                )

import           Control.Monad                  ( forM_
                                                , (>=>)
                                                )
import           System.FilePath                ( takeDirectory
                                                , (</>)
                                                )
import           Tenet.Exprs                    ( ModuleN )
import qualified Tenet.Exprs.Steps             as E
import           Tenet.Show                     ( showSExpr
                                                , stdLayout
                                                )
import           Tenet.State                    ( NameState )

type TransStep = (String, ModuleN -> NameState ModuleN)

stack :: [TransStep]
stack = [("0-convert", return), ("1-let-collapse", return . E.lcMod), ("2-lambda-lift", E.llMod)]

stack' :: [TransStep]
stack' = scanl1 step stack
  where
    step :: TransStep -> TransStep -> TransStep
    step (_, priorAct) (suffix, act) = (suffix, priorAct >=> act)

gsexpr :: FilePath -> FilePath -> Spec
gsexpr srcFile tgtDir = forM_ stack' makeTest
  where
    makeTest :: TransStep -> Spec
    makeTest (suffix, action) = gtext srcFile tgtDir "sexpr" suffix $ showSExpr stdLayout . E.runTransforms action

spec :: Spec
spec = describe "Tenet.Exprs.steps" $ do
    examples <- runIO exampleFiles
    forM_ examples $ \(rpath, spath) -> gsexpr spath ("Tenet.Exprs" </> takeDirectory rpath)
