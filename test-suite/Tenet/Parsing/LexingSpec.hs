{-# LANGUAGE OverloadedStrings #-}

module Tenet.Parsing.LexingSpec
    ( spec
    )
where

import           Test.Golden                    ( gdescribe
                                                , gitMp
                                                , gitfailMp
                                                )
import           Test.Hspec                     ( Spec )

import qualified Tenet.Parsing.Lexing          as L

spec :: Spec
spec = do
    gdescribe "Tenet.Parsing.Lexing.identifier" $ do
        gitMp L.identifier "bareword1" "foobar1234  "
        gitMp L.identifier "bareword2" "bareword  "
        gitfailMp L.identifier "bareword keyword 1 fail" "if  "
        gitfailMp L.identifier "bareword keyword 2 fail" "return  "
        gitMp L.identifier "quoted"              "`foo bar 1234`  "
        gitMp L.identifier "quoted just a space" "` `  "
        gitfailMp L.identifier "quoted empty" "``  "
        gitMp L.identifier "regular"  "`foo bar 1234`  "
        gitMp L.identifier "escapes1" "`foo\\nbar`  "
        gitMp L.identifier "escapes2" "`foo\\tbar`  "
        gitMp L.identifier "escapes3" "`foo\\\\bar`  "
        gitMp L.identifier "quotes"   "`foo\"bar`  "
        gitMp L.identifier "escapes4" "`foo\\\"bar`  "
    gdescribe "Tenet.Parsing.Lexing.quotedString" $ do
        gitMp L.quotedString "regular"  "\"foobar\"  "
        gitMp L.quotedString "escapes1" "\"foo\\nbar\"  "
        gitMp L.quotedString "escapes2" "\"foo\\tbar\"  "
        gitMp L.quotedString "escapes3" "\"foo\\\\bar\"  "
        gitMp L.quotedString "escapes4" "\"foo\\\"bar\"  "
    gdescribe "Tenet.Parsing.Lexing.decimal" $ do
        gitMp L.decimal "regular" "12345  "
        gitfailMp L.decimal "negative" "-12345  " -- Signs are not handled by decimal.
        gitfailMp L.decimal "positive" "+12345  "
