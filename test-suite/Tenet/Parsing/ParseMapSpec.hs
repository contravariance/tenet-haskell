module Tenet.Parsing.ParseMapSpec
    ( spec
    )
where

import           Debug.Trace                    ( trace )
import           Test.Hspec
import           Test.QuickCheck

import           Tenet.Parsing.Common           ( Parser )
import qualified Tenet.Parsing.ParseMap        as Pm

import           Data.List                      ( isPrefixOf
                                                , maximumBy
                                                )
import           Data.Maybe                     ( isJust
                                                , isNothing
                                                )
import           Data.Ord                       ( comparing )
import qualified Data.Text                     as T
import           Text.Megaparsec                ( parseMaybe
                                                , takeRest
                                                )

shouldMatch :: [String] -> String -> Maybe String
shouldMatch positives text =
    let hits = [ cand | cand <- positives, cand `isPrefixOf` text ]
    in  if null hits then Nothing else Just $ maximumBy (comparing length) hits

doesMatch :: [String] -> String -> Maybe String
doesMatch positives text =
    let list :: [(T.Text, String)]
        list = zip (map T.pack positives) positives
        parser :: Parser String
        parser = Pm.parseToken (Pm.fromList list) <* takeRest
        txt    = T.pack text
    in  parseMaybe parser txt

checkMatch :: [String] -> String -> Bool
checkMatch positives text =
    let sm  = shouldMatch positives text
        dm  = doesMatch positives text
        shw = show sm ++ " == " ++ show dm
    in  sm == dm || trace shw False

newtype NameList =
  NameList [String]
  deriving (Show)

newtype Name =
  Name String
  deriving (Show)

instance Arbitrary NameList where
    arbitrary = NameList <$> (listOf1 . listOf1 . elements $ "ab_")

instance Arbitrary Name where
    arbitrary = Name <$> (listOf1 . elements $ "ab_")

namesMatch :: NameList -> Name -> Bool
namesMatch positives candidate =
    let NameList pos = positives
        Name     cnd = candidate
    in  checkMatch pos cnd

posMatch :: NameList -> Bool
posMatch positives =
    let NameList pos = positives in (isJust . shouldMatch pos) (head pos) && checkMatch pos (head pos)

posMatchExtra :: NameList -> Bool
posMatchExtra positives =
    let NameList pos = positives
    in  (isJust . shouldMatch pos) (head pos ++ "extra") && checkMatch pos (head pos ++ "extra")

negMatch :: NameList -> Bool
negMatch positives = let NameList pos = positives in isNothing $ shouldMatch pos "nope"

negMatchFollowed :: NameList -> Bool
negMatchFollowed positives = let NameList pos = positives in isNothing . shouldMatch pos $ "nope" ++ head pos

spec :: Spec
spec = describe "Tenet.Parsing.ParseMap" $ do
    it "parser matches tokens" $ property namesMatch
    it "parser matches known good token" $ property posMatch
    it "parser matches known good token and junk" $ property posMatchExtra
    it "parser fails known invalid token" $ property negMatch
    it "parser fails known invalid token followed by known good" $ property negMatchFollowed
