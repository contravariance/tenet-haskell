{-# LANGUAGE OverloadedStrings #-}

module Tenet.Parsing.EscapesSpec
    ( spec
    )
where

import           Test.Hspec                     ( Spec
                                                , describe
                                                , it
                                                , shouldBe
                                                )

-- import           Tenet.Parsing.Common (Spc (..))
import           Tenet.Parsing.Escapes

spec :: Spec
spec = do
    describe "showCharLiteral" $ do
        it "quote1" $ showCharLiteral '"' '"' `shouldBe` "\\\""
        it "quote2" $ showCharLiteral '`' '`' `shouldBe` "\\`"
        it "quote3" $ showCharLiteral '"' '`' `shouldBe` "`"
        it "quote4" $ showCharLiteral '`' '"' `shouldBe` "\""
        it "regular1" $ showCharLiteral '"' 'a' `shouldBe` "a"
        it "regular2" $ showCharLiteral '"' 'z' `shouldBe` "z"
        it "regular3" $ showCharLiteral '"' '@' `shouldBe` "@"
        it "escape1" $ showCharLiteral '"' '\a' `shouldBe` "\\a"
        it "escape2" $ showCharLiteral '"' '\n' `shouldBe` "\\n"
        it "escape3" $ showCharLiteral '"' '\t' `shouldBe` "\\t"
        it "hex1" $ showCharLiteral '"' (toEnum 0x7f) `shouldBe` "\\x7f"
        it "hex2" $ showCharLiteral '"' (toEnum 0x401) `shouldBe` "\\u0401"
        it "hex3" $ showCharLiteral '"' (toEnum 0x10001) `shouldBe` "\\U00010001"
    describe "showStringLiteral" $ do
        it "typical" $ showStringLiteral '`' "typical" `shouldBe` "`typical`"
        it "spaces and such" $ showStringLiteral '`' "spaces and such" `shouldBe` "`spaces and such`"
        it "random crap"
            $          showStringLiteral '`' "'quotes'\n\"dquote\"\t`grave`null\0"
            `shouldBe` "`'quotes'\\n\"dquote\"\\t\\`grave\\`null\\x00`"
    describe "showIdentifier" $ do
        it "empty" $ showIdentifier "" `shouldBe` "``"
        it "keyword1" $ showIdentifier "type" `shouldBe` "`type`"
        it "keyword2" $ showIdentifier "typePrefix" `shouldBe` "typePrefix"
        it "keyword3" $ showIdentifier "and" `shouldBe` "`and`"
        it "numeric tail" $ showIdentifier "bareword01234" `shouldBe` "bareword01234"
