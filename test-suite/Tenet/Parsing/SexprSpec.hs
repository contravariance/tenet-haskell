{-# LANGUAGE OverloadedStrings #-}

module Tenet.Parsing.SexprSpec
    ( spec
    )
where

import           Test.Golden                    ( gdescribe
                                                , gitMp
                                                )
import           Test.Hspec                     ( Spec )

import qualified Tenet.Exprs.Parsing           as E

spec :: Spec
spec = do
    gdescribe "Tenet.Exprs.Parsing.import" $ do
        gitMp E.importStmt "import value" "import mod_name.mod_name (thing as this_thing);"
        gitMp E.importStmt "import Type"  "import mod_name.mod_name (Some_Type as This_Type);"
    gdescribe "Tenet.Exprs.Parsing.typeDef" $ do
        gitMp E.typeDef "type int"   "type MyInt := !Int;"
        gitMp E.typeDef "type union" "type DateTime := [!Union epoch_ce:!Int epoch_posix:!Int iso8601:Iso8601Simple];"
        gitMp
            E.typeDef
            "type enum"
            "type DayOfWeek := [!Union fri:[!Record] mon:[!Record] sat:[!Record] sun:[!Record] thu:[!Record] tue:[!Record] wed:[!Record]];"
    gdescribe "Tenet.Exprs.Parsing.def" $ do
        gitMp E.sExprDef "def func bang types"   "def my_func := {def a:[!List !Int] b:String ::Value 55}"
        gitMp E.sExprDef "def func simple types" "def my_func := {def a:Foo b:Bar ::Value 55}"
    gdescribe "Tenet.Exprs.Parsing.type" $ do
        gitMp E.typeParse "bang int"      "!Int"
        gitMp E.typeParse "bang bool"     "!Bool"
        gitMp E.typeParse "bang List Foo" "[!List Foo]"
        gitMp E.typeParse "Foo Bar"       "[Foo Dat:Bar]"
