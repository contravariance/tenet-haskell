{-# LANGUAGE OverloadedStrings #-}

module Tenet.Parsing.ExprSpec
    ( spec
    )
where

import           Test.Hspec                     ( Spec )

import           Tenet.Parsing                  ( parseExpr )
import           Test.Golden                    ( gdescribe
                                                , parseTest
                                                , PassFail(..)
                                                )

spec :: Spec
spec = do
    let pt      = parseTest parseExpr
    let git     = pt Pass
    let gitfail = pt Fail
    gdescribe "Tenet.Parsing.Expr.atoms" $ do
        git "opAtom1"        "-5-3"
        git "opAtom1s"       "- 5 "
        git "opAtom1s again" "- - 5 "
        git "nameAtom1"      "foobar1234  "
        git "integerLit1"    "1234  "
        git "stringLit1"     "\"foobar12345\"  "
    gdescribe "Tenet.Parsing.Expr.records" $ do
        git "recordCon0"    "()  "
        git "recordCon0s"   "( )  "
        git "recordCon1"    "(foo:123)  "
        git "recordCon2"    "(foo:123,)  "
        git "recordCon3"    "( foo : 123 , ) "
        git "recordConDbl1" "(foo:)  "
        git "recordConDbl2" "(foo:,)  "
        git "recordConDbl3" "( foo : , ) "
    gdescribe "Tenet.Parsing.Expr.parens" $ do
        git "parens0 fail"             "()  "
        git "enum in parens"           "(#tag )"
        git "enum literally in parens" "(tag ~ ( ))"
        git "opAtom1"                  "(-5-3)"
        git "opAtom1s"                 "(- 5 )"
        git "opAtom1s again"           "(- - 5 )"
        git "nameAtom1"                "(foobar1234  )"
        git "integerLit1"              "(1234  )"
        git "stringLit1"               "(\"foobar12345\"  )"
    gdescribe "Tenet.Parsing.Expr.tagOps" $ do
        git "enum value"              "# tag"
        git "enum literally"          "tag ~ ()"
        git "tag enum"                "tag1 ~ # tag2"
        git "tag enum literally"      "tag1 ~ tag2 ~ ()"
        git "tag number"              "tag ~ 55"
        git "tag negative number1"    "tag ~ (-55)"
        git "tag negative number2"    "tag ~ -55"
        git "tag tag number"          "tag1 ~ tag2 ~ 55"
        git "tag tag negative number" "tag1 ~ tag2 ~ -55"
    gdescribe "Tenet.Parsing.Expr.apply" $ do
        git "parens empty apply"                      "foo()  "
        git "parens empty apply apply"                "foo()()  "
        git "parens empty apply spaces"               "foo ( )  "
        git "parens single element"                   "(1234)"
        git "parens single element spaces"            "( 1234 ) "
        git "parens apply single"                     "foo(1234)"
        git "parens apply single spaces"              "foo ( 1234 ) "
        git "parens apply single apply empty"         "foo(1234)()"
        git "parens apply single apply empty spaces"  "foo ( 1234 ) ( ) "
        git "parens apply single apply single"        "foo(1234)(1234)"
        git "parens apply single apply single spaces" "foo ( 1234 ) ( 1234 ) "
        git "parens guarded expression"               "(1234|999)"
        git "parens guarded expression spaces"        "( 1234 | 999 ) "
        gitfail "parens fail two comma separated values" "(1234,5678)"
        git "parens2 apply" "foo(1234,5678)"
        git "parens2s"      "foo ( 1234 , 5678 ) "
        git "parensN1"      "foo(foo:123)"
        git "parensN1s"     "foo ( foo : 123 ) "
        git "parensN2"      "foo(foo:123,bar:\"qux\")"
        git "parensN2s"     "foo ( foo : 123 , bar : \"qux\" ) "
        git "parensN2 dc"   "foo(foo:,bar:\"qux\")"
        git "parensN2s dc"  "foo ( foo : 123 , bar : ) "
        git "parensP0"      "foo(...)"
        git "parensP0s"     "foo ( ... ) "
        git "parensP1"      "foo(foo:123,...)"
        git "parensP1s"     "foo ( foo : 123 , ... ) "
        git "parensP2"      "foo(foo:123,bar:\"qux\"...)"
        git "parensP2s"     "foo ( foo : 123 , bar : \"qux\" ... ) "
        git "parensP2 dc"   "foo(foo:,bar:\"qux\",...)"
        git "parensP2s dc"  "foo ( foo : 123 , bar : , ... ) "
    gdescribe "Tenet.Parsing.Expr.brackets" $ do
        git "brackets0"   "[]  "
        git "brackets0s"  "[ ]  "
        git "brackets1"   "[123]  "
        git "brackets1c"  "[123,]  "
        git "brackets1cs" "[ 123 , ] "
        git "brackets2"   "[123,456]"
        git "brackets2c"  "[123,456,]"
        git "brackets2cs" "[ 123 , 456 , ] "
    gdescribe "Tenet.Parsing.Expr.maps" $ do
        git "bracketsColon0"   "{:}  "
        git "bracketsColon0s"  "{ : }  "
        git "bracketsColon1"   "{123:456}  "
        git "bracketsColon1c"  "{123:456,}  "
        git "bracketsColon1cs" "{ 123 : 456 , } "
        git "bracketsColon2"   "{123:456,456:789}"
        git "bracketsColon2c"  "{123:456,456:789,}"
        git "bracketsColon2cs" "{ 123 : 456 , 456 : 789 } "
    gdescribe "Tenet.Parsing.Expr.accessors" $ do
        git "slot lookup three"    "name.foo.bar.qux"
        git "tag lookup three"     "name?foo?bar?qux"
        git "index lookup three"   "name[foo][bar][qux]"
        git "slot and tag lookup"  "name.foo.bar?bar?qux"
        git "tag and index lookup" "name?foo[bar + 1]"
    gdescribe "Tenet.Parsing.Expr.guard" $ do
        git "exprSeries1"  "1+2*\"string value\"|name1234(foo:1,bar:2)"
        git "exprSeries1s" "1 + 2 * \"string value\" | name1234 ( foo : 1 , bar : 2 ) "
        git "exprSeries2"  "(1+2*\"string value\"|name1234(foo:1,bar:2))"
    gdescribe "Tenet.Parsing.Expr.lazy.untyped" $ do
        git "lazy0"       "#(a+b*c)"
        git "lazy3s"      "#( a + b * c )"
        git "lazyApply1"  "#(a+b*c)(a:7,b:8,c:9)"
        git "lazyApply1s" "#( a + b * c ) ( a : 7 , b : 8 , c : 9 ) "
    gdescribe "Tenet.Parsing.Expr.lambdas.typed" $ do
        git "lambda1 typed"      "func(a:Int){return a+b*c}"
        git "lambda3 typed"      "func(a:Int,b:Int,c:Int){return a+b*c}"
        git "lambda3c typed"     "func(a:Int,b:Int,c:Int,){return a+b*c}"
        git "lambda3s typed"     "func( a : Int , b : Int , c : Int ) {return  a + b * c }"
        git "lambdaApply1 typed" "(func(a:Int,b:Int,c:Int){return a+b*c})(a:7,b:8,c:9)"
        git "lambdaApply1s typed"
            "(func ( a : Int , b : Int , c : Int ) { return a + b * c ; } ) ( a : 7 , b : 8 , c : 9 ) "
        git "lambda1 untyped"      "func(a){return a+b*c}"
        git "lambda3 untyped"      "func(a,b,c){return a+b*c}"
        git "lambda3c untyped"     "func(a,b,c,){return a+b*c}"
        git "lambda3s untyped"     "func ( a , b  , c  ) {return  a + b * c }"
        git "lambdaApply1 untyped" "(func (a,b,c){return a+b*c})(a:7,b:8,c:9)"
        gitfail "lambda1 typed return"   "func(a:Int) -> Int {return a+b*c}"
        gitfail "lambda1 untyped return" "func(a) -> Int {return a+b*c}"
    gdescribe "Tenet.Parsing.Expr.specialFunc" $ do
        git "set_slot"    "!!set_slot(slot, (slot:5), 7)"
        git "set_index"   "!!set_index(3, [4, 5, 6, 7, 8], 10)"
        git "set_variant" "!!set_variant(tag, tag~88, 99)"
        git "for_loop"    "!!for_loop(temp_name, [2, 3, 4, 5], (a:, b:, c:))"
