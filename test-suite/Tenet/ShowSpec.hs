module Tenet.ShowSpec
    ( spec
    )
where

import           Prelude                 hiding ( readFile )
import           Test.Golden                    ( resultFiles )
import           Test.Hspec                     ( Spec
                                                , describe
                                                , it
                                                , runIO
                                                , shouldBe
                                                )

import           Tenet.Ast                      ( DocN
                                                , nullNode
                                                )
import           Tenet.State.Out                ( errorOut
                                                , mapLeft
                                                )
import           Tenet.Exprs                    ( ModuleN )
import           Tenet.Exprs.Parsing            ( moduleParse )
import qualified Tenet.Parsing                 as P
import qualified Tenet.Parsing.Common          as C
import           Tenet.Show                     ( showAst
                                                , showSExpr
                                                , stdLayout
                                                )

import           Control.Monad                  ( forM_ )
import           Data.Text                      ( Text )
import           Data.Text.IO                   ( readFile )

astParse :: FilePath -> Text -> DocN
astParse path contents = nullNode <$ errorOut (P.parseDoc path contents)

sexprParse :: FilePath -> Text -> ModuleN
sexprParse path contents = errorOut $ mapLeft (C.parseErrorPretty contents) $ C.parse moduleParse path contents

ps :: String -> String
ps name = name -- "parse(show(" ++ name ++ ")"

astPspRoundtrip :: FilePath -> Text -> Spec
astPspRoundtrip path contents = it path $ do
    let doc    = astParse path contents
    let reshow = showAst stdLayout doc
    let doc'   = astParse (ps path) reshow
    doc `shouldBe` doc'

sexprPspRoundtrip :: FilePath -> Text -> Spec
sexprPspRoundtrip path contents = it path $ do
    let modu   = sexprParse path contents
    let reshow = showSExpr stdLayout modu
    let modu'  = sexprParse (ps path) reshow
    modu `shouldBe` modu'

spec :: Spec
spec = do
    describe "AST parse-show-parse" $ do
        astFiles <- runIO $ resultFiles ".tenet"
        forM_ astFiles $ \(rpath, spath) -> do
            contents <- runIO $ readFile spath
            astPspRoundtrip rpath contents
    describe "SExpr parse-show-parse" $ do
        astFiles <- runIO $ resultFiles ".sexpr"
        forM_ astFiles $ \(rpath, spath) -> do
            contents <- runIO $ readFile spath
            sexprPspRoundtrip rpath contents
