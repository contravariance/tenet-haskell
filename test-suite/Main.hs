-- Hspec <https://hspec.github.io>.
import           Spec                           ( spec )
import           Test.Hspec.Formatters.Jenkins  ( XmlRef
                                                , xmlFormatter
                                                , xmlRef
                                                )
import           Test.Hspec.Runner

import           Control.Monad                  ( unless )
import           System.Environment             ( getArgs
                                                , withArgs
                                                )
import           System.Exit                    ( exitFailure )

main :: IO ()
main = do
    ref     <- xmlRef
    summary <- interceptArgs (hspecConfig ref defaultConfig) (`hspecWithResult` spec)
    unless (summaryFailures summary == 0) exitFailure

interceptArgs :: ([String] -> (a, [String])) -> (a -> IO b) -> IO b
interceptArgs modArgs monad = do
    args <- getArgs
    let (config, args') = modArgs args
    withArgs args' (monad config)

hspecConfig :: XmlRef -> Config -> [String] -> (Config, [String])
hspecConfig ref config args@(first : rest)
    | first == "-x" || first == "--xml" = hspecConfig ref config { configFormatter = Just $ xmlFormatter ref } rest
    | (not . null) rest && (first == "-o" || first == "--output") = hspecConfig
        ref
        config { configOutputFile = Right $ head rest }
        (tail rest)
    | otherwise = (config, args)
hspecConfig _ config args = (config, args)
