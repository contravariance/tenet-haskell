{-# LANGUAGE OverloadedStrings #-}

-- | The unified tree adapts the monads that work on different structures into a single unified form for testing and introspection.
module Tenet.Steps
    ( scanKleisi
    , showUnified
    , unifyParseAst
    , unifyParseMod
    , unifyDocXform
    , unifyModXform
    , unifyDocToMod
    , allSteps
    , parseStep
    , Unified(..)
    )
where

import           Data.Text                      ( Text )
import qualified HTMLEntities.Text             as HE

import           Tenet.Ast                      ( DocN
                                                , nullNode
                                                )
import qualified Tenet.Deimperative            as D
import           Tenet.Exprs                    ( ModuleN )
import qualified Tenet.Exprs.Steps             as E
import           Tenet.Parsing                  ( parseDoc )
import           Tenet.Show                     ( Encoding(..)
                                                , Format(..)
                                                , showAstFmt
                                                , showSExprFmt
                                                )
import           Tenet.State                    ( NameState
                                                , errorOut
                                                )

-- | Run the happy parser, failing with an error message
parseStep :: FilePath -> Text -> NameState DocN
parseStep path contents = pure $ fmap (const nullNode) $ errorOut $ parseDoc path contents

-- | Wraps all available steps as a list of unified steps.
allSteps :: [(Text, Unified -> NameState Unified)]
allSteps =
    [ ("Parse"           , unifyParseAst parseStep)
    , ("GatherNames"     , unifyDocXform D.gatherAstStep)
    , ("EscapeReturns"   , unifyDocXform D.escapeReturns)
    , ("AccumulateLoops" , unifyDocXform D.accumulateLoops)
    , ("GuardStmts"      , unifyDocXform D.guardStmts)
    , ("ExtendIfs"       , unifyDocXform D.extendIfs)
    , ("ExciseContinues" , unifyDocXform D.exciseContinues)
    , ("SimplifyPatterns", unifyDocXform D.simplifyPatterns)
    , ("UnbindPatterns"  , unifyDocXform D.unbindPatterns)
    , ("Delens"          , unifyDocXform D.delens)
    , ("UpdateImports"   , unifyDocXform D.updateImports)
    , ("SsaForm"         , unifyDocXform D.ssaForm)
    , ("GuardExprs"      , unifyDocXform D.guardExprs)
    , ("ConvertSExpr"    , unifyDocToMod $ return . E.exDoc)
    , ("GatherGlobals"   , unifyModXform E.gatherSExprStep)
    , ("LetCollapse"     , unifyModXform $ return . E.lcMod)
    , ("LambdaLift"      , unifyModXform E.llMod)
    ]

-- | Wrap three kinds of data in a unified form: flat text, the 'Tenet.Ast.Doc' and the symbolic expression 'Tenet.Exprs.Mod'.
data Unified
  = UFlat FilePath
          Text
  | UDoc DocN
  | UMod ModuleN
  deriving (Show)

fromFlat :: Monad m => (FilePath -> Text -> m a) -> (Unified -> m a)
fromFlat monad = unified
  where
    unified (UFlat path contents) = monad path contents
    unified val                   = return . error $ "Expected flat input to this step, not " ++ what val

fromDoc :: Monad m => (DocN -> m a) -> (Unified -> m a)
fromDoc monad = unified
  where
    unified (UDoc doc) = monad doc
    unified val        = return . error $ "Expected Doc input to this step, not " ++ what val

fromMod :: Monad m => (ModuleN -> m a) -> (Unified -> m a)
fromMod monad = unified
  where
    unified (UMod md) = monad md
    unified val       = return . error $ "Expected Module input to this step, not " ++ what val

what :: Unified -> String
what (UFlat _ _) = "flat"
what (UDoc _   ) = "Doc"
what (UMod _   ) = "Module"

toDoc :: Monad m => (Unified -> m DocN) -> (Unified -> m Unified)
toDoc monad = unified where unified uni = UDoc <$> monad uni

toMod :: Monad m => (Unified -> m ModuleN) -> (Unified -> m Unified)
toMod monad = unified where unified uni = UMod <$> monad uni

-- | Wrap a 'UFlat' to 'Doc' step as a unified step.
unifyParseAst :: Monad m => (FilePath -> Text -> m DocN) -> (Unified -> m Unified)
unifyParseAst monad = toDoc $ fromFlat monad

-- | Wrap a 'UFlat' to 'Module' step as a unified step.
unifyParseMod :: Monad m => (FilePath -> Text -> m ModuleN) -> (Unified -> m Unified)
unifyParseMod monad = toMod $ fromFlat monad

-- | Wrap a 'Doc' to 'Doc' step as a unified step.
unifyDocXform :: Monad m => (DocN -> m DocN) -> (Unified -> m Unified)
unifyDocXform monad = toDoc $ fromDoc monad

-- | Wrap a 'Module' to 'Module' step as a unified step.
unifyModXform :: Monad m => (ModuleN -> m ModuleN) -> (Unified -> m Unified)
unifyModXform monad = toMod $ fromMod monad

-- | Wrap a 'Doc' to 'Module' step as a unified step.
unifyDocToMod :: Monad m => (DocN -> m ModuleN) -> (Unified -> m Unified)
unifyDocToMod monad = toMod $ fromDoc monad

-- | Show a unified step using the given output 'Format'.
showUnified :: Format -> Unified -> Text
showUnified Format { encoding = Html } (UFlat _ contents) = HE.text contents
showUnified _                          (UFlat _ contents) = contents
showUnified fmt                        (UDoc doc        ) = showAstFmt fmt doc
showUnified fmt                        (UMod md         ) = showSExprFmt fmt md

-- | Given a list of pairs of monadic functions of the form @(n, a -> m a)@ this will generate a scan by (effectively)
-- repeatedly applying the Kleisi arrow 'Control.Monad.>=>' and saving intermediate results. The first part of the tuple may be a
-- name or other data to associate with the results.
scanKleisi :: Monad m => [(n, a -> m a)] -> (a -> m [(n, a)])
scanKleisi []             = error "scanKleisi: steps must not be empty"
scanKleisi [(name, step)] = \src -> do
    val <- step src
    return [(name, val)]
scanKleisi ((name, step) : steps) = \src -> do
    value     <- step src
    remainder <- scanKleisi steps value
    return $ (name, value) : remainder
