{-# LANGUAGE DeriveDataTypeable, DeriveFunctor #-}

-- | The symbolic expression internal language tree drops most of the complexity of the Tenet language.
-- A symbolic expression is simple to evaluate by having no control statements.
module Tenet.Exprs
    ( SExpr(..)
    , SExprN
    , Type(..)
    , TypeN
    , Module(..)
    , ModuleN
    , Generic(..)
    , GenericN
    , Imp(..)
    , ImpN
    , CaseArgs(..)
    , CaseArgsN
    , Args
    , ArgsN
    , Sig
    , SigN
    , MapPair
    , MapPairN
    , caseElems
    , failPrim
    )
where

import           Data.Generics                  ( Data
                                                , Typeable
                                                )
import           Data.Map.Strict                ( Map
                                                , elems
                                                )
import           Data.Text                      ( Text )
import           Tenet.Ast.Node                 ( Node )
import           Tenet.Primitives               ( Primitive(SpecialPrim)
                                                , SpecialFunc(Failure)
                                                )

-- | A map of function argument names to expressions used in function application.
type Args n = Map Text (SExpr n)
type ArgsN = Args Node

-- | A map of function arguments to type expressions used in a function definition.
type Sig n = Map Text (Type n)
type SigN = Sig Node

-- | Type alias to represent a key-value pair within a map expression.
type MapPair n = (SExpr n, SExpr n)
type MapPairN = MapPair Node

-- | Type to handle different types of case statements.
data CaseArgs n
  = CTags (Args n)
  | CStrings (Args n)
  | CIntegers (Map Integer (SExpr n))
  deriving (Eq, Read, Show, Data, Typeable, Functor)
type CaseArgsN = CaseArgs Node

-- | Data type for non-type information within the s-expr language.
data SExpr n
  = SApp { aNode :: n
         , aHead :: SExpr n
         , aArgs :: Args n }
  | SCase { cNode :: n
          , cHead :: SExpr n
          , cArgs :: CaseArgs n
          , cWild :: Maybe (SExpr n) }
  | SFunc { fNode :: n
          , fSig  :: Sig n
          , fRet  :: Type n
          , fBody :: SExpr n }
  | SBool { lbNode :: n
          , lbVal  :: Bool }
  | SInt { liNode :: n
         , liVal  :: Integer }
  | SLet { sNode :: n
         , sBind :: Args n
         , sExpr :: SExpr n }
  | SList { clNode  :: n
          , clElems :: [SExpr n] }
  | SMap { cmNode  :: n
         , cmPairs :: [MapPair n] }
  | SName { nNode :: n
          , nName :: Text }
  | SPrim { prNode   :: n
          , prHead   :: Primitive
          , prParams :: [Text]
          , prArgs   :: [SExpr n] }
  | SPart { pNode :: n
          , pHead :: SExpr n
          , pArgs :: Args n }
  | SSet { ctNode  :: n
         , ctElems :: [SExpr n] }
  | SStr { lsNode :: n
         , lsVal  :: Text }
  | STag { uNode :: n
         , uTag  :: Text
         , uVar  :: SExpr n }
  | STup { tNode :: n
         , tArgs :: Args n }
  deriving (Eq, Read, Show, Data, Typeable, Functor)
type SExprN = SExpr Node

-- | Extract the elements of the case args.
caseElems :: CaseArgs n -> [SExpr n]
caseElems (CTags     args) = elems args
caseElems (CStrings  args) = elems args
caseElems (CIntegers args) = elems args

-- | A type expression that may be used in a type alias definition or a function signature.
data Type n
  = TBottom n
  | TUnspecified n
  | TName { tnNode   :: n
          , tnName   :: Text
          , tnParams :: Sig n }
  | TFunction { tfNode :: n
              , tfArgs :: Sig n
              , tfRet  :: Type n }
  | TBool n
  | TInt n
  | TStr n
  | TList { tiNode :: n
          , tiElem :: Type n }
  | TMap { tmNode :: n
         , tmKey  :: Type n
         , tmVal  :: Type n }
  | TSet { tsNode :: n
         , tsElem :: Type n }
  | TRecord { ttNode :: n
           , ttArgs :: Sig n }
  | TUnion { tuNode :: n
           , tuVar  :: Sig n }
  deriving (Eq, Read, Show, Data, Typeable, Functor)
type TypeN = Type Node

{- If I do this, there might be TExpr's...
  | TListItem { tliNode :: n, tliBase :: Type }
  | TSetItem { tsiNode :: n, tsiBase :: Type }
  | TMapKey { tmkNode :: n, tmkBase :: Type }
  | TMapValue { tmvNode :: n, tmvBase :: Type }
  | TRecordRemove { ttrNode :: n, ttrBase :: Type, ttrMinus :: Type }
  | TRecordTimes { tttNode :: n, tttLeft :: Type, tttRight :: Type }
 -}
-- | A type expression may include type parameters if it is generic.
data Generic n = Generic
  { gNode   :: n
  , gParams :: [Text]
  , gType   :: Type n
  } deriving (Eq, Read, Show, Data, Typeable, Functor)
type GenericN = Generic Node

-- | An import tracks the source module and name.
data Imp n = Imp
  { iNode    :: n
  , iModule  :: [Text]
  , iSrcName :: Text
  } deriving (Eq, Read, Show, Data, Typeable, Functor)
type ImpN = Imp Node

-- | A module includes a map of type names to types, names to expressions, and names to imports.
data Module n = Mod
  { mTypes :: Map Text (Generic n)
  , mDefs  :: Map Text (SExpr n)
  , mImps  :: Map Text (Imp n)
  } deriving (Eq, Read, Show, Data, Typeable, Functor)
type ModuleN = Module Node

-- | A constructor for a failure primitive.
failPrim :: n -> SExpr n
failPrim node = SPrim { prNode = node, prHead = SpecialPrim Failure, prArgs = [], prParams = [] }
