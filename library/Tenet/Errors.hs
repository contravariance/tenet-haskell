{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE UndecidableInstances #-}

-- | Track errors while compiling Tenet source.
module Tenet.Errors
    ( Severity(..)
    , Issue(..)
    , ErrorPart(..)
    , errorC
    , naError
    , neError
    , nrError
    , csfError
    , t
    , s
    , n
    , l
    )
where

import           Data.Text                      ( Text
                                                , intercalate
                                                , pack
                                                , unpack
                                                , unwords
                                                )
import           Prelude                 hiding ( unwords )
import           Tenet.Ast.Node                 ( Node
                                                , locate
                                                )
import           Tenet.Ast                      ( BuiltinError
                                                , ErrorName
                                                )

-- | Enumeration of the severities of an error.
data Severity
  = Warning -- ^ An issue that must be corrected but does not stop compilation.
  | Error -- ^ An issue that stops compilation.
  | Fatal -- ^ An issue that reflects a failure in the compiler.
  deriving (Show, Eq, Ord)

-- | Identify an issue and where in the source it exists.
data Issue
  = UndefinedName { unName :: Text
                  , unNode :: Node }
    -- ^ Name was not defined, flagged by SSA
  | CantReassign { diName  :: Text
                 , diNode1 :: Node
                 , diNode2 :: Node }
    -- ^ Name can't be reassigned, flagged by SSA
  | DuplicateArg { dpName :: Text
                   , dpNode :: Node }
    -- ^ Argument name was repeated in a function def
  | UnknownError { ukName :: Text
                 , ukNode :: Node }
    -- ^ Unknown error found in `catch` clause
  | MaskedError { meName :: ErrorName
                 , meNode1 :: Node
                 , meNode2 :: Node }
    -- ^ Prior catch clause masks this clause
  | UncaughtError { ucError :: BuiltinError
                  , ucNode :: Node }
    -- ^ An error wasn't caught
  deriving (Show)

-- | Our fancy error messages let you add parts with useful mechanics.
data ErrorPart
  = T Text -- ^ Ordinary text.
  | N Node -- ^ A node will indicate where it is in the source.
  deriving (Show)

unpart :: ErrorPart -> Text
unpart (T part) = part
unpart (N node) = locate node

-- | Construct a text part
t :: Text -> ErrorPart
t = T

-- | Construct a string part
s :: Show e => e -> ErrorPart
s = T . pack . show

-- | Construct a node part
n :: Node -> ErrorPart
n = N

-- | Construct a comma-separated list
l :: (a -> Text) -> [a] -> ErrorPart
l convert = T . intercalate ", " . map convert

-- | Concatenate an error message.
errorC :: [ErrorPart] -> v
errorC = error . unpack . unwords . map unpart

-- | A generic "control simplification failure" error message.
csfError :: [ErrorPart] -> v
csfError parts = errorC $ T "Control simplification failure:" : parts

-- | A CSF error indicating an element was not absorbed.
naError :: Text -> Node -> dummy
naError what node = csfError [T "did not absorb", T what, T "at", N node]

-- | A CSF error indicating a statement was not removed, e.g. a continue statement.
nrError :: Text -> Node -> dummy
nrError what node = csfError [T "did not remove", T what, T "statement at", N node]

-- | A CSF error indicating an expression was not removed, e.g. a guard expression.
neError :: Text -> Node -> dummy
neError what node = csfError [T "did not remove", T what, T "expression at", N node]
