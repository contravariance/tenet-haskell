{-# LANGUAGE NamedFieldPuns #-}

-- | A unified state monad to use between manipulations of the Tenet AST and s-expr language.
module Tenet.State
    ( Names
    , NameState
    , collectLifts
    , addLift
    , initial
    , putNames
    , setNames
    , putGlobals
    , setGlobals
    , getGlobals
    , reportIssues
    , runNameState
    , getBuiltin
    , getImports
    , lookupLexical
    , setLexical
    , newLexical
    , recallLexical
    , restoreLexical
    , saveLexical
    , makeTemp
    , Use(..)
    , Change
    , cval
    , actify
    , skipify
    , skipDef
    , foldOut
    , liftC
    , liftC2
    , liftC3
    , liftC4
    , liftCd
    , liftCd2
    , liftCd3
    , liftCd4
    , Out
    , OneAct(..)
    , mergeActions
    , rewriteActions
    , maybeAction
    , maybeActionM
    , tfail
    , tfail'
    , liftOut
    , errorOut
    )
where

import           Control.Monad.State.Strict     ( State
                                                , get
                                                , put
                                                , runState
                                                , state
                                                )
import           Data.Map.Strict                ( (!?) )
import qualified Data.Map.Strict               as M
import qualified Data.Set                      as S
import           Data.Text                      ( Text
                                                , append
                                                , cons
                                                , last
                                                )
import           Prelude                 hiding ( last )
import           Tenet.Ast.Node                 ( Node )
import           Tenet.Errors                   ( Issue(..) )
import           Tenet.Exprs                    ( SExprN )
import           Tenet.Parsing.Escapes          ( decimal )
import           Tenet.State.Names              ( findFreeName )
import           Tenet.State.Out                ( Out
                                                , tfail
                                                , tfail'
                                                , liftOut
                                                , errorOut
                                                )
import           Tenet.State.Actions            ( OneAct(..)
                                                , maybeAction
                                                , maybeActionM
                                                , mergeActions
                                                , rewriteActions
                                                )
import           Tenet.State.Change             ( Change
                                                , cval
                                                , actify
                                                , skipify
                                                , skipDef
                                                , foldOut
                                                , liftC
                                                , liftC2
                                                , liftC3
                                                , liftC4
                                                , liftCd
                                                , liftCd2
                                                , liftCd3
                                                , liftCd4
                                                )

-- | The Names object is the data type of the state monad.
data Names = Names
  { names     :: S.Set Text -- ^ All names, global, lexical, types
  , imports   :: M.Map Text Text -- ^ Map source (being imported) to target (correct name)
  , lexical   :: M.Map Text Counter -- ^ The lexical state used to determine SSA form.
  , lexCounts :: M.Map Text Int -- ^ A supplement to the lexical state used to determine SSA form.
  , lifts     :: [(Text, SExprN)] -- ^ Used in 'Tenet.Exprs.LambdaLift'
  , globals   :: S.Set Text -- ^ Used in 'Tenet.Exprs.LambdaLift'
  , issues    :: [Issue] -- ^ Current issues, warnings and errors.
  } deriving (Show)

-- | Indicates a name is able to be reused or can only be used once.
data Use
  = Reuse
  | Once
  deriving (Show, Eq)

-- | Identifies a name's use state, and what it's been renamed to. (Poorly named.)
data Counter = Counter
  { node   :: Node
  , use    :: Use
  , rename :: Text
  } deriving (Show)

-- | The state monad type used to manage operational state in AST and s-expr transformations.
type NameState = State Names

-- | Runs a state monad using the standard state monad.
runNameState :: Names -> NameState a -> (a, Names)
runNameState names monad = runState monad names

-- | Imports should be returned in source, target order.
getImports :: NameState [(Text, Text)]
getImports = M.assocs . imports <$> get

-- | The initial state of naming and transformation.
initial :: Names
initial = Names { names     = S.empty
                , imports   = M.empty
                , lexical   = M.empty
                , issues    = []
                , lexCounts = M.empty
                , lifts     = []
                , globals   = S.empty
                }

-- | Update the state with newly gathered names.
setNames :: Names -> S.Set Text -> Names
setNames dat names = dat { names }

-- | Monadically update the state with newly gathered names.
putNames :: S.Set Text -> NameState ()
putNames names = state (\stat -> ((), stat { names }))

-- | Update the state with newly gathered global names.
setGlobals :: Names -> S.Set Text -> Names
setGlobals dat globals = dat { globals }

-- | Monadically update the state with newly gathered global names.
putGlobals :: S.Set Text -> NameState ()
putGlobals globals = state (\stat -> ((), stat { globals }))

-- | Monadically add issues.
reportIssues :: [Issue] -> NameState ()
reportIssues newIssues = state (\stat@Names { issues } -> ((), stat { issues = issues ++ newIssues }))

-- | Given a base name, append a suffix as needed to construct a unique identifier.
uniqueName :: Text -> NameState Text
uniqueName name = state $ uniqueName' name

uniqueName' :: Text -> Names -> (Text, Names)
uniqueName' name old@Names { names } =
    let name' = findFreeName name names in (name', old { names = S.insert name' names })

-- | Make a temporary name guaranteed to not conflict with existing names.
makeTemp :: Text -> NameState Text
makeTemp = uniqueName

-- | Add a builtin function to the imports.
-- Not used much because the special functions are always available and never
-- conflict with user-defined functions.
getBuiltin :: Text -> NameState Text
getBuiltin name = state builtin'
  where
    builtin' stat@Names { imports } = case imports !? name of
        Just name' -> (name', stat)
        Nothing ->
            let (name', stat') = uniqueName' name stat in (name', stat' { imports = M.insert name name' imports })

-- | Construct a proposed lexical name.
lexicalName :: Text -> Int -> Text
lexicalName name num | last name == '_' = name `append` decimal num
                     | otherwise        = name `append` (cons '_' . decimal) num

-- | Attempt to set a name in the present lexical scope by determining a unique name.
setLexical :: Use -> Text -> Node -> NameState Text
setLexical use name node = state setLex'
  where
    setLex' stat@Names { lexical, lexCounts, issues } =
        let
            num               = M.findWithDefault 0 name lexCounts
            lexCountsHit      = M.insert name (num + 1) lexCounts
            (nextName, stat') = uniqueName' (lexicalName name num) stat
            lexicalHit        = M.insert name Counter { node, rename = nextName, use } lexical
            lexicalMiss       = M.insert name Counter { node, rename = name, use } lexical
        in
            case (use, lexical !? name) of
                (_, Nothing) -> (name, stat { lexical = lexicalMiss })
                (Reuse, Just Counter { use = Reuse }) ->
                    (nextName, stat' { lexical = lexicalHit, lexCounts = lexCountsHit })
                (_, Just Counter { node = oldNode }) ->
                    ( nextName
                    , stat' { lexical   = lexicalHit
                            , lexCounts = lexCountsHit
                            , issues    = CantReassign name oldNode node : issues
                            }
                    )

-- | Set a rename back to the value found in a prior state instance.
recallLexical :: Text -> Names -> NameState ()
recallLexical name prior = do
    stat@Names { lexical } <- get
    let Names { lexical = lexical' } = prior
    case lexical' !? name of
        Nothing  -> put stat { lexical = M.delete name lexical }
        Just val -> put stat { lexical = M.insert name val lexical }

-- | Save the current lexical name state.
saveLexical :: NameState Names
saveLexical = get

-- | Restore the lexical name state saved by saveLexical.
restoreLexical :: Names -> NameState ()
restoreLexical Names { lexical } = state restore' where restore' stat = ((), stat { lexical })

-- | A new lexical scope will add formals to the local state
-- and mark all names as reusable.
newLexical :: [Text] -> Node -> NameState Names
newLexical formals node = do
    stat@Names { lexical, issues } <- get
    let formalMap = M.fromListWith (+) [ (name, 1) | name <- formals ] :: M.Map Text Int
    let issues'   = [ DuplicateArg name node | name <- (M.keys . M.filter (> 1)) formalMap ] ++ issues
    let lexical'  = M.mapWithKey setName formalMap `M.union` M.map setReuse lexical
    put stat { lexical = lexical', issues = issues' }
    return stat
  where
    setName :: Text -> p -> Counter
    setName name _ = Counter { node, use = Reuse, rename = name }
    setReuse ctr = ctr { use = Reuse }

-- | Lookup a name in the current lexical scope, returning the renamed version if present. May also flag an 'UndefinedName' issue if it's not available.
lookupLexical :: Text -> Node -> NameState Text
lookupLexical name node = do
    stat@Names { lexical, issues } <- get
    let (name', issues') = case lexical !? name of
            Nothing                 -> (name, UndefinedName name node : issues)
            Just Counter { rename } -> (rename, issues)
    put stat { issues = issues' }
    return name'

-- | Get the lifted expressions and reset lifts to be empty
collectLifts :: NameState [(Text, SExprN)]
collectLifts = state collect where collect stat@Names { lifts } = (lifts, stat { lifts = [] })

-- | Adds a lifted expression to the lift state.
addLift :: Text -> SExprN -> NameState ()
addLift nam lift = state add
  where
    add stat@Names { lifts, globals } = ((), stat { lifts = (nam, lift) : lifts, globals = S.insert nam globals })

-- | Gets the global names from current state.
getGlobals :: NameState (S.Set Text)
getGlobals = globals <$> get
