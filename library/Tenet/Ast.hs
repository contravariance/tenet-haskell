-- | The concrete version of Tenet.Ast.Base which implements the abstract syntax tree of the Tenet language.
module Tenet.Ast
    ( module Tenet.Ast.Types
    , module Tenet.Ast.Functions
    )
where

import           Tenet.Ast.Types
import           Tenet.Ast.Functions
