{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE DeriveDataTypeable #-}

-- | Combines the representations of Tenet language and the internal s-expression documents as plain text, ANSI or HTML.
module Tenet.Show
    ( showAst
    , showAstAnsi
    , showAstFmt
    , showAstHtml
    , showSExpr
    , showSExprAnsi
    , showSExprFmt
    , showSExprHtml
    , stdLayout
    , withWidth
    , Format(..)
    , Encoding(..)
    , LayoutOptions
    )
where

import           Data.Generics                  ( Data
                                                , Typeable
                                                )
import           Data.Text                      ( Text )
import           Data.Text.Prettyprint.Doc      ( LayoutOptions(..)
                                                , PageWidth(AvailablePerLine)
                                                )
import qualified Tenet.Ast.Show                as A
import qualified Tenet.Exprs.Show              as E
import           Tenet.Show.Common              ( showDoc
                                                , showDocAnsi
                                                , showDocHtml
                                                )

-- | An enumeration of possible output formats.
data Encoding
  = Plain
  | Ansi
  | Html
  deriving (Data, Typeable, Eq, Show)

-- | Simple formatting options to pass to our layout functions.
data Format = Format
  { width    :: Int
  , encoding :: Encoding
  }

-- | Represent a Tenet language document as plain text.
showAst :: A.FormatAst a => LayoutOptions -> a -> Text
showAst opts = showDoc opts . A.fmt

-- | Represent a Tenet language document as ANSI colored text.
showAstAnsi :: A.FormatAst a => LayoutOptions -> a -> Text
showAstAnsi opts = showDocAnsi opts . A.fmt

-- | Represent a Tenet language document as HTML colored text.
showAstHtml :: A.FormatAst a => LayoutOptions -> a -> Text
showAstHtml opts = showDocHtml opts . A.fmt

-- | Represent a Tenet language document as a given output format.
showAstFmt :: A.FormatAst a => Format -> a -> Text
showAstFmt Format { width, encoding = Plain } = showAst $ withWidth width
showAstFmt Format { width, encoding = Ansi }  = showAstAnsi $ withWidth width
showAstFmt Format { width, encoding = Html }  = showAstHtml $ withWidth width

-- | Represent an s-expr document as plain text.
showSExpr :: E.FormatSExpr s => LayoutOptions -> s -> Text
showSExpr opts = showDoc opts . E.fmt

-- | Represent an s-expr document as ANSI colored text.
showSExprAnsi :: E.FormatSExpr s => LayoutOptions -> s -> Text
showSExprAnsi opts = showDocAnsi opts . E.fmt

-- | Represent an s-expr document as HTML colored text.
showSExprHtml :: E.FormatSExpr a => LayoutOptions -> a -> Text
showSExprHtml opts = showDocHtml opts . E.fmt

-- | Represent an s-expr document as a given output format.
showSExprFmt :: E.FormatSExpr a => Format -> a -> Text
showSExprFmt Format { width, encoding = Plain } = showSExpr $ withWidth width
showSExprFmt Format { width, encoding = Ansi }  = showSExprAnsi $ withWidth width
showSExprFmt Format { width, encoding = Html }  = showSExprHtml $ withWidth width

-- | The standard layout allows 120 characters per line.
stdLayout :: LayoutOptions
stdLayout = withWidth 120

-- | At the moment, the only configuration offered is line width.
withWidth :: Int -> LayoutOptions
withWidth width = LayoutOptions { layoutPageWidth = AvailablePerLine width 0.95 }
