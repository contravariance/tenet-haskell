{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE NamedFieldPuns     #-}
{-# LANGUAGE OverloadedStrings  #-}

-- | The 'Node type' and supporting functions to track the origin of derived values.
module Tenet.Ast.Node
    ( Node
    , nullNode
    , spanNode
    , atNode
    , locate
    , isNullNode
    , before
    , stmtNode
    )
where

import           Data.Generics                  ( Data )
import           Data.Text                      ( Text
                                                , pack
                                                )
import           Tenet.State.Out                ( Out
                                                , tfail'
                                                )
import           TextShow                       ( TextShow(..)
                                                , Builder
                                                , fromText
                                                , showt
                                                )
import           Text.Megaparsec.Pos            ( Pos
                                                , SourcePos(..)
                                                , unPos
                                                )

-- | Identifies a range of text within a file.
data LcRange
  = At Int
       Int
  | Inline Int
           Int
           Int
  | Span Int
         Int
         Int
         Int
  deriving (Eq, Ord, Show, Read, Data)

instance TextShow LcRange where
    showb s = case s of
        At line col                -> " at " <> locLc line col
        Inline line colS colE      -> " at " <> locLcc line colS colE
        Span lineS colS lineE colE -> " from " <> locLc lineS colS <> " through " <> locLc lineE colE
      where
        locLc :: Int -> Int -> Builder
        locLc line col = "l" <> showb line <> "c" <> showb col
        locLcc :: Int -> Int -> Int -> Builder
        locLcc line colS colE = "l" <> showb line <> " c" <> showb colS <> "-c" <> showb colE

lcInline :: Pos -> Pos -> Pos -> LcRange
lcInline line start end = Inline (unPos line) (unPos start) (unPos end)

lcSpan :: Pos -> Pos -> Pos -> Pos -> LcRange
lcSpan line1 col1 line2 col2 = Span (unPos line1) (unPos col1) (unPos line2) (unPos col2)

-- | The basic type to track positions within the source file.
data Node
  = Nnull
    -- ^ Null node.
  | Nspan { nPath :: Text
          , nSpan :: LcRange }
    -- ^ A keyword or possibly one token statement that lives on a single line.
  | Nst { nstP :: Text
        , nstH :: LcRange
        , nstT :: LcRange }
    -- ^ A statement with a head and tail span.
  deriving (Eq, Ord, Read, Data, Show)

instance Semigroup Node where
    Nnull <> x = x
    x     <> _ = x

instance Monoid Node where
    mempty = Nnull

-- | Gets a brief text representation of a Node.
instance TextShow Node where
    showb n = case n of
        Nnull                            -> "(loc?)"
        Nspan { nPath = np, nSpan = ns } -> "file " <> fromText np <> showb ns
        Nst { nstP = np, nstH = nsH, nstT = nsT } ->
            "file " <> fromText np <> ": head" <> showb nsH <> "; tail" <> showb nsT

locate :: Node -> Text
locate = showt

-- | Deconstruct two positions two find the file they're in and the range between them.
dePos :: SourcePos -> SourcePos -> Out (Text, LcRange)
dePos pos1 pos2
    | name1 /= name2
    = tfail' $ "Positions from different files: " <> pack name1 <> " and " <> pack name2
    | (line1, col1) > (line2, col2)
    = tfail'
        $  "Positions in "
        <> pack name1
        <> " out of order: "
        <> showt (unPos line1, unPos col1)
        <> " after "
        <> showt (unPos line2, unPos col2)
    | line1 == line2
    = pure (pack name1, lcInline line1 col1 col2)
    | otherwise
    = pure (pack name1, lcSpan line1 col1 line2 col2)
  where
    SourcePos { sourceName = name1, sourceLine = line1, sourceColumn = col1 } = pos1
    SourcePos { sourceName = name2, sourceLine = line2, sourceColumn = col2 } = pos2

-- | Construct a null node to represent an unknown position.
nullNode :: Node
nullNode = Nnull

-- | Identify if a node is null
isNullNode :: Node -> Bool
isNullNode Nnull = True
isNullNode _     = False

-- | Construct a 'Node' spanning two 'SourcePos'.
spanNode :: SourcePos -> SourcePos -> Out Node
spanNode pos1 pos2 = do
    (path, rng) <- dePos pos1 pos2
    return $ Nspan { nPath = path, nSpan = rng }

-- | Construct a 'Node' at a specific 'SourcePos'.
atNode :: SourcePos -> Node
atNode SourcePos { sourceName, sourceLine, sourceColumn } =
    Nspan { nPath = pack sourceName, nSpan = At (unPos sourceLine) (unPos sourceColumn) }

-- | Determine if a prior range is before a later range.
before :: LcRange -> LcRange -> Bool
before prior later = before' (derange prior) (derange later)
  where
    derange (At line col                           ) = ((line, col), (line, col + 1))
    derange (Inline line start end                 ) = ((line, start), (line, end))
    derange (Span startLine startCol endLine endCol) = ((startLine, startCol), (endLine, endCol))
    before' :: ((Int, Int), (Int, Int)) -> ((Int, Int), (Int, Int)) -> Bool
    before' (pos1, pos2) (pos3, pos4) = pos1 <= pos2 && pos2 <= pos3 && pos3 <= pos4

-- | Build a node using a head span and tail span.
stmtNode :: SourcePos -> SourcePos -> SourcePos -> SourcePos -> Out Node
stmtNode headStart headStop tailStart tailStop = do
    (path , headSpan) <- dePos headStart headStop
    (path2, tailSpan) <- dePos tailStart tailStop
    if path /= path2
        then tfail' $ "Positions from different files: " <> path <> " and " <> path2
        else if headSpan `before` tailSpan
            then return Nst { nstP = path, nstH = headSpan, nstT = tailSpan }
            else tfail' $ "In " <> path <> " the range " <> showt headSpan <> " wasn't before " <> showt tailSpan
