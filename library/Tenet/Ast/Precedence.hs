{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE NamedFieldPuns       #-}

-- | This is used by the Tenet show framework to correctly assign parentheses. The precedences must match the implicit precedence set in parsing.
module Tenet.Ast.Precedence
    ( Ambiguous(..)
    )
where

import           Tenet.Ast                      ( BinaryOp(..)
                                                , Expr(..)
                                                , SpecialFunc(..)
                                                , UnaryOp(..)
                                                )

-- | A type class for elements that can be placed in ambiguous syntax. They are resolved by specifying their precedence.
-- Precedences should be consistent with Tenet.cf.
class Ambiguous opType where
  precedence :: opType -> Int

instance Ambiguous UnaryOp where
    precedence UnMinus = 10
    precedence UnPlus  = 10
    precedence UnNot   = 4

instance Ambiguous BinaryOp where
    precedence BinTimes        = 7
    precedence BinFloorDivide  = 7
    precedence BinDivide       = 7
    precedence BinModulo       = 7
    precedence BinIntersect    = 7
    precedence BinUnion        = 6
    precedence BinDiff         = 6
    precedence BinPlus         = 6
    precedence BinConcat       = 6
    precedence BinMinus        = 6
    precedence BinEquals       = 5
    precedence BinNotEqual     = 5
    precedence BinLessThan     = 5
    precedence BinLessEqual    = 5
    precedence BinGreaterThan  = 5
    precedence BinGreaterEqual = 5
    precedence BinIn           = 5
    precedence BinNotIn        = 5
    precedence BinAnd          = 3
    precedence BinOr           = 2
    precedence BinXor          = 1
    precedence BinEqv          = 1
    precedence BinImp          = 1

instance Ambiguous (Expr n) where
    precedence Name{}           = 11
    precedence Special{}        = 11
    precedence BooleanLit{}     = 11
    precedence IntegerLit{}     = 11
    precedence StringLit{}      = 11
    precedence RecordCon{}      = 11
    precedence ListCon{}        = 11
    precedence SetCon{}         = 11
    precedence MapCon{}         = 11
    precedence LazyExpr{}       = 11
    precedence LambdaExpr{}     = 11
    precedence Application{}    = 9
    precedence Partial{}        = 9
    precedence Indexing{}       = 9
    precedence Slot{}           = 9
    precedence Variant{}        = 9
    precedence Tagged{}         = 8
    precedence UnExpr { ueOp }  = precedence ueOp
    precedence BinExpr { beOp } = precedence beOp
    precedence Guarded{}        = 0

instance Ambiguous SpecialFunc where
    precedence _ = 100
