-- | Boundary checks for rewriting the AST.

module Tenet.Ast.Descend
    ( Descent(..)
    , Descend(..)
    , alwaysDescend
    )
where

import           Tenet.Ast.Types

data Descend
  = Descend -- evaluate children, reconstruct this node, then run op against reconstructed node.
  | Node -- Don't descend, but run op against current node
  | Ignore -- Skip this node altogether
  deriving (Show, Eq, Ord)

-- | The descent dictionary inspects nodes to determine where we should stop our descent.
data Descent n = Descent { dDoc :: Doc n -> Descend
                         , dTopStmt :: TopStmt n -> Descend
                         , dFuncData :: FuncData n -> Descend
                         , dLetData :: LetData n -> Descend
                         , dTypeExpr :: TypeExpr n -> Descend
                         , dStatement :: Statement n -> Descend
                         , dCaseStmt :: CaseStmt n -> Descend
                         , dCatchStmt :: CatchStmt n -> Descend
                         , dPattern :: Pattern n -> Descend
                         , dExpr :: Expr n -> Descend
                         , dLhsExpr :: LhsExpr n -> Descend
                         }

-- | The default dictionary always descends.
alwaysDescend :: Descent n
alwaysDescend = Descent { dDoc       = _always
                        , dTopStmt   = _always
                        , dFuncData  = _always
                        , dLetData   = _always
                        , dTypeExpr  = _always
                        , dStatement = _always
                        , dCaseStmt  = _always
                        , dCatchStmt = _always
                        , dPattern   = _always
                        , dExpr      = _always
                        , dLhsExpr   = _always
                        }

_always :: a -> Descend
_always _ = Descend
