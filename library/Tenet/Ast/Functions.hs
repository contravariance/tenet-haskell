{-# LANGUAGE NamedFieldPuns #-}

-- | The concrete version of Tenet.Ast.Base which implements the abstract syntax tree of the Tenet language.
module Tenet.Ast.Functions
    ( nullNode
    , letSimple
    , letStmt
    , letStmt'
    , enumVal
    , enumVal'
    , returnStmt
    , caseStmt
    )
where

import           Data.Text                      ( Text )
import           Tenet.Ast.Types
import           Tenet.Ast.Node                 ( nullNode )

-- | Constructs a simple assignment statement.
letSimple :: n -> Text -> Expr n -> Statement n
letSimple node name expr = LetStmt { lsLet = letDataSimple node name expr }

-- | Constructs a LetData value with a simple assignment.
letDataSimple :: n -> Text -> Expr n -> LetData n
letDataSimple node name expr = LetData { lAssignOp = Assign
                                       , lTarget   = LhsLens { llVarName = name, llPhrases = [], llNode = node }
                                       , lValue    = expr
                                       , lNode     = node
                                       }

-- | Constructor for a let statement.
letStmt :: LetData n -> Statement n
letStmt letDat = LetStmt { lsLet = letDat }

-- | Complete constructor for a let Statement.
letStmt' :: LhsExprN -> AssignOp -> ExprN -> StatementN
letStmt' lhs assign rhs = letStmt $ LetData { lTarget = lhs, lAssignOp = assign, lValue = rhs, lNode = nullNode }

-- | Shortcut to construct an enumerated value expression.
enumVal' :: n -> Text -> Expr n
enumVal' node tag = Tagged { tgTag = tag, tgVariant = unitVal node, tgNode = node }

-- | Shortcut to construct an enumerated value expression with a null node.
enumVal :: Text -> ExprN
enumVal = enumVal' nullNode

-- | Shortcut to construct an empty record, the so-called unit value.
unitVal :: n -> Expr n
unitVal node = RecordCon { tcArgs = [], tcNode = node }

-- | Constructor for a return statement.
returnStmt :: ExprN -> StatementN
returnStmt rsValue = ReturnStmt { rsNode = nullNode, rsValue }

-- | Constructor for a case statement.
caseStmt :: PatternN -> [StatementN] -> CaseStmtN
caseStmt csPat csBody = CaseStmt { csNode = nullNode, csPat, csBody }
