{-# LANGUAGE ApplicativeDo, FlexibleInstances, MultiParamTypeClasses, RecordWildCards, ScopedTypeVariables, TupleSections #-}

-- | A rewriter than uses boundary checks.

module Tenet.Ast.Scan
    ( Scannable(..)
    )
where

import           Control.Applicative            ( liftA2 )
import           Control.Monad.Identity         ( Identity(runIdentity) )
import           Tenet.Ast.Descend              ( Descent(..)
                                                , Descend(..)
                                                )
import           Tenet.Ast.Types
import           Tenet.State.Change             ( Change(..)
                                                , bindC
                                                , cval
                                                , foldOut
                                                , liftCd
                                                , liftCd2
                                                , liftCd3
                                                )

type Tf m a = a -> m (Change a)

class Scannable o i a where
  transformM :: Monad m => Descent a -> Tf m i -> Tf m o

  transform :: Descent a -> (i -> Change i) -> o -> o
  transform d i o = cval $ runIdentity $ transformM d (pure . i) o

  scan :: Descent a -> o -> [i]

_tf :: (Monad m, Scannable o i a) => Descent a -> Tf m i -> Tf m o
_tf = transformM

_tfDoc :: Monad m => Tf m [TopStmt a] -> Descent a -> Tf m (Doc a) -> Tf m (Doc a)
_tfDoc rwT d op n@(Doc sx) = descend1 (dDoc d n) Doc (rwT sx) op n

_scDoc :: [o] -> ([TopStmt a] -> [o]) -> Descent a -> Doc a -> [o]
_scDoc nx scT d n@(Doc sx) = _scan (dDoc d n) nx (scT sx)

instance Scannable (Doc a) (Doc a) a where
    transformM d op n = _tfDoc (_tf d op) d op n
    scan d n = _scDoc [n] (scan d) d n
instance Scannable (Doc a) (TopStmt a) a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) [TopStmt a] a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) (FuncData a) a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) (LetData a) a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) (TypeExpr a) a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) (Statement a) a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) [Statement a] a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) (CaseStmt a) a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) [CaseStmt a] a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) (CatchStmt a) a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) [CatchStmt a] a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) (Pattern a) a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) (Expr a) a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) [Expr a] a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n
instance Scannable (Doc a) (LhsExpr a) a where
    transformM d op n = _tfDoc (_tf d op) d noOp n
    scan d n = _scDoc [] (scan d) d n

_tfTopStmt
    :: Monad m
    => Tf m (TypeExpr a)
    -> Tf m (LetData a)
    -> Tf m (FuncData a)
    -> Descent a
    -> Tf m (TopStmt a)
    -> Tf m (TopStmt a)
_tfTopStmt rwTe rwLd rwFd d op n = case n of
    ImportTs{}       -> descend0 d' op n
    TypeDef nn a b c -> descend1 d' (TypeDef nn a b) (rwTe c) op n
    GlobalLet a      -> descend1 d' GlobalLet (rwLd a) op n
    FuncTs    a      -> descend1 d' FuncTs (rwFd a) op n
    where d' = dTopStmt d n

_scTopStmt :: [o] -> (TypeExpr a -> [o]) -> (LetData a -> [o]) -> (FuncData a -> [o]) -> Descent a -> TopStmt a -> [o]
_scTopStmt nx scTe scLd scFd d n = case n of
    ImportTs{}      -> nx
    TypeDef _ _ _ c -> _scan d' nx (scTe c)
    GlobalLet a     -> _scan d' nx (scLd a)
    FuncTs    a     -> _scan d' nx (scFd a)
    where d' = dTopStmt d n

instance Scannable (TopStmt a) (Doc a) a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) (TopStmt a) a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d op n
    scan d n = _scTopStmt [n] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) [TopStmt a] a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) (FuncData a) a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) (LetData a) a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) (TypeExpr a) a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) (Statement a) a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) [Statement a] a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) (CaseStmt a) a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) [CaseStmt a] a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) (CatchStmt a) a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) [CatchStmt a] a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) (Pattern a) a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) (Expr a) a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) [Expr a] a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n
instance Scannable (TopStmt a) (LhsExpr a) a where
    transformM d op n = _tfTopStmt (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scTopStmt [] (scan d) (scan d) (scan d) d n

instance Scannable [TopStmt a] (Doc a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] (TopStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] [TopStmt a] a where
    transformM d op n = listList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] (FuncData a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] (LetData a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] (TypeExpr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] (Statement a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] [Statement a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] (CaseStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] [CaseStmt a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] (CatchStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] [CatchStmt a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] (Pattern a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] (Expr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] [Expr a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [TopStmt a] (LhsExpr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n

_tfFuncData
    :: Monad m => Tf m [Statement a] -> Tf m (TypeExpr a) -> Descent a -> Tf m (FuncData a) -> Tf m (FuncData a)
_tfFuncData rwS rwTe d op n@FuncData {..} = descend3 d' con (_tfTypeParams rwTe dParams) (rwTe dRet) (rwS dBody) op n
  where
    con p r b = n { dParams = p, dRet = r, dBody = b }
    d' = dFuncData d n

_scFuncData :: [o] -> ([Statement a] -> [o]) -> (TypeExpr a -> [o]) -> Descent a -> FuncData a -> [o]
_scFuncData nx scS scTe d n@FuncData {..} = _scan d' nx (_scTypeParams scTe dParams ++ scTe dRet ++ scS dBody)
    where d' = dFuncData d n

instance Scannable (FuncData a) (Doc a) a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) (TopStmt a) a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) [TopStmt a] a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) (FuncData a) a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d op n
    scan d n = _scFuncData [n] (scan d) (scan d) d n
instance Scannable (FuncData a) (LetData a) a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) (TypeExpr a) a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) (Statement a) a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) [Statement a] a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) (CaseStmt a) a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) [CaseStmt a] a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) (CatchStmt a) a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) [CatchStmt a] a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) (Pattern a) a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) (Expr a) a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) [Expr a] a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n
instance Scannable (FuncData a) (LhsExpr a) a where
    transformM d op n = _tfFuncData (_tf d op) (_tf d op) d noOp n
    scan d n = _scFuncData [] (scan d) (scan d) d n

_tfLetData :: Monad m => Tf m (Expr a) -> Tf m (LhsExpr a) -> Descent a -> Tf m (LetData a) -> Tf m (LetData a)
_tfLetData rwE rwLh d op n@LetData {..} = descend2 d' con (rwLh lTarget) (rwE lValue) op n
  where
    con t v = n { lTarget = t, lValue = v }
    d' = dLetData d n

_scLetData :: [o] -> (Expr a -> [o]) -> (LhsExpr a -> [o]) -> Descent a -> LetData a -> [o]
_scLetData nx scE scLh d n@LetData {..} = _scan (dLetData d n) nx (scLh lTarget ++ scE lValue)

instance Scannable (LetData a) (Doc a) a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) (TopStmt a) a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) [TopStmt a] a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) (FuncData a) a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) (LetData a) a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d op n
    scan d n = _scLetData [n] (scan d) (scan d) d n
instance Scannable (LetData a) (TypeExpr a) a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) (Statement a) a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) [Statement a] a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) (CaseStmt a) a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) [CaseStmt a] a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) (CatchStmt a) a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) [CatchStmt a] a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) (Pattern a) a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) (Expr a) a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) [Expr a] a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n
instance Scannable (LetData a) (LhsExpr a) a where
    transformM d op n = _tfLetData (_tf d op) (_tf d op) d noOp n
    scan d n = _scLetData [] (scan d) (scan d) d n

_tfTypeParam :: Monad m => Tf m (TypeExpr a) -> Tf m (TypeParam a)
_tfTypeParam rwTe n@(name, typeExpr) = liftCd n (name, ) <$> rwTe typeExpr

_tfTypeParams :: Monad m => Tf m (TypeExpr a) -> Tf m [TypeParam a]
_tfTypeParams rwTe n = descendList (_tfTypeParam rwTe) n

_scTypeParams :: (TypeExpr a -> [o]) -> [TypeParam a] -> [o]
_scTypeParams scTe n = concatMap (scTe . snd) n

_tfTypeExpr :: Monad m => Tf m (TypeExpr a) -> Descent a -> Tf m (TypeExpr a) -> Tf m (TypeExpr a)
_tfTypeExpr rwTe d op n = case n of
    TypeExpr {..}     -> descend1 d' (TypeExpr teNode teName) (rwTp teParams) op n
    FuncType {..}     -> descend2 d' (FuncType ftNode) (rwTp ftParams) (rwTe ftReturn) op n
    ListType {..}     -> descend1 d' (ListType ltNode) (rwTe ltElem) op n
    SetType {..}      -> descend1 d' (SetType stNode) (rwTe stElem) op n
    MapType {..}      -> descend2 d' (MapType mtNode) (rwTe mtKey) (rwTe mtVal) op n
    RecordType {..}   -> descend1 d' (RecordType rtNode) (rwTp rtSlots) op n
    UnionType {..}    -> descend1 d' (UnionType utNode) (rwTp utTags) op n
    UnspecifiedType _ -> descend0 d' op n
  where
    d'   = dTypeExpr d n
    rwTp = _tfTypeParams rwTe

_scTypeExpr :: [o] -> (TypeExpr a -> [o]) -> Descent a -> TypeExpr a -> [o]
_scTypeExpr nx scTe d n = case n of
    TypeExpr {..}     -> _scan d' nx (scTp teParams)
    FuncType {..}     -> _scan d' nx (scTp ftParams ++ scTe ftReturn)
    ListType {..}     -> _scan d' nx (scTe ltElem)
    SetType {..}      -> _scan d' nx (scTe stElem)
    MapType {..}      -> _scan d' nx (scTe mtKey ++ scTe mtVal)
    RecordType {..}   -> _scan d' nx (scTp rtSlots)
    UnionType {..}    -> _scan d' nx (scTp utTags)
    UnspecifiedType _ -> nx
  where
    d'   = dTypeExpr d n
    scTp = _scTypeParams scTe

instance Scannable (TypeExpr a) (Doc a) a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) (TopStmt a) a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) [TopStmt a] a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) (FuncData a) a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) (LetData a) a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) (TypeExpr a) a where
    transformM d op n = _tfTypeExpr (_tf d op) d op n
    scan d n = _scTypeExpr [n] (scan d) d n
instance Scannable (TypeExpr a) (Statement a) a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) [Statement a] a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) (CaseStmt a) a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) [CaseStmt a] a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) (CatchStmt a) a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) [CatchStmt a] a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) (Pattern a) a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) (Expr a) a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) [Expr a] a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n
instance Scannable (TypeExpr a) (LhsExpr a) a where
    transformM d op n = _tfTypeExpr (_tf d op) d noOp n
    scan d n = _scTypeExpr [] (scan d) d n

_tfStatement
    :: Monad m
    => Tf m [Statement a]
    -> Tf m (FuncData a)
    -> Tf m (LetData a)
    -> Tf m [CaseStmt a]
    -> Tf m [CatchStmt a]
    -> Tf m (Expr a)
    -> Descent a
    -> Tf m (Statement a)
    -> Tf m (Statement a)
_tfStatement rwSx rwFd rwLd rwCs rwCc rwE d op n = case n of
    FuncStmt {..}    -> descend1 d' FuncStmt (rwFd dsFunc) op n
    LetStmt {..}     -> descend1 d' LetStmt (rwLd lsLet) op n
    ForStmt {..}     -> descend2 d' (ForStmt fsNode fsVarName) (rwE fsIterable) (rwSx fsBody) op n
    ReturnStmt {..}  -> descend1 d' (ReturnStmt rsNode) (rwE rsValue) op n
    IfStmt {..}      -> descend3 d' (IfStmt ifNode) (rwE ifCondition) (rwSx ifThenBody) (rwSx ifElseBody) op n
    SwitchStmt {..}  -> descend2 d' (SwitchStmt ssNode) (rwE ssSubject) (rwCs ssCases) op n
    PassStmt{}       -> descend0 d' op n
    ContinueStmt{}   -> descend0 d' op n
    BreakStmt{}      -> descend0 d' op n
    TryStmt {..}     -> descend2 d' (TryStmt trNode) (rwSx trTryBody) (rwCc trCatches) op n
    InvalidStmt {..} -> descend1 d' (InvalidStmt ivNode ivIssue) (rwSx ivBody) op n
    where d' = dStatement d n

_scStatement
    :: [o]
    -> ([Statement a] -> [o])
    -> (FuncData a -> [o])
    -> (LetData a -> [o])
    -> ([CaseStmt a] -> [o])
    -> ([CatchStmt a] -> [o])
    -> (Expr a -> [o])
    -> Descent a
    -> Statement a
    -> [o]
_scStatement nx scSx scFd scLd scCs scCc scE d n = case n of
    FuncStmt {..}    -> _scan d' nx (scFd dsFunc)
    LetStmt {..}     -> _scan d' nx (scLd lsLet)
    ForStmt {..}     -> _scan d' nx (scE fsIterable ++ scSx fsBody)
    ReturnStmt {..}  -> _scan d' nx (scE rsValue)
    IfStmt {..}      -> _scan d' nx (scE ifCondition ++ scSx ifThenBody ++ scSx ifElseBody)
    SwitchStmt {..}  -> _scan d' nx (scE ssSubject ++ scCs ssCases)
    PassStmt{}       -> nx
    ContinueStmt{}   -> nx
    BreakStmt{}      -> nx
    TryStmt {..}     -> _scan d' nx (scSx trTryBody ++ scCc trCatches)
    InvalidStmt {..} -> _scan d' nx (scSx ivBody)
    where d' = dStatement d n

instance Scannable (Statement a) (Doc a) a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) (TopStmt a) a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) [TopStmt a] a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) (FuncData a) a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) (LetData a) a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) (TypeExpr a) a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) (Statement a) a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d op n
    scan d n = _scStatement [n] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) [Statement a] a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) (CaseStmt a) a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) [CaseStmt a] a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) (CatchStmt a) a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) [CatchStmt a] a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) (Pattern a) a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) (Expr a) a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) [Expr a] a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Statement a) (LhsExpr a) a where
    transformM d op n = _tfStatement (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scStatement [] (scan d) (scan d) (scan d) (scan d) (scan d) (scan d) d n

instance Scannable [Statement a] (Doc a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] (TopStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] [TopStmt a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] (FuncData a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] (LetData a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] (TypeExpr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] (Statement a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] [Statement a] a where
    transformM d op n = listList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] (CaseStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] [CaseStmt a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] (CatchStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] [CatchStmt a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] (Pattern a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] (Expr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] [Expr a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Statement a] (LhsExpr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n

_tfCaseStmt
    :: Monad m => Tf m [Statement a] -> Tf m (Pattern a) -> Descent a -> Tf m (CaseStmt a) -> Tf m (CaseStmt a)
_tfCaseStmt rwSx rwP d op n = case n of
    CaseStmt {..}    -> descend2 d' (CaseStmt csNode) (rwP csPat) (rwSx csBody) op n
    DefaultStmt {..} -> descend1 d' (DefaultStmt dsNode) (rwSx dsBody) op n
    where d' = dCaseStmt d n

_scCaseStmt :: [o] -> ([Statement a] -> [o]) -> (Pattern a -> [o]) -> Descent a -> CaseStmt a -> [o]
_scCaseStmt nx scSx scP d n = case n of
    CaseStmt {..}    -> _scan d' nx (scP csPat ++ scSx csBody)
    DefaultStmt {..} -> _scan d' nx (scSx dsBody)
    where d' = dCaseStmt d n

instance Scannable (CaseStmt a) (Doc a) a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) (TopStmt a) a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) [TopStmt a] a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) (FuncData a) a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) (LetData a) a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) (TypeExpr a) a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) (Statement a) a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) [Statement a] a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) (CaseStmt a) a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d op n
    scan d n = _scCaseStmt [n] (scan d) (scan d) d n
instance Scannable (CaseStmt a) [CaseStmt a] a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) (CatchStmt a) a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) [CatchStmt a] a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) (Pattern a) a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) (Expr a) a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) [Expr a] a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n
instance Scannable (CaseStmt a) (LhsExpr a) a where
    transformM d op n = _tfCaseStmt (_tf d op) (_tf d op) d noOp n
    scan d n = _scCaseStmt [] (scan d) (scan d) d n

instance Scannable [CaseStmt a] (Doc a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] (TopStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] [TopStmt a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] (FuncData a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] (LetData a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] (TypeExpr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] (Statement a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] [Statement a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] (CaseStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] [CaseStmt a] a where
    transformM d op n = listList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] (CatchStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] [CatchStmt a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] (Pattern a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] (Expr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] [Expr a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CaseStmt a] (LhsExpr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n

_tfCatchStmt :: Monad m => Tf m [Statement a] -> Descent a -> Tf m (CatchStmt a) -> Tf m (CatchStmt a)
_tfCatchStmt rwSx d op n@CatchStmt {..} =
    descend1 (dCatchStmt d n) (CatchStmt ecNode ecExceptions) (rwSx ecBody) op n

_scCatchStmt :: [o] -> ([Statement a] -> [o]) -> Descent a -> CatchStmt a -> [o]
_scCatchStmt nx scSx d n@CatchStmt {..} = _scan (dCatchStmt d n) nx (scSx ecBody)

instance Scannable (CatchStmt a) (Doc a) a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) (TopStmt a) a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) [TopStmt a] a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) (FuncData a) a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) (LetData a) a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) (TypeExpr a) a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) (Statement a) a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) [Statement a] a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) (CaseStmt a) a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) [CaseStmt a] a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) (CatchStmt a) a where
    transformM d op n = _tfCatchStmt (_tf d op) d op n
    scan d n = _scCatchStmt [n] (scan d) d n
instance Scannable (CatchStmt a) [CatchStmt a] a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) (Pattern a) a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) (Expr a) a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) [Expr a] a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n
instance Scannable (CatchStmt a) (LhsExpr a) a where
    transformM d op n = _tfCatchStmt (_tf d op) d noOp n
    scan d n = _scCatchStmt [] (scan d) d n

instance Scannable [CatchStmt a] (Doc a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] (TopStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] [TopStmt a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] (FuncData a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] (LetData a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] (TypeExpr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] (Statement a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] [Statement a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] (CaseStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] [CaseStmt a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] (CatchStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] [CatchStmt a] a where
    transformM d op n = listList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] (Pattern a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] (Expr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] [Expr a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [CatchStmt a] (LhsExpr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n

_tfSlotPat :: Monad m => Tf m (Pattern a) -> Tf m (SlotPat a)
_tfSlotPat rwP n@(name, patt) = liftCd n (name, ) <$> rwP patt

_tfSlotPats :: Monad m => Tf m (Pattern a) -> Tf m [SlotPat a]
_tfSlotPats rwP n = descendList (_tfSlotPat rwP) n

_scSlotPats :: (Pattern a -> [o]) -> [SlotPat a] -> [o]
_scSlotPats scP n = concatMap (scP . snd) n

_tfPattern :: Monad m => Tf m (Pattern a) -> Descent a -> Tf m (Pattern a) -> Tf m (Pattern a)
_tfPattern rwP d op n = case n of
    NamePat{}      -> descend0 d' op n
    IntegerPat{}   -> descend0 d' op n
    StringPat{}    -> descend0 d' op n
    RecordPat {..} -> descend1 d' (RecordPat tpNode) (rwSp tpSlots) op n
    TagPat {..}    -> descend1 d' (TagPat upNode upTag) (rwP upVar) op n
    AnyPat{}       -> descend0 d' op n
  where
    d'   = dPattern d n
    rwSp = _tfSlotPats rwP

_scPattern :: [o] -> (Pattern a -> [o]) -> Descent a -> Pattern a -> [o]
_scPattern nx scP d n = case n of
    NamePat{}      -> nx
    IntegerPat{}   -> nx
    StringPat{}    -> nx
    RecordPat {..} -> _scan d' nx (scSp tpSlots)
    TagPat {..}    -> _scan d' nx (scP upVar)
    AnyPat{}       -> nx
  where
    d'   = dPattern d n
    scSp = _scSlotPats scP

instance Scannable (Pattern a) (Doc a) a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) (TopStmt a) a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) [TopStmt a] a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) (FuncData a) a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) (LetData a) a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) (TypeExpr a) a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) (Statement a) a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) [Statement a] a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) (CaseStmt a) a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) [CaseStmt a] a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) (CatchStmt a) a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) [CatchStmt a] a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) (Pattern a) a where
    transformM d op n = _tfPattern (_tf d op) d op n
    scan d n = _scPattern [n] (scan d) d n
instance Scannable (Pattern a) (Expr a) a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) [Expr a] a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n
instance Scannable (Pattern a) (LhsExpr a) a where
    transformM d op n = _tfPattern (_tf d op) d noOp n
    scan d n = _scPattern [] (scan d) d n

_tfArgument :: Monad m => Tf m (Expr a) -> Tf m (Argument a)
_tfArgument rwE n@(name, expr) = liftCd n (name, ) <$> rwE expr

_tfArguments :: Monad m => Tf m (Expr a) -> Tf m [Argument a]
_tfArguments rwE n = descendList (_tfArgument rwE) n

_scArguments :: (Expr a -> [o]) -> [Argument a] -> [o]
_scArguments scE n = concatMap (scE . snd) n

_tfMapPair :: Monad m => Tf m (Expr a) -> Tf m (MapPair a)
_tfMapPair rwE n@(key, val) = liftA2 (liftCd2 n (,)) (rwE key) (rwE val)

_scMapPair :: (Expr a -> [o]) -> MapPair a -> [o]
_scMapPair scE (key, val) = scE key ++ scE val

_tfMapPairs :: Monad m => Tf m (Expr a) -> Tf m [MapPair a]
_tfMapPairs rwE n = descendList (_tfMapPair rwE) n

_scMapPairs :: (Expr a -> [o]) -> [MapPair a] -> [o]
_scMapPairs scE n = concatMap (_scMapPair scE) n

_tfExpr
    :: Monad m
    => Tf m [Statement a]
    -> Tf m (Expr a)
    -> Tf m [Expr a]
    -> Tf m (TypeExpr a)
    -> Descent a
    -> Tf m (Expr a)
    -> Tf m (Expr a)
_tfExpr rwSx rwE rwEx rwTe d op n = case n of
    UnExpr {..}      -> descend1 d' (UnExpr ueNode ueOp) (rwE ueExpr) op n
    BinExpr {..}     -> descend2 d' (BinExpr beNode beOp) (rwE beLeft) (rwE beRight) op n
    Name{}           -> descend0 d' op n
    IntegerLit{}     -> descend0 d' op n
    StringLit{}      -> descend0 d' op n
    BooleanLit{}     -> descend0 d' op n
    RecordCon {..}   -> descend1 d' (RecordCon tcNode) (rwAx tcArgs) op n
    ListCon {..}     -> descend1 d' (ListCon lcNode) (rwEx lcElems) op n
    SetCon {..}      -> descend1 d' (SetCon seNode) (rwEx seElems) op n
    MapCon {..}      -> descend1 d' (MapCon mcNode) (rwMx mcPairs) op n
    Application {..} -> descend2 d' (Application apNode) (rwE apHead) (rwAx apArgs) op n
    Partial {..}     -> descend2 d' (Partial paNode) (rwE paHead) (rwAx paArgs) op n
    Special {..}     -> descend1 d' (Special scNode scHead scParams) (rwEx scArgs) op n
    Indexing {..}    -> descend2 d' (Indexing ixNode) (rwE ixHead) (rwE ixIndex) op n
    Guarded {..}     -> descend2 d' (Guarded geNode) (rwE geHead) (rwE geGuard) op n
    Tagged {..}      -> descend1 d' (Tagged tgNode tgTag) (rwE tgVariant) op n
    Slot {..}        -> descend1 d' (Slot atNode atSlot) (rwE atValue) op n
    Variant {..}     -> descend1 d' (Variant vrNode vrTag) (rwE vrValue) op n
    LambdaExpr {..}  -> descend2 d' (LambdaExpr laNode) (rwTp laParams) (rwSx laBody) op n
    LazyExpr {..}    -> descend1 d' (LazyExpr lzNode) (rwE lzBody) op n
  where
    d'   = dExpr d n
    rwAx = _tfArguments rwE
    rwMx = _tfMapPairs rwE
    rwTp = _tfTypeParams rwTe

_scExpr
    :: [o]
    -> ([Statement a] -> [o])
    -> (Expr a -> [o])
    -> ([Expr a] -> [o])
    -> (TypeExpr a -> [o])
    -> Descent a
    -> Expr a
    -> [o]
_scExpr nx scSx scE scEx scTe d n = case n of
    UnExpr {..}      -> _scan d' nx (scE ueExpr)
    BinExpr {..}     -> _scan d' nx (scE beLeft ++ scE beRight)
    Name{}           -> nx
    IntegerLit{}     -> nx
    StringLit{}      -> nx
    BooleanLit{}     -> nx
    RecordCon {..}   -> _scan d' nx (scAx tcArgs)
    ListCon {..}     -> _scan d' nx (scEx lcElems)
    SetCon {..}      -> _scan d' nx (scEx seElems)
    MapCon {..}      -> _scan d' nx (scMx mcPairs)
    Application {..} -> _scan d' nx (scE apHead ++ scAx apArgs)
    Partial {..}     -> _scan d' nx (scE paHead ++ scAx paArgs)
    Special {..}     -> _scan d' nx (scEx scArgs)
    Indexing {..}    -> _scan d' nx (scE ixHead ++ scE ixIndex)
    Guarded {..}     -> _scan d' nx (scE geHead ++ scE geGuard)
    Tagged {..}      -> _scan d' nx (scE tgVariant)
    Slot {..}        -> _scan d' nx (scE atValue)
    Variant {..}     -> _scan d' nx (scE vrValue)
    LambdaExpr {..}  -> _scan d' nx (scTp laParams ++ scSx laBody)
    LazyExpr {..}    -> _scan d' nx (scE lzBody)
  where
    d'   = dExpr d n
    scAx = _scArguments scE
    scMx = _scMapPairs scE
    scTp = _scTypeParams scTe

instance Scannable (Expr a) (Doc a) a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) (TopStmt a) a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) [TopStmt a] a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) (FuncData a) a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) (LetData a) a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) (TypeExpr a) a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) (Statement a) a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) [Statement a] a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) (CaseStmt a) a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) [CaseStmt a] a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) (CatchStmt a) a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) [CatchStmt a] a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) (Pattern a) a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) (Expr a) a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d op n
    scan d n = _scExpr [n] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) [Expr a] a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n
instance Scannable (Expr a) (LhsExpr a) a where
    transformM d op n = _tfExpr (_tf d op) (_tf d op) (_tf d op) (_tf d op) d noOp n
    scan d n = _scExpr [] (scan d) (scan d) (scan d) (scan d) d n

instance Scannable [Expr a] (Doc a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] (TopStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] [TopStmt a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] (FuncData a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] (LetData a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] (TypeExpr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] (Statement a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] [Statement a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] (CaseStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] [CaseStmt a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] (CatchStmt a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] [CatchStmt a] a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] (Pattern a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] (Expr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] [Expr a] a where
    transformM d op n = listList (_tf d op) n
    scan d n = concatMap (scan d) n
instance Scannable [Expr a] (LhsExpr a) a where
    transformM d op n = descendList (_tf d op) n
    scan d n = concatMap (scan d) n

_tfLhsExpr :: Monad m => Tf m (Expr a) -> Descent a -> Tf m (LhsExpr a) -> Tf m (LhsExpr a)
_tfLhsExpr rwE d op n = case n of
    LhsLens {..}     -> descend1 d' (LhsLens llNode llVarName) (rwLx llPhrases) op n
    LhsDestruct {..} -> descend1 d' (LhsDestruct ldNode) (rwDx ldPhrases) op n
  where
    d'   = dLhsExpr d n
    rwLx = _tfLensPhrases rwE
    rwDx = _tfDestructPhrases rwE

_scLhsExpr :: [o] -> (Expr a -> [o]) -> Descent a -> LhsExpr a -> [o]
_scLhsExpr nx scE d n = case n of
    LhsLens {..}     -> _scan d' nx (scLx llPhrases)
    LhsDestruct {..} -> _scan d' nx (scDx ldPhrases)
  where
    d'   = dLhsExpr d n
    scLx = _scLensPhrases scE
    scDx = _scDestructPhrases scE

instance Scannable (LhsExpr a) (Doc a) a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) (TopStmt a) a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) [TopStmt a] a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) (FuncData a) a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) (LetData a) a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) (TypeExpr a) a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) (Statement a) a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) [Statement a] a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) (CaseStmt a) a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) [CaseStmt a] a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) (CatchStmt a) a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) [CatchStmt a] a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) (Pattern a) a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) (Expr a) a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) [Expr a] a where
    transformM d op n = _tfLhsExpr (_tf d op) d noOp n
    scan d n = _scLhsExpr [] (scan d) d n
instance Scannable (LhsExpr a) (LhsExpr a) a where
    transformM d op n = _tfLhsExpr (_tf d op) d op n
    scan d n = _scLhsExpr [n] (scan d) d n

_tfLensPhrase :: Monad m => Tf m (Expr a) -> Tf m (LensPhrase a)
_tfLensPhrase rwE n = case n of
    VariantLens{}  -> noOp n
    RecordLens{}   -> noOp n
    IndexLens {..} -> liftCd n (IndexLens idNode) <$> rwE idIndex

_tfLensPhrases :: Monad m => Tf m (Expr a) -> Tf m [LensPhrase a]
_tfLensPhrases rwE n = descendList (_tfLensPhrase rwE) n

_scLensPhrase :: (Expr a -> [o]) -> LensPhrase a -> [o]
_scLensPhrase scE n = case n of
    VariantLens{}  -> []
    RecordLens{}   -> []
    IndexLens {..} -> scE idIndex

_scLensPhrases :: (Expr a -> [o]) -> [LensPhrase a] -> [o]
_scLensPhrases scE n = concatMap (_scLensPhrase scE) n

_tfDestructPhrase :: Monad m => Tf m (Expr a) -> Tf m (DestructPhrase a)
_tfDestructPhrase rwE n = case n of
    SimpleDestruct{}      -> noOp n
    RenamingDestruct {..} -> liftCd n (RenamingDestruct rdNode rdSrc rdTgt) <$> rwLx rdPhrases
    ComplexDestruct {..}  -> liftCd n (ComplexDestruct cdNode cdSrc) <$> rwDx cdPhrases
  where
    rwLx = _tfLensPhrases rwE
    rwDx = _tfDestructPhrases rwE

_tfDestructPhrases :: Monad m => Tf m (Expr a) -> Tf m [DestructPhrase a]
_tfDestructPhrases rwE n = descendList (_tfDestructPhrase rwE) n

_scDestructPhrase :: (Expr a -> [o]) -> DestructPhrase a -> [o]
_scDestructPhrase scE n = case n of
    SimpleDestruct{}      -> []
    RenamingDestruct {..} -> _scLensPhrases scE rdPhrases
    ComplexDestruct {..}  -> _scDestructPhrases scE cdPhrases

_scDestructPhrases :: (Expr a -> [o]) -> [DestructPhrase a] -> [o]
_scDestructPhrases scE n = concatMap (_scDestructPhrase scE) n

-- | the direct op won't be defined if we're doing a transform
noOp :: Applicative m => Tf m a
noOp v = pure $ Skip v

descend0 :: Monad m => Descend -> Tf m a -> Tf m a
descend0 Ignore _      n = noOp n
descend0 _      direct n = direct n

descend1 :: Monad m => Descend -> (c1 -> a) -> m (Change c1) -> Tf m a -> Tf m a
descend1 Ignore  _      _    _      n = noOp n
descend1 Node    _      _    direct n = direct n
descend1 Descend constr arg1 direct n = do
    arg1' <- arg1
    liftCd n constr arg1' `bindC` direct

descend2 :: Monad m => Descend -> (c1 -> c2 -> a) -> m (Change c1) -> m (Change c2) -> Tf m a -> Tf m a
descend2 Ignore  _      _    _    _      n = noOp n
descend2 Node    _      _    _    direct n = direct n
descend2 Descend constr arg1 arg2 direct n = do
    arg1' <- arg1
    arg2' <- arg2
    liftCd2 n constr arg1' arg2' `bindC` direct

descend3
    :: Monad m
    => Descend
    -> (c1 -> c2 -> c3 -> a)
    -> m (Change c1)
    -> m (Change c2)
    -> m (Change c3)
    -> Tf m a
    -> Tf m a
descend3 Ignore  _      _    _    _    _      n = noOp n
descend3 Node    _      _    _    _    direct n = direct n
descend3 Descend constr arg1 arg2 arg3 direct n = do
    arg1' <- arg1
    arg2' <- arg2
    arg3' <- arg3
    liftCd3 n constr arg1' arg2' arg3' `bindC` direct

-- | Allow altering a block of items by handling a one-item list and then returning 0 or more items.
-- Results are concatenated into a new list of elements.
listList :: Monad m => Tf m [a] -> [a] -> m (Change [a])
listList rwElem n = do
    let items = map (: []) n  -- items :: [[a]]
    changes <- mapM rwElem items  -- changes :: [Change [a]]
    return $ mconcat changes

-- | Pass through list descent.
descendList :: Monad m => Tf m a -> Tf m [a]
descendList rwElem n = foldOut <$> mapM rwElem n

_scan :: Descend -> [o] -> [o] -> [o]
_scan Ignore  _  _  = []
_scan Node    nx _  = nx
_scan Descend nx sx = nx ++ sx
