{-# LANGUAGE DeriveDataTypeable, DeriveFunctor #-}

-- | The underlying data types implementing the abstract syntax tree of the Tenet language.
module Tenet.Ast.Types
    ( Argument
    , ArgumentN
    , BuiltinError(..)
    , ErrorName
    , CaseStmt(..)
    , CaseStmtN
    , Pattern(..)
    , PatternN
    , SlotPat
    , SlotPatN
    , FuncData(..)
    , FuncDataN
    , DestructPhrase(..)
    , DestructPhraseN
    , Doc(..)
    , DocN
    , CatchStmt(..)
    , CatchStmtN
    , Expr(..)
    , ExprN
    , Import(..)
    , ImportN
    , LensPhrase(..)
    , LensPhraseN
    , LetData(..)
    , LetDataN
    , LhsExpr(..)
    , LhsExprN
    , MapPair
    , MapPairN
    , ModuleName
    , Node
    , SpecialFunc(..)
    , Statement(..)
    , StatementN
    , TopStmt(..)
    , TopStmtN
    , TypeExpr(..)
    , TypeExprN
    , TypeParam
    , TypeParamN
    , AssignOp(..)
    , BinaryOp(..)
    , UnaryOp(..)
    )
where

import           Data.Data                      ( )
import           Data.Generics                  ( Data
                                                , Typeable
                                                )
import           Data.Generics.Uniplate.Data    ( )
import           Data.Text                      ( Text )
import           Tenet.Ast.Node                 ( Node )
import           Tenet.Primitives               ( AssignOp(..)
                                                , BinaryOp(..)
                                                , SpecialFunc(..)
                                                , UnaryOp(..)
                                                , BuiltinError(..)
                                                )

-- | A module name is a list of name parts, so @foo.bar.qux@ is @["foo", "bar", "qux"]@.
type ModuleName = [Text]

-- | An argument to a function call, or a record constructor.
type Argument n = (Text, Expr n)
type ArgumentN = Argument Node

-- | A key-value pair within a map constructor.
type MapPair n = (Expr n, Expr n)
type MapPairN = MapPair Node

-- | Type parameters are used in generic types.
type TypeParam n = (Text, TypeExpr n)
type TypeParamN = TypeParam Node

-- | An error name represents a builtin error, or an unknown name.
type ErrorName = Either Text BuiltinError

-- | A single imported item, part of 'ImportTs'.
data Import n
  = Import { imNode    :: n
           , imSrcName :: Text
           , imTgtName :: Text }
    -- ^ > import a.b.c.imSrcName as imTgtName
  | ImportSimple { imsNode :: n
                 , imsName :: Text }
    -- ^ > import a.b.c.imsName
  deriving (Eq, Show, Read, Typeable, Data, Functor)
type ImportN = Import Node

-- | The Tenet document is a list of top statements.
data Doc n =
  Doc [TopStmt n]
  deriving (Eq, Show, Read, Typeable, Data, Functor)
type DocN = Doc Node

-- | A top statement in a Tenet document.
data TopStmt n
  = ImportTs { itNode    :: n
             , itModule  :: ModuleName
             , itImports :: [Import n] }
    -- ^ > import ModuleName (Import, Import, Import)
  | TypeDef { tdNode   :: n
            , tdName   :: Text
            , tdParams :: [Text]
            , tdType   :: TypeExpr n }
    -- ^ > type tdName[tdParams] := tdType
  | GlobalLet (LetData n)
    -- ^ > global_name := value
  | FuncTs (FuncData n)
    -- ^ > func top_function(...) { ... }
  deriving (Eq, Show, Read, Typeable, Data, Functor)
type TopStmtN = TopStmt Node

-- | A function definition that may be used at the top level or as a nested function.
--
-- > func dName(dParams, dParams) -> dRet {
-- >     dBody
-- > }
data FuncData n = FuncData
  { dNode   :: n
  , dName   :: Text
  , dParams :: [TypeParam n]
  , dRet    :: TypeExpr n
  , dBody   :: [Statement n]
  } deriving (Eq, Show, Read, Typeable, Data, Functor)
type FuncDataN = FuncData Node

-- | An assignment may be used at the top level or within a function.
--
-- > let lTarget := lValue
data LetData n = LetData
  { lNode     :: n
  , lTarget   :: LhsExpr n
  , lAssignOp :: AssignOp
  , lValue    :: Expr n
  } deriving (Eq, Show, Read, Typeable, Data, Functor)
type LetDataN = LetData Node

-- | A type expression may be used within a type definition, or within a function signature.
data TypeExpr n
  = TypeExpr { teNode   :: n
             , teName   :: Text
             , teParams :: [TypeParam n] }
    -- ^ > teName[teParam, teParam]
  | FuncType { ftNode   :: n
             , ftParams :: [TypeParam n]
             , ftReturn :: TypeExpr n }
    -- ^ > Func(ftParam, ftParam) -> ftReturn
  | ListType { ltNode :: n
             , ltElem :: TypeExpr n }
  | SetType  { stNode :: n
             , stElem :: TypeExpr n }
  | MapType  { mtNode  :: n
             , mtKey   :: TypeExpr n
             , mtVal   :: TypeExpr n }
  | RecordType { rtNode  :: n
               , rtSlots :: [TypeParam n] }
  | UnionType { utNode  :: n
              , utTags  :: [TypeParam n] }
  | UnspecifiedType n
    -- ^ Fallback type when it's not specified.
  deriving (Eq, Show, Read, Typeable, Data, Functor)
type TypeExprN = TypeExpr Node

-- | A statement is used within functions, including control statements and the definition of nested functions.
data Statement n
  = FuncStmt { dsFunc  :: FuncData n }
    -- ^ A function funcinition.
  | LetStmt { lsLet  :: LetData n }
    -- ^ An assignment statement.
  | ForStmt { fsNode     :: n
            , fsVarName  :: Text
            , fsIterable :: Expr n
            , fsBody     :: [Statement n] }
    -- ^ > for fsVarName in fsIterable {
    -- >     fsBody
    -- > }
  | ReturnStmt { rsNode  :: n
               , rsValue :: Expr n }
    -- ^ > return rsValue
  | IfStmt { ifNode      :: n
           , ifCondition :: Expr n
           , ifThenBody  :: [Statement n]
           , ifElseBody  :: [Statement n] }
    -- ^ > if ifCondition {
    -- >     ifThenBody
    -- > } else {
    -- >     ifElseBody
    -- > }
  | SwitchStmt { ssNode    :: n
               , ssSubject :: Expr n
               , ssCases   :: [CaseStmt n] }
    -- ^ > switch ssSubject {
    -- >     ssCases
    -- > }
  | PassStmt { psNode :: n }
    -- ^ > pass
  | ContinueStmt { cnNode :: n }
    -- ^ > continue
  | BreakStmt { bsNode :: n }
    -- ^ > break
  | TryStmt { trNode      :: n
            , trTryBody    :: [Statement n]
            , trCatches    :: [CatchStmt n] }
    -- ^ > try {
    -- >     trTryBody
    -- > } catch trCatches {
    -- >     trBody
    -- > }
  | InvalidStmt { ivNode :: n
                , ivIssue :: Text
                , ivBody :: [Statement n] }
    -- ^ > __error__ "ivIssue" {
    -- >     ivBody
    -- > }
  deriving (Eq, Show, Read, Typeable, Data, Functor)
type StatementN = Statement Node

-- | Many case statements make up the body of a switch statement.
data CaseStmt n
  = CaseStmt { csNode :: n
             , csPat  :: Pattern n
             , csBody :: [Statement n] }
  -- ^ > case csPat:
  -- >   csBody
  | DefaultStmt { dsNode :: n
                , dsBody :: [Statement n] }
  -- ^ > default:
  -- >   dsBody
  deriving (Eq, Show, Read, Typeable, Data, Functor)
type CaseStmtN = CaseStmt Node

-- | A catch clause identifies the exceptions caught and statements to run.
data CatchStmt n
  = CatchStmt { ecNode :: n
              , ecExceptions :: [ErrorName]
              , ecBody :: [Statement n]
              }
  deriving (Eq, Show, Read, Typeable, Data, Functor)
type CatchStmtN = CatchStmt Node

-- | A pattern is the structure used to match an expression in a case statement.
data Pattern n
  = NamePat { npNode :: n
            , npName :: Text }
    -- ^ Matches /anything/ but binds it to a name. This does not match based on the contents of the name!
  | IntegerPat { ipNode :: n
               , ipVal  :: Integer }
    -- ^ Matches a literal integer.
  | StringPat { spNode :: n
              , spVal  :: Text }
    -- ^ Matches a literal string.
  | RecordPat { tpNode  :: n
             , tpSlots :: [SlotPat n] }
    -- ^ Matches a record pattern; form is @{slot: pattern, slot: pattern}@.
  | TagPat { upNode :: n
           , upTag  :: Text
           , upVar  :: Pattern n }
    -- ^ Matches a tag pattern; form is @tag ~ pattern@.
  | AnyPat { wpNode :: n }
    -- ^ Matches anything without binding; form is @\<*\>@.
  deriving (Eq, Show, Read, Typeable, Data, Functor)
type PatternN = Pattern Node

-- | The slot pattern alias matches a slot with a nested pattern.
type SlotPat n = (Text, Pattern n)
type SlotPatN = SlotPat Node

-- | An expression may be used as the right-hand side of an assignment, or as a return expression. It may also be used
-- within an indexing expression in an assignment.
data Expr n
  = UnExpr { ueNode :: n
           , ueOp   :: UnaryOp
           , ueExpr :: Expr n }
    -- ^ A unary expression, either prefix or postfix.
  | BinExpr { beNode  :: n
            , beOp    :: BinaryOp
            , beLeft  :: Expr n
            , beRight :: Expr n }
    -- ^ A binary expression of the form @beLeft beOp beRight@.
  | Name { neNode :: n
         , neName :: Text }
    -- ^ A variable name.
  | IntegerLit { ilNode :: n
               , ilVal  :: Integer }
    -- ^ A literal integer value.
  | StringLit { slNode :: n
              , slVal  :: Text }
    -- ^ A literal string value
  | BooleanLit { blNode :: n
               , blVal :: Bool }
    -- ^ A literal boolean value
  | RecordCon { tcNode :: n
             , tcArgs :: [Argument n] }
    -- ^ A record constructor.
  | ListCon { lcNode  :: n
            , lcElems :: [Expr n] }
    -- ^ A list constructor.
  | SetCon { seNode  :: n
           , seElems :: [Expr n] }
    -- ^ A set constructor.
  | MapCon { mcNode  :: n
           , mcPairs :: [MapPair n] }
    -- ^ A map constructor.
  | Application { apNode :: n
                , apHead :: Expr n
                , apArgs :: [Argument n] }
    -- ^ A user-defined function application of the form @apHead(apArgs)@.
  | Partial { paNode :: n
            , paHead :: Expr n
            , paArgs :: [Argument n] }
    -- ^ A partial function application of the form @paHead(paArgs, ...)@.
  | Special { scNode   :: n
            , scHead   :: SpecialFunc
            , scParams :: [Text]
            , scArgs   :: [Expr n] }
    -- ^ A special function application of the form @!!scHead(scParams, scArgs)@.
  | Indexing { ixNode  :: n
             , ixHead  :: Expr n
             , ixIndex :: Expr n }
    -- ^ A container indexing expression of the form @ixHead[ixIndex]@.
  | Guarded { geNode  :: n
            , geHead  :: Expr n
            , geGuard :: Expr n }
    -- ^ A guarded expression of the form @geHead | geGuard@.
  | Tagged { tgNode    :: n
           , tgTag     :: Text
           , tgVariant :: Expr n }
    -- ^ A tagged expression constructor of the form @tgTag ~ tgVariant@.
  | Slot { atNode  :: n
         , atSlot  :: Text
         , atValue :: Expr n }
    -- ^ A record slot dereference of the form @atValue.atSlot@.
  | Variant { vrNode  :: n
            , vrTag   :: Text
            , vrValue :: Expr n }
    -- ^ A union variant dereference of the form @vrValue ? vrTag@.
  | LambdaExpr { laNode   :: n
               , laParams :: [TypeParam n]
               , laBody   :: [Statement n] }
    -- ^ A lambda expression of the form @(laParams) -> laBody@.
  | LazyExpr { lzNode   :: n
             , lzBody   :: Expr n }
    -- ^ A lambda expression of the form @(laParams) -> laBody@.
  deriving (Eq, Show, Read, Typeable, Data, Functor)
type ExprN = Expr Node

-- | A left-hand side expression is the target of an assignment.
-- A lens indicates that the assignment is assigning to an element within
-- a the left-hand side, e.g. an element within a list.
-- A destructuring expression unpacks a record into multiple targets.
data LhsExpr n
  = LhsLens { llNode    :: n
            , llVarName :: Text
            , llPhrases :: [LensPhrase n] }
    -- ^ > llVarName[llPhrase].llPhrase ? llPhrase
  | LhsDestruct { ldNode    :: n
                , ldPhrases :: [DestructPhrase n] }
    -- ^ > {slot -> var_name, slot -> var_name}
  deriving (Eq, Show, Read, Typeable, Data, Functor)
type LhsExprN = LhsExpr Node

-- | A lens phrase is an operation in a left-hand side lensing expression.
-- Given a name, the lens steps inside the left-hand side.
data LensPhrase n
  = VariantLens { vlNode :: n
                , vlTag  :: Text }
    -- ^ > union_name ? vlTag
  | RecordLens { tlNode :: n
              , tlName :: Text }
    -- ^ > record_name.tlName
  | IndexLens { idNode  :: n
              , idIndex :: Expr n }
    -- ^ > container_name[idIndex]
  deriving (Eq, Show, Read, Typeable, Data, Functor)
type LensPhraseN = LensPhrase Node

-- | A destructuring phrase allows multiple assignment within records.
data DestructPhrase n
  = SimpleDestruct { sdNode :: n
                   , sdTgt  :: Text }
    -- ^ A simple destructuring uses the same slot and target names.
  | RenamingDestruct { rdNode    :: n
                     , rdSrc     :: Text
                     , rdTgt     :: Text
                     , rdPhrases :: [LensPhrase n] }
    -- ^ A renaming destructuring assigns a different name to the target.
  | ComplexDestruct { cdNode    :: n
                    , cdSrc     :: Text
                    , cdPhrases :: [DestructPhrase n] }
    -- ^ A complex destructuring can take apart a nested record.
  deriving (Eq, Show, Read, Typeable, Data, Functor)
type DestructPhraseN = DestructPhrase Node
