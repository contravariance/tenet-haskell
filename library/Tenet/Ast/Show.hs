{-# LANGUAGE FlexibleContexts     #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE NamedFieldPuns       #-}
{-# LANGUAGE OverloadedStrings    #-}

-- | Represents a Tenet language document as a pretty document.
module Tenet.Ast.Show
    ( FormatAst(..)
    )
where

import           Prelude                 hiding ( (<>) )

import           Data.List                      ( intersperse )
import           Data.List.NonEmpty             ( NonEmpty(..) )
import qualified Data.List.NonEmpty            as NE
import           Data.Text                      ( Text )
import           Data.Text.Prettyprint.Doc      ( cat
                                                , emptyDoc
                                                , group
                                                , hcat
                                                , hsep
                                                , line
                                                , line'
                                                , nest
                                                , pretty
                                                , punctuate
                                                , sep
                                                , softline'
                                                , space
                                                , vsep
                                                , (<+>)
                                                , (<>)
                                                )
import qualified Data.Text.Prettyprint.Doc.Internal
                                               as PPI

import           Tenet.Ast.Types
import           Tenet.Ast.Precedence           ( Ambiguous(..) )
import           Tenet.Parsing.Escapes          ( showStringLiteral )
import           Tenet.Primitives               ( showSpecial
                                                , showBError
                                                )
import           Tenet.Show.Annotations
import           Tenet.Show.Common

enclose :: (Text -> Pdoc) -> Text -> Text -> Pdoc -> Pdoc
enclose grp open close PPI.Empty = grp open <+> grp close
enclose grp open close doc       = group $ nest 4 (grp open <> line <> doc) <> line <> grp close

enclose' :: (Text -> Pdoc) -> Text -> Text -> Pdoc -> Pdoc
enclose' grp open close PPI.Empty = grp open <> grp close
enclose' grp open close doc       = group $ nest 4 (grp open <> line' <> doc) <> line' <> grp close

parens :: Pdoc -> Pdoc
parens = enclose' grouping "(" ")"

brackets :: Pdoc -> Pdoc
brackets = enclose' grouping "[" "]"

braces :: Pdoc -> Pdoc
braces = enclose grouping "{" "}"

parensType :: Pdoc -> Pdoc
parensType = enclose' typeGrouping "(" ")"

bracketsType :: Pdoc -> Pdoc
bracketsType = enclose' typeGrouping "[" "]"

bracesType :: Pdoc -> Pdoc
bracesType = enclose typeGrouping "{" "}"

precFmt :: (FormatAst ast, Ambiguous ast, Ambiguous op) => op -> ast -> Pdoc
precFmt current inside = if precedence current > precedence inside then parens $ fmt inside else fmt inside

-- | The FormatAst typeclass is used for a polymorphic implementation of the `fmt` function.
class FormatAst t where
  fmt :: t -> Pdoc

instance FormatAst SlotPatN where
    fmt (name, pat@NamePat { npName }) | name == npName = slotName name <> colon
                                       | otherwise      = slotName name <> colon <+> fmt pat
    fmt (name, pat) = slotName name <> colon <+> fmt pat

instance FormatAst TypeParamN where
    fmt (name, UnspecifiedType{}) = argName name
    fmt (name, typeExprVal      ) = argName name <> colon <+> fmt typeExprVal

fmtParams :: (Text, TypeExprN) -> Pdoc
fmtParams (name, UnspecifiedType{}) = paramName name <> colon <+> keyword "!Unspec"
fmtParams (name, typeExprVal      ) = paramName name <> colon <+> fmt typeExprVal

fmtTags :: (Text, TypeExprN) -> Pdoc
fmtTags (name, UnspecifiedType{}          ) = paramName name
fmtTags (name, RecordType { rtSlots = [] }) = sharp <> paramName name
fmtTags (name, typeExprVal                ) = paramName name <> tilde <> fmt typeExprVal

fmtArgs :: (Text, ExprN) -> Pdoc
fmtArgs (name, expr@Name { neName }) | name == neName = argName name <> colon
                                     | otherwise      = argName name <> colon <+> fmt expr
fmtArgs (name, expr) = argName name <> colon <+> fmt expr

fmtSlots :: (Text, ExprN) -> Pdoc
fmtSlots (name, expr@Name { neName }) | name == neName = slotName name <> colon
                                      | otherwise      = slotName name <> colon <+> fmt expr
fmtSlots (name, expr) = slotName name <> colon <+> fmt expr

instance FormatAst DocN where
    fmt (Doc tops) = vsep . intersperse softline' . map fmt $ tops

instance FormatAst TopStmtN where
    fmt ImportTs { itModule, itImports } =
        keyword "import" <+> dottedModuleName <+> commasMap fmt parens itImports <> ";"
        where dottedModuleName = hcat $ punctuate dot $ map moduleName itModule
    fmt TypeDef { tdName, tdParams, tdType } =
        keyword "type"
            <+> typeName tdName
            <>  opt (commasMap typeName brackets) tdParams
            <+> assign
            <+> fmt tdType
            <>  ";"
    fmt (GlobalLet dat        ) = fmt dat
    fmt (FuncTs    funcDataVal) = fmt funcDataVal

instance FormatAst ImportN where
    fmt ImportSimple { imsName }        = importName imsName
    fmt Import { imSrcName, imTgtName } = moduleName imSrcName <+> keyword "as" <+> importName imTgtName

instance FormatAst FuncDataN where
    fmt FuncData { dName, dParams, dRet, dBody } = block fmt funcClause dBody
        where funcClause = keyword "func" <+> lhsName dName <> commasMap fmt parens dParams <> arrowify dRet

instance FormatAst LetDataN where
    fmt LetData { lTarget, lAssignOp, lValue } = fmt lTarget <+> fmt lAssignOp <+> fmt lValue <> ";"

arrowify :: TypeExprN -> Pdoc
arrowify UnspecifiedType{} = emptyDoc
arrowify retVal            = space <> rightArrow <+> fmt retVal

instance FormatAst TypeExprN where
    fmt TypeExpr { teName, teParams } = typeName teName <> opt (commasMap fmtParams bracketsType) teParams
    fmt FuncType { ftParams, ftReturn } =
        typeName' "Func" <> commasMap fmtParams parensType ftParams <+> arrowify ftReturn
    fmt RecordType { rtSlots }   = commasMap fmtParams parensType rtSlots
    fmt ListType { ltElem }      = bracketsType $ fmt ltElem
    fmt SetType { stElem }       = bracesType $ fmt stElem
    fmt MapType { mtKey, mtVal } = bracesType $ fmt mtKey <> colon <+> fmt mtVal
    fmt UnionType { utTags }     = parensType . sep . operate pipe . map fmtTags $ utTags
    fmt UnspecifiedType{}        = emptyDoc

instance FormatAst StatementN where
    fmt FuncStmt { dsFunc }                       = fmt dsFunc
    fmt LetStmt { lsLet }                         = fmt lsLet
    fmt ForStmt { fsVarName, fsIterable, fsBody } = block fmt forClause fsBody
        where forClause = keyword "for" <+> lhsName fsVarName <+> keyword "in" <+> fmt fsIterable
    fmt ReturnStmt { rsValue }            = keyword "return" <+> fmt rsValue <> ";"
    fmt SwitchStmt { ssSubject, ssCases } = flatBlock fmt switchClause ssCases
        where switchClause = keyword "switch" <+> fmt ssSubject
    fmt stmt@IfStmt{}                   = chainBlocks fmt . NE.toList $ unpackChain stmt
    fmt stmt@TryStmt{}                  = chainBlocks fmt . NE.toList $ unpackChain stmt
    fmt PassStmt{}                      = keyword "pass"
    fmt ContinueStmt{}                  = keyword "continue"
    fmt BreakStmt{}                     = keyword "break"
    fmt InvalidStmt { ivIssue, ivBody } = block (errorDoc . fmt) (errorText ivIssue <> colon) ivBody

unpackChain :: StatementN -> NonEmpty (Pdoc, [StatementN])
unpackChain TryStmt { trTryBody, trCatches } =
    let tryBlock = (keyword "try", trTryBody)
        fmtCatch :: CatchStmtN -> (Pdoc, [StatementN])
        fmtCatch CatchStmt { ecExceptions, ecBody } = (keyword "catch" <+> commasMap fmt id ecExceptions, ecBody)
        catchBlocks = map fmtCatch trCatches
    in  tryBlock :| catchBlocks
unpackChain IfStmt { ifCondition, ifThenBody, ifElseBody } =
    let ifCond     = keyword "if" <+> fmt ifCondition
        thenClause = ifThenBody
        elseClause = ifElseBody
    in  case elseClause of
            [] -> (ifCond, thenClause) :| []
            [elif@IfStmt{}] ->
                let (headCond, headBody) :| tail' = unpackChain elif
                in  (ifCond, thenClause) :| (keyword "else" <+> headCond, headBody) : tail'
            _ -> (ifCond, thenClause) :| [(keyword "else", elseClause)]
unpackChain _ = error "unpackChain doesn't support this statement type."

instance FormatAst (Either Text BuiltinError) where
    fmt (Right bErr) = typeName' $ showBError bErr
    fmt (Left  txt ) = errorText txt

instance FormatAst CaseStmtN where
    fmt CaseStmt { csPat, csBody } = caseStmt fmt caseClause csBody where caseClause = keyword "case" <+> fmt csPat
    fmt DefaultStmt { dsBody }     = caseStmt fmt (keyword "default") dsBody

instance FormatAst ExprN where
    fmt g = case g of
        cur@UnExpr { ueOp, ueExpr }           -> fmt ueOp <+> precFmt cur ueExpr
        cur@BinExpr { beOp, beLeft, beRight } -> precFmt cur beLeft <+> fmt beOp <+> precFmt cur beRight
        Name { neName }                       -> rhsName neName
        IntegerLit { ilVal }                  -> literal ilVal
        StringLit { slVal }                   -> literal . showStringLiteral '"' $ slVal
        BooleanLit { blVal = True }           -> literal ("true" :: Text)
        BooleanLit { blVal = False }          -> literal ("false" :: Text)
        RecordCon { tcArgs }                  -> commasMap fmtSlots parens tcArgs
        ListCon { lcElems }                   -> commasMap fmt brackets lcElems
        SetCon { seElems }                    -> commasMap fmt braces seElems
        MapCon { mcPairs = [] }               -> grouping "{" <> punct ":" <> grouping "}"
        MapCon { mcPairs }                    -> commasMap mapPair braces mcPairs
        cur@Application { apHead, apArgs }    -> precFmt cur apHead <> commasMap fmtArgs parens apArgs
        cur@Partial { paHead, paArgs } -> precFmt cur paHead <> commas parens (map fmtArgs paArgs ++ [punct "..."])
        cur@Special { scHead, scParams, scArgs } ->
            precFmt cur scHead <> commas parens (map typeName scParams ++ map fmt scArgs)
        cur@Indexing { ixHead, ixIndex }                        -> precFmt cur ixHead <> brackets (fmt ixIndex)
        cur@Guarded { geHead, geGuard }                         -> precFmt cur geHead <+> pipe <+> precFmt cur geGuard
        Tagged { tgTag, tgVariant = RecordCon { tcArgs = [] } } -> sharp <> tagName tgTag
        cur@Tagged { tgTag, tgVariant }                         -> tagName tgTag <+> tilde <+> precFmt cur tgVariant
        cur@Slot { atValue, atSlot }                            -> precFmt cur atValue <> dot <> slotName atSlot
        cur@Variant { vrValue, vrTag }                          -> precFmt cur vrValue <+> qmark <+> tagName vrTag
        LambdaExpr { laParams, laBody } ->
            let namesDoc = hsep $ punctuate comma $ map fmt laParams
            in  block fmt (keyword "func" <+> parens namesDoc) laBody
        LazyExpr { lzBody } -> punct "#(" <+> fmt lzBody <+> punct ")"

mapPair :: (ExprN, ExprN) -> Pdoc
mapPair (key, val) = fmt key <> colon <> fmt val

instance FormatAst LhsExprN where
    fmt LhsLens { llVarName, llPhrases } = lhsName llVarName <> cat (map fmt llPhrases)
    fmt LhsDestruct { ldPhrases }        = parens (sep (punctuate comma (map fmt ldPhrases)))

instance FormatAst LensPhraseN where
    fmt VariantLens { vlTag } = qmark <+> tagName vlTag
    fmt RecordLens { tlName } = dot <> slotName tlName
    fmt IndexLens { idIndex } = brackets $ fmt idIndex

instance FormatAst DestructPhraseN where
    fmt SimpleDestruct { sdTgt } = lhsName sdTgt
    fmt RenamingDestruct { rdSrc, rdTgt, rdPhrases = [] } | rdSrc == rdTgt = lhsName rdSrc <> colon
    fmt RenamingDestruct { rdSrc, rdTgt, rdPhrases } =
        slotName rdSrc <+> colon <+> lhsName rdTgt <> cat (map fmt rdPhrases)
    fmt ComplexDestruct { cdSrc, cdPhrases } = slotName cdSrc <> colon <+> commasMap fmt parens cdPhrases

instance FormatAst PatternN where
    fmt NamePat { npName }      = lhsName npName
    fmt AnyPat{}                = punct "*"
    fmt IntegerPat { ipVal }    = literal ipVal
    fmt StringPat { spVal }     = literal . showStringLiteral '"' $ spVal
    fmt RecordPat { tpSlots }   = commasMap fmt parens tpSlots
    fmt TagPat { upTag, upVar = RecordPat { tpSlots = [] } } = sharp <> tagName upTag
    fmt TagPat { upTag, upVar } = tagName upTag <+> tilde <+> fmt upVar

instance FormatAst UnaryOp where
    fmt UnMinus = valueOp "-"
    fmt UnPlus  = valueOp "+"
    fmt UnNot   = keyword "not"

instance FormatAst BinaryOp where
    fmt BinXor          = keyword "xor"
    fmt BinEqv          = keyword "eqv"
    fmt BinImp          = keyword "imp"
    fmt BinOr           = keyword "or"
    fmt BinAnd          = keyword "and"
    fmt BinEquals       = valueOp "=="
    fmt BinNotEqual     = valueOp "!="
    fmt BinLessThan     = valueOp "<"
    fmt BinLessEqual    = valueOp "<="
    fmt BinGreaterThan  = valueOp ">"
    fmt BinGreaterEqual = valueOp ">="
    fmt BinIn           = keyword "in"
    fmt BinNotIn        = keyword "not" <+> keyword "in"
    fmt BinPlus         = valueOp "+"
    fmt BinMinus        = valueOp "-"
    fmt BinConcat       = valueOp "++"
    fmt BinUnion        = valueOp "||"
    fmt BinDiff         = valueOp "--"
    fmt BinTimes        = valueOp "*"
    fmt BinDivide       = valueOp "/"
    fmt BinFloorDivide  = valueOp "//"
    fmt BinModulo       = valueOp "%"
    fmt BinIntersect    = valueOp "&&"

instance FormatAst AssignOp where
    fmt Assign            = assignOp ":="
    fmt AssignPlus        = assignOp "+="
    fmt AssignMinus       = assignOp "-="
    fmt AssignTimes       = assignOp "*="
    fmt AssignDivide      = assignOp "/="
    fmt AssignFloorDivide = assignOp "//="
    fmt AssignModulo      = assignOp "%="

instance FormatAst SpecialFunc where
    fmt = pretty . showSpecial
