{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}

-- | Declares the Nodeable typeclass and instances of it in the AST and the symbolic-expression language.
module Tenet.Ast.Nodeable
    ( Nodeable(..)
    , (=@=)
    )
where

import           Data.Functor                   ( ($>) )
import           Tenet.Ast.Types
import           Tenet.Exprs

(=@=) :: (Functor f, Eq (f ())) => f a -> f b -> Bool
a =@= b = (a $> ()) == (b $> ())

class Nodeable h where
  getNode :: h -> Node
  setNode :: Node -> h -> h

instance Nodeable Node where
    getNode n = n
    setNode n _ = n

g :: Nodeable h => h -> Node
g = getNode

s :: Nodeable h => Node -> h -> h
s = setNode

instance (Nodeable h) => Nodeable (TopStmt h) where
    getNode (FuncTs    dd)      = g dd
    getNode (GlobalLet ld)      = g ld
    getNode ImportTs { itNode } = g itNode
    getNode TypeDef { tdNode }  = g tdNode
    setNode n (FuncTs    dd)          = FuncTs (s n dd)
    setNode n (GlobalLet ld)          = GlobalLet (s n ld)
    setNode n val@ImportTs { itNode } = val { itNode = s n itNode }
    setNode n val@TypeDef { tdNode }  = val { tdNode = s n tdNode }

instance (Nodeable h) => Nodeable (Import h) where
    getNode Import { imNode }        = g imNode
    getNode ImportSimple { imsNode } = g imsNode
    setNode n val@Import { imNode }        = val { imNode = s n imNode }
    setNode n val@ImportSimple { imsNode } = val { imsNode = s n imsNode }

instance (Nodeable h) => Nodeable (FuncData h) where
    getNode FuncData { dNode } = g dNode
    setNode n val@FuncData { dNode } = val { dNode = s n dNode }

instance (Nodeable h) => Nodeable (LetData h) where
    getNode LetData { lNode } = g lNode
    setNode n val@LetData { lNode } = val { lNode = s n lNode }

instance (Nodeable h) => Nodeable (TypeExpr h) where
    getNode (UnspecifiedType node) = g node
    getNode TypeExpr { teNode }    = g teNode
    getNode FuncType { ftNode }    = g ftNode
    getNode ListType { ltNode }    = g ltNode
    getNode SetType { stNode }     = g stNode
    getNode MapType { mtNode }     = g mtNode
    getNode RecordType { rtNode }  = g rtNode
    getNode UnionType { utNode }   = g utNode
    setNode n (UnspecifiedType node)    = UnspecifiedType (s n node)
    setNode n val@TypeExpr { teNode }   = val { teNode = s n teNode }
    setNode n val@FuncType { ftNode }   = val { ftNode = s n ftNode }
    setNode n val@ListType { ltNode }   = val { ltNode = s n ltNode }
    setNode n val@SetType { stNode }    = val { stNode = s n stNode }
    setNode n val@MapType { mtNode }    = val { mtNode = s n mtNode }
    setNode n val@RecordType { rtNode } = val { rtNode = s n rtNode }
    setNode n val@UnionType { utNode }  = val { utNode = s n utNode }

instance (Nodeable h) => Nodeable (Statement h) where
    getNode FuncStmt { dsFunc }     = getNode dsFunc
    getNode BreakStmt { bsNode }    = g bsNode
    getNode ContinueStmt { cnNode } = g cnNode
    getNode ForStmt { fsNode }      = g fsNode
    getNode IfStmt { ifNode }       = g ifNode
    getNode InvalidStmt { ivNode }  = g ivNode
    getNode LetStmt { lsLet }       = g lsLet
    getNode PassStmt { psNode }     = g psNode
    getNode ReturnStmt { rsNode }   = g rsNode
    getNode SwitchStmt { ssNode }   = g ssNode
    getNode TryStmt { trNode }      = g trNode
    setNode n val@FuncStmt { dsFunc }     = val { dsFunc = s n dsFunc }
    setNode n val@BreakStmt { bsNode }    = val { bsNode = s n bsNode }
    setNode n val@ContinueStmt { cnNode } = val { cnNode = s n cnNode }
    setNode n val@ForStmt { fsNode }      = val { fsNode = s n fsNode }
    setNode n val@IfStmt { ifNode }       = val { ifNode = s n ifNode }
    setNode n val@InvalidStmt { ivNode }  = val { ivNode = s n ivNode }
    setNode n val@LetStmt { lsLet }       = val { lsLet = s n lsLet }
    setNode n val@PassStmt { psNode }     = val { psNode = s n psNode }
    setNode n val@ReturnStmt { rsNode }   = val { rsNode = s n rsNode }
    setNode n val@SwitchStmt { ssNode }   = val { ssNode = s n ssNode }
    setNode n val@TryStmt { trNode }      = val { trNode = s n trNode }

instance (Nodeable h) => Nodeable (CaseStmt h) where
    getNode CaseStmt { csNode }    = g csNode
    getNode DefaultStmt { dsNode } = g dsNode
    setNode n val@CaseStmt { csNode }    = val { csNode = s n csNode }
    setNode n val@DefaultStmt { dsNode } = val { dsNode = s n dsNode }

instance (Nodeable h) => Nodeable (Pattern h) where
    getNode IntegerPat { ipNode } = g ipNode
    getNode NamePat { npNode }    = g npNode
    getNode AnyPat { wpNode }     = g wpNode
    getNode StringPat { spNode }  = g spNode
    getNode TagPat { upNode }     = g upNode
    getNode RecordPat { tpNode }  = g tpNode
    setNode n val@IntegerPat { ipNode } = val { ipNode = s n ipNode }
    setNode n val@NamePat { npNode }    = val { npNode = s n npNode }
    setNode n val@AnyPat { wpNode }     = val { wpNode = s n wpNode }
    setNode n val@StringPat { spNode }  = val { spNode = s n spNode }
    setNode n val@TagPat { upNode }     = val { upNode = s n upNode }
    setNode n val@RecordPat { tpNode }  = val { tpNode = s n tpNode }

instance (Nodeable h) => Nodeable (Expr h) where
    getNode Application { apNode } = g apNode
    getNode BinExpr { beNode }     = g beNode
    getNode BooleanLit { blNode }  = g blNode
    getNode Guarded { geNode }     = g geNode
    getNode Indexing { ixNode }    = g ixNode
    getNode IntegerLit { ilNode }  = g ilNode
    getNode LambdaExpr { laNode }  = g laNode
    getNode LazyExpr { lzNode }    = g lzNode
    getNode ListCon { lcNode }     = g lcNode
    getNode MapCon { mcNode }      = g mcNode
    getNode SetCon { seNode }      = g seNode
    getNode Name { neNode }        = g neNode
    getNode Partial { paNode }     = g paNode
    getNode Special { scNode }     = g scNode
    getNode StringLit { slNode }   = g slNode
    getNode Tagged { tgNode }      = g tgNode
    getNode Slot { atNode }        = g atNode
    getNode Variant { vrNode }     = g vrNode
    getNode RecordCon { tcNode }   = g tcNode
    getNode UnExpr { ueNode }      = g ueNode
    setNode n val@Application { apNode } = val { apNode = s n apNode }
    setNode n val@BooleanLit { blNode }  = val { blNode = s n blNode }
    setNode n val@BinExpr { beNode }     = val { beNode = s n beNode }
    setNode n val@Guarded { geNode }     = val { geNode = s n geNode }
    setNode n val@Indexing { ixNode }    = val { ixNode = s n ixNode }
    setNode n val@IntegerLit { ilNode }  = val { ilNode = s n ilNode }
    setNode n val@LambdaExpr { laNode }  = val { laNode = s n laNode }
    setNode n val@LazyExpr { lzNode }    = val { lzNode = s n lzNode }
    setNode n val@ListCon { lcNode }     = val { lcNode = s n lcNode }
    setNode n val@MapCon { mcNode }      = val { mcNode = s n mcNode }
    setNode n val@SetCon { seNode }      = val { seNode = s n seNode }
    setNode n val@Name { neNode }        = val { neNode = s n neNode }
    setNode n val@Partial { paNode }     = val { paNode = s n paNode }
    setNode n val@Special { scNode }     = val { scNode = s n scNode }
    setNode n val@StringLit { slNode }   = val { slNode = s n slNode }
    setNode n val@Tagged { tgNode }      = val { tgNode = s n tgNode }
    setNode n val@Slot { atNode }        = val { atNode = s n atNode }
    setNode n val@Variant { vrNode }     = val { vrNode = s n vrNode }
    setNode n val@RecordCon { tcNode }   = val { tcNode = s n tcNode }
    setNode n val@UnExpr { ueNode }      = val { ueNode = s n ueNode }

instance (Nodeable h) => Nodeable (LhsExpr h) where
    getNode LhsDestruct { ldNode } = g ldNode
    getNode LhsLens { llNode }     = g llNode
    setNode n val@LhsDestruct { ldNode } = val { ldNode = s n ldNode }
    setNode n val@LhsLens { llNode }     = val { llNode = s n llNode }

instance (Nodeable h) => Nodeable (LensPhrase h) where
    getNode IndexLens { idNode }   = g idNode
    getNode RecordLens { tlNode }  = g tlNode
    getNode VariantLens { vlNode } = g vlNode
    setNode n val@IndexLens { idNode }   = val { idNode = s n idNode }
    setNode n val@RecordLens { tlNode }  = val { tlNode = s n tlNode }
    setNode n val@VariantLens { vlNode } = val { vlNode = s n vlNode }

instance (Nodeable h) => Nodeable (DestructPhrase h) where
    getNode ComplexDestruct { cdNode }  = g cdNode
    getNode RenamingDestruct { rdNode } = g rdNode
    getNode SimpleDestruct { sdNode }   = g sdNode
    setNode n val@ComplexDestruct { cdNode }  = val { cdNode = s n cdNode }
    setNode n val@RenamingDestruct { rdNode } = val { rdNode = s n rdNode }
    setNode n val@SimpleDestruct { sdNode }   = val { sdNode = s n sdNode }

instance (Nodeable h) => Nodeable (SExpr h) where
    getNode SApp { aNode }   = g aNode
    getNode SCase { cNode }  = g cNode
    getNode SFunc { fNode }  = g fNode
    getNode SBool { lbNode } = g lbNode
    getNode SInt { liNode }  = g liNode
    getNode SLet { sNode }   = g sNode
    getNode SList { clNode } = g clNode
    getNode SMap { cmNode }  = g cmNode
    getNode SName { nNode }  = g nNode
    getNode SPrim { prNode } = g prNode
    getNode SPart { pNode }  = g pNode
    getNode SSet { ctNode }  = g ctNode
    getNode SStr { lsNode }  = g lsNode
    getNode STag { uNode }   = g uNode
    getNode STup { tNode }   = g tNode
    setNode n val@SApp { aNode }   = val { aNode = s n aNode }
    setNode n val@SCase { cNode }  = val { cNode = s n cNode }
    setNode n val@SFunc { fNode }  = val { fNode = s n fNode }
    setNode n val@SBool { lbNode } = val { lbNode = s n lbNode }
    setNode n val@SInt { liNode }  = val { liNode = s n liNode }
    setNode n val@SLet { sNode }   = val { sNode = s n sNode }
    setNode n val@SList { clNode } = val { clNode = s n clNode }
    setNode n val@SMap { cmNode }  = val { cmNode = s n cmNode }
    setNode n val@SName { nNode }  = val { nNode = s n nNode }
    setNode n val@SPrim { prNode } = val { prNode = s n prNode }
    setNode n val@SPart { pNode }  = val { pNode = s n pNode }
    setNode n val@SSet { ctNode }  = val { ctNode = s n ctNode }
    setNode n val@SStr { lsNode }  = val { lsNode = s n lsNode }
    setNode n val@STag { uNode }   = val { uNode = s n uNode }
    setNode n val@STup { tNode }   = val { tNode = s n tNode }

instance (Nodeable h) => Nodeable (Type h) where
    getNode (TBottom      n)     = g n
    getNode (TUnspecified n)     = g n
    getNode (TBool        n)     = g n
    getNode (TInt         n)     = g n
    getNode (TStr         n)     = g n
    getNode TName { tnNode }     = g tnNode
    getNode TFunction { tfNode } = g tfNode
    getNode TList { tiNode }     = g tiNode
    getNode TMap { tmNode }      = g tmNode
    getNode TSet { tsNode }      = g tsNode
    getNode TRecord { ttNode }   = g ttNode
    getNode TUnion { tuNode }    = g tuNode
    setNode n (TBottom      m)         = TBottom (s n m)
    setNode n (TUnspecified m)         = TUnspecified (s n m)
    setNode n (TBool        m)         = TBool (s n m)
    setNode n (TInt         m)         = TInt (s n m)
    setNode n (TStr         m)         = TStr (s n m)
    setNode n val@TName { tnNode }     = val { tnNode = s n tnNode }
    setNode n val@TFunction { tfNode } = val { tfNode = s n tfNode }
    setNode n val@TList { tiNode }     = val { tiNode = s n tiNode }
    setNode n val@TMap { tmNode }      = val { tmNode = s n tmNode }
    setNode n val@TSet { tsNode }      = val { tsNode = s n tsNode }
    setNode n val@TRecord { ttNode }   = val { ttNode = s n ttNode }
    setNode n val@TUnion { tuNode }    = val { tuNode = s n tuNode }

instance (Nodeable h) => Nodeable (CatchStmt h) where
    getNode CatchStmt { ecNode } = g ecNode
    setNode n val@CatchStmt { ecNode } = val { ecNode = s n ecNode }

instance (Nodeable h) => Nodeable (Generic h) where
    getNode Generic { gNode } = g gNode
    setNode n val@Generic { gNode } = val { gNode = s n gNode }
