{-# OPTIONS_GHC -Wno-unused-matches -Wno-name-shadowing #-}
{-# LANGUAGE FlexibleContexts, OverloadedStrings, TupleSections, ScopedTypeVariables #-}

module Tenet.Ast.Abstractify
    ( toDoc
    , toStmt
    , toTopStmt
    , toExpr
    , toTypeExpr
    , Out
    , tfail
    )
where

import qualified Tenet.Ast                     as A
import           Tenet.State.Out                ( Out
                                                , maybeFail
                                                , tfail
                                                )
import qualified Tenet.Primitives              as P
import qualified Tenet.Grammar.AbsTenet        as G
import           Control.Monad                  ( when )
import           Data.Convertible.Utf8          ( Convertible
                                                , convert
                                                )
import           Data.Text                      ( Text )
import           TextShow                       ( TextShow )

toDoc :: TextShow n => G.Module n -> Out (A.Doc n)
toDoc (G.MModule _ px sx) = do
    pragmas <- mapM pragmaToTop px
    stmts   <- mapM stmtToTop sx
    return $ A.Doc (pragmas ++ stmts)

toTopStmt :: TextShow n => G.TopStmt n -> Out (A.TopStmt n)
toTopStmt (G.TSPragma _ a) = pragmaToTop a
toTopStmt (G.TSStmt   _ a) = stmtToTop a

pragmaToTop :: forall n . TextShow n => G.Pragma n -> Out (A.TopStmt n)
pragmaToTop g = case g of
    G.PTenetVersion n _   -> tfail n "Version pragma not implemented"
    G.PMeta       n _  _  -> tfail n "Meta pragma not implemented"
    G.PGen        n _  _  -> tfail n "Generator pragma not implemented"
    G.PImportOne  n ax b  -> A.ImportTs n <$> mapM toModName ax <*> mapM toImport [b]
    G.PImportMany n ax bx -> A.ImportTs n <$> mapM toModName ax <*> mapM toImport bx
  where
    toModName :: G.ModName n -> Out Text
    toModName (G.TModName _ (G.ValId a)) = pure $ convert a
    toImport :: G.NameSpec n -> Out (A.Import n)
    toImport g = case g of
        G.NSValSimple n (G.ValId a)             -> pure $ A.ImportSimple n (convert a)
        G.NSValRename n (G.ValId a) (G.ValId b) -> pure $ A.Import n (convert a) (convert b)
        G.NSTypSimple n (G.TypId a)             -> pure $ A.ImportSimple n (convert a)
        G.NSTypRename n (G.TypId a) (G.TypId b) -> pure $ A.Import n (convert a) (convert b)

{- Handle statements that should only appear at the top of a module. -}
stmtToTop :: forall n . TextShow n => G.Statement n -> Out (A.TopStmt n)
stmtToTop g = case g of
    G.STypeDef n (G.TLName _ (G.TypId a)) _ b -> A.TypeDef n (convert a) [] <$> toTypeExpr b
    G.STypeDef n (G.TLParams _ (G.TypId a) bx) _ c ->
        A.TypeDef n (convert a) <$> mapM toTypeParam bx <*> toTypeExpr c
    G.SDef  n a b c    -> A.GlobalLet <$> defToLetData n a b c
    G.SLet  n a b c    -> A.GlobalLet <$> toLetData n a b c
    G.SBare n a b c    -> A.GlobalLet <$> toLetData n a b c
    G.SFunc n a bx c d -> A.FuncTs <$> toFuncData n a bx c d
    G.SIf n _ _ _      -> tfail n "If statements not allowed at top level."
    G.SSwitch n _ _    -> tfail n "Switch statements not allowed at top level."
    G.STry    n _ _    -> tfail n "Try statements not allowed at top level."
    G.SFor n _ _ _     -> tfail n "For statements not allowed at top level."
    G.SReturn n _      -> tfail n "Return statements not allowed at top level."
    G.SPass     n      -> tfail n "Pass statements not allowed at top level."
    G.SContinue n      -> tfail n "Continue statements not allowed at top level."
    G.SBreak    n      -> tfail n "Break statements not allowed at top level."
  where
    toTypeParam :: G.TypId -> Out Text
    toTypeParam (G.TypId a) = pure $ convert a

toStmt :: forall n . TextShow n => G.Statement n -> Out (A.Statement n)
toStmt g = case g of
    G.STypeDef n _ _ _                 -> tfail n "Type definitions not allowed outside top level."
    G.SDef     n a b c                 -> A.LetStmt <$> defToLetData n a b c
    G.SLet     n a b c                 -> A.LetStmt <$> toLetData n a b c
    G.SBare    n a b c                 -> A.LetStmt <$> toLetData n a b c
    G.SFunc n a bx c d                 -> A.FuncStmt <$> toFuncData n a bx c d
    G.SIf n a b c                      -> A.IfStmt n <$> toExpr a <*> toBody b <*> toElseBody c
    G.SSwitch n a (G.CBCaseBlock _ bx) -> A.SwitchStmt n <$> toExpr a <*> mapM toCaseStmt bx
    G.STry    n a bx                   -> A.TryStmt n <$> toBody a <*> mapM toCatchStmt bx
    G.SFor n (G.ValId a) b c           -> A.ForStmt n (convert a) <$> toExpr b <*> toBody c
    G.SReturn n a                      -> A.ReturnStmt n <$> toExpr a
    G.SPass     n                      -> pure $ A.PassStmt n
    G.SContinue n                      -> pure $ A.ContinueStmt n
    G.SBreak    n                      -> pure $ A.BreakStmt n
  where
    toElseBody :: G.ElseClause n -> Out [A.Statement n]
    toElseBody (G.ECEmpty _       ) = pure []
    toElseBody (G.ECElse _ a      ) = toBody a
    toElseBody (G.ECElseIf n a b c) = (: []) <$> (A.IfStmt n <$> toExpr a <*> toBody b <*> toElseBody c)

    toCaseStmt (G.CCCase n a bx ) = A.CaseStmt n <$> toPattern a <*> mapM toStmt bx
    toCaseStmt (G.CCDefault n ax) = A.DefaultStmt n <$> mapM toStmt ax

    toPattern :: G.PatternExpr n -> Out (A.Pattern n)
    toPattern g = case g of
        G.PEAny       n              -> pure $ A.AnyPat n
        G.PEBoolTrue  n              -> tfail n "Boolean patterns not implemented."
        G.PEBoolFalse n              -> tfail n "Boolean patterns not implemented."
        G.PEEnum n (G.ValId a)       -> pure $ A.TagPat n (convert a) (A.RecordPat n [])
        G.PEUnion n (G.ValId a) b    -> A.TagPat n (convert a) <$> toPattern b
        G.PEName       n (G.ValId a) -> pure $ A.NamePat n (convert a)
        G.PEInteger    n a           -> pure $ A.IntegerPat n a
        G.PEPosInteger n a           -> pure $ A.IntegerPat n a
        G.PENegInteger n a           -> pure $ A.IntegerPat n (-a)
        G.PEString     n a           -> pure $ A.StringPat n (convert a)
        G.PERecord     n ax          -> A.RecordPat n <$> mapM toSlotPat ax

    toSlotPat g = case g of
        G.SPEAbbrev n (G.ValId a) -> let a' = (convert a :: Text) in pure (a', A.NamePat n a')
        G.SPEFull n (G.ValId a) b -> (convert a, ) <$> toPattern b

    toCatchStmt (G.CCatchClause n ax b) = A.CatchStmt n <$> mapM toErrorName ax <*> toBody b

    toErrorName :: G.Exception n -> Out A.ErrorName
    toErrorName (G.EException _ (G.TypId a)) = pure $ P.readBError $ convert a

toLetData'
    :: forall ao n
     . TextShow n
    => (ao -> Out P.AssignOp)
    -> n
    -> G.LetLhsExpr n
    -> ao
    -> G.ValExpr n
    -> Out (A.LetData n)
toLetData' toAssignOp n a b c = A.LetData n <$> toLhsExpr a <*> toAssignOp b <*> toExpr c
  where
    toLhsExpr :: G.LetLhsExpr n -> Out (A.LhsExpr n)
    toLhsExpr g = case g of
        G.LLTyped n (G.ValId a) _  -> pure $ A.LhsLens n (convert a) []
        G.LLLens  n (G.ValId a) bx -> A.LhsLens n (convert a) <$> mapM toLensPhrase bx
        G.LLDestruct n ax          -> A.LhsDestruct n <$> mapM toDestructPhrase ax
    toLensPhrase :: G.LensExpr n -> Out (A.LensPhrase n)
    toLensPhrase g = case g of
        G.LEVariant n (G.ValId a) -> pure $ A.VariantLens n (convert a)
        G.LESlot    n (G.ValId a) -> pure $ A.RecordLens n (convert a)
        G.LEIndex   n a           -> A.IndexLens n <$> toExpr a
        G.LESlice n _ _           -> tfail n "Index slicing not implemented."
    toDestructPhrase :: G.DestructurePhrase n -> Out (A.DestructPhrase n)
    toDestructPhrase g = case g of
        G.DPSimple n (G.ValId a) -> pure $ A.SimpleDestruct n (convert a)
        G.DPRename n (G.ValId a) (G.ValId b) cx ->
            A.RenamingDestruct n (convert a) (convert b) <$> mapM toLensPhrase cx
        G.DPRecord n (G.ValId a) bx -> A.ComplexDestruct n (convert a) <$> mapM toDestructPhrase bx

toLetData :: TextShow n => n -> G.LetLhsExpr n -> G.AugAssignOp n -> G.ValExpr n -> Out (A.LetData n)
toLetData = toLetData' augToAssignOp

defToLetData :: TextShow n => n -> G.LetLhsExpr n -> G.AssignOp n -> G.ValExpr n -> Out (A.LetData n)
defToLetData = toLetData' simpleToAssignOp

simpleToAssignOp :: G.AssignOp n -> Out P.AssignOp
simpleToAssignOp g = case g of
    G.AAssign  _ -> pure A.Assign
    G.AAssignC _ -> pure A.Assign

augToAssignOp :: TextShow n => G.AugAssignOp n -> Out P.AssignOp
augToAssignOp g = case g of
    G.AOAssign   _ -> pure A.Assign
    G.AOAssignC  _ -> pure A.Assign
    G.AOAdd      _ -> pure A.AssignPlus
    G.AOMinus    _ -> pure A.AssignMinus
    G.AOTimes    _ -> pure A.AssignTimes
    G.AODivide   _ -> pure A.AssignDivide
    G.AOFloorDiv _ -> pure A.AssignFloorDivide
    G.AOModulo   _ -> pure A.AssignModulo
    G.AOConcat   n -> tfail n "Concatenation augmented assign not implemented"
    G.AOUnion    n -> tfail n "Union augmented assign not implemented"
    G.AOInter    n -> tfail n "Intersection augmented assign not implemented"
    G.AODiff     n -> tfail n "Difference augmented assign not implemented"


toFuncData
    :: forall n . TextShow n => n -> G.ValId -> [G.SigArg n] -> G.SigReturn n -> G.Block n -> Out (A.FuncData n)
toFuncData n (G.ValId a) bx c d = A.FuncData n (convert a) <$> sigArgsToParams bx <*> toReturn c <*> toBody d
  where
    toReturn :: G.SigReturn n -> Out (A.TypeExpr n)
    toReturn (G.SREmpty n  ) = pure $ A.UnspecifiedType n
    toReturn (G.SRArrow _ a) = toTypeExpr a

toBody :: TextShow n => G.Block n -> Out [A.Statement n]
toBody (G.BBlock _ ax) = mapM toStmt ax

toExpr :: forall n . TextShow n => G.ValExpr n -> Out (A.Expr n)
toExpr g = case g of
    G.VEGuard n a b                 -> A.Guarded n <$> t a <*> t b
    G.VELambda n a b c              -> toLambdaExpr n a b c
    G.VELogXor n a b                -> A.BinExpr n P.BinXor <$> t a <*> t b
    G.VELogEqv n a b                -> A.BinExpr n P.BinEqv <$> t a <*> t b
    G.VELogImp n a b                -> A.BinExpr n P.BinImp <$> t a <*> t b
    G.VELogOr  n a b                -> A.BinExpr n P.BinOr <$> t a <*> t b
    G.VELogAnd n a b                -> A.BinExpr n P.BinAnd <$> t a <*> t b
    G.VELogNot n a                  -> A.UnExpr n P.UnNot <$> t a
    G.VERelEq       n a b           -> A.BinExpr n P.BinEquals <$> t a <*> t b
    G.VERelNotEq    n a b           -> A.BinExpr n P.BinNotEqual <$> t a <*> t b
    G.VERelLess     n a b           -> A.BinExpr n P.BinLessThan <$> t a <*> t b
    G.VERelLessEq   n a b           -> A.BinExpr n P.BinLessEqual <$> t a <*> t b
    G.VERelGrt      n a b           -> A.BinExpr n P.BinGreaterThan <$> t a <*> t b
    G.VERelGrtEq    n a b           -> A.BinExpr n P.BinGreaterEqual <$> t a <*> t b
    G.VERelIn       n a b           -> A.BinExpr n P.BinIn <$> t a <*> t b
    G.VERelNotIn    n a b           -> A.BinExpr n P.BinNotIn <$> t a <*> t b
    G.VEAddPlus     n a b           -> A.BinExpr n P.BinPlus <$> t a <*> t b
    G.VEAddMinus    n a b           -> A.BinExpr n P.BinMinus <$> t a <*> t b
    G.VEAddConcat   n a b           -> A.BinExpr n P.BinConcat <$> t a <*> t b
    G.VEAddUnion    n a b           -> A.BinExpr n P.BinUnion <$> t a <*> t b
    G.VEAddDiff     n a b           -> A.BinExpr n P.BinDiff <$> t a <*> t b
    G.VEMulTimes    n a b           -> A.BinExpr n P.BinTimes <$> t a <*> t b
    G.VEMulDivide   n a b           -> A.BinExpr n P.BinDivide <$> t a <*> t b
    G.VEMulFloorDiv n a b           -> A.BinExpr n P.BinFloorDivide <$> t a <*> t b
    G.VEMulModulo   n a b           -> A.BinExpr n P.BinModulo <$> t a <*> t b
    G.VEMulInter    n a b           -> A.BinExpr n P.BinIntersect <$> t a <*> t b
    G.VEApply       n h a           -> A.Application n <$> t h <*> toApplyArgs a
    G.VEPartApply   n h a           -> A.Partial n <$> t h <*> toApplyArgs a
    G.VESpecial     n a b           -> toSpecial n a b
    G.VEParamSlot   n a (G.ValId b) -> A.Slot n (convert b) <$> t a
    G.VEParamVar    n a (G.ValId b) -> A.Variant n (convert b) <$> t a
    G.VEUnaryPlus  n a              -> A.UnExpr n P.UnPlus <$> t a
    G.VEUnaryMinus n a              -> A.UnExpr n P.UnMinus <$> t a
    G.VEIndex n a b                 -> A.Indexing n <$> t a <*> t b
    G.VEIndexSlice n _ _ _          -> tfail n "Index slicing not implemented"
    G.VEConUnion n (G.ValId a) b    -> A.Tagged n (convert a) <$> t b
    G.VELitInt    n a               -> pure $ A.IntegerLit n a
    G.VELitString n a               -> pure $ A.StringLit n (convert a)
    G.VELitFalse n                  -> pure $ A.BooleanLit n False
    G.VELitTrue  n                  -> pure $ A.BooleanLit n True
    G.VELitList n ax                -> A.ListCon n <$> mapM t ax
    G.VELitSet  n ax                -> A.SetCon n <$> mapM t ax
    G.VELitMapEmpty n               -> pure $ A.MapCon n []
    G.VELitMap    n ax              -> A.MapCon n <$> mapM toMapPair ax
    G.VELitEnum   n (G.ValId a)     -> pure $ A.Tagged n (convert a) (A.RecordCon n [])
    G.VELitRecord n ax              -> A.RecordCon n <$> mapM toRecArg ax
    G.VELazyExpr  n a               -> A.LazyExpr n <$> t a
    G.VEName      n (G.ValId a)     -> pure $ A.Name n (convert a)
  where
    t :: G.ValExpr n -> Out (A.Expr n)
    t = toExpr

toLambdaExpr :: TextShow n => n -> [G.SigArg n] -> G.SigReturn n -> G.Block n -> Out (A.Expr n)
toLambdaExpr n _  (G.SRArrow _ _) _ = tfail n "Lambda expressions with return types not implemented"
toLambdaExpr n ax (G.SREmpty _  ) b = A.LambdaExpr n <$> sigArgsToParams ax <*> toBody b

toTypeExpr :: forall n . TextShow n => G.TypeExpr n -> Out (A.TypeExpr n)
toTypeExpr g = case g of
    G.TEFunc    n ax          b -> A.FuncType n <$> sigArgsToParams ax <*> t b
    G.TEUnion   n a           b -> A.UnionType n <$> mapM toUnion (flatten a ++ flatten b)
    G.TEVariant n (G.ValId a) b -> do
        b' <- t b
        return $ A.UnionType n [(convert a, b')]
    G.TEList n a                    -> A.ListType n <$> t a
    G.TESet  n a                    -> A.SetType n <$> t a
    G.TEMap n a b                   -> A.MapType n <$> t a <*> t b
    G.TERecord n ax                 -> A.RecordType n <$> mapM toRec ax
    G.TEEnum   n (G.ValId a)        -> pure $ A.UnionType n [(convert a, A.RecordType n [])]
    G.TENameParams n (G.TypId a) bx -> A.TypeExpr n (convert a) <$> mapM toParam bx
    G.TEName n (G.TypId a)          -> pure $ A.TypeExpr n (convert a) []
    G.TEUnspecified n               -> pure $ A.UnspecifiedType n
  where
    t :: G.TypeExpr n -> Out (A.TypeExpr n)
    t = toTypeExpr

    flatten :: G.TypeExpr n -> [G.TypeExpr n]
    flatten (G.TEUnion _ a b) = flatten a ++ flatten b
    flatten a                 = [a]

    toUnion :: G.TypeExpr n -> Out (A.TypeParam n)
    toUnion g = case g of
        (G.TEVariant _ (G.ValId a) b) -> (convert a, ) <$> t b
        (G.TEEnum n (G.ValId a)     ) -> pure (convert a, A.RecordType n [])
        G.TEUnion n _ _               -> tfail n "[Internal error] unions should have been removed by flatten"
        _                             -> tfail (getTypeExprNode g) "Only variants and enums allowed in | operator"

    toRec :: G.RecParam n -> Out (A.TypeParam n)
    toRec (G.RPRecParam _ (G.ValId a) b) = (convert a, ) <$> t b

    toParam :: G.TypeParam n -> Out (A.TypeParam n)
    toParam (G.TPTypeParam _ (G.TypId a) b) = (convert a, ) <$> t b

toApplyArgs :: forall n . TextShow n => [G.AppArg n] -> Out [A.Argument n]
toApplyArgs ax = case ax of
    G.AAPos _ e1 : G.AAPos _ e2 : ax' -> do
        e1' <- toExpr e1
        e2' <- toExpr e2
        kw  <- mapM oneArg ax'
        return $ ("left", e1') : ("right", e2') : kw
    G.AAPos _ e1 : ax' -> do
        e1' <- toExpr e1
        kw  <- mapM oneArg ax'
        return $ ("value", e1') : kw
    ax' -> mapM oneArg ax'
  where
    oneArg :: G.AppArg n -> Out (A.Argument n)
    oneArg (G.AAKeyword _ (G.ValId a) b) = (convert a, ) <$> toExpr b
    oneArg (G.AAKwShort n (G.ValId a)  ) = let a' = (convert a :: Text) in pure (a', A.Name n a')
    oneArg (G.AAPos     n _            ) = tfail n "Positional argument not allowed here."
    oneArg (G.AAType n _ _             ) = tfail n "Generic application not implemented."

sigArgsToParams :: forall n . TextShow n => [G.SigArg n] -> Out [A.TypeParam n]
sigArgsToParams ax = mapM oneArg ax
  where
    oneArg :: G.SigArg n -> Out (A.TypeParam n)
    oneArg (G.SASimple n (G.ValId a)    ) = pure (convert a, A.UnspecifiedType n)
    oneArg (G.SAAnnotate n (G.ValId a) b) = (convert a, ) <$> toTypeExpr b
    oneArg (G.SAGeneric n _             ) = tfail n "Generic type parameters not implemented."

toSpecial :: TextShow n => n -> G.ValId -> [G.ValExpr n] -> Out (A.Expr n)
toSpecial n (G.ValId h) ax = do
    let h' = convert h :: Text
    special <- maybeFail ("No such special " <> h') (P.readSpecial h')
    let numParams      = P.specialNumParams special
    let numArgs        = P.specialNumArgs special
    let (params, args) = splitAt numParams ax
    when (length params /= numParams) (tfail n $ "For " <> h' <> ": Number of type parameters didn't match.")
    when (length args /= numArgs)     (tfail n $ "For " <> h' <> ": Number of arguments didn't match.")
    params' <- mapM toSpecialTypeParam params
    args'   <- mapM toExpr args
    pure $ A.Special n special params' args'

toSpecialTypeParam :: TextShow n => G.ValExpr n -> Out Text
toSpecialTypeParam g = case g of
    G.VEName      n s -> pc n s
    G.VELitEnum   n s -> pc n s
    G.VELitString n s -> pc' s
    e                 -> tfail (getValExprNode e) "Invalid expression found in type parameter."
  where
    pc' :: Convertible a Text => a -> Out Text
    pc' = pure . convert
    pc :: n -> G.ValId -> Out Text
    pc _ (G.ValId s) = pc' s

toMapPair :: TextShow n => G.MapPair n -> Out (A.MapPair n)
toMapPair (G.MPMapPair _ a b) = (,) <$> toExpr a <*> toExpr b

toRecArg :: TextShow n => G.RecPair n -> Out (A.Argument n)
toRecArg (G.RPShort n (G.ValId a)   ) = let a' = (convert a :: Text) in pure (a', A.Name n a')
toRecArg (G.RPNormal _ (G.ValId a) b) = (convert a, ) <$> toExpr b

getTypeExprNode :: G.TypeExpr a -> a
getTypeExprNode g = case g of
    G.TEVariant n _ _    -> n
    G.TEEnum n _         -> n
    G.TEUnion n _ _      -> n
    G.TEFunc  n _ _      -> n
    G.TEList n _         -> n
    G.TESet  n _         -> n
    G.TEMap n _ _        -> n
    G.TERecord n _       -> n
    G.TENameParams n _ _ -> n
    G.TEName n _         -> n
    G.TEUnspecified n    -> n

getValExprNode :: G.ValExpr a -> a
getValExprNode g = case g of
    G.VEGuard n _ _        -> n
    G.VELambda n _ _ _     -> n
    G.VELogXor n _ _       -> n
    G.VELogEqv n _ _       -> n
    G.VELogImp n _ _       -> n
    G.VELogOr  n _ _       -> n
    G.VELogAnd n _ _       -> n
    G.VELogNot n _         -> n
    G.VERelEq       n _ _  -> n
    G.VERelNotEq    n _ _  -> n
    G.VERelLess     n _ _  -> n
    G.VERelLessEq   n _ _  -> n
    G.VERelGrt      n _ _  -> n
    G.VERelGrtEq    n _ _  -> n
    G.VERelIn       n _ _  -> n
    G.VERelNotIn    n _ _  -> n
    G.VEAddPlus     n _ _  -> n
    G.VEAddMinus    n _ _  -> n
    G.VEAddConcat   n _ _  -> n
    G.VEAddUnion    n _ _  -> n
    G.VEAddDiff     n _ _  -> n
    G.VEMulTimes    n _ _  -> n
    G.VEMulDivide   n _ _  -> n
    G.VEMulFloorDiv n _ _  -> n
    G.VEMulModulo   n _ _  -> n
    G.VEMulInter    n _ _  -> n
    G.VEApply       n _ _  -> n
    G.VEPartApply   n _ _  -> n
    G.VESpecial     n _ _  -> n
    G.VEParamSlot   n _ _  -> n
    G.VEParamVar    n _ _  -> n
    G.VEUnaryPlus  n _     -> n
    G.VEUnaryMinus n _     -> n
    G.VEIndex n _ _        -> n
    G.VEIndexSlice n _ _ _ -> n
    G.VEConUnion n _ _     -> n
    G.VELitInt    n _      -> n
    G.VELitString n _      -> n
    G.VELitFalse n         -> n
    G.VELitTrue  n         -> n
    G.VELitList n _        -> n
    G.VELitSet  n _        -> n
    G.VELitMapEmpty n      -> n
    G.VELitMap    n _      -> n
    G.VELitEnum   n _      -> n
    G.VELitRecord n _      -> n
    G.VELazyExpr  n _      -> n
    G.VEName      n _      -> n
