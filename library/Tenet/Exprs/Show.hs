{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections     #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- | Represents a symbolic expression module as a pretty document.
module Tenet.Exprs.Show
    ( FormatSExpr(..)
    )
where

import           Prelude                 hiding ( (<>) )

import           Data.Map                       ( Map
                                                , assocs
                                                )
import           Data.Maybe                     ( maybeToList )
import           Data.Text                      ( Text )
import           Data.Text.Prettyprint.Doc      ( emptyDoc
                                                , group
                                                , hcat
                                                , line
                                                , line'
                                                , nest
                                                , punctuate
                                                , sep
                                                , vsep
                                                , (<+>)
                                                , (<>)
                                                )
import           Data.Text.Prettyprint.Doc.Internal
                                                ( Doc(Empty) )

import           Tenet.Exprs
import           Tenet.Parsing.Escapes          ( showStringLiteral )
import           Tenet.Primitives               ( showSPrim )
import           Tenet.Show.Annotations
import           Tenet.Show.Common

type Grouping = (Text, Text)

-- | Groups a list with parentheses.
parens :: Grouping
parens = ("(", ")")

-- | Group a list with braces.
braces :: Grouping
braces = ("{", "}")

-- | Group a list with brackets.
brackets :: Grouping
brackets = ("[", "]")

enclose' :: Grouping -> Pdoc -> Pdoc -> Pdoc
enclose' (open, close) Empty   Empty = grouping open <> grouping close
enclose' (open, close) Empty   doc   = group $ nest 2 (grouping open <> line' <> doc) <> line' <> grouping close
enclose' (open, close) headDoc Empty = grouping open <> headDoc <> grouping close
enclose' (open, close) headDoc doc =
    group $ nest 2 (grouping open <> headDoc <> line <> doc) <> line' <> grouping close

-- | Lay out key:val or key:: pairs.
layArgPairs :: [(Pdoc, Pdoc)] -> Pdoc
layArgPairs []   = Empty
layArgPairs args = sep $ map pArg args
  where
    pArg (key  , Empty) = key <> doubleColon
    pArg (Empty, val  ) = doubleColon <> val
    pArg (key  , val  ) = key <> colon <> val

-- | A form with a head and a list of argument pairs:
layHeadArgs :: Grouping -> Pdoc -> [(Pdoc, Pdoc)] -> Pdoc
layHeadArgs grp headDoc args = enclose' grp headDoc (layArgPairs args)

-- | A form with a head, argument pairs and body.
layHeadArgsBody :: Grouping -> Pdoc -> [(Pdoc, Pdoc)] -> Pdoc -> Pdoc
layHeadArgsBody grp headDoc args Empty   = layHeadArgs grp headDoc args
layHeadArgsBody grp headDoc []   bodyDoc = enclose' grp headDoc bodyDoc
layHeadArgsBody grp headDoc args bodyDoc = enclose' grp headDoc $ layArgPairs args <> line <> bodyDoc

-- | A form with a head and list of elems
layHeadElems :: Grouping -> Pdoc -> [Pdoc] -> Pdoc
layHeadElems grp headDoc elemDocs = enclose' grp headDoc (sep elemDocs)

-- | Unpack various forms of argument pairs that have a text name as pairs of pdocs.
unpackMap :: FormatSExpr v => (k -> Pdoc) -> Map k v -> [(Pdoc, Pdoc)]
unpackMap keyFmt args = [ (keyFmt key, fmt val) | (key, val) <- assocs args ]

-- | Unpack a signature and optional return type.
unpackSig :: forall n . Sig n -> Type n -> [(Pdoc, Pdoc)]
unpackSig sig ret =
    let pSig :: (Text, Type n) -> (Pdoc, Pdoc)
        pSig (name, typ) = case typ of
            TUnspecified _ -> (argName name, Empty)
            _              -> (argName name, fmt typ)
        pRet :: [(Pdoc, Pdoc)]
        pRet = case ret of
            TUnspecified _ -> []
            _              -> [(Empty, fmt ret)]
    in  map pSig (assocs sig) ++ pRet

-- | The FormatSExpr typeclass is used for a polymorphic implementation of the `fmt` function.
class FormatSExpr t where
  fmt :: t -> Pdoc

instance FormatSExpr (Module n) where
    fmt Mod { mTypes, mDefs, mImps } =
        let typePairs = assocs mTypes
            defPairs  = assocs mDefs
            imports   = map fmt $ assocs mImps
            params :: [Text] -> Pdoc
            params [] = emptyDoc
            params p  = enclose' brackets emptyDoc . sep . punctuate comma . map typeName $ p
            upType :: (Text, Generic n) -> Doc SynType
            upType (name, Generic { gParams, gType }) =
                    keyword "type" <+> typeName name <> params gParams <+> assign <+> fmt gType <> semicolon
            upDef :: (Text, SExpr n) -> Pdoc
            upDef (name, sExpr) = keyword "def" <+> lhsName name <+> assign <+> fmt sExpr <> semicolon
            typeDefs = map upType typePairs
            defs     = map upDef defPairs
        in  vsep $ imports ++ typeDefs ++ defs

instance FormatSExpr (Text, Imp n) where
    fmt (tgt, Imp { iModule, iSrcName }) =
        keyword "import"
            <+> (hcat . punctuate dot $ map moduleName iModule)
            <>  enclose' parens emptyDoc (importName iSrcName <+> keyword "as" <+> lhsName tgt)
            <>  semicolon

instance FormatSExpr (SExpr n) where
    fmt SBool { lbVal = False } = literal ("false" :: Text)
    fmt SBool { lbVal = True }  = literal ("true" :: Text)
    fmt SInt { liVal }          = literal liVal
    fmt SStr { lsVal }          = literal . showStringLiteral '"' $ lsVal
    fmt SName { nName }         = rhsName nName
    -- (head arg:val arg:val)
    fmt SApp { aHead, aArgs }   = layHeadArgs parens (fmt aHead) (unpackMap argName aArgs)
    -- (case head arg:val ::val)
    fmt SCase { cHead, cArgs, cWild } =
        let cls :: Text
            pArgs :: [(Pdoc, Pdoc)]
            (cls, pArgs) = case cArgs of
                CTags     args -> ("tags", unpackMap tagName args)
                CStrings  args -> ("strings", unpackMap (literal . showStringLiteral '"') args)
                CIntegers args -> ("integers", unpackMap literal args)
            pWild :: [(Pdoc, Pdoc)]
            pWild = maybeToList $ (Empty, ) . fmt <$> cWild
        in  layHeadArgs parens (keyword "case" <+> keyword cls <+> fmt cHead) (pArgs ++ pWild)
    -- {def arg:Type body}
    fmt SFunc { fSig, fRet, fBody } = layHeadArgsBody braces (keyword "def") (unpackSig fSig fRet) (fmt fBody)
    -- (let bind:val bind:val body)
    fmt SLet { sBind, sExpr }       = layHeadArgsBody parens (keyword "let") (unpackMap lhsName sBind) (fmt sExpr)
    -- [list elem elem elem]
    fmt SList { clElems }           = layHeadElems brackets (typeName "list") (map fmt clElems)
    -- [set elem elem elem]
    fmt SSet { ctElems }            = layHeadElems brackets (typeName "set") (map fmt ctElems)
    -- [map elem:elem elem:elem]
    fmt SMap { cmPairs } = layHeadArgs brackets (typeName "map") [ (fmt key, fmt val) | (key, val) <- cmPairs ]
    -- (!head arg arg arg)
    fmt SPrim { prHead, prParams, prArgs } =
        layHeadElems parens (primName $ showSPrim prHead) (map typeName prParams ++ map fmt prArgs)
    -- {head arg:val arg:val}
    fmt SPart { pHead, pArgs }                            = layHeadArgs braces (fmt pHead) (unpackMap argName pArgs)
    -- #tag
    fmt STag { uTag, uVar = STup { tArgs } } | null tArgs = sharp <> tagName uTag
    -- tag~expr
    fmt STag { uTag, uVar }                               = tagName uTag <> tilde <> fmt uVar
    -- [. arg:val arg:val]
    fmt STup { tArgs }                                    = layHeadArgs brackets dot (unpackMap slotName tArgs)

instance FormatSExpr (Type n) where
    fmt (TBottom      _) = typeName' "!Bottom"
    fmt (TUnspecified _) = typeName' "!Unspec"
    fmt (TBool        _) = typeName' "!Bool"
    fmt (TInt         _) = typeName' "!Int"
    fmt (TStr         _) = typeName' "!Str"
    -- | [TypeName param:Type param:Type]
    fmt TName { tnName, tnParams } | null tnParams = typeName tnName
                                   | otherwise = layHeadArgs brackets (typeName tnName) (unpackMap paramName tnParams)
    -- | [!Func param:Type param:Type]
    fmt TFunction { tfArgs, tfRet = (TUnspecified _) } =
        layHeadArgs brackets (typeName' "!Func") (unpackMap paramName tfArgs)
    -- | [fun param:Type param:Type ::Type]
    fmt TFunction { tfArgs, tfRet } =
        layHeadArgs brackets (typeName' "!Func") (unpackMap paramName tfArgs ++ [(Empty, fmt tfRet)])
    -- | [!List Type]
    fmt TList { tiElem }      = layHeadElems brackets (typeName' "!List") [fmt tiElem]
    -- | [!Map Type Type]
    fmt TMap { tmKey, tmVal } = layHeadElems brackets (typeName' "!Map") [fmt tmKey, fmt tmVal]
    -- | [!Set Type]
    fmt TSet { tsElem }       = layHeadElems brackets (typeName' "!Set") [fmt tsElem]
    -- | [!Record param:Type param:Type]
    fmt TRecord { ttArgs }    = layHeadArgs brackets (typeName' "!Record") (unpackMap paramName ttArgs)
    -- | [!Union param:Type param:Type]
    fmt TUnion { tuVar }      = layHeadArgs brackets (typeName' "!Union") (unpackMap paramName tuVar)
-- instance FormatSExpr (SExpr, SExpr) where
--   fmt (left, right) = fmt left <> colon <> fmt right
-- instance FormatSExpr (Text, SExpr) where
--   fmt (name, value) = argName name <> colon <> fmt value
-- instance FormatSExpr (Text, Type) where
--   fmt (name, TUnspecified {}) = typeName name <> doubleColon
--   fmt (name, typeVal)         = typeName name <> colon <> fmt typeVal
