{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}

-- | Implements parsing of the internal symbolic expression language.
module Tenet.Exprs.Parsing
    ( moduleParse
    , importStmt
    , typeDef
    , sExprDef
    , typeParse
    )
where

import           Data.Functor                   ( ($>) )
import           Data.Map                       ( Map
                                                , empty
                                                , fromList
                                                , singleton
                                                , unions
                                                )
import           Data.Maybe                     ( fromMaybe )
import           Data.Text                      ( Text )
import           Tenet.Ast.Node                 ( nullNode )
import           Tenet.Exprs
import           Tenet.Parsing.Common           ( Parser
                                                , locate
                                                , point
                                                )
import           Tenet.Parsing.Lexing           ( primitiveName
                                                , specialTypeName
                                                )
import qualified Tenet.Parsing.Lexing          as L
import           Tenet.Primitives               ( SimplePatternType(..)
                                                , SpecialTypeName(..)
                                                , primNumArgs
                                                , primNumParams
                                                )
import           Text.Megaparsec                ( count
                                                , many
                                                , notFollowedBy
                                                , optional
                                                , sepBy1
                                                , sepEndBy
                                                , try
                                                , (<|>)
                                                )

symbol = L.symbol

punct = L.punct

betweenBrackets :: Parser a -> Parser a
betweenBrackets = L.betweenBrackets

betweenCurlies :: Parser a -> Parser a
betweenCurlies = L.betweenCurlies

betweenParens :: Parser a -> Parser a
betweenParens = L.betweenParens

decimal = L.decimal

paramIdent = L.identifier
typeIdent = L.identifier
valueIdent = L.identifier

quotedString = L.quotedString

-- | The module parser is the top level parser for an s-expr file.
moduleParse :: Parser ModuleN
moduleParse = mCombine <$> many (typeDef <|> sExprDef <|> importStmt)

eMod :: ModuleN
eMod = Mod empty empty empty

mType :: Text -> GenericN -> ModuleN
mType nam val = eMod { mTypes = singleton nam val }

mDef :: Text -> SExprN -> ModuleN
mDef nam val = eMod { mDefs = singleton nam val }

mImp :: [Text] -> Text -> Text -> ModuleN
mImp iModule iSrcName tgt = eMod { mImps = singleton tgt Imp { iNode = nullNode, iModule, iSrcName } }

mCombine :: [ModuleN] -> ModuleN
mCombine mods =
    let mT = unions $ map mTypes mods
        mD = unions $ map mDefs mods
        mI = unions $ map mImps mods
    in  Mod { mTypes = mT, mDefs = mD, mImps = mI }

-- | import mod ( foo as bar ) ;
importStmt :: Parser ModuleN
importStmt =
    mImp
        <$> (symbol "import" *> (valueIdent `sepBy1` punct '.'))
        <*> (punct '(' *> valueIdent)
        <*> (symbol "as" *> valueIdent <* punct ')' <* punct ';')

-- | type FooBar := [SomeType]
-- | type FooBar[A, B, C] := [SomeType]
typeDef :: Parser ModuleN
typeDef =
    let typeParamsParse :: Parser [Text]
        typeParamsParse = fromMaybe [] <$> (optional . betweenBrackets $ typeIdent `sepEndBy` punct ',')
        genericParse    = locate $ Generic nullNode <$> typeParamsParse <*> (symbol ":=" *> typeParse <* punct ';')
    in  mType <$> (symbol "type" *> typeIdent) <*> genericParse

typeParse :: Parser TypeN
typeParse =
    try specialWordParse <|> try typeWordParse <|> try specialTypeParse <|> try functionTypeParse <|> namedTypeParse

specialWordParse :: Parser TypeN
specialWordParse =
    let typeFromName BottomName = TBottom nullNode
        typeFromName UnspecName = TUnspecified nullNode
        typeFromName BoolName   = TBool nullNode
        typeFromName IntName    = TInt nullNode
        typeFromName StrName    = TStr nullNode
        typeFromName name       = error $ show name ++ " requires type parameters."
    in  locate $ typeFromName <$> specialTypeName

typeWordParse :: Parser TypeN
typeWordParse =
    let simple tnName = TName { tnNode = nullNode, tnName, tnParams = empty } in locate $ simple <$> typeIdent

specialTypeParse :: Parser TypeN
specialTypeParse = locate . betweenBrackets $ do
    name <- specialTypeName
    case name of
        BottomName -> return $ TBottom nullNode
        UnspecName -> return $ TUnspecified nullNode
        BoolName   -> return $ TBool nullNode
        IntName    -> return $ TInt nullNode
        StrName    -> return $ TStr nullNode
      -- [!List Type]
        ListName   -> TList nullNode <$> typeParse
      -- [!Set Type]
        SetName    -> TSet nullNode <$> typeParse
      -- [!Map Type Type]
        MapName    -> TMap nullNode <$> typeParse <*> typeParse
      -- [!Record param:Type param:Type]
        RecordName -> TRecord nullNode <$> sigParse
      -- [!Union param:Type param:Type]
        UnionName  -> TUnion nullNode <$> sigParse

-- | [@ param:Type param:Type]
-- | [@ param:Type param:Type -> Type]
functionTypeParse :: Parser TypeN
functionTypeParse =
    locate
        .   betweenBrackets
        $   TFunction nullNode
        <$> (symbol "!Func" *> sigParse)
        <*> (fromMaybe (TUnspecified nullNode) <$> optional (symbol "::" *> typeParse))

-- | [TypeName Param:Type Param:Type]
namedTypeParse :: Parser TypeN
namedTypeParse = locate . betweenBrackets $ TName nullNode <$> typeIdent <*> sigParse

-- | def name := expr
sExprDef :: Parser ModuleN
sExprDef = mDef <$> (symbol "def" *> valueIdent) <*> (symbol ":=" *> sExprParse)

sExprParse :: Parser SExprN
sExprParse = first <|> second <|> third
  where
    first  = sIntParse <|> sStrParse <|> try sEnumParse <|> try sTagParse <|> try sNameParse
    second = try sAppParse <|> try sPrimParse <|> try sLetParse <|> try sPartParse <|> try sFuncParse
    third  = try sCaseParse <|> try sListParse <|> try sSetParse <|> try sMapParse <|> try sTupParse

sIntParse :: Parser SExprN
sIntParse = locate $ SInt nullNode <$> decimal

sStrParse :: Parser SExprN
sStrParse = locate $ SStr nullNode <$> quotedString

sNameParse :: Parser SExprN
sNameParse = locate $ SName nullNode <$> valueIdent <* notFollowedBy (punct '~')

-- | #tag
sEnumParse :: Parser SExprN
sEnumParse =
    let unit :: SExprN
        unit = STup { tNode = nullNode, tArgs = empty }
    in  locate $ STag nullNode <$> (punct '#' *> paramIdent) <*> point unit

-- | tag~sexpr
sTagParse :: Parser SExprN
sTagParse = locate $ STag nullNode <$> paramIdent <*> (punct '~' *> sExprParse)

-- | (head arg:val arg:val)
sAppParse :: Parser SExprN
sAppParse = locate . betweenParens $ SApp nullNode <$> sExprParse <*> argsParse

-- | (case pat arg:val ::val)
sCaseParse :: Parser SExprN
sCaseParse = locate . betweenParens $ do
    symbol "case"
    spt   <- L.simplePatternTypeName
    cHead <- sExprParse
    cArgs <- sCaseArgsParse spt
    cWild <- optional (symbol "::" *> sExprParse)
    return SCase { cNode = nullNode, cHead, cArgs, cWild }

sCaseArgsParse :: SimplePatternType -> Parser CaseArgsN
sCaseArgsParse SptTags    = CTags <$> argsParse
sCaseArgsParse SptStrings = CStrings . fromList <$> many stringArgParse
    where stringArgParse = (,) <$> quotedString <*> (punct ':' *> sExprParse)
sCaseArgsParse SptIntegers = CIntegers . fromList <$> many integerArgParse
    where integerArgParse = (,) <$> decimal <*> (punct ':' *> sExprParse)

-- | (!head arg arg arg)
sPrimParse :: Parser SExprN
sPrimParse = locate . betweenParens $ do
    name   <- primitiveName
    params <- count (primNumParams name) valueIdent
    args   <- count (primNumArgs name) sExprParse
    return SPrim { prNode = nullNode, prHead = name, prParams = params, prArgs = args }

-- | {head arg:val arg:val}
sPartParse :: Parser SExprN
sPartParse = locate . betweenCurlies $ SPart nullNode <$> sExprParse <*> argsParse

-- | [. arg:val arg:val]
sTupParse :: Parser SExprN
sTupParse = locate . betweenBrackets $ STup nullNode <$> (punct '.' *> argsParse)

-- | arg:val
argParse :: Parser (Text, SExprN)
argParse = (,) <$> paramIdent <*> (punct ':' *> sExprParse)

argsParse :: Parser (Map Text SExprN)
argsParse = fromList <$> (many . try) argParse

-- | {def arg:Type ::Type body}
sFuncParse :: Parser SExprN
sFuncParse =
    locate . betweenCurlies $ SFunc nullNode <$> (symbol "def" *> sigParse) <*> returnTypeParse <*> sExprParse

-- | ::Type
returnTypeParse :: Parser TypeN
returnTypeParse = symbol "::" *> typeParse <|> point (TUnspecified nullNode)

-- | name:Type name:Type name:: ...
sigParse :: Parser SigN
sigParse = fromList <$> (many . try) ((,) <$> paramIdent <*> colonTypeParse)
    where colonTypeParse = symbol "::" $> TUnspecified nullNode <|> punct ':' *> typeParse

-- | (@ bind:val bind:val | body)
sLetParse :: Parser SExprN
sLetParse = locate . betweenParens $ SLet nullNode <$> (symbol "let" *> argsParse) <*> sExprParse

-- | [list elem elem elem]
sListParse :: Parser SExprN
sListParse = locate . betweenBrackets $ SList nullNode <$> (symbol "list" *> many sExprParse)

-- | [set elem elem elem]
sSetParse :: Parser SExprN
sSetParse = locate . betweenBrackets $ SSet nullNode <$> (symbol "set" *> many sExprParse)

-- | [map elem:elem elem:elem]
sMapParse :: Parser SExprN
sMapParse = locate . betweenBrackets $ SMap nullNode <$> (symbol "map" *> many mapPairParse)

-- | elem:elem
mapPairParse :: Parser (SExprN, SExprN)
mapPairParse = (,) <$> sExprParse <*> (punct ':' *> sExprParse)
