{-# LANGUAGE NamedFieldPuns #-}

-- | Implements let expression collapse. This attempts to lift inner names to the containing let expression if they don't depend on names defined in the outer expression.
module Tenet.Exprs.LetCollapse
    ( lcMod
    )
where

import           Data.Generics.Uniplate.Data    ( rewrite
                                                , universe
                                                )
import qualified Data.Map.Strict               as M
import qualified Data.Set                      as S
import           Data.Text                      ( Text )
import           Tenet.Exprs

-- | Applies let collapse to the module.
lcMod :: ModuleN -> ModuleN
lcMod md@Mod { mDefs } = md { mDefs = M.map (rewrite lcExpr) mDefs }

-- We're going to match `{@ {@}}` patterns.
-- We'll lift all patterns from "inner" to "outer" if nothing in "inner" depends on any names in "outer".
lcExpr :: SExprN -> Maybe SExprN
lcExpr val@SLet { sBind = outer, sExpr = SLet { sBind = inner, sExpr = innerExpr } }
    | all notDependent (M.toList inner) && validNames = Just val { sBind = outer `M.union` inner, sExpr = innerExpr }
    | otherwise = Nothing
  where
    outerNames = M.keysSet outer
    validNames = M.keysSet inner `S.disjoint` outerNames -- Should be unnecessary due to SSA.
    notDependent :: (Text, SExprN) -> Bool
    notDependent (_, bound) = namesUsed bound `S.disjoint` outerNames
lcExpr _ = Nothing

namesUsed :: SExprN -> S.Set Text
namesUsed expr = S.fromList [ nName | SName { nName } <- universe expr ]
