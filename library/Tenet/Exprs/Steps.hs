-- | Collects the symbolic-expression language steps into a single module.
module Tenet.Exprs.Steps
    ( exDoc
    , llMod
    , lcMod
    , runTransforms
    , gatherSExprStep
    )
where

import           Tenet.Ast                      ( DocN )
import           Tenet.Deimperative             ( runFullStack )
import           Tenet.Exprs                    ( ModuleN )
import           Tenet.Exprs.Convert            ( exDoc )
import           Tenet.Exprs.LambdaLift         ( llMod )
import           Tenet.Exprs.LetCollapse        ( lcMod )

import           Tenet.State                    ( NameState
                                                , putGlobals
                                                , putNames
                                                , runNameState
                                                , setGlobals
                                                , setNames
                                                )
import qualified Tenet.State.Module            as M

-- | Runs the full deimperative stack against a Tenet document and then s-expr transformation steps.
runTransforms :: (ModuleN -> NameState ModuleN) -> DocN -> ModuleN
runTransforms monad astSrc = fst $ runNameState modState' (monad firstMod)
  where
    (finalDoc, modState) = runFullStack astSrc
    firstMod             = exDoc finalDoc
    modState'            = (`setNames` M.gatherNames firstMod) . (`setGlobals` M.gatherGlobals firstMod) $ modState

-- | A preparatory step for s-expr manipulations that load the names and globals into state.
gatherSExprStep :: ModuleN -> NameState ModuleN
gatherSExprStep md = do
    putNames $ M.gatherNames md
    putGlobals $ M.gatherGlobals md
    return md
