{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}

-- | Monad and supporting functions to lift lambdas to the top level.
module Tenet.Exprs.LambdaLift
    ( llMod
    )
where

import           Control.Monad                  ( (>=>) )
import           Data.Generics.Uniplate.Data    ( transformM )
import qualified Data.Map.Strict               as M
import           Data.Set                       ( (\\) )
import qualified Data.Set                      as S
import           Data.Text                      ( Text )
import           Tenet.Ast                      ( nullNode )
import           Tenet.Exprs
import           Tenet.State                    ( NameState
                                                , addLift
                                                , collectLifts
                                                , getGlobals
                                                , makeTemp
                                                )

sname :: Text -> SExprN
sname = SName nullNode

tunspec :: TypeN
tunspec = TUnspecified nullNode

-- | Traverse the document to find all lambdas, move them into the "lifted" state, and finally add them to the definitions.
llMod :: ModuleN -> NameState ModuleN
llMod md@Mod { mDefs } = do
    mDefs' <- traverse liftTop mDefs
    lifted <- collectLifts
    return md { mDefs = mDefs' `M.union` M.fromList lifted }

liftTop :: SExprN -> NameState SExprN
liftTop fun@SFunc { fBody } = (\body -> fun { fBody = body }) <$> liftExpr fBody
liftTop expr                = liftExpr expr

liftExpr :: SExprN -> NameState SExprN
liftExpr = transformM liftNamed >=> transformM liftAnon

liftNamed :: SExprN -> NameState SExprN
liftNamed le@SLet { sBind } = (\bind -> le { sBind = bind }) <$> M.traverseWithKey liftNamed' sBind
liftNamed other             = pure other

liftNamed' :: Text -> SExprN -> NameState SExprN
liftNamed' name func@SFunc { fNode, fSig } = do
    globals <- getGlobals
    let closure = funcClosure func globals
    addLift name func { fSig = fSig `M.union` liftSig closure }
    return $ SPart { pNode = fNode, pHead = sname name, pArgs = partArgs closure }
liftNamed' _ other = return other

funcClosure :: SExprN -> S.Set Text -> S.Set Text
funcClosure SFunc { fSig, fBody } globalNames = vars fBody \\ M.keysSet fSig \\ globalNames
funcClosure _                     _           = error "logic error!"

liftSig :: S.Set Text -> M.Map Text TypeN
liftSig = M.fromSet $ const tunspec

partArgs :: S.Set Text -> M.Map Text SExprN
partArgs = M.fromSet sname

liftAnon :: SExprN -> NameState SExprN
liftAnon func@SFunc { fNode, fSig } = do
    globals <- getGlobals
    name    <- makeTemp "anon_"
    let closure = funcClosure func globals
    addLift name func { fSig = fSig `M.union` liftSig closure }
    return $ SPart { pNode = fNode, pHead = sname name, pArgs = partArgs closure }
liftAnon other = return other

-- | Since we're already in SSA, there are no shadowed or duplicated names.
-- So we can determine what is vars by looking at what's read.
vars :: SExprN -> S.Set Text
vars SApp { aHead, aArgs }  = S.unions . map vars $ aHead : M.elems aArgs
vars SBool{}                = S.empty
vars SCase { cHead, cArgs } = S.unions . map vars $ cHead : caseElems cArgs
vars SFunc { fSig, fBody }  = vars fBody \\ M.keysSet fSig
vars SInt{}                 = S.empty
vars SLet { sBind, sExpr }  = S.unions $ (vars sExpr \\ M.keysSet sBind) : map vars (M.elems sBind)
vars SList { clElems }      = S.unions . map vars $ clElems
vars SMap { cmPairs }       = S.unions . concatMap (\(a, b) -> [vars a, vars b]) $ cmPairs
vars SName { nName }        = S.singleton nName
vars SPrim { prArgs }       = S.unions . map vars $ prArgs
vars SPart { pHead, pArgs } = S.unions . map vars $ pHead : M.elems pArgs
vars SSet { ctElems }       = S.unions . map vars $ ctElems
vars SStr{}                 = S.empty
vars STag { uVar }          = vars uVar
vars STup { tArgs }         = S.unions . map vars $ M.elems tArgs
