{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}

-- | Function and supporting functions to convert a fully transformed AST into the s-expression language.
module Tenet.Exprs.Convert
    ( exDoc
    )
where

import           Data.List                      ( foldl' )
import           Data.Map.Strict                ( empty
                                                , fromList
                                                , fromListWithKey
                                                , insert
                                                , singleton
                                                , union
                                                )
import           Data.Maybe                     ( mapMaybe )
import           Data.Text                      ( Text )
import           Tenet.Ast
import           Tenet.Deimperative.PatternDeconstruction
                                                ( PatternClass(..)
                                                , findPatternClass
                                                )
import           Tenet.Errors
import           Tenet.Exprs
import           Tenet.Primitives               ( Primitive(..) )

-- import           Tenet.Show                               (showAst)
-- | Convert a Tenet language document into an s-expression document.
exDoc :: DocN -> ModuleN
exDoc (Doc stmts) = foldl' exDoc' Mod { mTypes = empty, mImps = empty, mDefs = empty } stmts

exDoc' :: ModuleN -> TopStmtN -> ModuleN
exDoc' modV@Mod { mTypes } TypeDef { tdNode, tdName, tdParams, tdType } =
    modV { mTypes = insert tdName (exTypeDef tdNode tdParams tdType) mTypes }
exDoc' modV@Mod { mImps } ImportTs { itModule, itImports } = modV { mImps = mImps' }
  where
    decon :: ImportN -> (Text, ImpN)
    decon Import { imNode, imSrcName, imTgtName } =
        (imTgtName, Imp { iNode = imNode, iModule = itModule, iSrcName = imSrcName })
    decon ImportSimple { imsNode, imsName } =
        (imsName, Imp { iNode = imsNode, iModule = itModule, iSrcName = imsName })
    mImps' = mImps `union` fromList (map decon itImports)
exDoc' modV@Mod { mDefs } (GlobalLet LetData { lTarget = LhsLens { llVarName, llPhrases = [] }, lAssignOp = Assign, lValue = expr })
    = modV { mDefs = insert llVarName (exExpr expr) mDefs }
exDoc' _ (GlobalLet LetData { lNode }) = errorC [t "Unexpanded let at", n lNode]
exDoc' modV@Mod { mDefs } (FuncTs FuncData { dNode, dName, dParams, dRet, dBody }) = modV
    { mDefs = insert dName
                     SFunc { fNode = dNode, fSig = exSig dParams, fRet = exType dRet, fBody = exStatement dBody }
                     mDefs
    }

exTypeDef :: Node -> [Text] -> TypeExprN -> GenericN
exTypeDef node params typeExprAst = Generic { gNode = node, gParams = params, gType = exType typeExprAst }

exType :: TypeExprN -> TypeN
exType g = case g of
    UnionType { utNode, utTags }                          -> TUnion { tuNode = utNode, tuVar = exSig utTags }
    RecordType { rtNode, rtSlots }                        -> TRecord { ttNode = rtNode, ttArgs = exSig rtSlots }
    ListType { ltNode, ltElem }                           -> TList { tiNode = ltNode, tiElem = exType ltElem }
    SetType { stNode, stElem }                            -> TSet { tsNode = stNode, tsElem = exType stElem }
    MapType { mtNode, mtKey, mtVal } -> TMap { tmNode = mtNode, tmKey = exType mtKey, tmVal = exType mtVal }
    TypeExpr { teNode, teName = "Int", teParams = [] }    -> TInt teNode
    TypeExpr { teNode, teName = "Str", teParams = [] }    -> TStr teNode
    TypeExpr { teNode, teName = "Unspecified", teParams = [] } -> TUnspecified teNode
    TypeExpr { teNode, teName = "Int", teParams }         -> error0Key teNode "Integer" teParams
    TypeExpr { teNode, teName = "Str", teParams }         -> error0Key teNode "String" teParams
    TypeExpr { teNode, teName = "Unspecified", teParams } -> error0Key teNode "Unspecified" teParams
    TypeExpr { teNode, teName, teParams } -> TName { tnNode = teNode, tnName = teName, tnParams = exSig teParams }
    FuncType { ftNode, ftParams, ftReturn } ->
        TFunction { tfNode = ftNode, tfArgs = exSig ftParams, tfRet = exType ftReturn }
    (UnspecifiedType node) -> TUnspecified node

error0Key :: Node -> Text -> [TypeParamN] -> TypeN
error0Key node name params =
    errorC [t "At", n node, t "builtin type", t name, t "accepts no params, got", l fst params]

exExpr :: ExprN -> SExprN
exExpr g = case g of
    UnExpr { ueNode, ueOp, ueExpr } ->
        SPrim { prNode = ueNode, prHead = UnaryPrim ueOp, prParams = [], prArgs = [exExpr ueExpr] }
    BinExpr { beNode, beOp, beLeft, beRight } ->
        SPrim { prNode = beNode, prHead = BinaryPrim beOp, prParams = [], prArgs = [exExpr beLeft, exExpr beRight] }
    Name { neNode, neName } -> SName { nNode = neNode, nName = neName }
    Special { scNode, scHead, scParams, scArgs } ->
        SPrim { prNode = scNode, prHead = SpecialPrim scHead, prParams = scParams, prArgs = map exExpr scArgs }
    Application { apNode, apHead, apArgs } -> SApp { aNode = apNode, aHead = exExpr apHead, aArgs = exArgs apArgs }
    Partial { paNode, paHead, paArgs }     -> SPart { pNode = paNode, pHead = exExpr paHead, pArgs = exArgs paArgs }
    Indexing { ixNode, ixHead, ixIndex }   -> SPrim { prNode   = ixNode
                                                    , prHead   = SpecialPrim GetIndex
                                                    , prParams = []
                                                    , prArgs   = [exExpr ixHead, exExpr ixIndex]
                                                    }
    Slot { atNode, atSlot, atValue } ->
        SPrim { prNode = atNode, prHead = SpecialPrim GetSlot, prParams = [atSlot], prArgs = [exExpr atValue] }
    Variant { vrNode, vrTag, vrValue } ->
        SPrim { prNode = vrNode, prHead = SpecialPrim GetVariant, prParams = [vrTag], prArgs = [exExpr vrValue] }
    Guarded { geNode }                  -> neError "guard operator |" geNode
    BooleanLit { blNode, blVal }        -> SBool { lbNode = blNode, lbVal = blVal }
    IntegerLit { ilNode, ilVal }        -> SInt { liNode = ilNode, liVal = ilVal }
    StringLit { slNode, slVal }         -> SStr { lsNode = slNode, lsVal = slVal }
    Tagged { tgNode, tgTag, tgVariant } -> STag { uNode = tgNode, uTag = tgTag, uVar = exExpr tgVariant }
    RecordCon { tcNode, tcArgs }        -> STup { tNode = tcNode, tArgs = exArgs tcArgs }
    ListCon { lcNode, lcElems }         -> SList { clNode = lcNode, clElems = map exExpr lcElems }
    MapCon { mcNode, mcPairs }          -> SMap { cmNode = mcNode, cmPairs = map exMapPair mcPairs }
    SetCon { seNode, seElems }          -> SSet { ctNode = seNode, ctElems = map exExpr seElems }
    LambdaExpr { laNode, laParams, laBody } ->
        SFunc { fNode = laNode, fSig = exSig laParams, fRet = TUnspecified nullNode, fBody = exStatement laBody }
    LazyExpr { lzNode } -> neError "lazy expr #()" lzNode

exStatement :: [StatementN] -> SExprN
exStatement (g : gx) = case g of
    FuncStmt { dsFunc = FuncData { dNode, dName, dParams, dRet, dBody } } -> SLet
        { sNode = nullNode
        , sBind = dName `singleton` SFunc { fNode = dNode
                                          , fSig  = exSig dParams
                                          , fRet  = exType dRet
                                          , fBody = exStatement dBody
                                          }
        , sExpr = exStatement gx
        }
    LetStmt { lsLet = LetData { lNode, lTarget = LhsLens { llVarName, llPhrases = [] }, lAssignOp = Assign, lValue = expr } }
        -> SLet { sNode = lNode, sBind = llVarName `singleton` exExpr expr, sExpr = exStatement gx }
    LetStmt { lsLet = LetData { lNode, lTarget } } ->
        errorC [t "Complex assignment", s lTarget, t "should have been converted at", n lNode]
    ReturnStmt { rsNode, rsValue } | null gx   -> exExpr rsValue
                                   | otherwise -> errorC [t "Statements following return at ", n rsNode]
    IfStmt { ifNode, ifCondition, ifThenBody, ifElseBody }
        | null gx
        -> let cHead = exExpr ifCondition
               cArgs = CTags $ fromList [("true", exStatement ifThenBody), ("false", exStatement ifElseBody)]
           in  SCase { cNode = ifNode, cHead, cArgs, cWild = Nothing }
        | otherwise
        -> naError "if" ifNode
    SwitchStmt { ssSubject, ssCases, ssNode }
        | null gx
        -> let args' = case findPatternClass ssCases of
                   AnyP _                -> Nothing
                   RecordP node _        -> csfError [t "record pattern not removed at", n node]
                   TagP    _    _        -> Just $ exCaseTags ssCases
                   IntP _                -> Just $ exCaseInts ssCases
                   StrP _                -> Just $ exCaseStrs ssCases
                   Incompat first second -> csfError [t "incompatible patterns", s first, t "and", s second]
               wild' = exCaseWilds ssCases
               cHead = exExpr ssSubject
           in  case (args', wild') of
                   (Just args, _        ) -> SCase { cNode = ssNode, cHead, cArgs = args, cWild = wild' }
                   (Nothing  , Just wild) -> wild
                   _                      -> csfError [t "empty switch at", n ssNode]
        | otherwise
        -> naError "switch" ssNode
    PassStmt{}              -> exStatement gx
    ContinueStmt { cnNode } -> nrError "continue" cnNode
    BreakStmt { bsNode }    -> nrError "break" bsNode
    ForStmt { fsNode }      -> nrError "for" fsNode
    TryStmt { trNode }      -> nrError "try" trNode
    InvalidStmt { ivNode }  -> nrError "__error__" ivNode
exStatement [] = errorC [t "Function terminates without return"]

exCaseWilds :: [CaseStmtN] -> Maybe SExprN
exCaseWilds cases = case mapMaybe exWild cases of
    stmt : _ -> Just (exStatement stmt)
    []       -> Nothing
  where
    exWild DefaultStmt { dsBody } = Just dsBody
    exWild CaseStmt { csPat = AnyPat{}, csBody } = Just csBody
    exWild CaseStmt { csPat = NamePat { npName, npNode } } = nrError npName npNode
    exWild _                      = Nothing

exCaseTags :: [CaseStmtN] -> CaseArgsN
exCaseTags cases = CTags . fromList $ mapMaybe exCase cases
  where
    exCase CaseStmt { csPat = TagPat { upTag, upVar = AnyPat{} }, csBody } = Just (upTag, exStatement csBody)
    exCase CaseStmt { csPat = TagPat { upTag, upNode } } =
        csfError [t "variant of tag", t upTag, t "not removed at ", n upNode]
    exCase _ = Nothing

exCaseInts :: [CaseStmtN] -> CaseArgsN
exCaseInts cases = CIntegers . fromList $ mapMaybe exCase cases
  where
    exCase CaseStmt { csPat = IntegerPat { ipVal }, csBody } = Just (ipVal, exStatement csBody)
    exCase _ = Nothing

exCaseStrs :: [CaseStmtN] -> CaseArgsN
exCaseStrs cases = CStrings . fromList $ mapMaybe exCase cases
  where
    exCase CaseStmt { csPat = StringPat { spVal }, csBody } = Just (spVal, exStatement csBody)
    exCase _ = Nothing

-- exMatch :: SExprN -> PatternN -> SExprN
-- exMatch _ NamePat {npName, npNode} = nrError npName npNode
-- exMatch _ AnyPat {wpNode} = trueVal wpNode
-- exMatch subj IntegerPat {ipNode, ipVal} = pEquals subj SInt {liNode = ipNode, liVal = ipVal}
-- exMatch subj StringPat {spNode, spVal} = pEquals subj SStr {lsNode = spNode, lsVal = spVal}
-- exMatch subj TagPat {upNode, upTag, upVar} =
--   pHasTag upNode subj upTag `pAnd` exMatch (pGetVar upNode subj upTag) upVar
-- exMatch subj RecordPat {tpNode, tpSlots} = foldl' andThen (trueVal tpNode) tpSlots
--   where
--     andThen accum (name, slotName) = accum `pAnd` exMatch (pGetSlot tpNode subj name) slotName
--
-- pAnd :: SExprN -> SExprN -> SExprN
-- pAnd left right
--   | isTrue left = right
--   | isTrue right = left
--   | otherwise = SPrim {prNode = nullNode, prHead = BinaryPrim BinAnd, prParams = [], prArgs = [left, right]}
--
-- pEquals :: SExprN -> SExprN -> SExprN
-- pEquals left right = SPrim {prNode = nullNode, prHead = BinaryPrim BinEquals, prParams = [], prArgs = [left, right]}
--
-- pHasTag :: Node -> SExprN -> Text -> SExprN
-- pHasTag node base tag = SPrim {prNode = node, prHead = SpecialPrim HasTag, prParams = [tag], prArgs = [base]}
--
-- pGetVar :: Node -> SExprN -> Text -> SExprN
-- pGetVar node base tag = SPrim {prNode = node, prHead = SpecialPrim GetVariant, prParams = [tag], prArgs = [base]}
--
-- pGetSlot :: Node -> SExprN -> Text -> SExprN
-- pGetSlot node base slotName =
--   SPrim {prNode = node, prHead = SpecialPrim GetSlot, prParams = [slotName], prArgs = [base]}
--
-- isTrue :: SExprN -> Bool
-- isTrue STag {uTag = "true", uVar = STup {tArgs}} = null tArgs
-- isTrue _                                         = False
--
-- trueVal :: Node -> SExprN
-- trueVal node = STag {uNode = node, uTag = "true", uVar = STup {tNode = node, tArgs = empty}}
failReplace :: Show v => Text -> v -> v -> v
failReplace key val1 val2 = errorC [t "Redefinition of", t key, t "from", s val1, t "to", s val2]

exParam :: TypeParamN -> (Text, TypeN)
exParam (name, typeExprAst) = (name, exType typeExprAst)

exSig :: [TypeParamN] -> SigN
exSig = fromListWithKey failReplace . map exParam

exArgs :: [ArgumentN] -> ArgsN
exArgs = fromListWithKey failReplace . map exArg

exArg :: ArgumentN -> (Text, SExprN)
exArg (name, argAst) = (name, exExpr argAst)

exMapPair :: (ExprN, ExprN) -> (SExprN, SExprN)
exMapPair (key, val) = (exExpr key, exExpr val)
-- sIf :: Node -> SExpr -> SExpr -> SExpr -> SExpr
-- sIf iNode iCond iTen iElse = SIf {iNode, iCond, iThen, iElse}
-- sIf node cond then_ else_ =
--   SPrim {prNode = node, prHead = SpecialPrim IfCond, prParams = [], prArgs = [cond, then_, else_]}
