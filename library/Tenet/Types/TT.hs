module Tenet.Types.TT
    ( uUnion
    , uInter
    , aUnion
    , aInter
    , asUnion
    , asInter
    , asDiff
    , top
    , bottom
    , anti
    , adv
    , integer
    , string
    , list
    , map
    , record
    , union
    )
where

import qualified Data.Map                      as M
import           Data.Map.Merge.Strict          ( mergeA
                                                , traverseMissing
                                                , zipWithAMatched
                                                , dropMissing
                                                , preserveMissing
                                                , WhenMissing
                                                , WhenMatched
                                                )
import qualified Data.Set                      as S
import           Data.Text                      ( Text )
import           Prelude                 hiding ( map )
-- import qualified Data.Text       as T

-- | User accessible types.
data User
 = Integer
 | String
 | List User
 | Set User
 | Map User User
 | Record (M.Map Text User)
 | Union (M.Map Text User)
 | Func (M.Map Text User) User
 deriving (Eq, Ord, Show)

-- | The user-accessible union of types.
uUnion :: User -> User -> Maybe User
Integer    `uUnion` Integer    = Just Integer
String     `uUnion` String     = Just String
List a     `uUnion` List b     = List <$> a `uUnion` b
Set  a     `uUnion` Set  b     = Set <$> a `uUnion` b
Map ka va  `uUnion` Map kb vb  = Map <$> ka `uUnion` kb <*> va `uUnion` vb
Record a   `uUnion` Record b   = Record <$> mergeA nukeMissing nukeMissing combineUnion a b
Union  a   `uUnion` Union  b   = Union <$> mergeA preserveMissing preserveMissing combineUnion a b
Func aa ra `uUnion` Func ab rb = Func <$> mergeA nukeMissing nukeMissing combineInter aa ab <*> ra `uUnion` rb
_          `uUnion` _          = Nothing

-- | The user-accessible intersection of types.
uInter :: User -> User -> Maybe User
Integer    `uInter` Integer    = Just Integer
String     `uInter` String     = Just String
List a     `uInter` List b     = List <$> a `uInter` b
Set  a     `uInter` Set  b     = Set <$> a `uInter` b
Map ka va  `uInter` Map kb vb  = Map <$> ka `uInter` kb <*> va `uInter` vb
Record a   `uInter` Record b   = Record <$> mergeA nukeMissing nukeMissing combineInter a b
Union  a   `uInter` Union  b   = Union <$> mergeA dropMissing dropMissing combineInter a b
Func aa ra `uInter` Func ab rb = Func <$> mergeA nukeMissing nukeMissing combineUnion aa ab <*> ra `uInter` rb
_          `uInter` _          = Nothing

nukeMissing :: WhenMissing Maybe p1 p2 y
nukeMissing = traverseMissing nuke'
  where
    nuke' :: p1 -> p2 -> Maybe a
    nuke' _ _ = Nothing

combineUnion :: WhenMatched Maybe p User User User
combineUnion = zipWithAMatched merge'
  where
    merge' :: p -> User -> User -> Maybe User
    merge' _ = uUnion

combineInter :: WhenMatched Maybe p User User User
combineInter = zipWithAMatched merge'
  where
    merge' :: p -> User -> User -> Maybe User
    merge' _ = uInter

data Slots = Closed | Open deriving (Eq, Ord, Show)

-- | For the type inference engine, these are the simple type values.
data Smp
 = SInteger
 | SString
 | SList Adv
 | SSet Adv
 | SMap Adv Adv
 | SProd (M.Map Text Adv) Slots
 | STag Text Adv
 | SFunc (M.Map Text Adv) Adv
 deriving (Eq, Ord, Show)

-- | For the type inference engine, these are advanced type values.
data Adv
 = Pos (S.Set Smp) -- ^ Represents an undiscriminated union of *positive* simple types.
 | Neg (S.Set Smp) -- ^ Represents an undiscriminated intersection of *negative* simple types.
 deriving (Eq, Ord, Show)

-- Pos [a, b, c] -> a or b or c
-- Neg [a, b, c] -> !a and !b and !c

-- | Performs a union for the type inference engine.
aUnion :: Adv -> Adv -> Adv
Pos a `aUnion` Pos b = Pos (a `asUnion` b)
Neg a `aUnion` Neg b = Neg (a `asInter` b)
Neg a `aUnion` Pos b = Neg (a `asDiff` b)
Pos a `aUnion` Neg b = Neg (b `asDiff` a)

-- | Performs a intersection for the type inference engine.
aInter :: Adv -> Adv -> Adv
Pos a `aInter` Pos b = Pos (a `asInter` b)
Neg a `aInter` Neg b = Neg (a `asUnion` b)
Pos a `aInter` Neg b = Pos (a `asDiff` b)
Neg a `aInter` Pos b = Pos (b `asDiff` a)

-- | We take a cartesian product of two sets and the operation between the two may return some list of values,
-- generally 0, 1 or 2.
cMap :: Ord c => (a -> b -> [c]) -> S.Set a -> S.Set b -> S.Set c
cMap func a b = S.fromList . concatMap (uncurry func) . S.toList $ S.cartesianProduct a b

-- | Implements the resolution of a combined union of two sets of simple types.
asUnion :: S.Set Smp -> S.Set Smp -> S.Set Smp
asUnion sa sb = cMap aaUnion sa sb

-- | Implements the resolution of a combined intersection of two sets of simple types.
asInter :: S.Set Smp -> S.Set Smp -> S.Set Smp
asInter sa sb = cMap aaInter sa sb

-- | Implements the resolution of a combined difference of two sets of simple types.
asDiff :: S.Set Smp -> S.Set Smp -> S.Set Smp
asDiff _ _ = undefined

-- | Implements the underlying union of two simple types, possibly returning multiple types.
aaUnion :: Smp -> Smp -> [Smp]
aaUnion SInteger     SInteger     = [SInteger]
aaUnion SString      SString      = [SString]
aaUnion (SList a   ) (SList b   ) = [SList (a `aUnion` b)]
aaUnion (SSet  a   ) (SSet  b   ) = [SSet (a `aUnion` b)]
aaUnion (SMap ka va) (SMap kb vb) = [SMap (ka `aUnion` kb) (va `aUnion` vb)]
aaUnion a@(STag ta va) b@(STag tb vb) | ta == tb  = [STag ta (va `aUnion` vb)]
                                      | otherwise = [a, b]
aaUnion _ _ = []

-- | Implements the underlying intersection of two simple types, possibly returning multiple types.
aaInter :: Smp -> Smp -> [Smp]
aaInter _ _ = undefined


-- | Constructs the simplest advanced version of a simple type, the single positive.
p1 :: Smp -> Adv
p1 = Pos . S.singleton

-- | Constructs the "top" advanced type, which is an empty 'Neg'.
top :: Adv
top = Neg S.empty

-- | Constructs the "bottom" advanced type, which is an empty 'Pos'.
bottom :: Adv
bottom = Pos S.empty

-- | Finds the anti-type by flipping 'Pos' with 'Neg'.
anti :: Adv -> Adv
anti (Pos a) = Neg a
anti (Neg a) = Pos a

-- | Finds the type inference equivalent of user-accessible types.
adv :: User -> Adv
adv Integer         = p1 SInteger
adv String          = p1 SString
adv (List item    ) = (p1 . SList) (adv item)
adv (Set  item    ) = (p1 . SSet) (adv item)
adv (Map key val  ) = p1 $ SMap (adv key) (adv val)
adv (Record slots ) = p1 $ SProd (M.map adv slots) Closed
adv (Union  tags  ) = (Pos . S.fromList) [ STag tag (adv typ) | (tag, typ) <- M.toList tags ]
adv (Func args ret) = p1 $ SFunc (M.map adv args) (adv ret)

-- | Constructs a type inference equivalent of an 'Tenet.Types.TT.Integer' type.
integer :: Adv
integer = p1 SInteger

-- | Constructs a type inference equivalent of a 'Tenet.Types.TT.String' type.
string :: Adv
string = p1 SString

-- | Constructs a type inference equivalent of a 'Tenet.Types.TT.List' type.
list :: Adv -> Adv
list = p1 . SList

-- | Constructs a type inference equivalent of a 'Tenet.Types.TT.Map' type.
map :: Adv -> Adv -> Adv
map key val = p1 $ SMap key val

-- | Constructs a type inference equivalent of a 'Tenet.Types.TT.Record' type.
record :: M.Map Text Adv -> Adv
record slots = p1 $ SProd slots Closed

-- | Constructs a type inference equivalent of a 'Tenet.Types.TT.Union' type.
union :: M.Map Text Adv -> Adv
union tagMap = (Pos . S.fromList) [ STag tag typ | (tag, typ) <- M.toList tagMap ]
