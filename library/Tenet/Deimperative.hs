{-# LANGUAGE OverloadedStrings #-}

-- | The initial process of translating the AST control structures into a simpler functional form is presently called "deimperative," which is a terrible name.
-- This module collects the different steps.
module Tenet.Deimperative
    ( accumulateLoops
    , delens
    , delensAssign
    , delensAssign'
    , delensDestructuring
    , delensUnpack
    , escapeReturns
    , exciseContinues
    , exciseContinues'
    , extendIfs
    , extendIfs'
    , flagBreaks
    , fullStack
    , gatherAstStep
    , gatherNames
    , guardExprs
    , guardStmts
    , runFullStack
    , runNameState
    , runTransforms
    , simplifyPatterns
    , ssaForm
    , stages
    , stagePairs
    , unbindPatterns
    , updateImports
    )
where

import           Data.Functor                   ( ($>) )
import           Control.Monad                  ( (>=>) )
import           Data.Text                      ( Text )

import           Tenet.Ast                      ( DocN )

import           Tenet.Deimperative.BreakFlagging
                                                ( flagBreaks )
import           Tenet.Deimperative.ContinueExcision
                                                ( exciseContinues
                                                , exciseContinues'
                                                )
import           Tenet.Deimperative.Delensing   ( delens
                                                , delensAssign
                                                , delensAssign'
                                                , delensDestructuring
                                                , delensUnpack
                                                )
import           Tenet.Deimperative.IfExtension ( extendIfs
                                                , extendIfs'
                                                )
import           Tenet.Deimperative.GuardExpression
                                                ( guardExprs )
import           Tenet.Deimperative.GuardStatement
                                                ( guardStmts )
import           Tenet.Deimperative.LoopAccumulation
                                                ( accumulateLoops )
import           Tenet.Deimperative.PatternDeconstruction
                                                ( simplifyPatterns )
import           Tenet.Deimperative.PatternUnbinding
                                                ( unbindPatterns )
import           Tenet.Deimperative.ReturnEscaping
                                                ( escapeReturns )
import           Tenet.Deimperative.SsaForm     ( ssaForm )
import           Tenet.Deimperative.UpdateImports
                                                ( updateImports )

import           Tenet.State                    ( NameState
                                                , Names
                                                , initial
                                                , putNames
                                                , runNameState
                                                , setNames
                                                )
import           Tenet.State.Names              ( gatherNames )

-- | Gathers names for the AST steps
gatherAstStep :: DocN -> NameState DocN
gatherAstStep ast = do
    putNames $ gatherNames (ast $> ())
    return ast

-- | Runs a single step (or combined steps) against a source, returning the resulting AST.
runTransforms :: (DocN -> NameState DocN) -> DocN -> DocN
runTransforms monad src = fst $ runTransforms' monad src

-- | Runs a single step (or combined steps) against a source, returning the resulting AST and state.
runTransforms' :: (DocN -> NameState DocN) -> DocN -> (DocN, Names)
runTransforms' monad src = runNameState (setNames initial $ gatherNames (src $> ())) (monad src)

-- | Runs the full stack, returning the final AST and state.
runFullStack :: DocN -> (DocN, Names)
runFullStack = runTransforms' fullStack

-- | Uses a kleisi arrow to combine the deimperative steps into a single operation.
fullStack :: DocN -> NameState DocN
fullStack = foldr1 (>=>) stages

-- | The standard deimperative stages as a list of monad functions.
stages :: [DocN -> NameState DocN]
stages = [ func | (_, func) <- stagePairs ]

-- | The standard deimperative stages as a list of names and monad functions.
stagePairs :: [(Text, DocN -> NameState DocN)]
stagePairs =
    [ ("escapeReturns"   , escapeReturns)
    , ("accumulateLoops" , accumulateLoops)
    , ("guardStmts"      , guardStmts)
    , ("extendIfs"       , extendIfs)
    , ("exciseContinues" , exciseContinues)
    , ("simplifyPatterns", simplifyPatterns)
    , ("unbindPatterns"  , unbindPatterns)
    , ("delens"          , delens)
    , ("updateImports"   , updateImports)
    , ("ssaForm"         , ssaForm)
    , ("guardExprs"      , guardExprs)
    ]
