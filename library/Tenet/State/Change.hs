{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE AllowAmbiguousTypes #-}

-- | Propagate changes intelligently in transforming operations.
--
-- The problem with Maybe is that we don't want to fail (via Nothing) on the first
-- non-changing result. Rather, if any element has changed, we want to take some
-- action. The semantics of the Change applicative are that any Act will make the
-- whole result an Act, and only if everything is Skip will the operation be skipped.
--
-- Change operates over Monoid when the element type is a Monoid.
-- Thus lists of changes can be combined with <>, and Act will accumulate.
--
-- Change is Monad, so a constructor can be called and if any arguments are Acts the
-- result is an Act.
--
-- The liftC# functions can lift a constructor into a Change.
-- The liftCd# functions accept a default value to entirely bypass computing the value.

module Tenet.State.Change
    ( Change(..)
    , actify
    , bindC
    , cval
    , foldOut
    , skipify
    , skipDef
    , liftC
    , liftC2
    , liftC3
    , liftC4
    , liftCd
    , liftCd2
    , liftCd3
    , liftCd4
    )
where

-- | Single change to a value, or the absence of a change.
data Change c = Act c | Skip c deriving (Show, Eq)

-- | Extract the change value, disregarding Act or Skip.
cval :: Change c -> c
cval (Act  x) = x
cval (Skip x) = x

-- | Force a change into an Act.
actify :: Change c -> Change c
actify (Skip x) = Act x
actify x        = x

-- | Force a change into a Skip.
skipify :: Change c -> Change c
skipify (Act x) = Skip x
skipify x       = x

-- | Downgrade a change to a Skip if it equals a default.
skipDef :: Eq c => c -> Change c -> Change c
skipDef _ a@(Skip _) = a
skipDef a b@(Act x) | a == x    = Skip a
                    | otherwise = b

-- instance Functor Change where
--     fmap = liftC

-- instance Applicative Change where
--     pure = Act

--     Skip f <*> Skip x = Skip $ f x
--     f      <*> x      = Act (cval f $ cval x)

--     liftA2 = liftC2

-- instance Monad Change where
--     Skip x >>= k = k x
--     Act  x >>= k = actify $ k x

instance Semigroup c => Semigroup (Change c) where
    Skip x <> Skip y = Skip (x <> y)
    x      <> y      = Act (cval x <> cval y)

instance Monoid c => Monoid (Change c) where
    mempty = Skip mempty

-- | Promote a constructor with one argument to a change operation.
liftC :: (a1 -> r) -> Change a1 -> Change r
liftC f (Act  x) = Act $ f x
liftC f (Skip x) = Skip $ f x

-- | Promote a constructor with two arguments to a change operation.
liftC2 :: (a1 -> a2 -> r) -> Change a1 -> Change a2 -> Change r
liftC2 f (Skip x) (Skip y) = Skip (f x y)
liftC2 f x        y        = Act $ f (cval x) (cval y)

-- | Promote a constructor with three arguments to a change operation.
liftC3 :: (a1 -> a2 -> a3 -> r) -> Change a1 -> Change a2 -> Change a3 -> Change r
liftC3 f (Skip a1) (Skip a2) (Skip a3) = Skip $ f a1 a2 a3
liftC3 f a1        a2        a3        = Act $ f (cval a1) (cval a2) (cval a3)

-- | Promote a constructor with four arguments to a change operation.
liftC4 :: (a1 -> a2 -> a3 -> a4 -> r) -> Change a1 -> Change a2 -> Change a3 -> Change a4 -> Change r
liftC4 f (Skip a1) (Skip a2) (Skip a3) (Skip a4) = Skip $ f a1 a2 a3 a4
liftC4 f a1        a2        a3        a4        = Act $ f (cval a1) (cval a2) (cval a3) (cval a4)

-- | Given a default and constructor expression with one change argument, determine the resulting value.
liftCd :: r -> (a1 -> r) -> Change a1 -> Change r
liftCd d _ (Skip _ ) = Skip d
liftCd _ f (Act  a1) = Act $ f a1

-- | Given a default and constructor expression with two change arguments, determine the resulting value.
liftCd2 :: r -> (a1 -> a2 -> r) -> Change a1 -> Change a2 -> Change r
liftCd2 d _ (Skip _) (Skip _) = Skip d
liftCd2 _ f a1       a2       = Act $ f (cval a1) (cval a2)

-- | Given a default and constructor expression with three change arguments, determine the resulting value.
liftCd3 :: r -> (a1 -> a2 -> a3 -> r) -> Change a1 -> Change a2 -> Change a3 -> Change r
liftCd3 d _ (Skip _) (Skip _) (Skip _) = Skip d
liftCd3 _ f a1       a2       a3       = Act $ f (cval a1) (cval a2) (cval a3)

-- | Given a default and constructor expression with four change arguments, determine the resulting value.
liftCd4 :: r -> (a1 -> a2 -> a3 -> a4 -> r) -> Change a1 -> Change a2 -> Change a3 -> Change a4 -> Change r
liftCd4 d _ (Skip _) (Skip _) (Skip _) (Skip _) = Skip d
liftCd4 _ f a1       a2       a3       a4       = Act $ f (cval a1) (cval a2) (cval a3) (cval a4)

-- | Convert a list of changes to a change of a list.
foldOut :: [Change a] -> Change [a]
foldOut ax = foldr (liftC2 (:)) (Skip []) ax

-- | Within a monad or applicative, we can't use Change as a monad.
bindC :: Applicative m => Change a -> (a -> m (Change b)) -> m (Change b)
bindC (Skip x) f = f x
bindC (Act  x) f = actify <$> f x
