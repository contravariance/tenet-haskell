{-# LANGUAGE ScopedTypeVariables #-}

module Tenet.State.Actions
    ( OneAct(..)
    , maybeAction
    , maybeActionM
    , mergeActions
    , rewriteActions
    )
where

-- | A One(ToMany)Act(ion) either acts on an input, or ignores it entirely. Used with mergeActions or rewriteActions,
-- it lets you do rewrites in terms of a single statement that is replaced with 0 or more statements. To ignore a
-- statement, return Ignore with the original. This is used to calculate the resulting block, if all results are
-- ignored, that signals rewriteBiM to stop processing.
data OneAct n = Act [n] | Ignore [n] deriving (Show, Eq)

instance Semigroup (OneAct n) where
    Ignore ax <> Ignore bx = Ignore (ax ++ bx)
    Act    ax <> Ignore bx = Act (ax ++ bx)
    Ignore ax <> Act    bx = Act (ax ++ bx)
    Act    ax <> Act    bx = Act (ax ++ bx)

instance Monoid (OneAct n) where
    mempty = Ignore []

-- | An Act element translates to Maybe doing something, or nothing.
maybeAction :: OneAct n -> Maybe [n]
maybeAction (Act    ax) = Just ax
maybeAction (Ignore _ ) = Nothing

maybeActionM :: Monad m => ([n] -> m (OneAct n)) -> [n] -> m (Maybe [n])
maybeActionM action block = maybeAction <$> action block

-- | Any Act element will result in Just [x], and if all are Ignore, it is Nothing.
mergeActions :: [OneAct n] -> Maybe [n]
mergeActions = maybeAction . mconcat

-- | Useful for handling blocks of statements with rewriteBiM. rewriteBiM wil operate on a block (a list of
-- statements) and this lets you take one statement and replace it with a list.
rewriteActions :: Monad m => (n -> m (OneAct n)) -> [n] -> m (Maybe [n])
rewriteActions action block = mergeActions <$> mapM action block
