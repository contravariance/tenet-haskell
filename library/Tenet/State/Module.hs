{-# LANGUAGE NamedFieldPuns #-}

-- | State information and manipulation specific to the symbolic-expression language.
module Tenet.State.Module
    ( gatherGlobals
    , gatherNames
    )
where

import           Data.Generics.Uniplate.Data    ( universe )
import           Data.Map                       ( elems
                                                , keysSet
                                                )
import qualified Data.Set                      as S
import           Data.Text                      ( Text )
import           Tenet.Exprs

-- | Gathers global names in the s-expression document.
gatherGlobals :: ModuleN -> S.Set Text
gatherGlobals Mod { mDefs, mImps } = keysSet mDefs `S.union` keysSet mImps

-- | Gathers all names in the s-expression document.
gatherNames :: ModuleN -> S.Set Text
gatherNames md@Mod { mDefs } = gatherGlobals md `S.union` extractNames
    where extractNames = S.fromList $ concatMap exSexpr $ elems mDefs

exSexpr :: SExprN -> [Text]
exSexpr expr = [ nName | SName { nName } <- universe expr ]
