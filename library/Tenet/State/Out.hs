{-# LANGUAGE OverloadedStrings, FlexibleContexts #-}

module Tenet.State.Out
    ( Out
    , tfail
    , tfail'
    , liftOut
    , errorOut
    , mapLeft
    , toOut
    , maybeFail
    , applyEither
    )
where

import           Data.Convertible.Utf8          ( convert
                                                , Convertible
                                                )
import           Data.Text                      ( Text )
import           TextShow                       ( TextShow(..)
                                                , showt
                                                )

-- | Simple error handling for pure transformations.
type Out = Either Text

tfail :: TextShow a => a -> Text -> Out b
tfail n t = let n' = showt n in if n' == "()" || n' == "" then Left t else Left $ n' <> ": " <> t

tfail' :: Text -> Out a
tfail' = Left

applyEither :: (a -> c) -> (b -> c) -> Either a b -> c
applyEither f _ (Left  x) = f x
applyEither _ g (Right y) = g y

-- | Transform an Out result to monadic failure
liftOut :: MonadFail m => Out a -> m a
liftOut = applyEither (fail . convert) return

errorOut :: Out a -> a
errorOut = applyEither (error . convert) id

mapLeft :: (a -> b) -> Either a c -> Either b c
mapLeft f = applyEither (Left . f) Right

toOut :: Convertible a Text => Either a b -> Out b
toOut = mapLeft convert

maybeFail :: Convertible e Text => e -> Maybe a -> Out a
maybeFail _ (Just v) = Right v
maybeFail e Nothing  = Left $ convert e
