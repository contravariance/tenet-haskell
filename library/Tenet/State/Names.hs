{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE MonoLocalBinds    #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE AllowAmbiguousTypes #-}

-- | Tenet avoids name conflicts by maintaining a list of all names, and can thus pick free names easily.
module Tenet.State.Names
    ( gatherNames
    , findSafePrefix
    , findSafeSuffix
    , findFreeName
    , prefixStart -- exported for testing
    , prefixStop -- exported for testing
    )
where

import           Data.Generics.Uniplate.Operations
                                                ( Biplate
                                                , universeBi
                                                )
import qualified Data.Set                      as S
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Tenet.Ast

-- | Gather all names in a document.
gatherNames :: Doc () -> S.Set Text
gatherNames doc = S.delete T.empty $ S.unions [funcs, stmts, pats, exprs, lhses, destrs]
  where
    funcs  = gthr doc (exFuncData :: FuncData () -> Text)
    stmts  = gthr doc (exStmt :: Statement () -> Text)
    pats   = gthr doc (exPattern :: Pattern () -> Text)
    exprs  = gthr doc (exExpr :: Expr () -> Text)
    lhses  = gthr doc (exLhsExpr :: LhsExpr () -> Text)
    destrs = gthr doc (exDestructPhrase :: DestructPhrase () -> Text)


gthr :: (Biplate y x) => y -> (x -> Text) -> S.Set Text
gthr doc extractor = S.fromList $ map extractor $ universeBi doc

exFuncData :: FuncData n -> Text
exFuncData FuncData { dName } = dName

exStmt :: Statement n -> Text
exStmt ForStmt { fsVarName } = fsVarName
exStmt _                     = ""

exPattern :: Pattern n -> Text
exPattern NamePat { npName } = npName
exPattern _                  = ""

exExpr :: Expr n -> Text
exExpr Name { neName }   = neName
exExpr Variant { vrTag } = vrTag
exExpr Slot { atSlot }   = atSlot
exExpr _                 = ""

exLhsExpr :: LhsExpr n -> Text
exLhsExpr LhsLens { llVarName } = llVarName
exLhsExpr _                     = ""

exDestructPhrase :: DestructPhrase n -> Text
exDestructPhrase SimpleDestruct { sdTgt }   = sdTgt
exDestructPhrase RenamingDestruct { rdTgt } = rdTgt
exDestructPhrase _                          = ""

-- | The first character that prefixes may begin with. This is used to partition the set of names.
prefixStart :: Char
prefixStart = 'a'

-- | Successor of the last character that prefixes may begin with. This is used to partition the set of names.
prefixStop :: Char
prefixStop = succ 'z'

prefixStart' :: Int
prefixStart' = fromEnum prefixStart

prefixStop' :: Int
prefixStop' = fromEnum prefixStop

isPrefix :: Char -> Bool
isPrefix c = prefixStart <= c && c < prefixStop

-- | Given a list of names, finds all the prefixes consisting of characters between `prefixStart` and up to but not including `prefixStop`.
usedPrefixes :: S.Set Text -> S.Set Text
usedPrefixes = S.delete T.empty . S.map (T.takeWhile isPrefix)

-- | Given a list of names, finds a safe suffix.
findSafeSuffix :: S.Set Text -> Text
findSafeSuffix = T.reverse . findSafePrefix . S.map T.reverse

-- | Given a list of names, finds a safe prefix.
findSafePrefix :: S.Set Text -> Text
findSafePrefix names = findFree T.empty prefixStart' prefixStop' (usedPrefixes names) `T.snoc` '_'

succText :: Text -> Text
succText name | T.null name = error "Can't get successor of empty string."
              | otherwise   = T.init name `T.snoc` (succ . T.last) name

-- | Given a discriminant name, adds letters to ensure that the new
-- name is unused within the set of names.
findFreeName :: Text -> S.Set Text -> Text
findFreeName name names =
    let (_, inSet, after) = S.splitMember name names
    in  if not inSet
            then name
            else let (used, _) = S.split (succText name) after in findFree name prefixStart' prefixStop' used

getMid' :: Int -> Int -> Int
getMid' start stop | start >= stop = error "start can't be >= stop"
                   | otherwise     = (start + stop) `div` 2

-- Do a split such that left is < pivot, right is >= pivot
split :: Ord a => a -> S.Set a -> (S.Set a, S.Set a)
split pivot set =
    let (left, found, right) = S.splitMember pivot set
        right'               = if found then pivot `S.insert` right else right
    in  (left, right')

findFree :: Text -> Int -> Int -> S.Set Text -> Text
findFree prefix start stop used
    | S.null used
    = prefix `T.snoc` toEnum start
    | start + 1 >= stop
    = findFree (prefix `T.snoc` toEnum start) prefixStart' prefixStop' used
    | otherwise
    = let mid           = getMid' start stop
          (left, right) = split (prefix `T.snoc` toEnum mid) used
      in  if S.size left < S.size right then findFree prefix start mid left else findFree prefix mid stop right
