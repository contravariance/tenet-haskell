{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}


-- | Centralizes lexical analysis used by the Tenet language.
module Tenet.Parsing.Lexing
    ( decimal
    , quotedString
    , identifier
    , betweenCurlies
    , betweenParens
    , betweenBrackets
    -- End of file
    , eof
    -- Hard tokens
    , keyword
    , symbol
    , punct
    -- Custom tokens
    , specialFuncName
    , primitiveName
    , specialTypeName
    , simplePatternTypeName
    -- Common lexemes
    )
where

import           Data.CharSet                   ( member
                                                , notMember
                                                , CharSet
                                                )
import           Data.Functor                   ( ($>) )
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Tenet.Parsing.Common           ( Parser )
import           Tenet.Parsing.Escapes          ( bwAny
                                                , bwHead
                                                -- , bwHeadLc'
                                                -- , bwHeadUc
                                                , bwTail
                                                , keywords
                                                , parseStringLiteral
                                                , ueChar
                                                , ueTok
                                                )
import qualified Tenet.Parsing.ParseMap        as Pm
import           Tenet.Primitives               ( Primitive
                                                , SimplePatternType(..)
                                                , SpecialFunc(..)
                                                , SpecialTypeName
                                                , primValues
                                                , showSPrim
                                                , showSpecial
                                                , showSpecialType
                                                , specialFuncs
                                                , specialTypeNames
                                                , sptMap
                                                )
import           Text.Megaparsec                ( between
                                                , lookAhead
                                                , notFollowedBy
                                                , satisfy
                                                , takeWhile1P
                                                , unexpected
                                                , (<|>)
                                                )
import qualified Text.Megaparsec               as M
import qualified Text.Megaparsec.Char          as C
import qualified Text.Megaparsec.Char.Lexer    as L

lineCmnt :: Parser ()
lineCmnt = L.skipLineComment ";;"

blockCmnt :: Parser ()
blockCmnt = L.skipBlockComment ";{" ";}"

-- | Space consumer that accepts newlines depending on space parameter.
spaceConsumer :: Parser ()
spaceConsumer = L.space C.space1 lineCmnt blockCmnt

-- | Parse to end of file
eof :: Parser ()
eof = spaceConsumer *> M.eof

-- | Consume whitespace after each lexeme.
lexeme :: Parser a -> Parser a
lexeme = L.lexeme spaceConsumer

-- | Parse a symbol.
symbol :: Text -> Parser ()
symbol sym = lexeme (C.string sym) $> ()

-- | Parse a keyword that mustn't be followed by letters
keyword :: Text -> Parser ()
keyword sym = lexeme $ C.string sym *> notFollowedBy (satisfy (`member` bwTail))

-- | Parsers a single character.
punct :: Char -> Parser ()
punct chr = lexeme (C.char chr) $> ()

-- | Parses a name as an identifier that meets an expected classification.
bareword_ :: CharSet -> Parser Text
bareword_ headCs = lexeme $ look >>= resolve
  where
    look :: Parser Text
    look = lookAhead $ takeWhile1P Nothing (`member` bwAny)
    resolve :: Text -> Parser Text
    resolve word | T.head word `notMember` headCs = unexpected . ueChar $ T.head word
                 | word `elem` keywords           = unexpected (ueTok word)
                 | T.any (`notMember` bwTail) (T.tail word) = unexpected (ueTok word)
                 | otherwise                      = C.string word

-- | Parses a name that identifies a global or local value.
-- valueIdent :: Parser Text
-- valueIdent = bareword_ bwHeadLc'

-- | Parses a name that identifies a parameter such as a slot or tag.
-- paramIdent :: Parser Text
-- paramIdent = bareword_ bwHeadLc'

-- | Parses a name that identifies a type name.
-- typeIdent :: Parser Text
-- typeIdent = bareword_ bwHeadUc

quotedString_ :: Parser Text
quotedString_ = parseStringLiteral '"'

-- | Parses a quoted string expression and the trailing whitespace.
quotedString :: Parser Text
quotedString = lexeme quotedString_

quotedSymbol_ :: Parser Text
quotedSymbol_ = notFollowedBy "``" *> parseStringLiteral '`'

-- | Parse a name as a string.
identifier :: Parser Text
identifier = lexeme $ quotedSymbol_ <|> bareword_ bwHead

-- | Parse a literal integer value.
decimal :: Parser Integer
decimal = lexeme L.decimal

-- | Parses an expression between curly braces.
betweenCurlies :: Parser a -> Parser a
betweenCurlies = between (punct '{') (punct '}')

-- | Parses an expression between parentheses.
betweenParens :: Parser a -> Parser a
betweenParens = between (punct '(') (punct ')')

-- | Parses an expression between square brackets.
betweenBrackets :: Parser a -> Parser a
betweenBrackets = between (punct '[') (punct ']')

-- | Case class in symbolic expressions
simplePatternTypeMap :: Pm.ParseMap SimplePatternType
simplePatternTypeMap = Pm.fromList sptMap

-- | Parse the type of a case in a symbolic expression
simplePatternTypeName :: Parser SimplePatternType
simplePatternTypeName = lexeme $ Pm.parseToken simplePatternTypeMap

specialFuncMap :: Pm.ParseMap SpecialFunc
specialFuncMap = Pm.fromList $ zip (map showSpecial specialFuncs) specialFuncs

-- | Parses a double-bang prefixed special function name.
specialFuncName :: Parser SpecialFunc
specialFuncName = lexeme $ Pm.parseToken specialFuncMap

sPrimNameMap :: Pm.ParseMap Primitive
sPrimNameMap = Pm.fromList $ zip (map showSPrim primValues) primValues

-- | Parses a single-bang prefixed primitive function name in the symbolic-expression language.
primitiveName :: Parser Primitive
primitiveName = lexeme $ Pm.parseToken sPrimNameMap

specialTypeNameMap :: Pm.ParseMap SpecialTypeName
specialTypeNameMap = Pm.fromList $ zip (map showSpecialType specialTypeNames) specialTypeNames

-- | Parses a single-bang prefixed special type name in the symbolic-expression language.
specialTypeName :: Parser SpecialTypeName
specialTypeName = lexeme $ Pm.parseToken specialTypeNameMap
