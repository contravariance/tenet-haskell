{-# LANGUAGE GADTs          #-}
{-# LANGUAGE NamedFieldPuns #-}

-- | Supporting functions to enable parsing keywords efficiently by using lookahead.
--
-- To do this, the ParseMap will:
--
-- * take a list of possible keys,
-- * group keys by length,
-- * then parseToken will look ahead for the longest possible token,
-- * it will then attempt to match until it finds a value.
module Tenet.Parsing.ParseMap
    ( ParseMap
    , fromList
    , parseToken
    )
where

import qualified Data.CharSet                  as Cs
import qualified Data.IntMap                   as I
import qualified Data.Map                      as M
import           Data.Maybe                     ( listToMaybe
                                                , mapMaybe
                                                )
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Text.Megaparsec                ( MonadParsec(..)
                                                , Token
                                                , count'
                                                , getOffset
                                                , lookAhead
                                                , parseError
                                                , satisfy
                                                )
import           Text.Megaparsec.Error.Builder  ( err
                                                , ulabel
                                                )

-- | A ParseMap maps tokens to values.
data ParseMap val = ParseMap
  { parseMap :: I.IntMap (M.Map Text val)
  , charSet  :: Cs.CharSet
  }

keyMap :: [(Text, val)] -> I.IntMap (M.Map Text val)
keyMap = I.fromListWith M.union . map byLength
  where
    byLength :: (Text, val) -> (Int, M.Map Text val)
    byLength (key, value) = (T.length key, M.singleton key value)

keyChars :: [Text] -> Cs.CharSet
keyChars = Cs.fromList . concatMap T.unpack

-- | Construct a 'ParseMap' form a list of keys and values they're mapped to.
fromList :: [(Text, val)] -> ParseMap val
fromList list = ParseMap (keyMap list) (keyChars . map fst $ list)

type ParseOps val = [(Int, M.Map Text val)]

lengthAndLess :: I.IntMap (M.Map Text val) -> Int -> ParseOps val
lengthAndLess pm len = case I.splitLookup len pm of
    (lessThan, Just atLen, _) -> (len, atLen) : I.toDescList lessThan
    (lessThan, Nothing   , _) -> I.toDescList lessThan

matchParseOps :: Text -> ParseOps val -> Maybe (Int, val)
matchParseOps text po =
    let candidate (len, txtMap) = (len, M.lookup (T.take len text) txtMap)
        valid :: (Int, Maybe val) -> Maybe (Int, val)
        valid (len, Just val) = Just (len, val)
        valid (_  , Nothing ) = Nothing
    in  listToMaybe . mapMaybe (valid . candidate) $ po

-- | Given a 'ParseMap', this constructs a parser to recognize a token and return the related value.
parseToken :: (MonadParsec e s m, Token s ~ Char) => ParseMap val -> m val
parseToken pm =
    let ParseMap { charSet, parseMap } = pm
        short                          = fst . I.findMin $ parseMap
        long                           = fst . I.findMax $ parseMap
        valid                          = satisfy (`Cs.member` charSet)
    in  do
            offset <- getOffset
            string <- lookAhead $ count' short long valid
            let text = T.pack string
            let ops  = lengthAndLess parseMap (T.length text)
            case matchParseOps text ops of
                Nothing           -> parseError $ err offset (ulabel string)
                Just (len, value) -> do
                    _ <- takeP (Just $ T.unpack text) len
                    return value
