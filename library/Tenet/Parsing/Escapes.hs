{-# LANGUAGE GADTs             #-}
{-# LANGUAGE OverloadedStrings #-}

-- | Standard routines for parsing and showing textual escapes.
module Tenet.Parsing.Escapes
    ( showCharLiteral
    , parseStringLiteral
    , showStringLiteral
    , isNotBareword
    , showIdentifier
    , bwHead
    , bwHeadLc
    , bwHeadLc'
    , bwHeadUc
    , bwTail
    , bwAny
    , decimal
    , keywords
    , punct
    , ueChar
    , ueTok
    )
where

import           Data.Bits                      ( shiftR
                                                , (.&.)
                                                )
import           Data.Char                      ( chr
                                                , intToDigit
                                                , isAsciiLower
                                                , isAsciiUpper
                                                , isDigit
                                                , isPrint
                                                , ord
                                                )
import qualified Data.CharSet                  as CS
import qualified Data.IntMap                   as I
import qualified Data.List.NonEmpty            as N
import qualified Data.Set                      as S
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Text.Megaparsec                ( ErrorItem(..)
                                                , MonadParsec
                                                , Token
                                                , Tokens
                                                , takeWhileP
                                                , unexpected
                                                )
import qualified Text.Megaparsec.Char          as C

-- | Escape a single character. Note: Tenet doesn't have separate character literals.
showCharLiteral :: Char -> Char -> Text
showCharLiteral qc char = showCharLiteral'
  where
    ordChar = ord char
    showCharLiteral' | I.member ordChar escapeNames    = escapeNames I.! ordChar
                     | char == qc                      = T.pack ['\\', char]
                     | isPrint char && ordChar < 0x100 = T.singleton char
                     | ordChar < 0x100                 = "\\x" `T.append` hexify 2 ordChar
                     | ordChar < 0x10000               = "\\u" `T.append` hexify 4 ordChar
                     | otherwise                       = "\\U" `T.append` hexify 8 ordChar

-- | Represent a double-quoted string literal escaping characters.
showStringLiteral :: Char -> Text -> Text
showStringLiteral qc text = qc `T.cons` T.concatMap (showCharLiteral qc) text `T.snoc` qc

-- | Parse a double-quoted string literal.
parseStringLiteral :: (MonadParsec e s m, Tokens s ~ Text, Token s ~ Char) => Char -> m Text
parseStringLiteral qc = C.char qc *> parseInsideString qc (Chunk "") <* C.char qc

-- | True iff the proposed identifier requires quotes.
isNotBareword :: Text -> Bool
isNotBareword text = T.null text || S.member text keywords || CS.notMember (T.head text) bwHead || T.any
    (`CS.notMember` bwTail)
    (T.tail text)

-- | Represent an identifier as a bareword if possible, falling back to backtick notation.
showIdentifier :: Text -> Text
showIdentifier text | isNotBareword text = showStringLiteral '`' text
                    | otherwise          = text

-- takeWhileP will stop on our quote character
-- Thus we ca see chunks ending in \ for all quoted quotes.
data Chunk
  = Chunk Text
  | Backslash Text
  | Escape Text
           Int
           Int
           Int
  | Failure (ErrorItem Char)
  deriving (Eq, Show)

parseInsideString :: (MonadParsec e s m, Tokens s ~ Text, Token s ~ Char) => Char -> Chunk -> m (Tokens s)
parseInsideString qc chunk = do
    rawText <- takeWhileP Nothing (notDelim qc)
    case parseChunk chunk rawText of
        Chunk     txt      -> return txt
        Failure   err      -> unexpected err
        Backslash txt      -> C.char qc >> parseInsideString qc (Chunk $ txt `T.snoc` qc)
        Escape txt _ num _ -> return $ txt `T.snoc` chr num

notDelim :: Char -> Char -> Bool
notDelim _  '\n' = False
notDelim qc char = qc /= char

parseChunk :: Chunk -> Text -> Chunk
parseChunk chunk text = T.foldl' nextChar chunk text
  where
    nextChar err@(Failure   _       ) _    = err
    nextChar (    Chunk     txt     ) '\\' = Backslash txt
    nextChar (    Chunk     txt     ) char = Chunk $ txt `T.snoc` char
    nextChar (    Backslash txt     ) char = escapeParser txt char
    nextChar (    Escape txt _ num _) '\\' = Backslash $ txt `T.snoc` chr num
    nextChar (    Escape txt _ num 0) char = Chunk $ txt `T.snoc` chr num `T.snoc` char
    nextChar (Escape txt base num size) char =
        let digit = charToDigit char
        in  if 0 <= digit && digit < base
                then Escape txt base (num * base + digit) (size - 1)
                else Chunk $ txt `T.snoc` chr num

charToDigit :: Char -> Int
charToDigit c | isDigit c      = ord c - ord '0'
              | isAsciiLower c = ord c - ord 'a' + 10
              | isAsciiUpper c = ord c - ord 'A' + 10
              | otherwise      = -1

simple :: Char -> Text -> Char -> Chunk
simple char txt _ = Chunk $ txt `T.snoc` char

same :: Text -> Char -> Chunk
same txt char = Chunk $ txt `T.snoc` char

unrecognized :: Text -> Char -> Chunk
unrecognized _ char = Failure . ueTok $ "\\" `T.snoc` char

octal :: Text -> Char -> Chunk
octal txt char = Escape txt 8 (ord char - ord '0') 2

hex :: Int -> Text -> Char -> Chunk
hex size txt _ = Escape txt 16 0 size

escapeParsers :: I.IntMap (Text -> Char -> Chunk)
escapeParsers = I.fromList
    [ (ord 'a' , simple '\a')
    , (ord 'b' , simple '\b')
    , (ord 'f' , simple '\f')
    , (ord 'n' , simple '\n')
    , (ord 'r' , simple '\r')
    , (ord 't' , simple '\t')
    , (ord 'v' , simple '\v')
    , (ord '0' , octal)
    , (ord '1' , octal)
    , (ord '2' , octal)
    , (ord '3' , octal)
    , (ord '4' , octal)
    , (ord '5' , octal)
    , (ord '6' , octal)
    , (ord '7' , octal)
    , (ord 'x' , hex 2)
    , (ord 'u' , hex 4)
    , (ord 'U' , hex 8)
    , (ord '\\', same)
    , (ord '\'', same)
    , (ord '`' , same)
    , (ord '"' , same)
    ]

escapeParser :: Text -> Char -> Chunk
escapeParser txt char = I.findWithDefault unrecognized (ord char) escapeParsers txt char

escapeNames :: I.IntMap Text
escapeNames = I.fromList
    [ (ord '\a', "\\a")
    , (ord '\b', "\\b")
    , (ord '\f', "\\f")
    , (ord '\n', "\\n")
    , (ord '\r', "\\r")
    , (ord '\t', "\\t")
    , (ord '\v', "\\v")
    , (ord '\\', "\\\\")
    ]

hexdigits :: Text
hexdigits = "0123456789abcdef"

hexify :: Int -> Int -> Text
hexify digits num = T.reverse $ T.unfoldrN digits unfoldOne num
    where unfoldOne accum = Just (T.index hexdigits (accum .&. 15), accum `shiftR` 4)

-- | Converts a number to its decimal text representation.
decimal :: Integral num => num -> Text
decimal number | number == 0 = "0"
               | number < 0  = T.cons '-' . T.reverse . T.unfoldr dec' . negate $ number
               | otherwise   = T.reverse . T.unfoldr dec' $ number
  where
    dec' :: Integral num => num -> Maybe (Char, num)
    dec' value
        | value == 0
        = Nothing
        | otherwise
        = let (msd, lsd) = divMod value 10
              digit      = intToDigit . fromIntegral $ lsd
          in  Just (digit, msd)

-- | Construct a parsing error for a character.
ueChar :: Char -> ErrorItem Char
ueChar char = Tokens (char N.:| [])

-- | Construct a parsing error for a token.
ueTok :: Text -> ErrorItem Char
ueTok text = case N.nonEmpty $ T.unpack text of
    Nothing     -> Label ('<' N.:| "<empty>>")
    Just tokens -> Tokens tokens

-- | Set of all keywords recognized by Tenet.
keywords :: S.Set Text
keywords = S.fromList
    [ "and"
    , "break"
    , "case"
    , "catch"
    , "continue"
    , "default"
    , "else"
    , "end"
    , "eqv"
    , "except"
    -- , "false"
    , "for"
    , "func"
    , "gen"
    , "guard"
    , "if"
    , "iff"
    , "imp"
    , "import"
    , "in"
    , "let"
    , "meta"
    , "not"
    , "or"
    , "pass"
    , "return"
    , "switch"
    , "tenet"
    , "then"
    -- , "true"
    , "try"
    , "type"
    , "while"
    , "xor"
    ]

-- wordOperators = ['and', 'eqv', 'iff', 'imp', 'not', 'xor', 'or']

-- statements = ['break', 'case', 'default', 'else', 'end', 'func', 'catch', 'guard', 'if', 'import', 'in', 'let', 'pass', 'return', 'switch', 'then', 'try', 'type', 'while']

-- builtinTypes = ['Func', 'Int', 'Str', 'List', 'Map', 'Set', 'Union', 'Record']

-- builtinFunctions = ['abs', 'count', 'divmod', 'index', 'len', 'lower', 'min', 'max', 'sum', 'substr', 'sublist', 'range', 'upper', 'zip', 'unzip']

-- | All "letter" characters as recognized by Tenet.
allLetters :: String
allLetters = lcLetters ++ ucLetters

-- | All "letter" characters as recognized by Tenet.
lcLetters :: String
lcLetters = ['a' .. 'z']

lcLetters' :: String
lcLetters' = '_' : lcLetters

-- | All "letter" characters as recognized by Tenet.
ucLetters :: String
ucLetters = ['A' .. 'Z']

-- | Set of characters allowed in the head (first char) of a bareword.
bwHead :: CS.CharSet
bwHead = CS.fromList $ '_' : allLetters

-- | Set of characters allowed in the head (first char) of a value name.
bwHeadLc :: CS.CharSet
bwHeadLc = CS.fromList lcLetters

-- | Set of characters allowed in the head (first char) of a value name; adding underscores for generated names.
bwHeadLc' :: CS.CharSet
bwHeadLc' = CS.fromList lcLetters'

-- | Set of characters allowed in the head (first char) of a type name.
bwHeadUc :: CS.CharSet
bwHeadUc = CS.fromList ucLetters

-- | Set of characters allowed in the tail (after first char) of a bareword.
bwTail :: CS.CharSet
bwTail = CS.fromList $ allLetters ++ ['0' .. '9'] ++ "_"

-- | Set of characters that may be found in a bareword. Union of bwHead and bwTail.
bwAny :: CS.CharSet
bwAny = bwTail

-- | Set of punctuation characters.
punct :: CS.CharSet
punct = CS.fromList ['"', '(', ')', ',', '.', ':', ';', '[', ']', '`', '{', '}']
