
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}

-- | Common functions used in parsing the internal symbolic-expression language.
--
-- These include:
--
-- * wrapping some of the Megaparsec structures
--   * the standard Parser type
--   * the ParseError type
--   * the `<?>` operator so we can quickly add debugging.
--   * the `try` parser combinator so we can test disabling it.
-- * storing location information in Node values.
module Tenet.Parsing.Common
    ( Parser
    , Parser'
    , ParseError
    , ParseErrorBundle
    , locate
    , point
    , parse
    , parseErrorPretty
    , try
    , (<?>)
    , (<??>)
    )
where

import           Tenet.Ast.Node                 ( atNode
                                                , spanNode
                                                )
import           Tenet.Ast.Nodeable             ( Nodeable
                                                , setNode
                                                )
import           Tenet.State                    ( liftOut )

import           Data.Text                      ( Text
                                                , pack
                                                )
import           Data.Void                      ( Void )
import qualified Text.Megaparsec               as M
import           Text.Megaparsec.Debug          ( dbg )

-- | The parser is a standard instance based on Text.
type Parser ast = M.Parsec Void Text ast

-- | A curried parser type.
type Parser' = M.Parsec Void Text

-- | Our standard parse error type.
type ParseErrorBundle = M.ParseErrorBundle Text Void

-- | A wrapper around the MegaParsec ParseError type.
type ParseError = M.ParseError Text Void

-- | Same as parse, but supporting the default state.
parse :: Parser a -> FilePath -> Text -> Either ParseErrorBundle a
parse parser filename source = M.runParser parser filename source

-- | Render an error bundle.
parseErrorPretty :: Text -> ParseErrorBundle -> Text
parseErrorPretty _ = pack . M.errorBundlePretty

-- | This simply replaces the normal label with dbg.
(<??>) :: Show a => Parser a -> String -> Parser a
parser <??> label = dbg label parser

-- | The <?> may be replaced with <??> to enable debugging.
(<?>) :: Parser a -> String -> Parser a
(<?>) = (M.<?>)

-- | Reimport of the Megaparsec try to make it easy to use try' to test turning off backtracking.
try :: Parser a -> Parser a
try = M.try

-- | Determine the start and end location, and then set the node for the parsed element.
locate :: Nodeable nodeable => Parser nodeable -> Parser nodeable
locate parser = do
    start  <- M.getSourcePos
    result <- parser
    end    <- M.getSourcePos
    span'  <- liftOut $ spanNode start end
    return $ setNode span' result

-- | For empty values, we try to indicate the point at which they're determined.
point :: Nodeable nodeable => nodeable -> Parser nodeable
point value = (`setNode` value) . atNode <$> M.getSourcePos
