{-# LANGUAGE NamedFieldPuns #-}

-- | Convert 'ReturnStmt's within loops to 'BreakStmt's and set flags so they can break out of the functions.
module Tenet.Deimperative.ReturnEscaping
    ( escapeReturns
    )
where

import           Data.Generics.Uniplate.Operations
                                                ( transformBiM )
import           Data.Text                      ( Text )
import           Tenet.Ast
import qualified Tenet.Deimperative.Terms      as Term
import           Tenet.State                    ( NameState )

-- | The return escaping monad that is applied to the 'Doc' as a whole.
escapeReturns :: DocN -> NameState DocN
escapeReturns doc = transformBiM transformBlock doc

data Names = Names
  { flagName  :: Text
  , localVar  :: Text
  , passTag   :: Text
  , returnTag :: Text
  } deriving (Show)

transformBlock :: [StatementN] -> NameState [StatementN]
transformBlock block = concat <$> mapM transformLoop block

transformLoop :: StatementN -> NameState [StatementN]
transformLoop stmt@ForStmt { fsNode, fsBody } = if isReturn fsBody
    then transform' <$> (Names <$> Term.flag <*> Term.temp <*> Term.pass <*> Term.return)
    else pure [stmt]
  where
    transform' base =
        let letFlag         = letSimple fsNode (flagName base) (enumVal' fsNode (passTag base))
            transformedStmt = stmt { fsBody = concatMap (rewriteReturns base) fsBody }
            postSwitch      = postStmt fsNode base
        in  [letFlag, transformedStmt, postSwitch]
transformLoop stmt = return [stmt]

postStmt :: Node -> Names -> StatementN
postStmt node base =
    let unitValue       = RecordPat { tpNode = node, tpSlots = [] } :: PatternN
        passPattern     = TagPat { upNode = node, upTag = passTag base, upVar = unitValue }
        passCase        = CaseStmt { csNode = node, csBody = [PassStmt { psNode = node }], csPat = passPattern }
        returnStatement = ReturnStmt { rsNode = node, rsValue = Name { neNode = node, neName = localVar base } }
        upVar           = NamePat { npNode = node, npName = localVar base }
        returnPattern   = TagPat { upNode = node, upTag = returnTag base, upVar }
        returnCase      = CaseStmt { csNode = node, csBody = [returnStatement], csPat = returnPattern }
        ssSubject       = Name { neNode = node, neName = flagName base }
    in  SwitchStmt { ssNode = node, ssSubject, ssCases = [passCase, returnCase] }

rewriteReturns :: Names -> StatementN -> [StatementN]
rewriteReturns base stmt = case stmt of
    ReturnStmt { rsNode, rsValue } ->
        let flagValue = Tagged { tgNode = rsNode, tgTag = returnTag base, tgVariant = rsValue }
            setFlag   = letSimple rsNode (flagName base) flagValue
        in  [setFlag, BreakStmt { bsNode = rsNode }]
    IfStmt { ifThenBody, ifElseBody } ->
        let tb = concatMap (rewriteReturns base) ifThenBody
            eb = concatMap (rewriteReturns base) ifElseBody
        in  [stmt { ifThenBody = tb, ifElseBody = eb }]
    SwitchStmt { ssCases } -> [stmt { ssCases = map (rewriteReturns' base) ssCases }]
    _                      -> [stmt]

rewriteReturns' :: Names -> CaseStmtN -> CaseStmtN
rewriteReturns' base cas@CaseStmt { csBody }    = cas { csBody = concatMap (rewriteReturns base) csBody }
rewriteReturns' base cas@DefaultStmt { dsBody } = cas { dsBody = concatMap (rewriteReturns base) dsBody }

isReturn :: [StatementN] -> Bool
isReturn = any isReturnStmt

isReturnStmt :: StatementN -> Bool
isReturnStmt stmt = case stmt of
    ReturnStmt{}           -> True
    IfStmt { ifThenBody, ifElseBody } -> isReturn ifThenBody || isReturn ifElseBody
    SwitchStmt { ssCases } -> any isReturnCase ssCases
    _                      -> False

isReturnCase :: CaseStmtN -> Bool
isReturnCase CaseStmt { csBody }    = isReturn csBody
isReturnCase DefaultStmt { dsBody } = isReturn dsBody
