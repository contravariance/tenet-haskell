{-# LANGUAGE NamedFieldPuns #-}

-- | Extract guard expressions as internal if expressions.
--
-- We could handle these by extracting guard expressions as try-except. The trouble is that expressions in loops would
-- have to be extracted as functions, which is more complexity for little gain.
--
-- Thus, the solution is to analyze the expression for possible exception throwing operations and check for them. The
-- examples:
--
-- > (a // b | exc)  => !!Choice(CheckInt0(b), a // b, exc)
-- > (a ? tag | exc) => !!Choice(CheckTag(tag, a), a ? tag, exc)
-- > (a[k] | exc)    => !!Choice(CheckIndex(k, a), a[k], exc)
--
-- Note that checks return `true~~` if the value being checked is valid, thus we bind checks with `and`.
-- If no exception throwing operation is detected, we should note a potential logic error and discard the exception.

module Tenet.Deimperative.GuardExpression
    ( guardExprs
    , findThrowing
    , andClause
    , ErrorCheck
    )
where

import           Data.Generics.Uniplate.Operations
                                                ( rewriteBi
                                                , children
                                                )
import           Data.List                      ( nubBy )
import           Tenet.Ast
import           Tenet.Ast.Nodeable             ( (=@=) )
import           Tenet.State                    ( NameState )

-- | Extracts guard expressions and rewrites them using the `!!Choice` primitive.
guardExprs :: DocN -> NameState DocN
guardExprs = return . rewriteBi rewriteGuard

rewriteGuard :: ExprN -> Maybe ExprN
rewriteGuard Guarded { geNode, geHead, geGuard } = Just $ makeChoice geNode (findThrowing geHead) geHead geGuard
rewriteGuard _ = Nothing

makeChoice :: Node -> [ErrorCheck] -> ExprN -> ExprN -> ExprN
makeChoice _ [] expr _ = expr
makeChoice scNode excs tryExpr excExpr =
    Special { scNode, scHead = Choice, scParams = [], scArgs = [andClause excs, tryExpr, excExpr] }

andClause :: [ErrorCheck] -> ExprN
andClause excs = foldr1 (BinExpr nullNode BinAnd) (nubBy (=@=) (map snd excs))

type ErrorCheck = (BuiltinError, ExprN)

-- | Examine a single expression to identify a potentially throwing expression.
findThrowing :: ExprN -> [ErrorCheck]
findThrowing expr = case expr of
  -- Floor divide throws on division by 0.
    BinExpr { beNode, beOp = BinFloorDivide, beRight } ->
        (DivisionByZero, Special beNode CheckInt0 [] [beRight]) : childThrows
    -- Regular division throws on division by 0.
    BinExpr { beNode, beOp = BinDivide, beRight } ->
        (DivisionByZero, Special beNode CheckInt0 [] [beRight]) : childThrows
    -- Modulo throws on division by 0.
    BinExpr { beNode, beOp = BinModulo, beRight } ->
        (DivisionByZero, Special beNode CheckInt0 [] [beRight]) : childThrows
    -- Variant extraction throws if the tag doesn't match.
    Variant { vrNode, vrTag, vrValue } -> (WrongTag, Special vrNode CheckTag [vrTag] [vrValue]) : childThrows
    -- Any index lookup may throw if the key is not found.
    Indexing { ixNode, ixHead, ixIndex } ->
        (OutOfBounds, Special ixNode CheckIndex [] [ixIndex, ixHead]) : childThrows
    -- Any choice means the contents are already guarded, so stop
    Special { scHead = Choice } -> []
    -- The builtin getters and setters potentially throw the same exceptions.
    -- This step should run before these are generated so exceptions are thrown against the
    -- original syntax.
    Special { scNode, scHead = GetIndex, scArgs } ->
        (OutOfBounds, Special scNode CheckIndex [] [head scArgs, scArgs !! 1]) : childThrows
    Special { scNode, scHead = SetIndex, scArgs } ->
        (OutOfBounds, Special scNode CheckBounds [] [head scArgs, scArgs !! 1]) : childThrows
    Special { scNode, scHead = GetVariant, scParams, scArgs } ->
        (WrongTag, Special scNode CheckTag [head scParams] [head scArgs]) : childThrows
    Special { scNode, scHead = SetVariant, scParams, scArgs } ->
        (WrongTag, Special scNode CheckTag [head scParams] [head scArgs]) : childThrows
    -- Recurse into nested expressions
    _ -> childThrows
    where childThrows = concatMap findThrowing (children expr)
