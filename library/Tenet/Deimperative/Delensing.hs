{-# LANGUAGE NamedFieldPuns #-}

-- | Simplifies assignment statements, including:
--
-- * lensing statements
-- * destructuring statements
-- * complex assignments
module Tenet.Deimperative.Delensing
    ( delens
    , delensDestructuring
    , delensAssign
    , delensAssign'
    , delensUnpack
    , delensUnpack'
    )
where

import           Control.Monad                  ( (>=>) )
import           Data.Generics.Uniplate.Operations
                                                ( rewriteBi
                                                , rewriteBiM
                                                )
import           Data.List                      ( foldl' )
import           Data.Text                      ( Text )
import           Tenet.Ast
import qualified Tenet.Deimperative.Terms      as Term
import           Tenet.State                    ( NameState
                                                , OneAct(..)
                                                , maybeActionM
                                                )

-- | Combines the destructuring, complex assignment and lens replacement steps.
delens :: DocN -> NameState DocN
delens = delensDestructuring >=> delensDestructuringGlobal >=> delensAssign >=> delensUnpack

-- | Simplifies destructuring assignment statements.
--
-- > {a, b, c} = x
--
-- This becomes:
--
-- > temp = x
-- > a = temp.a
-- > b = temp.b
-- > c = temp.c
delensDestructuring :: DocN -> NameState DocN
delensDestructuring = rewriteBiM (maybeActionM simplifyDestructure)

delensDestructuringGlobal :: DocN -> NameState DocN
delensDestructuringGlobal = rewriteBiM (maybeActionM simplifyDestructureGlobal)

-- | Simplifies complex assignment; monadic wrapper.
--
-- > a += b
-- > a = a + b
delensAssign :: DocN -> NameState DocN
delensAssign = return . delensAssign'

-- | Simplifies complex assignment.
--
-- > a += b
-- > a = a + b
delensAssign' :: DocN -> DocN
delensAssign' = rewriteBi simplifyAssign

-- | Simplifies lensing assignments; monadic wrapper.
--
-- > a.x = b
-- > a = !!set_slot(x, a, b)
delensUnpack :: DocN -> NameState DocN
delensUnpack = return . delensUnpack'

-- | Simplifies lensing assignments.
--
-- > a.x = b
-- > a = !!set_slot(x, a, b)
delensUnpack' :: DocN -> DocN
delensUnpack' = rewriteBi unpackLens

simplifyDestructure :: [StatementN] -> NameState (OneAct StatementN)
simplifyDestructure []          = return $ Ignore []
simplifyDestructure (stmt : sx) = case stmt of
    LetStmt { lsLet = dat@LetData { lTarget = LhsDestruct{} } } -> do
        name <- Term.temp
        return $ Act $ expandLet dat (map wrapLet) name ++ sx
    _ -> return $ Ignore []
  where
    wrapLet :: LetDataN -> StatementN
    wrapLet lsLet = stmt { lsLet }

simplifyDestructureGlobal :: [TopStmtN] -> NameState (OneAct TopStmtN)
simplifyDestructureGlobal []            = return $ Ignore []
simplifyDestructureGlobal (stmt : rest) = case stmt of
    GlobalLet dat@LetData { lTarget = LhsDestruct{} } -> do
        name <- Term.temp
        return $ Act $ expandLet dat (map GlobalLet) name ++ rest
    _ -> return $ Ignore []

expandLet :: LetDataN -> ([LetDataN] -> a) -> Text -> a
expandLet dat@LetData { lTarget = LhsDestruct { ldNode, ldPhrases }, lAssignOp } wrap tempName =
    let tempLet     = dat { lTarget = LhsLens { llVarName = tempName, llPhrases = [], llNode = ldNode } }
        assignParts = map (destruct ldNode tempName lAssignOp) ldPhrases
    in  wrap (tempLet : assignParts)
expandLet dat wrap _ = wrap [dat]

simplifyAssign :: LetDataN -> Maybe LetDataN
simplifyAssign dat@LetData { lNode, lAssignOp, lTarget, lValue } = do
    binOp  <- assignToBin lAssignOp -- regular assign means we're done
    target <- lhsToRhs lTarget -- if it's not simiplified, we fail here
    let value' = BinExpr { beNode = lNode, beOp = binOp, beLeft = target, beRight = lValue }
    return dat { lAssignOp = Assign, lValue = value' }

unpackLens :: LetDataN -> Maybe LetDataN
unpackLens LetData { lTarget = LhsLens { llPhrases = [] } } = Nothing
-- llVarName.tlName ...phrases... = lsValue
unpackLens dat@LetData { lTarget = tgt@LhsLens { llVarName, llNode, llPhrases }, lAssignOp = Assign, lValue } = let'
  where
    let' =
        let value = unpack (lhsToRhs' llNode llVarName llPhrases) lValue
        in  Just dat { lTarget = tgt { llPhrases = [] }, lValue = value }
    -- I think this could just be a foldr over the phrases.
    unpack :: ExprN -> ExprN -> ExprN
    unpack Variant { vrNode, vrValue, vrTag } rhs = unpack vrValue $ Special vrNode SetVariant [vrTag] [vrValue, rhs]
    unpack Slot { atNode, atValue, atSlot }   rhs = unpack atValue $ Special atNode SetSlot [atSlot] [atValue, rhs]
    unpack Indexing { ixNode, ixHead, ixIndex } rhs =
        unpack ixHead $ Special ixNode SetIndex [] [ixIndex, ixHead, rhs]
    unpack _ rhs = rhs
unpackLens _ = Nothing

destruct :: Node -> Text -> AssignOp -> DestructPhraseN -> LetDataN
destruct node temp op s =
    let (lTarget, atNode, atSlot) = case s of
            SimpleDestruct { sdNode, sdTgt } ->
                (LhsLens { llVarName = sdTgt, llNode = sdNode, llPhrases = [] }, sdNode, sdTgt)
            RenamingDestruct { rdNode, rdSrc, rdTgt, rdPhrases } ->
                (LhsLens { llVarName = rdTgt, llNode = rdNode, llPhrases = rdPhrases }, rdNode, rdSrc)
            ComplexDestruct { cdNode, cdSrc, cdPhrases } ->
                (LhsDestruct { ldNode = cdNode, ldPhrases = cdPhrases }, cdNode, cdSrc)
    in  LetData { lTarget
                , lNode     = node
                , lAssignOp = op
                , lValue    = (Slot { atNode, atValue = Name nullNode temp, atSlot })
                }

-- setter :: Text -> Node -> Expr -> Expr -> Expr -> Expr
-- setter funcName node lhs attrib rhs =
--   Application
--     { apNode = node
--     , apHead = Name {neNode = node, neName = funcName}
--     , apArgs = [(Term.argBase, lhs), (Term.argIndex, attrib), (Term.argValue, rhs)]
--     }
assignToBin :: AssignOp -> Maybe BinaryOp
assignToBin a = case a of
    AssignPlus        -> Just BinPlus
    AssignMinus       -> Just BinMinus
    AssignTimes       -> Just BinTimes
    AssignFloorDivide -> Just BinFloorDivide
    AssignDivide      -> Just BinDivide
    AssignModulo      -> Just BinModulo
    Assign            -> Nothing

lhsToRhs :: LhsExprN -> Maybe ExprN
-- An LhsLens is a name followed by a series of phrases.
-- So these should be left-associated operations.
lhsToRhs LhsLens { llNode, llVarName, llPhrases } = Just $ lhsToRhs' llNode llVarName llPhrases
lhsToRhs LhsDestruct{}                            = Nothing

lhsToRhs' :: Node -> Text -> [LensPhraseN] -> ExprN
lhsToRhs' node varName phrases = foldl' addPhrase nameExpr phrases
  where
    nameExpr :: ExprN
    nameExpr = Name { neNode = node, neName = varName }
    addPhrase :: ExprN -> LensPhraseN -> ExprN
    addPhrase expr VariantLens { vlNode, vlTag } = Variant { vrNode = vlNode, vrTag = vlTag, vrValue = expr }
    addPhrase expr RecordLens { tlNode, tlName } = Slot { atNode = tlNode, atSlot = tlName, atValue = expr }
    addPhrase expr IndexLens { idNode, idIndex } = Indexing { ixNode = idNode, ixHead = expr, ixIndex = idIndex }
