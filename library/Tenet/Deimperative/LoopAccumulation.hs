{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TupleSections  #-}

-- | Rewrite loop bodies as functions and the loops themselves as invocations to special functions.
module Tenet.Deimperative.LoopAccumulation
    ( accumulateLoops
    )
where

import           Data.Generics.Uniplate.Operations
                                                ( transform
                                                , transformBiM
                                                )
import qualified Data.Set                      as S
import           Data.Text                      ( Text )
import           Tenet.Ast
import qualified Tenet.Deimperative.Terms      as Term
import           Tenet.State                    ( NameState )

-- | The monad to rewrite the document without loop constructs.
accumulateLoops :: DocN -> NameState DocN
accumulateLoops doc = transformBiM (fmap concat . mapM rewriteLoop) doc

rewriteLoop :: StatementN -> NameState [StatementN]
rewriteLoop ForStmt { fsNode, fsVarName, fsIterable, fsBody } = do
    funcName <- Term.body
    let (namesUsed, accumTup, stopExpr, contExpr) = rewriteTerms' fsBody
    let funcForBody = FuncStmt
            { dsFunc = FuncData
                           { dNode   = fsNode
                           , dName   = funcName
                           , dParams = [ (Term.argIter , UnspecifiedType nullNode)
                                       , (Term.argAccum, accumType namesUsed)
                                       ]
                           , dRet    = scUnionType namesUsed
                           , dBody = [unpackAccum namesUsed, letSimple fsNode fsVarName (Name nullNode Term.argIter)]
                                     ++ rewriteBreaks stopExpr contExpr fsBody
                                     ++ [returnStmt contExpr]
                           }
            }
    let forAssign = letStmt' (lhsAccum namesUsed)
                             Assign
                             (Special nullNode ForLoop [] [Name nullNode funcName, fsIterable, accumTup])
    return [funcForBody, forAssign]
rewriteLoop other = return [other]

rewriteTerms' :: [StatementN] -> ([Text], ExprN, ExprN, ExprN)
rewriteTerms' body =
    let namesUsed = names body
        accumTup  = rhsAccum namesUsed
        stopExpr  = Tagged nullNode Term.stopTag accumTup
        contExpr  = Tagged nullNode Term.contTag accumTup
    in  (namesUsed, accumTup, stopExpr, contExpr)

rewriteBreaks :: ExprN -> ExprN -> [StatementN] -> [StatementN]
rewriteBreaks stopExpr contExpr body = map (transform rewrite) body
  where
    rewrite BreakStmt { bsNode }    = ReturnStmt { rsNode = bsNode, rsValue = stopExpr }
    rewrite ContinueStmt { cnNode } = ReturnStmt { rsNode = cnNode, rsValue = contExpr }
    rewrite stmt                    = stmt

accumType :: [Text] -> TypeExprN
accumType namesUsed = RecordType { rtNode = nullNode, rtSlots = map (, UnspecifiedType nullNode) namesUsed }

scUnionType :: [Text] -> TypeExprN
scUnionType namesUsed =
    let recordType = accumType namesUsed
    in  UnionType { utNode = nullNode, utTags = [(Term.contTag, recordType), (Term.stopTag, recordType)] }

unpackAccum :: [Text] -> StatementN
unpackAccum namesUsed = letStmt' (lhsAccum namesUsed) Assign (Name nullNode Term.argAccum)

lhsAccum :: [Text] -> LhsExprN
lhsAccum namesUsed = LhsDestruct nullNode $ map (SimpleDestruct nullNode) namesUsed

rhsAccum :: [Text] -> ExprN
rhsAccum namesUsed = RecordCon nullNode $ map arg namesUsed where arg name = (name, Name nullNode name)

names :: [StatementN] -> [Text]
names = S.toList . namesIn

-- We set up our accumulator based on the names defined in the body of the loop.
namesIn :: [StatementN] -> S.Set Text
namesIn []       = S.empty
namesIn (s : sx) = case s of
    FuncStmt { dsFunc = FuncData { dName } } -> dName `S.insert` namesIn sx
    LetStmt { lsLet = LetData { lTarget } } -> namesInLhs lTarget `S.union` namesIn sx
    ForStmt{}                               -> namesIn sx
    ReturnStmt{}                            -> namesIn sx
    IfStmt { ifThenBody, ifElseBody }       -> namesOver [ifThenBody, ifElseBody, sx]
    SwitchStmt { ssCases }                  -> S.unions $ namesIn sx : map namesInCase ssCases
    TryStmt { trTryBody, trCatches }        -> S.unions $ namesIn trTryBody : namesIn sx : map namesInCatch trCatches
    PassStmt{}                              -> namesIn sx
    ContinueStmt{}                          -> namesIn sx
    BreakStmt{}                             -> namesIn sx
    InvalidStmt{}                           -> namesIn sx

namesOver :: [[StatementN]] -> S.Set Text
namesOver = S.unions . map namesIn

namesInLhs :: LhsExprN -> S.Set Text
namesInLhs LhsLens { llVarName }     = S.singleton llVarName
namesInLhs LhsDestruct { ldPhrases } = (S.unions . map namesInPhrase) ldPhrases

namesInPhrase :: DestructPhraseN -> S.Set Text
namesInPhrase SimpleDestruct { sdTgt }      = S.singleton sdTgt
namesInPhrase RenamingDestruct { rdTgt }    = S.singleton rdTgt
namesInPhrase ComplexDestruct { cdPhrases } = (S.unions . map namesInPhrase) cdPhrases

namesInCase :: CaseStmtN -> S.Set Text
namesInCase CaseStmt { csBody }    = namesIn csBody
namesInCase DefaultStmt { dsBody } = namesIn dsBody

namesInCatch :: CatchStmtN -> S.Set Text
namesInCatch CatchStmt { ecBody } = namesIn ecBody
