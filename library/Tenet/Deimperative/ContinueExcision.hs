-- | Remove inaccessible statements after continue, break and return.
module Tenet.Deimperative.ContinueExcision
    ( exciseContinues
    , exciseContinues'
    )
where

import           Data.Generics.Uniplate.Operations
                                                ( rewriteBi )
import           Tenet.Ast
import           Tenet.State                    ( NameState
                                                , OneAct(..)
                                                , maybeAction
                                                )

-- | Monadic wrapper to remove inaccessible statements after continue, break and return.
exciseContinues :: DocN -> NameState DocN
exciseContinues docAst = return $ exciseContinues' docAst

-- | Function to remove inaccessible statements after continue, break and return.
exciseContinues' :: DocN -> DocN
exciseContinues' docAst =
    (rewriteBi addPassToCase . rewriteBi addPassToFuncData . rewriteBi addPassToStmt . rewriteBi
            (maybeAction . transform)
        )
        docAst

transform :: [StatementN] -> OneAct StatementN
transform []     = Ignore []
transform [stmt] = case stmt of
    PassStmt{} -> Act []
    _          -> Ignore [stmt]
transform (stmt : sx) = case stmt of
    ContinueStmt{} -> Act [stmt]
    BreakStmt{}    -> Act [stmt]
    ReturnStmt{}   -> Act [stmt]
    PassStmt{}     -> Act [] <> transform sx
    _              -> Ignore [stmt] <> transform sx

dummyPass :: [StatementN]
dummyPass = [PassStmt { psNode = nullNode }]

addPassToStmt :: StatementN -> Maybe StatementN
addPassToStmt stmt@IfStmt { ifThenBody = [] } = Just $ stmt { ifThenBody = dummyPass }
addPassToStmt stmt@ForStmt { fsBody = [] }    = Just $ stmt { fsBody = dummyPass }
addPassToStmt _                               = Nothing

addPassToFuncData :: FuncDataN -> Maybe FuncDataN
addPassToFuncData dd@FuncData { dBody = [] } = Just $ dd { dBody = dummyPass }
addPassToFuncData _                          = Nothing

addPassToCase :: CaseStmtN -> Maybe CaseStmtN
addPassToCase cas@CaseStmt { csBody = [] }    = Just $ cas { csBody = dummyPass }
addPassToCase cas@DefaultStmt { dsBody = [] } = Just $ cas { dsBody = dummyPass }
addPassToCase _                               = Nothing
