{-# LANGUAGE NamedFieldPuns #-}

-- | Rewrite lexical variable names so that each name is assigned to once, known as single static assignment form.
module Tenet.Deimperative.SsaForm
    ( ssaForm
    )
where

import           Data.Generics.Uniplate.Operations
                                                ( transformM )
import           Data.Text                      ( Text )
import           Tenet.Ast

import           Tenet.State                    ( NameState
                                                , Names
                                                , Use(..)
                                                , lookupLexical
                                                , newLexical
                                                , recallLexical
                                                , restoreLexical
                                                , saveLexical
                                                , setLexical
                                                )

-- | Rewrites the lexically assigned names in the document in SSA form.
ssaForm :: DocN -> NameState DocN
ssaForm = doDoc

doDoc :: DocN -> NameState DocN
doDoc (Doc stmts) = Doc <$> doTop stmts

doTop :: [TopStmtN] -> NameState [TopStmtN]
doTop = mapM doTop'

doTop' :: TopStmtN -> NameState TopStmtN
doTop' stmt@ImportTs { itImports } =
    let stmt' imps = stmt { itImports = imps } in stmt' <$> mapM importTarget itImports
doTop' (GlobalLet letDat ) = GlobalLet <$> doLetData letDat
doTop' (FuncTs    funcDat) = FuncTs <$> doFuncData funcDat
doTop' td@TypeDef{}        = return td

importTarget :: ImportN -> NameState ImportN
importTarget imp@Import { imTgtName, imNode } =
    let imp' :: Text -> ImportN
        imp' name = imp { imTgtName = name }
    in  imp' <$> setLexical Once imTgtName imNode
importTarget imp@ImportSimple { imsName, imsNode } =
    let imp' :: Text -> ImportN
        imp' name = imp { imsName = name }
    in  imp' <$> setLexical Once imsName imsNode

doLetData :: LetDataN -> NameState LetDataN
doLetData dat@LetData { lTarget = lens@LhsLens { llNode, llVarName, llPhrases }, lValue } =
    let dat' :: ExprN -> Text -> [LensPhraseN] -> LetDataN
        dat' value name phrases = dat { lTarget = lens { llVarName = name, llPhrases = phrases }, lValue = value }
    in  dat' <$> doExpr lValue <*> setLexical Reuse llVarName llNode <*> doPhrases llPhrases
doLetData dat@LetData { lTarget = des@LhsDestruct { ldPhrases }, lValue } =
    let dat' :: ExprN -> [DestructPhraseN] -> LetDataN
        dat' value phrases = dat { lTarget = des { ldPhrases = phrases }, lValue = value }
    in  dat' <$> doExpr lValue <*> mapM doDestructPhrase ldPhrases

doPhrases :: [LensPhraseN] -> NameState [LensPhraseN]
doPhrases phrases = mapM doPhrase phrases
  where
    doPhrase IndexLens { idNode, idIndex } = IndexLens idNode <$> doExpr idIndex
    doPhrase phrase                        = pure phrase

doDestructPhrase :: DestructPhraseN -> NameState DestructPhraseN
doDestructPhrase SimpleDestruct { sdNode, sdTgt } =
    let phr' name = RenamingDestruct { rdNode = sdNode, rdSrc = sdTgt, rdTgt = name, rdPhrases = [] }
    in  phr' <$> setLexical Reuse sdTgt sdNode
doDestructPhrase phr@RenamingDestruct { rdNode, rdTgt, rdPhrases } =
    let phr' phrases name = phr { rdTgt = name, rdPhrases = phrases }
    in  phr' <$> doPhrases rdPhrases <*> setLexical Reuse rdTgt rdNode
doDestructPhrase phr@ComplexDestruct { cdPhrases } =
    let phr' phrases = phr { cdPhrases = phrases } in phr' <$> mapM doDestructPhrase cdPhrases

doFuncData :: FuncDataN -> NameState FuncDataN
doFuncData dat@FuncData { dNode, dName, dParams, dBody } = do
    name'     <- setLexical Once dName dNode
    saveState <- newLexical (map fst dParams) dNode
    body'     <- mapM doStmt dBody
    restoreLexical saveState
    return $ dat { dName = name', dBody = body' }

doStmt :: StatementN -> NameState StatementN
doStmt stmt = case stmt of
    FuncStmt { dsFunc } -> FuncStmt <$> doFuncData dsFunc
    LetStmt { lsLet }   -> LetStmt <$> doLetData lsLet
    ForStmt { fsNode, fsVarName, fsIterable, fsBody } -> do
        ssa0      <- saveLexical
        iterable' <- doExpr fsIterable
        varName'  <- setLexical Once fsVarName fsNode
        body'     <- mapM doStmt fsBody
        recallLexical fsVarName ssa0
        _ <- saveLexical
        return $ stmt { fsVarName = varName', fsIterable = iterable', fsBody = body' }
    IfStmt { ifCondition, ifThenBody, ifElseBody } -> do
        condition' <- doExpr ifCondition
        ssa0       <- saveLexical
        then'      <- mapM doStmt ifThenBody
        _          <- saveLexical
        restoreLexical ssa0
        else' <- mapM doStmt ifElseBody
        _     <- saveLexical
        restoreLexical ssa0
        return $ stmt { ifCondition = condition', ifThenBody = then', ifElseBody = else' }
    SwitchStmt { ssSubject, ssCases } -> do
        subject' <- doExpr ssSubject
        ssa0     <- saveLexical
        cases'   <- mapM (doCaseStmt ssa0) ssCases
        -- restoreLexical ssa0
        return $ stmt { ssSubject = subject', ssCases = map fst cases' }
    -- Catch blocks should be removed, this could also throw an error.
    TryStmt { trTryBody } -> do
        body' <- mapM doStmt trTryBody
        return $ stmt { trTryBody = body' }
    ReturnStmt { rsNode, rsValue } -> ReturnStmt rsNode <$> doExpr rsValue
    PassStmt{}                     -> pure stmt
    ContinueStmt{}                 -> pure stmt
    BreakStmt{}                    -> pure stmt
    InvalidStmt{}                  -> pure stmt

doCaseStmt :: Names -> CaseStmtN -> NameState (CaseStmtN, Names)
doCaseStmt ssa0 cs@DefaultStmt { dsBody } = do
    restoreLexical ssa0
    body' <- mapM doStmt dsBody
    ssa1  <- saveLexical
    return (cs { dsBody = body' }, ssa1)
doCaseStmt ssa0 cs@CaseStmt { csPat, csBody } = do
    restoreLexical ssa0
    pat'  <- doPattern csPat
    body' <- mapM doStmt csBody
    ssa1  <- saveLexical
    return (cs { csPat = pat', csBody = body' }, ssa1)

doExpr :: ExprN -> NameState ExprN
doExpr expr = transformM doExpr' expr
  where
    doExpr' Name { neNode, neName } = Name neNode <$> lookupLexical neName neNode
    doExpr' expr'                   = pure expr'

doPattern :: PatternN -> NameState PatternN
doPattern pat = transformM doPat' pat
  where
    doPat' NamePat { npNode, npName } = NamePat npNode <$> setLexical Reuse npName npNode
    doPat' pat'                       = pure pat'
