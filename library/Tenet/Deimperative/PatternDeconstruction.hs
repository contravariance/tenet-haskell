{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}

-- | Deconstruct complex patterns in switch statements. The resulting switch statements are all simply selecting
-- between enumerated values, which is far more straightforward to implement.
module Tenet.Deimperative.PatternDeconstruction
    ( simplifyPatterns
    , isNarrowEq
    , patternsPerSlot
    , CaseMap(..)
    , findPatternClass
    , PatternClass(..)
    )
where

import           Control.Monad                  ( forM )
import           Data.Generics.Uniplate.Operations
                                                ( rewriteBiM )
import           Data.List                      ( find
                                                , intercalate
                                                , nubBy
                                                )
import qualified Data.Map                      as M
import           Data.Maybe                     ( catMaybes )
import qualified Data.Set                      as S
import           Data.Text                      ( Text
                                                , unpack
                                                )
import           Tenet.Ast
import           Tenet.Ast.Nodeable             ( getNode
                                                , (=@=)
                                                , Nodeable(..)
                                                )
import           Tenet.State                    ( NameState
                                                , OneAct(..)
                                                , makeTemp
                                                , rewriteActions
                                                )

-- | Simplify patterns by breaking out complex patterns into nested switch statements.
simplifyPatterns :: DocN -> NameState DocN
simplifyPatterns = rewriteBiM $ rewriteActions simplifySwitch

simplifySwitch :: StatementN -> NameState (OneAct StatementN)
simplifySwitch ss@SwitchStmt { ssCases }
    | all isSimple ssCases = return $ Ignore [ss]
    | otherwise = case findPatternClass ssCases of
        TagP    _           tags  -> simplifyTags tags ss
        RecordP _           slots -> simplifyTup slots ss
        pc@(    Incompat _ _)     -> error $ show pc
        pc                        -> error $ "Unexpected: " ++ show pc
simplifySwitch ss = return $ Ignore [ss]

-- | Find the common pattern class for the cases of a switch statement, or a valid indicating an error.
findPatternClass :: [CaseStmtN] -> PatternClass
findPatternClass cases = foldr1 commonPatClass $ map patClass cases

-- | Given a switch over complex patterns over tags, we want to switch just over the tags and delegate the rest of the
-- pattern to nested switch statements.
simplifyTags :: S.Set Text -> StatementN -> NameState (OneAct StatementN)
simplifyTags tags ss@SwitchStmt{} = do
    ssCases' <- forM (S.toList tags) $ \tag -> do
        variantName <- makeTemp "var_"
        let tagNode = findTag tag ss
        return $ CaseStmt
            { csNode = tagNode
            , csPat  = TagPat { upNode = tagNode
                              , upTag  = tag
                              , upVar  = NamePat { npNode = tagNode, npName = variantName }
                              }
            , csBody = nestedSwitchTag tag variantName ss
            }
    return $ Act [ss { ssCases = ssCases' }]
simplifyTags _ stat = error $ "Unexpected: " ++ show stat

-- | Given a complex switch, extract all case statements that have a given tag at he top level (or are wild) as their
-- own switch.
--
-- Thus:
--
-- > case tag1 ~ pattern:
-- >   body
-- > case tag2 ~ ignored:
-- >   ignored
--
-- ... becomes ...
--
-- > switch temp_var
-- >   case pattern:
-- >     body
nestedSwitchTag :: Text -> Text -> StatementN -> [StatementN]
nestedSwitchTag tag varName SwitchStmt { ssNode, ssCases } =
    [SwitchStmt { ssNode, ssSubject = Name { neNode = nullNode, neName = varName }, ssCases = cases' }]
  where
    cases' :: [CaseStmtN]
    cases' = concatMap checkCase ssCases
    checkCase :: CaseStmtN -> [CaseStmtN]
    checkCase cas@DefaultStmt{} = [cas]
    checkCase cas@CaseStmt { csPat = TagPat { upTag, upVar } } | upTag == tag = [cas { csPat = upVar }]
                                                               | otherwise    = []
    checkCase _ = []
nestedSwitchTag _ _ invalid = error $ "Logic error, didn't expect " ++ show invalid

-- | Given a complex switch that ranges over a record, break each slot into its own nested switch statement.
simplifyTup :: S.Set Text -> StatementN -> NameState (OneAct StatementN)
simplifyTup slots ss@SwitchStmt { ssSubject, ssCases } = if null slots
    then handleEmptyRecord ss
    else do
        slotMap <- tempMap slots
        case buildSwitch M.empty slotMap (map (caseMap slots) ssCases) of
            Nothing        -> return $ Ignore [ss]
            Just newSwitch -> return $ Act [letTemps slotMap ssSubject, newSwitch]
simplifyTup _ stmt = error $ "Logic error, didn't expect " ++ show stmt

letTemps :: TempMap -> ExprN -> StatementN
letTemps tMap subject = LetStmt
    { lsLet = LetData { lNode     = subjectNode
                      , lAssignOp = Assign
                      , lValue    = subject
                      , lTarget   = LhsDestruct { ldNode = subjectNode, ldPhrases = map makePhrase . M.toList $ tMap }
                      }
    }
  where
    subjectNode = getNode subject
    makePhrase :: (Text, Text) -> DestructPhraseN
    makePhrase (slot, temp) = RenamingDestruct { rdNode = nullNode, rdSrc = slot, rdTgt = temp, rdPhrases = [] }

-- | Handle the empty record case by extracting the body of the first case.
handleEmptyRecord :: StatementN -> NameState (OneAct StatementN)
handleEmptyRecord SwitchStmt { ssNode, ssCases } =
    if null ssCases then error $ "Illegal empty switch at " ++ show ssNode else return $ Act $ csBody (head ssCases)
handleEmptyRecord stmt = error $ "Logic error, didn't expect " ++ show stmt

buildSwitch :: PatternMap -> TempMap -> [CaseMap] -> Maybe StatementN
buildSwitch partial temps cases = do
    ((currentSlot, currentTemp), remaining) <- M.minViewWithKey temps
    -- The current patterns are all the patterns matched by the current slot in order.
    currentPats                             <- justList $ patternsPerSlot currentSlot cases
    let recurseRecord :: PatternN -> Maybe CaseStmtN
        recurseRecord thisPat = do
            let partial' = M.insert currentSlot thisPat partial
            -- The cases are those that match thisPat
            cases' <- justList $ filter (\cm -> thisPat `isNarrowEq` cmLookup currentSlot cm) cases
            -- The switch is matching currentTemp, so the case is thisPat, and the body is
            body   <- buildSwitch partial' remaining cases'
            Just $ caseStmt thisPat [body]
    -- The cases we're building for this switch are...
    ssCases <- justList . catMaybes $ if M.null remaining
                            -- If there's nothing remaining after this slot, we want to pass in the actual case bodies.
        then map (firstCasePerPattern currentSlot cases) currentPats
        else map recurseRecord currentPats
    return SwitchStmt { ssNode = nullNode, ssSubject = Name { neNode = nullNode, neName = currentTemp }, ssCases }
  -- From our temps we determine the currentSlot and the temp name associated with it.
  -- We'll also pass the remaining temps to child invocations.

-- | Represents the possible top-level patterns that can be in a case statement to evaluate whether it is valid and decide the type of logic to apply.
data PatternClass
  = AnyP Node  -- ^ A wildcard pattern.
  | TagP Node
         (S.Set Text) -- ^ Pattern matching is over a set of tags.
  | RecordP Node
           (S.Set Text) -- ^ Pattern matching is over slots in a record.
  | IntP Node -- ^ Pattern matching is over integers.
  | StrP Node -- ^ Pattern matching is over strings.
  | Incompat PatternClass
             PatternClass -- ^ Pattern matching is incompatible between the child classes.
  deriving (Eq, Ord)

instance Show PatternClass where
    show (AnyP node          ) = "default pattern from " ++ show node
    show (TagP    node tags  ) = "tags pattern {" ++ commaSep tags ++ "} from " ++ show node
    show (RecordP node slots ) = "record pattern {" ++ commaSep slots ++ "} from " ++ show node
    show (IntP node          ) = "integer pattern from " ++ show node
    show (StrP node          ) = "string pattern from " ++ show node
    show (Incompat left right) = show left ++ " incompatible with " ++ show right

-- | Map slots to patterns
type PatternMap = M.Map Text PatternN

-- | Map slots to temporary names
type TempMap = M.Map Text Text

-- | Map slots to patterns and contain information needed to generate a final case statement.
data CaseMap = CaseMap
  { cmMap  :: PatternMap
  , cmNode :: Node
  , cmBody :: [StatementN]
  }

-- | Construct a case map from a known set of slots and a case statemnt.
-- While missing slots should register as an error, in this code they're
-- tolerated and assumed to be wildcards.
caseMap :: S.Set Text -> CaseStmtN -> CaseMap
caseMap slots stmt =
    let
        wc :: PatternMap
        wc                      = M.fromSet (const AnyPat { wpNode = nullNode }) slots
        (cmMap, cmNode, cmBody) = case stmt of
            DefaultStmt { dsNode, dsBody }                 -> (wc, dsNode, dsBody)
            CaseStmt { csPat = AnyPat{}, csNode, csBody }  -> (wc, csNode, csBody)
            CaseStmt { csPat = NamePat{}, csNode, csBody } -> (wc, csNode, csBody)
            CaseStmt { csPat = RecordPat { tpSlots }, csNode, csBody } ->
                (M.fromList tpSlots `M.union` wc, csNode, csBody)
            CaseStmt { csNode, csBody } -> (M.empty, csNode, csBody)
    in
        CaseMap { cmMap, cmNode, cmBody }

-- | Construct a case statement from one slot in a case map.
cmMakeCase :: Text -> CaseMap -> CaseStmtN
cmMakeCase slot CaseMap { cmMap, cmNode, cmBody } =
    CaseStmt { csNode = cmNode, csPat = pmLookup slot cmMap, csBody = cmBody }

-- | Identify the slots in use in a case map.
-- cmSlots :: CaseMap -> S.Set Text
-- cmSlots = M.keysSet . cmMap
-- | Look up the pattern in a slot with a wildcard as a default.
cmLookup :: Text -> CaseMap -> PatternN
cmLookup slot CaseMap { cmMap } = pmLookup slot cmMap

pmLookup :: Text -> PatternMap -> PatternN
pmLookup slot pMap = M.findWithDefault AnyPat { wpNode = nullNode } slot pMap

-- | Update a case map with a name and pattern.
-- cmInsert :: Text -> Pattern -> CaseMap -> CaseMap
-- cmInsert slot pat cm@CaseMap{cmMap} = cm{cmMap=M.insert slot pat cmMap}
-- | Check if all candidate SlotPats are narrower or equal to the target SlotPats.
-- areNarrowEqCaseMaps :: CaseMap -> CaseMap -> Bool
-- areNarrowEqCaseMaps cand tgt =
--   let allNarrow = M.intersectionWith isNarrowEq (cmMap cand) (cmMap tgt)
--    in cmSlots cand == cmSlots tgt && M.foldl (&&) True allNarrow
-- | Check if all candidate SlotPats are narrower or equal to the target SlotPats.
areNarrowEqSlots :: [SlotPatN] -> [SlotPatN] -> Bool
areNarrowEqSlots left right =
    let left'     = M.fromList left
        right'    = M.fromList right
        allNarrow = M.intersectionWith isNarrowEq left' right'
    in  M.keysSet left' == M.keysSet right' && M.foldl (&&) True allNarrow

-- | When building the last switch, the body should be from the first case statement matching the pattern.
firstCasePerPattern :: Text -> [CaseMap] -> PatternN -> Maybe CaseStmtN
firstCasePerPattern slot cases pat = cmMakeCase slot <$> find matchCase cases
    where matchCase oneCase = pat `isNarrowEq` cmLookup slot oneCase

-- | Given a slot name and a list of case maps, extract the sub-pattern corresponding to the slot from each case.
--
-- Representing the case statements as Tenet syntax for brevity:
--
-- > patternsPerSlot "foo" [
-- >    {foo: 6, bar: 3},
-- >    {foo: 5, bar: 4},
-- >    {foo: 6, bar: 7},
-- > ] = [IntegerPat 6, IntegerPat 5]
patternsPerSlot :: Text -> [CaseMap] -> [PatternN]
patternsPerSlot slot = nubBy (=@=) . map (cmLookup slot)

-- | Given a set of names, generate a map of names to temp names.
tempMap :: S.Set Text -> NameState (M.Map Text Text)
tempMap names' = do
    let names = S.toAscList names'
    pairs <- forM names $ \name -> do
        tempName <- makeTemp "slot_"
        return (name, tempName)
    return $ M.fromDistinctAscList pairs

-- | For now, just return the statement node, but we could return the specific pattern containing the tag in question.
findTag :: Text -> StatementN -> Node
findTag _ stat = getNode stat

-- | Construct a pattern class from a case statement.
patClass :: CaseStmtN -> PatternClass
patClass CaseStmt { csPat } = case csPat of
    NamePat { npNode }            -> AnyP npNode
    IntegerPat { ipNode }         -> IntP ipNode
    StringPat { spNode }          -> StrP spNode
    RecordPat { tpSlots, tpNode } -> RecordP tpNode (S.fromList [ slotName | (slotName, _) <- tpSlots ])
    TagPat { upTag, upNode }      -> TagP upNode (S.singleton upTag)
    AnyPat { wpNode }             -> AnyP wpNode
patClass DefaultStmt { dsNode } = AnyP dsNode

-- | Determine the common pattern class according to compatiblity rules, preserving Node information.
--
-- 1. Any incompatibility persists.
-- 2. An "Any" pattern is compatible with another pattern, but prefer the narrower pattern.
-- 3. Record patterns are only compatible if the slots are identical.
-- 4. All other patterns are compatible within the same class.
commonPatClass :: PatternClass -> PatternClass -> PatternClass
commonPatClass pc@(Incompat _ _) _                 = pc
commonPatClass _                 pc@(Incompat _ _) = pc
commonPatClass (AnyP _)          pc                = pc
commonPatClass pc                (AnyP _)          = pc
commonPatClass pc@(IntP _)       (IntP _)          = pc
commonPatClass pc@(StrP _)       (StrP _)          = pc
commonPatClass left@(RecordP _ lSlots) right@(RecordP _ rSlots) | lSlots == rSlots = left
                                                                | otherwise        = Incompat left right
commonPatClass (TagP nd tags1) (TagP _ tags2) = TagP nd (S.union tags1 tags2)
commonPatClass left            right          = Incompat left right

-- | Identify a simple case that doesn't need to be deconstructed
isSimple :: CaseStmtN -> Bool
isSimple DefaultStmt{}                  = True
isSimple CaseStmt { csPat = NamePat{} } = True
isSimple CaseStmt { csPat = AnyPat{} }  = True
isSimple CaseStmt { csPat = StringPat{} } = True
isSimple CaseStmt { csPat = IntegerPat{} } = True
isSimple CaseStmt { csPat = TagPat { upVar = NamePat{} } } = True
isSimple CaseStmt { csPat = TagPat { upVar = AnyPat{} } } = True
isSimple _                              = False

-- | Determine if a candidate pattern is narrower or equal to a pattern in the switch.
isNarrowEq :: PatternN -> PatternN -> Bool
isNarrowEq _                           AnyPat{}                    = True
isNarrowEq _                           NamePat{}                   = True
isNarrowEq IntegerPat { ipVal = lVal } IntegerPat { ipVal = rVal } = lVal == rVal
isNarrowEq StringPat { spVal = lVal }  StringPat { spVal = rVal }  = lVal == rVal
isNarrowEq TagPat { upTag = lTag, upVar = lVar } TagPat { upTag = rTag, upVar = rVar } =
    lTag == rTag && lVar `isNarrowEq` rVar
isNarrowEq RecordPat { tpSlots = lSlots } RecordPat { tpSlots = rSlots } = areNarrowEqSlots lSlots rSlots
isNarrowEq _                              _                              = False

-- | Stringify a set of texts as a comma-separated list.
commaSep :: S.Set Text -> String
commaSep = intercalate ", " . map unpack . S.toList

-- | Converts a list to Just a list if non-empty
justList :: [x] -> Maybe [x]
justList []   = Nothing
justList list = Just list
