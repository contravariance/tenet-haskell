{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}

-- | Replace break statements with flags and continues.
-- This isn't needed at the moment because we're replacing loops with
-- functions, so breaks are simply replaced with returns.
module Tenet.Deimperative.BreakFlagging
    ( flagBreaks
    )
where

import           Data.Generics.Uniplate.Operations
                                                ( transformBiM )
import           Data.Text                      ( Text )
import           Tenet.Ast
import qualified Tenet.Deimperative.Terms      as Term
import           Tenet.State                    ( NameState )

-- | Convert all breaks in the document into continues and flags.
flagBreaks :: DocN -> NameState DocN
flagBreaks doc = transformBiM transformLoop doc

transformLoop :: [StatementN] -> NameState [StatementN]
transformLoop block = concat <$> mapM transformLoop' block

transformLoop' :: StatementN -> NameState [StatementN]
transformLoop' stmt@ForStmt { fsNode, fsBody } = if isBreak fsBody
    then do
        flagName <- Term.break
        let letFlag = letSimple fsNode flagName (enumVal' fsNode "true")
        let ifWrap = IfStmt { ifNode      = fsNode
                            , ifCondition = Name { neNode = fsNode, neName = flagName }
                            , ifThenBody  = concatMap (rewriteBreaks flagName) fsBody
                            , ifElseBody  = []
                            }
        return [letFlag, stmt { fsBody = [ifWrap] }]
    else return [stmt]
transformLoop' stmt = return [stmt]

rewriteBreaks :: Text -> StatementN -> [StatementN]
rewriteBreaks flag stmt = case stmt of
    BreakStmt { bsNode } ->
        let letFlag = letSimple bsNode flag (enumVal' bsNode "false")
            cntStmt = ContinueStmt { cnNode = bsNode }
        in  [letFlag, cntStmt]
    IfStmt { ifThenBody, ifElseBody } ->
        let tb = concatMap (rewriteBreaks flag) ifThenBody
            eb = concatMap (rewriteBreaks flag) ifElseBody
        in  [stmt { ifThenBody = tb, ifElseBody = eb }]
    SwitchStmt { ssCases } -> [stmt { ssCases = map rewriteBreaks' ssCases }]
    _                      -> [stmt]
  where
    rewriteBreaks' :: CaseStmtN -> CaseStmtN
    rewriteBreaks' cas@CaseStmt { csBody }    = cas { csBody = concatMap (rewriteBreaks flag) csBody }
    rewriteBreaks' cas@DefaultStmt { dsBody } = cas { dsBody = concatMap (rewriteBreaks flag) dsBody }

isBreak :: [StatementN] -> Bool
isBreak sx = any isBreak' sx
  where
    isBreak' :: StatementN -> Bool
    isBreak' BreakStmt{}            = True
    isBreak' IfStmt { ifThenBody, ifElseBody } = isBreak ifThenBody || isBreak ifElseBody
    isBreak' SwitchStmt { ssCases } = any isBreakCase ssCases
    isBreak' _                      = False

    isBreakCase :: CaseStmtN -> Bool
    isBreakCase CaseStmt { csBody }    = isBreak csBody
    isBreakCase DefaultStmt { dsBody } = isBreak dsBody
