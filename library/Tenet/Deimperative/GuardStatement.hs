{-# LANGUAGE NamedFieldPuns #-}

-- | Convert guard statements to if-else statements.
--
-- > try {
-- >   stmt1
-- >   stmt2
-- >   stmt3
-- > } catch A, B {
-- >   rescue1
-- > } catch C, D {
-- >   rescue2
-- > }
--
-- Should become:
--
-- > stmt1
-- > if check1 {
-- >   stmt2
-- >   if check2 {
-- >     stmt3
-- >   } else {
-- >     rescue2
-- >   }
-- > } else {
-- >   rescue1
-- > }
--
-- Thus the algorithm is simply to scan down the list of statements,
-- find expressions that may throw the caught exceptions,
-- and insert checks for those exceptions before them.

module Tenet.Deimperative.GuardStatement
    ( guardStmts
    )
where

import           Data.Generics.Uniplate.Operations
                                                ( rewriteBiM )
import           Data.Either                    ( rights )
import           Data.List                      ( foldl'
                                                , nubBy
                                                )
import qualified Data.Map                      as M
import qualified Data.Set                      as S
import           Data.Text                      ( Text )
import           Tenet.Ast
import           Tenet.Ast.Nodeable             ( (=@=) )
import           Tenet.Deimperative.GuardExpression
                                                ( findThrowing
                                                , ErrorCheck
                                                )
import           Tenet.Errors                   ( Issue(..) )
import           Tenet.State                    ( NameState
                                                , reportIssues
                                                , OneAct(..)
                                                , maybeActionM
                                                )

-- | Convert 'TryStmt' to a chain of 'IfStmt' checking for exceptions.
guardStmts :: DocN -> NameState DocN
guardStmts = rewriteBiM $ maybeActionM guardStmt

-- | Identify try-except blocks and replace them.
guardStmt :: [StatementN] -> NameState (OneAct StatementN)
guardStmt []       = return $ Ignore []
guardStmt (s : sx) = case s of
    TryStmt { trNode, trTryBody, trCatches } -> do
        reportIssues (checkErrs trCatches)
        let errMap = mapCatches trCatches
        return $ Act (if null errMap then trTryBody ++ sx else unroll trNode errMap trTryBody ++ sx)
    _ -> guardStmt sx

-- | Find errors in catch statements.
checkErrs :: [CatchStmtN] -> [Issue]
checkErrs catches =
    let expanded :: [(ErrorName, Node)]
        expanded = [ (err, ecNode) | CatchStmt { ecNode, ecExceptions } <- catches, err <- ecExceptions ]

        checkErr :: (M.Map ErrorName Node, [Issue]) -> (ErrorName, Node) -> (M.Map ErrorName Node, [Issue])
        checkErr (seen, issues) (err, ecNode) =
                let issues' :: [Issue]
                    issues' = case err of
                        Left txt -> UnknownError { ukName = txt, ukNode = ecNode } : issues
                        _        -> issues
                    issues'' = case seen M.!? err of
                        Nothing -> issues'
                        Just priorNode -> MaskedError { meName = err, meNode1 = priorNode, meNode2 = ecNode } : issues'
                in  (M.insert err ecNode seen, issues'')
    in  snd $ foldl' checkErr (M.empty, []) expanded

data Catching = Catching { cStmt :: [StatementN], cErrs :: S.Set BuiltinError }

mapCatches :: [CatchStmtN] -> [Catching]
mapCatches catches =
    [ Catching ecBody (S.fromList $ rights ecExceptions) | CatchStmt { ecBody, ecExceptions } <- catches ]

-- | Unroll the statements in a try body applying necessary checks as if-else statements.
unroll :: Node -> [Catching] -> [StatementN] -> [StatementN]
-- When the try body is exhausted, pass our prior work through.
unroll _    _      []          = []
unroll node errMap (stmt : sx) = case stmt of
    tryBody@IfStmt { ifThenBody, ifElseBody } ->
        let ifThen' = unroll node errMap (ifThenBody ++ sx)
            ifElse' = unroll node errMap (ifElseBody ++ sx)
        in  unroll' node errMap [tryBody { ifThenBody = ifThen', ifElseBody = ifElse' }]
    _ -> let after = unroll node errMap sx in unroll' node errMap (stmt : after)

-- | Unroll a statement whose branches are unrolled.
-- To handle multiple catches, given:
-- > try { expr }
-- > catch A, B { rescue1 }
-- > catch C, D { rescue2 }
-- > catch E, F { rescue3 }
-- And if expr throws A and E, we want to generate:
-- > if check_A() {
-- >   if check_E() { stmt }
-- >   else { rescue3 }
-- > else { rescue1 }
unroll' :: Node -> [Catching] -> [StatementN] -> [StatementN]
unroll' ifNode errChecks curStmt = fst $ foldl' unroll'' (curStmt, findStmtThrowing $ head curStmt) errChecks
  where
    unroll'' :: ([StatementN], [ErrorCheck]) -> Catching -> ([StatementN], [ErrorCheck])
    unroll'' (stmt, errChecks') Catching { cStmt, cErrs } =
        let op :: ([ExprN], [ErrorCheck]) -> ErrorCheck -> ([ExprN], [ErrorCheck])
            op (used, unused) errCheck@(err, check) =
                    if S.member err cErrs then (check : used, unused) else (used, errCheck : unused)
            (throws, errChecks'') = foldl' op ([], []) errChecks'
        in  if null throws
                then (stmt, errChecks'')
                else
                    let clause = foldr1 (BinExpr nullNode BinAnd) (nubBy (=@=) throws)
                    in  ( [IfStmt { ifNode, ifCondition = clause, ifThenBody = stmt, ifElseBody = cStmt }]
                        , errChecks''
                        )

-- | Finds the checks that need to be performed within a statement.
findStmtThrowing :: StatementN -> [ErrorCheck]
findStmtThrowing stmt = case stmt of
    LetStmt { lsLet = LetData { lTarget, lValue } } -> findLhsThrowing lTarget ++ findThrowing lValue
    ReturnStmt { rsValue }   -> findThrowing rsValue
    IfStmt { ifCondition }   -> findThrowing ifCondition
    SwitchStmt { ssSubject } -> findThrowing ssSubject
    _                        -> []

-- | Finds the checks that need to be performed on the left-hand side of an assignment.
findLhsThrowing :: LhsExprN -> [ErrorCheck]
findLhsThrowing LhsDestruct { ldPhrases }                = concatMap findDestructThrowing ldPhrases
findLhsThrowing LhsLens { llNode, llVarName, llPhrases } = findLensThrowing llNode llVarName llPhrases

-- | Scan destructuring phrases for exceptions. Destructuring itself does not raise errors.
findDestructThrowing :: DestructPhraseN -> [ErrorCheck]
findDestructThrowing SimpleDestruct{}                              = []
findDestructThrowing RenamingDestruct { rdNode, rdTgt, rdPhrases } = findLensThrowing rdNode rdTgt rdPhrases
findDestructThrowing ComplexDestruct { cdPhrases }                 = concatMap findDestructThrowing cdPhrases

-- | Scan a lensing phrase, accumulating the expression being checked and the error checks.
findLensThrowing :: Node -> Text -> [LensPhraseN] -> [ErrorCheck]
findLensThrowing _      _      []      = []
findLensThrowing neNode neName phrases = snd $ foldl' oneLens (Name { neNode, neName }, []) phrases

oneLens :: (ExprN, [ErrorCheck]) -> LensPhraseN -> (ExprN, [ErrorCheck])
oneLens (obj, prior) phrase = case phrase of
    RecordLens { tlNode, tlName } -> (Slot { atNode = tlNode, atSlot = tlName, atValue = obj }, prior)
    VariantLens { vlNode, vlTag } ->
        ( Variant { vrNode = vlNode, vrTag = vlTag, vrValue = obj }
        , (WrongTag, Special vlNode CheckTag [vlTag] [obj]) : prior
        )
    IndexLens { idNode, idIndex } ->
        ( Indexing { ixNode = idNode, ixHead = obj, ixIndex = idIndex }
        , (OutOfBounds, Special idNode CheckBounds [] [idIndex, obj]) : findThrowing idIndex ++ prior
        )
