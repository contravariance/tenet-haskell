{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- | Replaces patterns that bind to a variable name with patterns that match 'AnyPat' and a separate assignment performing the binding.
module Tenet.Deimperative.PatternUnbinding
    ( unbindPatterns
    )
where

import           Data.Generics.Uniplate.Operations
                                                ( rewriteBiM )
import           Data.Text                      ( Text )
import           Tenet.Ast
import qualified Tenet.Deimperative.Terms      as Term
import           Tenet.State                    ( NameState
                                                , OneAct(..)
                                                , rewriteActions
                                                )

-- | Rewrite bound 'Pattern's in the AST 'Doc'.
unbindPatterns :: DocN -> NameState DocN
unbindPatterns = rewriteBiM $ rewriteActions unbindSwitch

unbindSwitch :: StatementN -> NameState (OneAct StatementN)
unbindSwitch ss@SwitchStmt { ssSubject = subName@Name{}, ssCases }
    | any doesBind ssCases = return $ Act [ss { ssCases = map (unbindCase subName) ssCases }]
    | otherwise            = return $ Ignore [ss]
unbindSwitch ss@SwitchStmt { ssSubject, ssCases }
    | any doesBind ssCases = do
        subjectName <- Term.temp
        let subName = Name { neName = subjectName, neNode = nullNode }
        let preStmt = letSimple nullNode subjectName ssSubject
        let ss'     = ss { ssSubject = subName, ssCases = map (unbindCase subName) ssCases }
        return $ Act [preStmt, ss']
    | otherwise = return $ Ignore [ss]
unbindSwitch ss = return $ Ignore [ss]

unbindCase :: ExprN -> CaseStmtN -> CaseStmtN
unbindCase _ ds@DefaultStmt{} = ds
unbindCase subject cs@CaseStmt { csPat, csBody } =
    let (csPat', stmts) = unbindPat subject csPat in cs { csPat = csPat', csBody = stmts ++ csBody }

-- | If a pattern is binding, find the NamePat's and replace them with assignments and wildcards.
unbindPat :: ExprN -> PatternN -> (PatternN, [StatementN])
unbindPat subject NamePat { npNode, npName } = (AnyPat { wpNode = npNode }, [letSimple npNode npName subject])
unbindPat subject pat@RecordPat { tpSlots } =
    let (slots', stmts) = unzip . map (unbindSlot subject) $ tpSlots in (pat { tpSlots = slots' }, concat stmts)
unbindPat subject pat@TagPat { upTag, upVar } =
    let subject'      = Variant { vrNode = nullNode, vrTag = upTag, vrValue = subject }
        (var', stmts) = unbindPat subject' upVar
    in  (pat { upVar = var' }, stmts)
unbindPat _ pat@IntegerPat{} = (pat, [])
unbindPat _ pat@StringPat{}  = (pat, [])
unbindPat _ pat@AnyPat{}     = (pat, [])

unbindSlot :: ExprN -> (Text, PatternN) -> ((Text, PatternN), [StatementN])
unbindSlot subject (name, pat) =
    let subject'      = Slot { atNode = nullNode, atSlot = name, atValue = subject }
        (pat', stmts) = unbindPat subject' pat
    in  ((name, pat'), stmts)

doesBind :: CaseStmtN -> Bool
doesBind DefaultStmt{}      = False
doesBind CaseStmt { csPat } = doesPat csPat

doesPat :: PatternN -> Bool
doesPat NamePat{}             = True
doesPat RecordPat { tpSlots } = any doesSlot tpSlots
doesPat TagPat { upVar }      = doesPat upVar
doesPat _                     = False

doesSlot :: SlotPatN -> Bool
doesSlot (_, pat) = doesPat pat
