{-# LANGUAGE OverloadedStrings #-}

-- | Special temporary names and reserved argument names.
module Tenet.Deimperative.Terms
    ( break
    , return
    , flag
    , temp
    , body
    , pass
    , contTag
    , stopTag
    , argAccum
    , argIter
    )
where

import           Data.Text                      ( Text )
import           Prelude                 hiding ( break
                                                , return
                                                )
import           Tenet.State                    ( NameState
                                                , makeTemp
                                                )

-- | Get a temporary name for break flags.
break :: NameState Text
break = makeTemp "break_"

-- | Get a temporary name for return flags.
return :: NameState Text
return = makeTemp "return_"

-- | Get a temporary name for loop body functions.
body :: NameState Text
body = makeTemp "body_"

-- | Get a temporary name for other flags.
flag :: NameState Text
flag = makeTemp "flag_"

-- | Get a temporary name for delensing and other uses.
temp :: NameState Text
temp = makeTemp "temp_"

-- | Get a temporary name for pass flags.
pass :: NameState Text
pass = makeTemp "pass_"

-- | The accumulator argument name.
argAccum :: Text
argAccum = "accum__"

-- | The continue tag for loops.
contTag :: Text
contTag = "cont"

-- | The stop tag for loops.
stopTag :: Text
stopTag = "stop"

-- | The iterator argument name.
argIter :: Text
argIter = "iter__"
