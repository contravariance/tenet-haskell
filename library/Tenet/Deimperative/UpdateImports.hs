{-# LANGUAGE OverloadedStrings #-}

-- | Update 'Tenet.Ast.Types.ImportStmt's to inject builtins.
-- Not used at the moment because of 'SpecialFunc's.
module Tenet.Deimperative.UpdateImports
    ( updateImports
    )
where

import           Data.Text                      ( Text )
import           Tenet.Ast
import           Tenet.State                    ( NameState
                                                , getImports
                                                )

-- | Monad to update the 'Doc' to inject imports.
updateImports :: DocN -> NameState DocN
updateImports doc = injectImports doc <$> getImports

injectImports :: DocN -> [(Text, Text)] -> DocN
injectImports doc            []      = doc
injectImports (Doc topStmts) imports = Doc $ builtinImport imports : topStmts

builtinImport :: [(Text, Text)] -> TopStmtN
builtinImport imports = ImportTs nullNode ["__builtin__"] $ map (uncurry (Import nullNode)) imports
