{-# LANGUAGE NamedFieldPuns #-}

-- | This flattens the control structures by concatenating all statements after an 'IfStmt', 'SwitchStmt' or 'TryStmt' onto all branches
module Tenet.Deimperative.IfExtension
    ( extendIfs
    , extendIfs'
    )
where

import           Data.Generics.Uniplate.Operations
                                                ( rewriteBi )
import           Tenet.Ast
import           Tenet.State                    ( NameState
                                                , OneAct(..)
                                                , maybeAction
                                                )

-- | Monadic wrapper for function that extends ifs in the 'Doc'.
extendIfs :: DocN -> NameState DocN
extendIfs doc = return $ extendIfs' doc

-- | Function that extends 'IfStmt's in the 'Doc'.
extendIfs' :: DocN -> DocN
extendIfs' doc = rewriteBi (maybeAction . extend) doc

extend :: [StatementN] -> OneAct StatementN
extend []          = Ignore []
extend [stmt     ] = Ignore [stmt]
extend (stmt : sx) = case stmt of
    IfStmt { ifThenBody, ifElseBody } -> Act [stmt { ifThenBody = ifThenBody ++ sx, ifElseBody = ifElseBody ++ sx }]
    SwitchStmt { ssCases }            -> Act [stmt { ssCases = map (extendCase sx) ssCases }]
    TryStmt { trTryBody, trCatches } ->
        Act [stmt { trTryBody = trTryBody ++ sx, trCatches = map (extendCatch sx) trCatches }]
    _ -> Ignore [stmt] <> extend sx

extendCase :: [StatementN] -> CaseStmtN -> CaseStmtN
extendCase tails cas@CaseStmt { csBody }    = cas { csBody = csBody ++ tails }
extendCase tails cas@DefaultStmt { dsBody } = cas { dsBody = dsBody ++ tails }

extendCatch :: [StatementN] -> CatchStmtN -> CatchStmtN
extendCatch tails cat@CatchStmt { ecBody } = cat { ecBody = ecBody ++ tails }
