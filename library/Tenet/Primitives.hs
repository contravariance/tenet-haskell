{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings  #-}

-- | Centralized registry of the primitive functions that support operators and translated control statements. This also includes enumerating the operators themselves.
--     Note that the double-bang syntax is parsed as regular Tenet, but is not supported.
module Tenet.Primitives
    ( Primitive(..)
    , UnaryOp(..)
    , BinaryOp(..)
    , AssignOp(..)
    , SimplePatternType(..)
    , SpecialFunc(..)
    , SpecialTypeName(..)
    , BuiltinError(..)
    , readPrim
    , showPrim
    , specialFuncs
    , readSpecial
    , showSpecial
    , lenSpecial
    , primNumParams
    , primNumArgs
    , specialNumParams
    , specialNumArgs
    , showSnake
    , showSPrim
    , primValues
    , showSpecialType
    , specialTypeNames
    , sptMap
    , readBError
    , showBError
    )
where

import           Data.Generics                  ( Data
                                                , Typeable
                                                )
import qualified Data.IntMap                   as I
import qualified Data.Map                      as M
import           Data.Text                      ( Text )
import qualified Data.Text                     as T
import           Data.Tuple                     ( swap )
import qualified Text.Casing                   as C

-- | Enumeration of unary operators used in Tenet.
data UnaryOp
  = UnMinus
  | UnPlus
  | UnNot
  deriving (Eq, Ord, Enum, Bounded, Show, Read, Typeable, Data)

-- | Enumeration of binary operators used in Tenet.
data BinaryOp
  = BinTimes
  | BinFloorDivide
  | BinDivide
  | BinModulo
  | BinPlus -- overloads: int
  | BinConcat -- overloads: str, list
  | BinMinus
  | BinEquals -- polymorphic
  | BinNotEqual -- ditto
  | BinLessThan -- ditto
  | BinLessEqual -- ditto
  | BinGreaterThan -- ditto
  | BinGreaterEqual -- ditto
  | BinIn -- overloads: list, set, dict
  | BinNotIn -- ditto
  | BinUnion -- polymorphic and overloads: set, dict
  | BinIntersect -- ditto
  | BinDiff -- ditto
  | BinAnd
  | BinOr
  | BinXor
  | BinEqv
  | BinImp
  deriving (Eq, Ord, Enum, Bounded, Show, Read, Typeable, Data)

-- | Enumeration of assignment operators used in Tenet.
data AssignOp
  = Assign
  | AssignPlus
  | AssignMinus
  | AssignTimes
  | AssignFloorDivide
  | AssignDivide
  | AssignModulo
  deriving (Eq, Ord, Enum, Bounded, Show, Read, Typeable, Data)

-- | Enumeration of special functions used in Tenet.
data SpecialFunc
  = GetIndex
  | GetSlot
  | GetVariant
  | SetSlot
  | SetIndex
  | SetVariant
  | ForLoop
  | Choice
  | CheckInt0
  | CheckTag
  | CheckIndex
  | CheckBounds
  | Failure
  deriving (Eq, Show, Read, Typeable, Data, Enum, Bounded, Ord)

-- | Enumeration of builtin type names used in Tenet.
data SpecialTypeName
  = BottomName
  | UnspecName
  | BoolName
  | IntName
  | StrName
  | ListName
  | MapName
  | SetName
  | RecordName
  | UnionName
  deriving (Eq, Ord, Enum, Show)

-- | Combines operators and special functions into a single type.
data Primitive
  = UnaryPrim UnaryOp
  | BinaryPrim BinaryOp
  | SpecialPrim SpecialFunc
  deriving (Eq, Ord, Show, Read, Typeable, Data)

instance Enum Primitive where
    fromEnum p = mapPrimInt M.! p
    toEnum i = mapIntPrim I.! i

instance Bounded Primitive where
    minBound = snd . I.findMin $ mapIntPrim
    maxBound = snd . I.findMax $ mapIntPrim

-- | In s-exprs, patterns may be one of three types.
data SimplePatternType
  = SptTags
  | SptStrings
  | SptIntegers
  deriving (Eq, Show, Typeable, Data)

-- | Maps the names of simple pattern types to the SimplePatternType enums.
sptMap :: [(Text, SimplePatternType)]
sptMap = [("tags", SptTags), ("strings", SptStrings), ("integers", SptIntegers)]

-- | The only errors are builtin.
data BuiltinError
  = DivisionByZero
  | Empty
  | KeyConflict
  | KeyMissing
  | OutOfBounds
  | WrongTag
  deriving (Eq, Ord, Show, Read, Enum, Typeable, Data)

beNames :: [(BuiltinError, Text)]
beNames =
    [ (DivisionByZero, "Div_By_Zero")
    , (Empty         , "Empty")
    , (KeyConflict   , "Key_Conflict")
    , (KeyMissing    , "Key_Missing")
    , (OutOfBounds   , "Out_Of_Bounds")
    , (WrongTag      , "Wrong_Tags")
    ]

beMapShow :: M.Map BuiltinError Text
beMapShow = M.fromList beNames

beMapRead :: M.Map Text BuiltinError
beMapRead = M.fromList $ map swap beNames

showBError :: BuiltinError -> Text
showBError = (beMapShow M.!)

readBError :: Text -> Either Text BuiltinError
readBError txt = case beMapRead M.!? txt of
    Just val -> Right val
    Nothing  -> Left txt

-- | Identify the number of type parameters a special function takes.
specialNumParams :: SpecialFunc -> Int
specialNumParams func = case func of
    GetIndex    -> 0
    GetSlot     -> 1 -- Slot
    GetVariant  -> 1 -- Tag
    SetSlot     -> 1 -- Slot
    SetIndex    -> 0
    SetVariant  -> 1 -- Tag
    ForLoop     -> 0
    Choice      -> 0
    CheckInt0   -> 0
    CheckTag    -> 1 -- Tag
    CheckIndex  -> 0
    CheckBounds -> 0
    Failure     -> 0

-- | Identify the number of function arguments a special function takes.
specialNumArgs :: SpecialFunc -> Int
specialNumArgs func = case func of
    GetIndex    -> 2 -- index, base
    GetSlot     -> 1 -- base
    GetVariant  -> 1 -- base
    SetSlot     -> 2 -- base, value
    SetIndex    -> 3 -- index, base, value
    SetVariant  -> 2 -- base, value
    ForLoop     -> 3 -- body, iterable, accum
    Choice      -> 3 -- cond, if, else
    CheckInt0   -> 1 -- var
    CheckTag    -> 1 -- var
    CheckIndex  -> 2 -- index, var
    CheckBounds -> 2 -- index, var
    Failure     -> 0

-- | Identify the number of type parameters a primitive takes.
primNumParams :: Primitive -> Int
primNumParams (SpecialPrim func) = specialNumParams func
primNumParams _                  = 0

-- | Identify the number of function arguments a primitive takes.
primNumArgs :: Primitive -> Int
primNumArgs (UnaryPrim   _   ) = 1
primNumArgs (BinaryPrim  _   ) = 2
primNumArgs (SpecialPrim func) = specialNumArgs func

-- | Generate a list from an enumeration.
enumList :: Enum e => [e]
enumList = enumFrom $ toEnum 0

-- | List of all special function heads.
specialFuncs :: [SpecialFunc]
specialFuncs = enumList

-- | List of all primitive function heads.
primValues :: [Primitive]
primValues =
    let unaries  = map UnaryPrim (enumList :: [UnaryOp])
        binaries = map BinaryPrim (enumList :: [BinaryOp])
        -- assigns = map AssignPrim (enumList :: [AssignOp])
        specials = map SpecialPrim (enumList :: [SpecialFunc])
    in  (unaries ++ binaries ++ specials)

-- | List of all primitive function names.
primNames :: [Text]
primNames =
    let unaries  = map showSnake (enumList :: [UnaryOp])
        binaries = map showSnake (enumList :: [BinaryOp])
        -- assigns = map showSnake (enumList :: [AssignOp])
        specials = map showSnake specialFuncs
    in  (unaries ++ binaries ++ specials)

mapIntPrim :: I.IntMap Primitive
mapIntPrim = I.fromList $ zip [0 ..] primValues

mapPrimInt :: M.Map Primitive Int
mapPrimInt = M.fromList $ zip primValues [0 ..]

mapPrimText :: M.Map Primitive Text
mapPrimText = M.fromList $ zip primValues primNames

mapTextPrim :: M.Map Text Primitive
mapTextPrim = M.fromList $ zip primNames primValues

mapTextSpecial :: M.Map Text SpecialFunc
mapTextSpecial =
    let specials = enumList :: [SpecialFunc]
        names    = map showSnake specials
    in  M.fromList $ zip names specials

-- | The maximum length of a special function name.
lenSpecial :: Int
lenSpecial = maximum . map T.length . M.keys $ mapTextSpecial

-- | Show a primitive name as a double-bang semi-Tenet expression.
showPrim :: Primitive -> Text
showPrim p = mapPrimText M.! p

-- | Attempt to read a primitive name and return its enumerated value if found.
readPrim :: Text -> Maybe Primitive
readPrim t = mapTextPrim M.!? t

-- | Attempt to read a special function name and return its enumerated value if found.
readSpecial :: Text -> Maybe SpecialFunc
readSpecial t = mapTextSpecial M.!? t

-- | Show a special function name as a double-bang semi-Tenet expression.
showSpecial :: SpecialFunc -> Text
showSpecial = T.append "!!" . showPrim . SpecialPrim

-- readSnake :: String -> Primitive
-- readSnake = read . C.toPascal . C.fromSnake
-- | Show some Haskell showable in snake case.
showSnake :: Show p => p -> Text
showSnake = T.pack . C.toQuietSnake . C.fromHumps . show

-- | In the symbolic expression language, we only use a single bang.
showSPrim :: Primitive -> Text
showSPrim = T.cons '!' . showPrim

-- | Show a special type in the symbolic expression language.
showSpecialType :: SpecialTypeName -> Text
showSpecialType = T.cons '!' . T.replace "Name" "" . T.pack . show

-- | The list of all special type names.
specialTypeNames :: [SpecialTypeName]
specialTypeNames = enumList
