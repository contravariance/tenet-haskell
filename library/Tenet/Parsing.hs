{-# LANGUAGE OverloadedStrings #-}

-- | Top level module for parsing the Tenet language provides some top level parsers.
module Tenet.Parsing
    ( parseDoc
    , parseStatement
    , parseTopStmt
    , parseTypeExpr
    , parseExpr
    )
where

import           Data.Convertible.Utf8          ( convert )
import           Data.Text                      ( Text )
import           Tenet.Ast                      ( Doc
                                                , Statement
                                                , TopStmt
                                                , TypeExpr
                                                , Expr
                                                )
import           Tenet.Ast.Abstractify          ( toDoc
                                                , toStmt
                                                , toTopStmt
                                                , toExpr
                                                , toTypeExpr
                                                )
import           Tenet.Grammar.ErrM             ( Err(..) )
import           Tenet.Grammar.LexTenet         ( tokens
                                                , Token
                                                )
import           Tenet.Grammar.ParTenet         ( pModule
                                                , pStatement
                                                , pTopStmt
                                                , pTypeExpr
                                                , pValExpr
                                                )
import           Tenet.State                    ( Out
                                                , tfail
                                                )

parse' :: ([Token] -> Err a) -> (a -> Out b) -> FilePath -> Text -> Out b
parse' parser toAst path contents = do
    -- Create a stream of tokens from the file contents.
    let toks = tokens (convert contents) :: [Token]
    -- Parse the token and raise an error if detected
    gparse <- toOut path $ parser toks
    -- Convert to our AST
    toAst gparse

-- | Parser for a Tenet document that includes all top statements.
parseDoc :: FilePath -> Text -> Out (Doc ())
parseDoc = parse' pModule toDoc

parseStatement :: FilePath -> Text -> Out (Statement ())
parseStatement = parse' pStatement toStmt

parseTopStmt :: FilePath -> Text -> Out (TopStmt ())
parseTopStmt = parse' pTopStmt toTopStmt

parseTypeExpr :: FilePath -> Text -> Out (TypeExpr ())
parseTypeExpr = parse' pTypeExpr toTypeExpr

parseExpr :: FilePath -> Text -> Out (Expr ())
parseExpr = parse' pValExpr toExpr

toOut :: FilePath -> Err a -> Out a
toOut _    (Ok  val) = pure val
toOut path (Bad msg) = tfail path (convert msg)
