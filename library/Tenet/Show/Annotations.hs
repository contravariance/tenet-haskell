{-# LANGUAGE OverloadedStrings #-}

-- | Annotations and constructors to represent syntactic categories in prettified output.
module Tenet.Show.Annotations
    ( argName'
    , assignOp
    , brace'
    , errorText
    , errorDoc
    , grouping
    , importName'
    , keyword
    , lhsName'
    , literal
    , moduleName'
    , paramName'
    , primName
    , punct
    , punct'
    , rhsName'
    , slotName'
    , tagName'
    , typeGrouping
    , typeName'
    , typeOp
    , valueOp
    , Pdoc
    , SynType
    , ansiColors
    , htmlColors
    )
where

import           Data.Text                      ( Text )

import           Data.Text.Prettyprint.Doc      ( Doc
                                                , Pretty
                                                , annotate
                                                , pretty
                                                , reAnnotate
                                                )
import           Data.Text.Prettyprint.Doc.Render.Terminal
                                                ( AnsiStyle
                                                , Color(..)
                                                , bold
                                                , italicized
                                                , underlined
                                                , bgColorDull
                                                , color
                                                , colorDull
                                                )
import           Tenet.Show.Html                ( HtmlStyle(..) )

-- | An enumeration of all syntactic categories.
-- These are based on the pygments lexing library and we don't use all of them.
data SynType
  = Text
  | Whitespace
  | Escape
  | Error
  | Other
  | Keyword
  | KeywordConstant
  | KeywordDeclaration
  | KeywordNamespace
  | KeywordPseudo
  | KeywordReserved
  | KeywordType
  | Name
  | NameAttribute
  | NameBuiltin
  | NameBuiltinPseudo
  | NameClass
  | NameConstant
  | NameDecorator
  | NameEntity
  | NameException
  | NameFunction
  | NameFunctionMagic
  | NameProperty
  | NameLabel
  | NameNamespace
  | NameOther
  | NameTag
  | NameVariable
  | NameVariableClass
  | NameVariableGlobal
  | NameVariableInstance
  | NameVariableMagic
  | Literal
  | LiteralDate
  | String
  | StringAffix
  | StringBacktick
  | StringChar
  | StringDelimiter
  | StringDoc
  | StringDouble
  | StringEscape
  | StringHeredoc
  | StringInterpol
  | StringOther
  | StringRegex
  | StringSingle
  | StringSymbol
  | Number
  | NumberBin
  | NumberFloat
  | NumberHex
  | NumberInteger
  | NumberIntegerLong
  | NumberOct
  | Operator
  | OperatorWord
  | Punctuation
  | Comment
  | CommentHashbang
  | CommentMultiline
  | CommentPreproc
  | CommentPreprocFile
  | CommentSingle
  | CommentSpecial
  | Generic
  | GenericDeleted
  | GenericEmph
  | GenericError
  | GenericHeading
  | GenericInserted
  | GenericOutput
  | GenericPrompt
  | GenericStrong
  | GenericSubheading
  | GenericTraceback
  deriving (Eq, Show, Ord)

-- | A type alias for a pretty doc; a document annotated with syntactic categories.
type Pdoc = Doc SynType

-- | Annotate a document with a syntactic category.
ann :: Pretty p => SynType -> p -> Pdoc
ann synType = annotate synType . pretty

-- | Construct a doc representing a function argument, but without escaping.
argName' :: Text -> Pdoc
argName' = ann Name

-- | Construct a doc representing an assignment operator.
assignOp :: Text -> Pdoc
assignOp = ann Operator

-- | Construct a doc representing braces punctuation.
brace' :: Text -> Pdoc
brace' = ann Punctuation

-- | Construct a doc representing invalid text.
errorText :: Text -> Pdoc
errorText = ann Error

-- | Construct a doc representing invalid code.
errorDoc :: Pdoc -> Pdoc
errorDoc = annotate Error

-- | Annotate a grouping doc.
grouping :: Text -> Pdoc
grouping = ann Punctuation

-- | Annotate a type grouping doc.
typeGrouping :: Text -> Pdoc
typeGrouping = ann Punctuation

-- | Annotate a doc representing an imported name, but without escaping.
importName' :: Pretty p => p -> Pdoc
importName' = ann NameNamespace

-- | Construct a doc representing a keyword.
keyword :: Text -> Pdoc
keyword = ann Keyword

-- | Construct a doc representing a name in the left-hand side of an assignment, without escaping.
lhsName' :: Text -> Pdoc
lhsName' = ann NameVariable

-- | Annotate a literal document.
literal :: Pretty p => p -> Pdoc
literal = ann Literal

-- | Annotate a doc representing a module name, but without escaping.
moduleName' :: Pretty p => p -> Pdoc
moduleName' = ann NameNamespace

-- | Construct a doc representing a type parameter, but without escaping.
paramName' :: Text -> Pdoc
paramName' = ann NameOther

-- | Construct a doc representing a primitive function.
primName :: Text -> Pdoc
primName = ann NameBuiltin

-- | Construct a doc representing punctuation.
punct :: Text -> Pdoc
punct = ann Punctuation

-- | Annotate a doc representing punctuation.
punct' :: Pdoc -> Pdoc
punct' = annotate Punctuation

-- | Construct a doc representing an expression name, but without escaping.
rhsName' :: Text -> Pdoc
rhsName' = ann NameVariable

-- | Construct a doc representing a slot name, but without escaping.
slotName' :: Text -> Pdoc
slotName' = ann NameAttribute

-- | Construct a doc representing a tag name, but without escaping.
tagName' :: Text -> Pdoc
tagName' = ann NameTag

-- | Construct a doc representing a type name, but without escaping.
typeName' :: Text -> Pdoc
typeName' = ann NameClass

-- | Construct a doc representing a type operator.
typeOp :: Text -> Pdoc
typeOp = ann Operator

-- | Construct a doc representing a value operator.
valueOp :: Text -> Pdoc
valueOp = ann Operator

-- | Convert a doc annotated with syntactic categories to ANSI color annotations.
ansiColors :: Pdoc -> Doc AnsiStyle
ansiColors = reAnnotate ansify

-- | Convert a syntax category to an ANSI style.
-- This is based on the Autumn scheme in pygments as it's reasonably friendly
-- to simple terminal colors that prettyprint supports.
ansify :: SynType -> AnsiStyle
ansify syn = case syn of
    Whitespace           -> colorDull White
    Comment              -> italicized <> colorDull White
    CommentPreproc       -> colorDull Green
    CommentSpecial       -> italicized <> colorDull Blue
    -- Inferred:
    CommentHashbang      -> italicized <> colorDull White
    CommentMultiline     -> italicized <> colorDull White
    CommentPreprocFile   -> italicized <> colorDull White
    CommentSingle        -> italicized <> colorDull White

    Keyword              -> colorDull Blue
    KeywordPseudo        -> mempty
    KeywordType          -> colorDull Cyan
    -- Inferred:
    KeywordConstant      -> colorDull Blue
    KeywordDeclaration   -> colorDull Blue
    KeywordNamespace     -> colorDull Blue
    KeywordReserved      -> colorDull Blue

    Operator             -> mempty
    OperatorWord         -> colorDull Blue

    NameBuiltin          -> colorDull Cyan
    NameFunction         -> colorDull Green
    NameClass            -> underlined <> colorDull Green
    NameNamespace        -> underlined <> colorDull Cyan
    NameVariable         -> colorDull Red
    NameConstant         -> colorDull Red
    NameEntity           -> bold <> colorDull Red
    NameAttribute        -> color Blue
    NameTag              -> bold <> color Blue
    NameDecorator        -> color Black
    -- Inferred:
    Name                 -> mempty
    NameException        -> mempty
    NameLabel            -> mempty
    NameBuiltinPseudo    -> colorDull Cyan
    NameFunctionMagic    -> colorDull Green
    NameProperty         -> mempty
    NameOther            -> mempty
    NameVariableClass    -> colorDull Red
    NameVariableGlobal   -> colorDull Red
    NameVariableInstance -> colorDull Red
    NameVariableMagic    -> colorDull Red

    String               -> colorDull Yellow
    StringSymbol         -> colorDull Blue
    StringRegex          -> colorDull Cyan
    -- Inferred:
    StringAffix          -> colorDull Yellow
    StringBacktick       -> colorDull Yellow
    StringChar           -> colorDull Yellow
    StringDelimiter      -> colorDull Yellow
    StringDoc            -> colorDull Yellow
    StringDouble         -> colorDull Yellow
    StringEscape         -> colorDull Yellow
    StringHeredoc        -> colorDull Yellow
    StringInterpol       -> colorDull Yellow
    StringOther          -> colorDull Yellow
    StringSingle         -> colorDull Yellow

    Number               -> colorDull Cyan
    -- Inferred:
    NumberBin            -> colorDull Cyan
    NumberFloat          -> colorDull Cyan
    NumberHex            -> colorDull Cyan
    NumberInteger        -> colorDull Cyan
    NumberIntegerLong    -> colorDull Cyan
    NumberOct            -> colorDull Cyan

    GenericHeading       -> bold <> colorDull Blue
    GenericSubheading    -> bold <> colorDull Magenta
    GenericDeleted       -> colorDull Red
    GenericInserted      -> colorDull Green
    GenericError         -> colorDull Red
    GenericEmph          -> italicized
    GenericStrong        -> bold
    GenericPrompt        -> color Black
    GenericOutput        -> colorDull White
    GenericTraceback     -> colorDull Red
    -- Inferred:
    Generic              -> mempty

    Error                -> bgColorDull White <> color Red

    -- Not specified:
    Text                 -> mempty
    Escape               -> mempty
    Other                -> mempty
    Literal              -> mempty
    LiteralDate          -> mempty
    Punctuation          -> mempty

-- | Convert a doc annotated with syntactic categories to an HTML annotated doc.
htmlColors :: Pdoc -> Doc HtmlStyle
htmlColors = reAnnotate htmlify

-- | Convert a syntax category to an HTML style annotation.
htmlify :: SynType -> HtmlStyle
htmlify syn = case syn of
    Text                 -> NoStyle
    Whitespace           -> ClassStyle "w"
    Escape               -> ClassStyle "esc"
    Error                -> ClassStyle "err"
    Other                -> ClassStyle "x"

    Keyword              -> ClassStyle "k"
    KeywordConstant      -> ClassStyle "kc"
    KeywordDeclaration   -> ClassStyle "kd"
    KeywordNamespace     -> ClassStyle "kn"
    KeywordPseudo        -> ClassStyle "kp"
    KeywordReserved      -> ClassStyle "kr"
    KeywordType          -> ClassStyle "kt"

    Name                 -> ClassStyle "n"
    NameAttribute        -> ClassStyle "na"
    NameBuiltin          -> ClassStyle "nb"
    NameBuiltinPseudo    -> ClassStyle "bp"
    NameClass            -> ClassStyle "nc"
    NameConstant         -> ClassStyle "no"
    NameDecorator        -> ClassStyle "nd"
    NameEntity           -> ClassStyle "ni"
    NameException        -> ClassStyle "ne"
    NameFunction         -> ClassStyle "nf"
    NameFunctionMagic    -> ClassStyle "fm"
    NameProperty         -> ClassStyle "py"
    NameLabel            -> ClassStyle "nl"
    NameNamespace        -> ClassStyle "nn"
    NameOther            -> ClassStyle "nx"
    NameTag              -> ClassStyle "nt"
    NameVariable         -> ClassStyle "nv"
    NameVariableClass    -> ClassStyle "vc"
    NameVariableGlobal   -> ClassStyle "vg"
    NameVariableInstance -> ClassStyle "vi"
    NameVariableMagic    -> ClassStyle "vm"

    Literal              -> ClassStyle "l"
    LiteralDate          -> ClassStyle "ld"

    String               -> ClassStyle "s"
    StringAffix          -> ClassStyle "sa"
    StringBacktick       -> ClassStyle "sb"
    StringChar           -> ClassStyle "sc"
    StringDelimiter      -> ClassStyle "dl"
    StringDoc            -> ClassStyle "sd"
    StringDouble         -> ClassStyle "s2"
    StringEscape         -> ClassStyle "se"
    StringHeredoc        -> ClassStyle "sh"
    StringInterpol       -> ClassStyle "si"
    StringOther          -> ClassStyle "sx"
    StringRegex          -> ClassStyle "sr"
    StringSingle         -> ClassStyle "s1"
    StringSymbol         -> ClassStyle "ss"

    Number               -> ClassStyle "m"
    NumberBin            -> ClassStyle "mb"
    NumberFloat          -> ClassStyle "mf"
    NumberHex            -> ClassStyle "mh"
    NumberInteger        -> ClassStyle "mi"
    NumberIntegerLong    -> ClassStyle "il"
    NumberOct            -> ClassStyle "mo"

    Operator             -> ClassStyle "o"
    OperatorWord         -> ClassStyle "ow"

    Punctuation          -> ClassStyle "p"

    Comment              -> ClassStyle "c"
    CommentHashbang      -> ClassStyle "ch"
    CommentMultiline     -> ClassStyle "cm"
    CommentPreproc       -> ClassStyle "cp"
    CommentPreprocFile   -> ClassStyle "cpf"
    CommentSingle        -> ClassStyle "c1"
    CommentSpecial       -> ClassStyle "cs"

    Generic              -> ClassStyle "g"
    GenericDeleted       -> ClassStyle "gd"
    GenericEmph          -> ClassStyle "ge"
    GenericError         -> ClassStyle "gr"
    GenericHeading       -> ClassStyle "gh"
    GenericInserted      -> ClassStyle "gi"
    GenericOutput        -> ClassStyle "go"
    GenericPrompt        -> ClassStyle "gp"
    GenericStrong        -> ClassStyle "gs"
    GenericSubheading    -> ClassStyle "gu"
    GenericTraceback     -> ClassStyle "gt"
