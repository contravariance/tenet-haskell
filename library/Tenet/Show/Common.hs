{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- | Common functions used in representing both Tenet and the symbolic expression internal language.
module Tenet.Show.Common
    ( doubleColon
    , tilde
    , sharp
    , qmark
    , rightArrow
    , colon
    , comma
    , dot
    , pipe
    , argName
    , importName
    , lhsName
    , moduleName
    , paramName
    , rhsName
    , slotName
    , tagName
    , typeName
    , atSign
    , semicolon
    , assign
    , opt
    , showDoc
    , showDocAnsi
    , showDocHtml
    , operate
    , commas
    , commasMap
    , block
    , blockPlus
    , flatBlock
    , chainBlocks
    , chainBlocks'
    , caseStmt
    , LayoutOptions
    )
where

import           Data.List                      ( foldl' )
import           Data.Text                      ( Text )
import           Data.Text.Prettyprint.Doc      ( LayoutOptions
                                                , emptyDoc
                                                , layoutSmart
                                                , nest
                                                , punctuate
                                                , sep
                                                , vsep
                                                , (<+>)
                                                )
import qualified Data.Text.Prettyprint.Doc     as PP
import qualified Data.Text.Prettyprint.Doc.Render.Terminal
                                               as Term
import           Data.Text.Prettyprint.Doc.Render.Text
                                                ( renderStrict )
import           Tenet.Parsing.Escapes          ( showIdentifier )
import           Tenet.Show.Annotations
import           Tenet.Show.Html                ( renderHtml )

-- | Construct the double-colon punctuation, used as syntactic sugar for tuples and arguments, as a doc.
doubleColon :: Pdoc
doubleColon = punct "::"

-- | Construct a tilde psuedo-operator, used to construct tagged expressions, as a doc.
tilde :: Pdoc
tilde = typeOp "~"

-- | Construct a sharp psuedo-operator, used to construct enum expressions, as a doc.
sharp :: Pdoc
sharp = typeOp "#"

-- | Construct a question mark operator, used to dereference tagged expressions, as a doc.
qmark :: Pdoc
qmark = punct "?"

-- | Construct a right arrow, used in lambdas and destructuring expressions, as a doc.
rightArrow :: Pdoc
rightArrow = punct "->"

-- | Construct a colon as a doc.
colon :: Pdoc
colon = punct' PP.colon

-- | Construct a comma as a doc.
comma :: Pdoc
comma = punct' PP.comma

-- | Construct a dot operator as a doc. Used in record access.
dot :: Pdoc
dot = punct' PP.dot

-- | Construct a pipe operator as a doc. Used in guard expressions and in symbolic expressions.
pipe :: Pdoc
pipe = punct' PP.pipe

-- | Construct a function argument name as a doc.
argName :: Text -> Pdoc
argName = argName' . showIdentifier

-- | Construct a type parameter name as a doc.
paramName :: Text -> Pdoc
paramName = paramName' . showIdentifier

-- | Construct an imported name as a doc.
importName :: Text -> Pdoc
importName = importName' . showIdentifier

-- | Construct a module name as a doc.
moduleName :: Text -> Pdoc
moduleName = moduleName' . showIdentifier

-- | Construct a name in the left-hand side of an assignment as a doc.
lhsName :: Text -> Pdoc
lhsName = lhsName' . showIdentifier

-- | Construct a name in an expression as a doc.
rhsName :: Text -> Pdoc
rhsName = rhsName' . showIdentifier

-- | Construct a slot name as a doc.
slotName :: Text -> Pdoc
slotName = slotName' . showIdentifier

-- | Construct a tag name as a doc.
tagName :: Text -> Pdoc
tagName = tagName' . showIdentifier

-- | Construct a type name as a doc.
typeName :: Text -> Pdoc
typeName = typeName' . showIdentifier

-- | Construct the at sign, used in symbolic expressions, as a doc.
atSign :: Pdoc
atSign = typeOp "@"

-- | Construct the semicolon terminator as a doc.
semicolon :: Pdoc
semicolon = punct ";"

-- | Construct the assignment operator as a doc.
assign :: Pdoc
assign = punct ":="

-- | Render an optional item.
-- Note: emptyDoc is not truly empty, it has height 1.
opt :: ([item] -> Pdoc) -> [item] -> Pdoc
opt _    []    = emptyDoc
opt func items = func items

-- | Render a doc as plain text.
showDoc :: LayoutOptions -> Pdoc -> Text
showDoc opts = renderStrict . layoutSmart opts

-- | Render a doc as ANSI text using ansiColors.
showDocAnsi :: LayoutOptions -> Pdoc -> Text
showDocAnsi opts = Term.renderStrict . layoutSmart opts . ansiColors

-- | Render a doc as HTML using htmlColors.
showDocHtml :: LayoutOptions -> Pdoc -> Text
showDocHtml opts = renderHtml . layoutSmart opts . htmlColors

-- | Like punctuate, but puts leading space before a separating operator.
operate
    :: PP.Doc ann -- ^ Operator, e.g. 'comma'
    -> [PP.Doc ann]
    -> [PP.Doc ann]
operate p = go
  where
    go []       = []
    go [d     ] = [d]
    go (d : ds) = (d <+> p) : go ds

-- | Construct a doc that is a bracketed comma separated list.
-- If grouper is parens, it would return:
-- > (thing, thing, thing)
commas :: (Pdoc -> Pdoc) -> [Pdoc] -> Pdoc
commas grouper = grouper . sep . punctuate comma

-- | Construct a doc that is a bracked comma separated list.
-- If grouper is parens, it would return:
-- > (mapper thing, mapper thing, mapper thing)
commasMap :: (thing -> Pdoc) -> (Pdoc -> Pdoc) -> [thing] -> Pdoc
commasMap mapper grouper = commas grouper . map mapper

openBrace :: Pdoc
openBrace = brace' "{"

closeBrace :: Pdoc
closeBrace = brace' "}"

-- | Construct a block that is an opening statement a body in a braces surrounded block.
-- > topDoc {
-- >     stmt
-- >     stmt
-- > }
block' :: Pdoc -> [Pdoc] -> Pdoc
block' topDoc stmts = vsep [(nest 4 . vsep) ((topDoc <+> openBrace) : stmts), closeBrace]

-- | Construct a block that is an opening statement a body in a braces surrounded block.
-- > topDoc {
-- >     cvtStmt stmt
-- >     cvtStmt stmt
-- > }
block :: (x -> Pdoc) -> Pdoc -> [x] -> Pdoc
block cvtStmt topDoc stmts = block' topDoc (map cvtStmt stmts)

-- | Construct a block that is an opening statement a body in a braces surrounded block.
-- > topDoc {
-- >     cvtStmt stmt
-- >     cvtStmt stmt
-- >     plus
-- >     plus
-- > }
blockPlus :: (x -> Pdoc) -> Pdoc -> [x] -> [Pdoc] -> Pdoc
blockPlus cvtStmt topDoc stmts plus = block' topDoc (map cvtStmt stmts ++ plus)

-- | Construct a flat block for a switch statement.
-- See also caseStmt.
-- > topDoc {
-- > doc
-- > doc
-- > }
flatBlock :: (x -> Pdoc) -> Pdoc -> [x] -> Pdoc
flatBlock cvtStmt topDoc stmts = vsep $ [topDoc <+> openBrace] ++ map cvtStmt stmts ++ [closeBrace]

-- | Construct a hanging indent for a case statement.
-- See also flatBlock.
-- > topDoc:
-- >   cvtStmt stmt
-- >   cvtStmt stmt
caseStmt :: (x -> Pdoc) -> Pdoc -> [x] -> Pdoc
caseStmt cvtStmt topDoc stmts = (nest 4 . vsep) $ topDoc <> colon : map cvtStmt stmts

-- | Construct a chain of blocks with leading statements.
-- > chain0.top {
-- >   chain0.stmt
-- >   chain0.stmt
-- > } chain1.top {
-- >   chain1.stmt
-- >   chain1.stmt
-- > }
chainBlocks' :: [(Pdoc, [Pdoc])] -> Pdoc
chainBlocks' chain = vsep $ lines' ++ [last']
  where
    (lines', last') = foldl' op ([], mempty) chain
    op :: ([Pdoc], Pdoc) -> (Pdoc, [Pdoc]) -> ([Pdoc], Pdoc)
    op ([], _) (head', body) = ([nest 4 . vsep $ (head' <+> openBrace) : body], closeBrace)
    op (priorLines, priorLast) (head', body) =
        (priorLines ++ [nest 4 . vsep $ (priorLast <+> head' <+> openBrace) : body], closeBrace)


-- | Construct a chain of blocks with leading statements.
-- > chain0.top {
-- >   cvtStmt chain0.stmt
-- >   cvtStmt chain0.stmt
-- > } chain1.top {
-- >   cvtStmt chain1.stmt
-- >   cvtStmt chain1.stmt
-- > }
chainBlocks :: forall x . (x -> Pdoc) -> [(Pdoc, [x])] -> Pdoc
chainBlocks cvtStmt = chainBlocks' . map cvt'
  where
    cvt' :: (Pdoc, [x]) -> (Pdoc, [Pdoc])
    cvt' (head', stmts) = (head', map cvtStmt stmts)
