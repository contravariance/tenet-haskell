{-# LANGUAGE OverloadedStrings #-}

-- | For rendering pretty docs as HTML, this module provides a rendering function and a data type for HTML style annotations.
module Tenet.Show.Html
    ( renderHtml
    , HtmlStyle(..)
    )
where

import           Data.Text                      ( Text )
import           Data.Text.Lazy                 ( toStrict )
import           Data.Text.Lazy.Builder         ( Builder
                                                , fromText
                                                , toLazyText
                                                )
import           Data.Text.Prettyprint.Doc      ( SimpleDocStream )
import           Data.Text.Prettyprint.Doc.Render.Util.StackMachine
                                                ( renderSimplyDecorated )
import qualified HTMLEntities.Builder          as H

-- | HTML nodes may have either no style or a class tag.
data HtmlStyle
  = NoStyle
  | ClassStyle Text
  deriving (Eq, Ord, Show)

-- | Renders an HTML styled docstream by surrounding annotated nodes with span tags.
renderHtml :: SimpleDocStream HtmlStyle -> Text
renderHtml = toStrict . toLazyText . renderSimplyDecorated plainText startAnn endAnn

plainText :: Text -> Builder
plainText = H.text

startAnn :: HtmlStyle -> Builder
startAnn NoStyle          = mempty
startAnn (ClassStyle cls) = fromText "<span class=\"" <> fromText cls <> "\">"

endAnn :: HtmlStyle -> Builder
endAnn NoStyle        = mempty
endAnn (ClassStyle _) = fromText "</span>"
