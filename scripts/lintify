#!/bin/bash

setup-globals() {
    if command -v hlint >&/dev/null; then
        HLINT=( hlint )
    else
        HLINT=( stack exec --package hlint -- hlint )
    fi
    if command -v brittany >&/dev/null; then
        BRITTANY=( brittany )
    else
        BRITTANY=( stack exec --package brittany -- brittany )
    fi
    TOPS=(benchmark executable library test-suite)
    REFACTOR_ARGS=(--ignore='Eta reduce')
}

main() {
    case "$1" in
        lint )
            for top in ${TOPS[@]}; do
                "${HLINT[@]}" lint "${REFACTOR_ARGS[@]}"  --no-summary $top
            done
            ;;
        refactor )
            for top in ${TOPS[@]}; do
                "${HLINT[@]}" "${REFACTOR_ARGS[@]}" --json $top | jq -r '.[] | .file'
            done | sort -u | while read path; do
                echo "Refactoring $path..."
                "${HLINT[@]}" "${REFACTOR_ARGS[@]}" --refactor --refactor-options='--inplace' "$path"
            done
            ;;
        pretty )
            find ${TOPS[@]} -name \*.hs -exec "${BRITTANY[@]}" --columns 118 --indent 4 --write-mode inplace '{}' '+'
            find ${TOPS[@]} -name \*.json -exec pretty-json -r 100 '{}' '+'
            ;;
        * )
            echo "Unknown command: $1"
            echo "Usage: $0 [lint|refactor|pretty]"
            ;;
    esac
}

setup-globals
main "$@"
