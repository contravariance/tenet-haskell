{-# LANGUAGE DeriveDataTypeable #-}

module Steps
    ( Step(..)
    , stepFunc
    )
where

import           Data.Data                      ( Data
                                                , Typeable
                                                )
import           Tenet.Ast                      ( DocN )
import           Tenet.Deimperative             ( accumulateLoops
                                                , delens
                                                , escapeReturns
                                                , exciseContinues
                                                , extendIfs
                                                , flagBreaks
                                                , fullStack
                                                , ssaForm
                                                , updateImports
                                                )
import           Tenet.State                    ( NameState )

data Step
  = EscapeReturns
  | AccumulateLoops
  | FlagBreaks
  | ExtendIfs
  | ExciseContinues
  | Delens
  | SsaForm
  | UpdateImports
  | FullStack
  deriving (Data, Typeable, Show, Eq)

stepFunc :: Step -> (DocN -> NameState DocN)
stepFunc s = case s of
    EscapeReturns   -> escapeReturns
    AccumulateLoops -> accumulateLoops
    FlagBreaks      -> flagBreaks
    ExtendIfs       -> extendIfs
    ExciseContinues -> exciseContinues
    Delens          -> delens
    SsaForm         -> ssaForm
    UpdateImports   -> updateImports
    FullStack       -> fullStack
