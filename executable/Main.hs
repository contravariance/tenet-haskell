{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE NamedFieldPuns     #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# OPTIONS_GHC -fno-cse #-}

-- no-cse lets us have multiple &= args expressions.
module Main
    ( main
    )
where

import           Control.Monad                  ( foldM
                                                , void
                                                )
import qualified Data.Set                      as S
import qualified Data.Text                     as T
import qualified Data.Text.IO                  as Tio
import           Steps                          ( Step(..)
                                                , stepFunc
                                                )
import           System.Console.CmdArgs
import           System.FilePath                ( replaceExtensions )
import           Tenet.Ast                      ( DocN
                                                , nullNode
                                                )
import           Tenet.Deimperative             ( gatherNames
                                                , runTransforms
                                                )
import           Tenet.Parsing                  ( parseDoc )
import           Tenet.Show                     ( showAst
                                                , showAstAnsi
                                                , showAstHtml
                                                , stdLayout
                                                , Encoding(..)
                                                )

-- Base types may be Bool, String, Int, Filepath, maybe others.
data TrOptions
  = Parse { source      :: [FilePath]
          , steps       :: [Step]
          , writeToFile :: Bool
          , format      :: Encoding }
  | Names { source :: [FilePath] }
  | Test { }
  deriving (Data, Typeable, Show, Eq)

-- Customize your options, including help messages, shortened names, etc.
parseAct :: TrOptions
parseAct = Parse { source      = def &= args
                 , steps       = def &= help "Transformation steps to run"
                 , writeToFile = def &= help "write to .ast.tenet instead of stdout"
                 , format      = Plain &= help "set to ansi for colorized output"
                 }

namesAct :: TrOptions
namesAct = Names { source = def &= args }

testAct :: TrOptions
testAct = Test{}

getOpts :: IO TrOptions -- Mode (CmdArgs TrOptions)
getOpts =
    cmdArgsRun
        $  cmdArgsMode
        $  modes [parseAct, namesAct, testAct]
        &= verbosityArgs [explicit, name "Verbose", name "V"] []
        &= versionArg [explicit, name "version", name "v", summary programInfo]
        &= summary (programInfo ++ copyright)
        &= help programAbout
        &= helpArg [explicit, name "help", name "h"]
        &= program programName
  where
    programName :: String
    programName = "tenet-cli"
    programVersion :: String
    programVersion = "0.1"
    programInfo :: String
    programInfo = programName ++ " version " ++ programVersion
    programAbout :: String
    programAbout = "Runs various translations of the Tenet language."
    copyright :: String
    copyright = " Copyright Ben Samuel 2020"

-- A nice blog post on CmdArgs here:
-- https://zuttobenkyou.wordpress.com/2011/04/19/haskell-using-cmdargs-single-and-multi-mode/
main :: IO ()
main = do
    opts <- getOpts
    case opts of
        Parse { source, steps, writeToFile, format } -> mapM_ (parseIo steps writeToFile format) source
        Names { source } -> mapM_ namesIo source
        Test{}           -> runTest

parseFile :: FilePath -> IO DocN
parseFile source = do
    contents <- Tio.readFile source
    let parseResult = parseDoc source contents
    case parseResult of
        Left parseError -> do
            Tio.putStrLn parseError
            fail "Parse failed."
        Right parsed -> return $ fmap (const nullNode) parsed

parseIo :: [Step] -> Bool -> Encoding -> FilePath -> IO ()
parseIo steps wtf fmt source = do
    parsed <- parseFile source
    let transformed = doSteps parsed steps
    let unparsed = case fmt of
            Plain -> showAst stdLayout transformed
            Ansi  -> showAstAnsi stdLayout transformed
            Html  -> showAstHtml stdLayout transformed
    let unparsed' = unparsed `T.snoc` '\n'
    if wtf
        then Tio.writeFile (replaceExtensions source "ast.tenet") unparsed'
        else putStrLn ("# " ++ source) >> Tio.putStr unparsed'

doSteps :: DocN -> [Step] -> DocN
doSteps docu steps = runTransforms (const chained) docu
  where
    doStep doc' step = stepFunc step doc'
    chained = foldM doStep docu steps

namesIo :: FilePath -> IO ()
namesIo source = do
    parsed <- parseFile source
    let used     = gatherNames $ void parsed
    let asString = T.intercalate ", " (S.toList used)
    putStrLn $ "# " ++ source
    Tio.putStrLn asString

runTest :: IO ()
runTest = Tio.putStrLn "Stub here to quickly test stuff."
